public class Triangle {
	public static void main(String[] args) {
		SimpleRobot robot = new SimpleRobot();

		for(int i = 0; i < 3; i++) {
			robot.moveForward(100);
			robot.turnLeft(120);
		}
	}
}

