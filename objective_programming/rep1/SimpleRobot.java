public class SimpleRobot {
	double x, y;
	double heading;

	public SimpleRobot() {
		x = y = 0.0;
		heading = 0.0;
	}

	public void setHeading(double heading) {
		this.heading = heading;
	}

	public void turnLeft(double degree) {
		setHeading(heading + degree);
	}

	public void turnRight(double degree) {
		turnLeft(-degree);
	}

	public void moveTo(double x, double y) {
		this.x = x;
		this.y = y;
	}

	public void moveForward(double step) {
		double radian = heading / 180. * Math.PI;
		double _x = this.x;
		double _y = this.y;
		double moveX = _x + Math.cos(radian) * step;
		double moveY = _y + Math.sin(radian) * step;

		moveTo(moveX, moveY);

		System.out.printf("%.2f %.2f\n%.2f %.2f\n\n", _x, _y, moveX, moveY);
	}
}

