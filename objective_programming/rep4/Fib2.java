import java.util.*;

public class Fib2 {
	// map(n) = fib(n)
	static HashMap<Long, Long> map = new HashMap<Long, Long>();

	static long fib(long n) {
		if(map.get(n) == null) map.put(n, n < 2 ? n : fib(n - 1) + fib(n - 2)); // if map(n) == null then map(n) = fib(n)

		return map.get(n);
	}

	public static void main(String[] args){
		System.out.println(fib(109));
	}
}

