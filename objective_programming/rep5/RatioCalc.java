import java.util.*;

public class RatioCalc {
	abstract class Op {
		abstract public String opName();
		abstract public void exec(Deque<Ratio> stack);
	}

	class PrintOp extends Op {
		public String opName() {return "=";}
		public void exec(Deque<Ratio> stack) {
			for(Ratio p: stack) {System.out.println(p.toString());}
		}
	}

	abstract class BinOp extends Op {
		abstract public Ratio op(Ratio rand1, Ratio rand2);
		public void exec(Deque<Ratio> stack) {
			Ratio v2 = stack.pop();
			Ratio v1 = stack.pop();

			stack.push(op(v1, v2));
		}
	}

	class AddOp extends BinOp {
		public String opName() {return "+";}
		public Ratio op(Ratio rand1, Ratio rand2) {return rand1.add(rand2);}
	}

	class SubOp extends BinOp {
		public String opName() {return "-";}
		public Ratio op(Ratio rand1, Ratio rand2) {return rand1.sub(rand2);}
	}

	class MulOp extends BinOp {
		public String opName() {return "*";}
		public Ratio op(Ratio rand1, Ratio rand2) {return rand1.multiply(rand2);}
	}

	class DivOp extends BinOp {
		public String opName() {return "/";}
		public Ratio op(Ratio rand1, Ratio rand2) {return rand1.divide(rand2);}
	}

	class NoOp extends Op {
		public String opName() {return "noop";}
		public void exec(Deque<Ratio> stack){}
	}

	Op[] optable = {new AddOp(), new SubOp(), new MulOp(), new DivOp(), new NoOp(), new PrintOp()};
	String[][] alias = {{"足す", "+"}, {"引く", "-"}, {"かける", "*"}, {"割る", "/"}, {"から", "noop"}, {"と", "noop"}, {"を", "noop"}, {"で", "noop"}};
	Deque<Ratio> stack;
	Scanner scanner;
	Map<String, Op> ops;

	RatioCalc() {
		stack = new LinkedList<Ratio>();
		scanner = new Scanner (System.in);
		ops = new HashMap<String, Op>();

		for(Op op: optable) {ops.put(op.opName(), op);}
	}

	public void run() {
		while(scanner.hasNext()) {
			if(scanner.hasNextInt()) {
				stack.push(new Ratio(scanner.nextInt(), 1));
			}else{
				String token = scanner.next();

				if((int)token.charAt(0) > (int)'n') {
					for(String[] str : alias) {
						if(token.equals(str[0])) {
							token = str[1];

							break;
						}
					}
				}

				try{ops.get(token).exec(stack);
				}catch(Exception e) {System.out.println("Invalid Operation: " + e);}
			}
		}
	}

	public static void main(String[] args) {
		new RatioCalc().run();
	}
}

