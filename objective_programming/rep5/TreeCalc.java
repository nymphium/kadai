import java.util.*;

public class TreeCalc {
	interface Tree {
		abstract public Ratio eval(); // '='
		abstract public String toString();
		abstract public void printLatex(); // 'latex'
	}

	class Leaf implements Tree {
		int value;

		Leaf(int value) {this.value = value;}

		public Ratio eval(){return new Ratio(this.value, 1);}

		public String toString() {return Integer.toString(value);}

		public void printLatex() {System.out.printf("{%s}", this.toString());}
	}

	class Node implements Tree {
		BinOp operator;
		Tree left, right;

		Node(BinOp operator, Tree left, Tree right) {
			this.operator = operator;
			this.left = left;
			this.right = right;
		}

		public Ratio eval() {return operator.op(left, right);}

		public String toString() {
			return String.format("(%s%s%s)", left.toString(), operator.opName(), right.toString());
		}

		public void printLatex() {
			System.out.println("{" + operator.opName() + "}\nchild {node");
			left.printLatex();
			System.out.println("}\nchild {node");
			right.printLatex();
			System.out.println("}");
		}
	}

	abstract class Op {
		abstract public String opName();
		abstract public void exec(Deque<Tree> stack);
	}

	class PrintOp extends Op {
		public String opName() {return "=";}
		public void exec(Deque<Tree> stack) {
			for(Tree p: stack) {System.out.println(p.eval());}
		}
	}

	class QuestionOp extends Op {
		public String opName() {return "?";}
		public void exec(Deque<Tree> stack) {
			for(Tree p: stack) {System.out.println(p.toString());}
		}
	}

	class LatexOp extends Op {
		public String opName() {return "latex";}
		public void exec(Deque<Tree> stack) {
			for(Tree p: stack) {p.printLatex();}
		}
	}

	class ClearOp extends Op {
		public String opName() {return "clear";}
		public void exec(Deque<Tree> stack) {
			while(stack.size() > 0) {stack.pop();}
		}
	}

	abstract class BinOp extends Op {
		abstract public Ratio op(Tree rand1, Tree rand2);
		public void exec(Deque<Tree> stack) {
			Tree v2 = stack.pop();
			Tree v1 = stack.pop();

			stack.push(new Node(this, v1, v2));
		}
	}

	class AddOp extends BinOp {
		public String opName() {return "+";}
		public Ratio op(Tree rand1, Tree rand2) {return rand1.eval().add(rand2.eval());}
	}

	class SubOp extends BinOp {
		public String opName() {return "-";}
		public Ratio op(Tree rand1, Tree rand2) {return rand1.eval().sub(rand2.eval());}
	}

	class MulOp extends BinOp {
		public String opName() {return "*";}
		public Ratio op(Tree rand1, Tree rand2) {return rand1.eval().multiply(rand2.eval());}
	}

	class DivOp extends BinOp {
		public String opName() {return "/";}
		public Ratio op(Tree rand1, Tree rand2) {return rand1.eval().divide(rand2.eval());}
	}

	class NoOp extends Op {
		public String opName() {return "noop";}
		public void exec(Deque<Tree> stack){}
	}

	Op[] optable = {new AddOp(), new SubOp(), new MulOp(), new DivOp(), new NoOp(), new PrintOp(), new QuestionOp(), new LatexOp(), new ClearOp()};
	String[][] alias = {{"足す", "+"}, {"引く", "-"}, {"かける", "*"}, {"割る", "/"}, {"から", "noop"}, {"と", "noop"}, {"を", "noop"}, {"で", "noop"}};
	Deque<Tree> stack;
	Scanner scanner;
	Map<String, Op> ops;

	TreeCalc() {
		stack = new LinkedList<Tree>();
		scanner = new Scanner (System.in);
		ops = new HashMap<String, Op>();

		for(Op op: optable) {ops.put(op.opName(), op);}
	}

	public void run() {
		while(scanner.hasNext()) {
			if(scanner.hasNextInt()) {
				stack.push(new Leaf(scanner.nextInt()));
			}else{
				String token = scanner.next();

				if((int)token.charAt(0) > (int)'n') {
					for(String[] str : alias) {
						if(token.equals(str[0])) {
							token = str[1];

							break;
						}
					}
				}

				try{ops.get(token).exec(stack);
				}catch(Exception e) {System.out.println("Invalid Operation: " + e);}
			}
		}
	}

	public static void main(String[] args) {
		new TreeCalc().run();
	}
}

