import java.util.*;

public class JCalc {
	abstract class Op {
		abstract public String opName();
		abstract public void exec(Deque<Integer> stack);
	}

	class PrintOp extends Op {
		public String opName() {return "=";}
		public void exec(Deque<Integer> stack) {
			for(int p: stack) {System.out.println(p);}
		
			// int v = stack.pop();

			// System.out.println(v);

			// stack.push(v);
		}
	}

	abstract class BinOp extends Op {
		abstract public int op(int rand1, int rand2);
		public void exec(Deque<Integer> stack) {
			int v2 = stack.pop();
			int v1 = stack.pop();

			stack.push(op(v1, v2));
		}
	}

	class AddOp extends BinOp {
		public String opName() {return "+";}
		public int op(int rand1, int rand2) {return rand1 + rand2;}
	}

	class SubOp extends BinOp {
		public String opName() {return "-";}
		public int op(int rand1, int rand2) {return rand1 - rand2;}
	}

	class MulOp extends BinOp {
		public String opName() {return "*";}
		public int op(int rand1, int rand2) {return rand1 * rand2;}
	}

	class DivOp extends BinOp {
		public String opName() {return "/";}
		public int op(int rand1, int rand2) {return rand1 / rand2;}
	}

	class NoOp extends Op {
		public String opName() {return "noop";}
		public void exec(Deque<Integer> stack){}
	}

	Op[] optable = {new AddOp(), new SubOp(), new MulOp(), new DivOp(), new NoOp(), new PrintOp()};
	String[][] alias = {{"足す", "+"}, {"引く", "-"}, {"かける", "*"}, {"割る", "/"}, {"から", "noop"}, {"と", "noop"}, {"を", "noop"}, {"で", "noop"}};
	Deque<Integer> stack;
	Scanner scanner;
	Map<String, Op> ops;

	JCalc() {
		stack = new LinkedList<Integer>();
		scanner = new Scanner (System.in);
		ops = new HashMap<String, Op>();

		for(Op op: optable) {ops.put(op.opName(), op);}
	}

	public void run() {
		while(scanner.hasNext()) {
			if(scanner.hasNextInt()) {
				stack.push(scanner.nextInt());
			}else{
				String token = scanner.next();

				// if ((int)token.charAt(0)  > (int)'n') { --- 'n' is largest value at opNames, and this `if` statement is to make complexity lower.
				//     Iter(alias: str) {
				//         if (token == str[0]) { token = str[1] }
				//     }
				// }
				if((int)token.charAt(0) > (int)'n') {
					for(String[] str : alias) {
						if(token.equals(str[0])) {
							token = str[1];

							break;
						}
					}
				}

				try{ops.get(token).exec(stack);
				}catch(Exception e) {System.out.println("Invalid Operation: " + e);}
			}
		}
	}

	public static void main(String[] args) {
		new JCalc().run();
	}
}

