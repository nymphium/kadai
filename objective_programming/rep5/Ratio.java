public class Ratio {
	private int num;    // 分子
	private int denom;  // 分母

	public Ratio(int num, int denom) {
		if (denom < 0) {
			num *= -1;
			denom *= -1;
		}
		int g = gcd(num, denom);    // 約分するために最大公約数を求める．
		this.num = num / g;
		this.denom = denom / g;
	}
	// クラスの外には見せないメソッド
	private int gcd(int p, int q) {
		// 最大公約数を計算する．
		if (q==0) return p;
		else return gcd(q,p%q);
	}
	private Ratio multiply(int num, int denom) {
		int n = (this.num*num) / (gcd(this.num,denom) * gcd(this.denom,num));
		int d = (this.denom*denom) / (gcd(this.num,denom) * gcd(this.denom,num));
		return new Ratio(n, d);
	}
	private  Ratio add(int num, int denom) {
		int n = (this.num*denom+num*this.denom) / gcd(this.denom,denom);
		int d = (this.denom*denom) / gcd(this.denom,denom);
		return new Ratio(n, d);
	}

	// 公開するメソッド
	public void printRatio() {
		System.out.println("分数の値は" + num + "/" + denom + "です．");
	}

	// 分数どうしの加減乗除
	public Ratio add(Ratio r) {
		// thisとrを足した結果のRatioを作って返す
		return add(r.num, r.denom);
	}
	public Ratio sub(Ratio r) {
		// thisからrを引いた結果のRatioを作って返す
		return add(-r.num, r.denom);          // addを使う．
	}
	public Ratio multiply(Ratio r) {
		// thisとrをかけた結果のRatioを作って返す
		return multiply(r.num, r.denom);
	}
	public Ratio divide(Ratio r) {
		// thisをrで割った結果のRatioを作って返す
		return multiply(r.denom,r.num);          // multiplyを使う．
	}

	// 整数との加減乗除
	public Ratio add(int i) {
		return add(i, 1);
	}
	public Ratio sub(int i) {
		return add(-i,1);
	}
	public Ratio multiply(int i) {
		return multiply(i, 1);
	}
	public Ratio divide(int i) {
		return multiply(1,i);
	}

	public String toString() {
		if(denom == 1) {
			return Integer.toString(num);
		}else{
			return num + "/" + denom;
		}
	}

	public boolean equals(Object obj) {
		boolean ret = obj instanceof Ratio;
		if(ret) {
			Ratio r = (Ratio)obj;

			return denom == r.denom && num == r.num;
		}else{
			return ret;
		}
	}
}

