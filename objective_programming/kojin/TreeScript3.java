// I defeated *Probrem-1*

import java.util.*;


public class TreeScript3 {
	abstract class Tree {
		abstract public Tree eval();
		abstract public Tree replace(String name, Tree r);
		abstract public String toString();
	}


	class IntLeaf extends Tree {
		int value;
		IntLeaf(int value) {this.value = value;}

		public Tree eval() {return this;}

		public Tree replace(String name, Tree r) {return this;}

		public String toString() {return Integer.toString(value);}
	}

	class NameLeaf extends Tree {
		String name;
		NameLeaf(String name) {this.name = name;}

		public Tree eval() {return this;}

		public Tree replace(String name, Tree r) {return name.equals(this.name) ? r : this;}

		public String toString() {return name;}
	}

	class Node extends Tree {
		Op operator;
		Tree left, right;
		Node(Op operator, Tree left, Tree right) {
			this.operator = operator;
			this.left = left; this.right = right;
		}

		public Tree replace(String name, Tree r) {
			return new Node(operator, left.replace(name, r), right.replace(name, r));
		}

		public Tree eval() {
			return operator.calc(left, right);
		}

		public String toString() {
			return String.format("( %s %s %s )", operator.opName(), left.toString(), right.toString());
		}
	}


	abstract class Op {
		abstract Tree calc(Tree left, Tree right);
		abstract String opName();
	}

	abstract class BinOp extends Op {
		abstract public int op(int rand1, int rand2);

		public Tree calc(Tree left, Tree right) {
			int l = ((IntLeaf)left.eval()).value;
			int r = ((IntLeaf)right.eval()).value;

			return new IntLeaf(op(l, r));
		}
	}

	class AddOp extends BinOp {
		public String opName() {return "+";}
		public int op(int rand1, int rand2) {return rand1 + rand2;}
	}

	class SubOp extends BinOp {
		public String opName() {return "-";}
		public int op(int rand1, int rand2) {return rand1 - rand2;}
	}

	class MulOp extends BinOp {
		public String opName() {return "*";}
		public int op(int rand1, int rand2) {return rand1 * rand2;}
	}

	class DivOp extends BinOp {
		public String opName() {return "/";}
		public int op(int rand1, int rand2) {return rand1 / rand2;}
	}

	class EqOp extends Op {
		public String opName() {return "==";}

		// ---
		// compare `left.value` and `right.value`
		// if `left.value` == `right.value` then
		//    return `( fun left ( fun left right )`
		// else return `( fun left ( fun right right )`
		// ---
		// arg. left: `IntLeaf`
		// arg. right: `IntLeaf`
		// return: `Node`
		public Tree calc(Tree left, Tree right) {
			IntLeaf l = (IntLeaf)left.eval();
			IntLeaf r = (IntLeaf)right.eval();
			NameLeaf retLeft = new NameLeaf("left");
			NameLeaf retRight = new NameLeaf("right");

			return new Node(new FunOp(), retLeft, new Node(new FunOp(), retRight, l.value == r.value ? retLeft : retRight));
		}
	}

	class DefOp extends Op {
		public String opName() {return "def";}

		// ---
		// return `register` method, and send `varName`, `right` to the method
		// ---
		// arg. left: `NameLeaf`, and `left.name` is variable name
		// arg. right: `Node`, and `right` is function
		// return: `NameLeaf`
		public Tree calc(Tree left, Tree right) {
			NameLeaf nameTree = (NameLeaf)left;
			String varName = nameTree.toString();

			register(varName, right);

			return left;
		}
	}

	class FunOp extends Op {
		public String opName() {return "fun";}
		public Tree calc(Tree left, Tree right) {return new Node(this, left, right);}
	}

	class CallOp extends Op {
		public String opName() {return "call";}

		public Tree calc(Tree left, Tree right) {
			Node fun;

			if(left.eval().getClass().getName().endsWith("Node")) {
				fun = (Node)left.eval();
			}
			else {
				fun = (Node)vars.get(((NameLeaf)left).toString());
			}

			NameLeaf param = (NameLeaf)fun.left;
			Tree body = fun.right;
			Tree expression = body.replace(param.name, right);

			if(verbose > 0) System.out.println("replace: " + expression.toString());

			return expression.eval();
		}
	}

	private Op[] optable = {new AddOp(), new SubOp(), new MulOp(), new DivOp(), new FunOp(), new CallOp(), new EqOp(), new DefOp()};
	private Scanner scanner;
	private Map<String, Op> ops;
	private Map<String, Tree> vars;
	static int verbose = 1;

	TreeScript3() {
		scanner = new Scanner(System.in);
		ops = new HashMap<String, Op>();
		vars = new HashMap<String, Tree>();

		for(Op op : optable) ops.put(op.opName(), op);
	}

	// ---
	// if `t` equals `Node`, return `Node` which has renamed Node using this method recursively
	// ---
	// arg. t: Tree
	// arg. renameFrom: String to be renamed
	// arg. renameTo: String to rename
	// return: Tree
	private Tree rename(Tree t, String renameFrom, String renameTo) {
		String clsName = t.getClass().getName();
		Tree ret = t;

		if(clsName.endsWith("Node")) {
			Node n = (Node)t;

			ret = new Node(n.operator, rename(n.left, renameFrom, renameTo), rename(n.right, renameFrom, renameTo));
		}
		else if(clsName.endsWith("NameLeaf")) {
			String name = ((NameLeaf)t).name;

			ret = new NameLeaf(name.equals(renameFrom) ? renameTo : name);
		}

		return ret;
	}

	/* ---
	register `{varName, funTree}` pair to `vars`, and `vars` is a `HashMap<variableName, Tree>` you registered
	---
	arg. varName: varName is "vars" key
	arg. funTree: Node, and funTree is `vars` value
	return: void */
	private void register(String varName, Tree funTree) {
		vars.put(varName, funTree);
	}

	private Tree parse() {
		if(scanner.hasNextInt()) {
			return new IntLeaf(scanner.nextInt());
		}
		else if(scanner.hasNext("\\(")) {
			scanner.next();
			String name = scanner.next();
			Tree left = parse();
			Tree right = parse();

			if(name.equals("fun")) {
				String var = ((NameLeaf)left).name;
				String rename = UUID.randomUUID().toString();

				left = rename(left, var, rename);
				right = rename(right, var, rename);
			}

			scanner.next("\\)");

			return new Node(ops.get(name), left, right);
		}
		else {
			String var = scanner.next();

			return vars.containsKey(var) ? vars.get(var) : new NameLeaf(var);
		}
	}

	public void run() {
		while(scanner.hasNext()) {
			try {
				Tree t = parse();

				System.out.printf("function: %s%n%s%n", t.toString(), t.eval());
			}
			catch (Exception e) {
				System.err.println("Error");
				e.printStackTrace(System.out);
			}
		}
	}

	public static void main(String[] args) {
		if(args.length > 0) verbose = 0;

		new TreeScript3().run();
	}
}

