do
  local _base_0 = {
    value = value,
    eval = function(self)
      return self
    end,
    replace = function(self, name, r)
      return self
    end,
    tostring = function(self)
      return self.value
    end
  }
  _base_0.__index = _base_0
  local _class_0 = setmetatable({
    __init = function(self, value)
      self.value = value
    end,
    __base = _base_0,
    __name = "IntLeaf"
  }, {
    __index = _base_0,
    __call = function(cls, ...)
      local _self_0 = setmetatable({}, _base_0)
      cls.__init(_self_0, ...)
      return _self_0
    end
  })
  _base_0.__class = _class_0
  IntLeaf = _class_0
end
local NameLeaf
do
  local _base_0 = {
    name = name,
    eval = function(self)
      return self
    end,
    replace = function(self, name, r)
      return name == self.name and r or self
    end,
    tostring = function(self)
      return self.name
    end
  }
  _base_0.__index = _base_0
  local _class_0 = setmetatable({
    __init = function(self, name)
      self.name = name
    end,
    __base = _base_0,
    __name = "NameLeaf"
  }, {
    __index = _base_0,
    __call = function(cls, ...)
      local _self_0 = setmetatable({}, _base_0)
      cls.__init(_self_0, ...)
      return _self_0
    end
  })
  _base_0.__class = _class_0
  NameLeaf = _class_0
end
local Node
do
  local _base_0 = {
    operator = operator,
    left = left,
    right = right,
    eval = function(self)
      return self.operator:calc(self.left, self.right)
    end,
    replace = function(self, name, r)
      return Node(self.operator, self.left:replace(name, r), self.right:replace(name, r))
    end,
    tostring = function(self)
      return string.format("( %s %s %s )", self.operator.op_name(), self.left:tostring(), self.right:tostring())
    end
  }
  _base_0.__index = _base_0
  local _class_0 = setmetatable({
    __init = function(self, operator, left, right)
      self.operator = operator
      self.left = left
      self.right = right
    end,
    __base = _base_0,
    __name = "Node"
  }, {
    __index = _base_0,
    __call = function(cls, ...)
      local _self_0 = setmetatable({}, _base_0)
      cls.__init(_self_0, ...)
      return _self_0
    end
  })
  _base_0.__class = _class_0
  Node = _class_0
end
local BinOp
do
  local _base_0 = {
    calc = function(self, left, right)
      local l = 1
      local r = 2
      return IntLeaf(self:op(l, r))
    end
  }
  _base_0.__index = _base_0
  local _class_0 = setmetatable({
    __init = function() end,
    __base = _base_0,
    __name = "BinOp"
  }, {
    __index = _base_0,
    __call = function(cls, ...)
      local _self_0 = setmetatable({}, _base_0)
      cls.__init(_self_0, ...)
      return _self_0
    end
  })
  _base_0.__class = _class_0
  BinOp = _class_0
end
local AddOp
do
  local _parent_0 = BinOp
  local _base_0 = {
    op_name = function()
      return "+"
    end,
    op = function(rand1, rand2)
      return rand1 + rand2
    end
  }
  _base_0.__index = _base_0
  setmetatable(_base_0, _parent_0.__base)
  local _class_0 = setmetatable({
    __init = function(self, ...)
      return _parent_0.__init(self, ...)
    end,
    __base = _base_0,
    __name = "AddOp",
    __parent = _parent_0
  }, {
    __index = function(cls, name)
      local val = rawget(_base_0, name)
      if val == nil then
        return _parent_0[name]
      else
        return val
      end
    end,
    __call = function(cls, ...)
      local _self_0 = setmetatable({}, _base_0)
      cls.__init(_self_0, ...)
      return _self_0
    end
  })
  _base_0.__class = _class_0
  if _parent_0.__inherited then
    _parent_0.__inherited(_parent_0, _class_0)
  end
  AddOp = _class_0
end
local SubOp
do
  local _parent_0 = BinOp
  local _base_0 = {
    op_name = function()
      return "-"
    end,
    op = function(rand1, rand2)
      return rand1 - rand2
    end
  }
  _base_0.__index = _base_0
  setmetatable(_base_0, _parent_0.__base)
  local _class_0 = setmetatable({
    __init = function(self, ...)
      return _parent_0.__init(self, ...)
    end,
    __base = _base_0,
    __name = "SubOp",
    __parent = _parent_0
  }, {
    __index = function(cls, name)
      local val = rawget(_base_0, name)
      if val == nil then
        return _parent_0[name]
      else
        return val
      end
    end,
    __call = function(cls, ...)
      local _self_0 = setmetatable({}, _base_0)
      cls.__init(_self_0, ...)
      return _self_0
    end
  })
  _base_0.__class = _class_0
  if _parent_0.__inherited then
    _parent_0.__inherited(_parent_0, _class_0)
  end
  SubOp = _class_0
end
local MulOp
do
  local _parent_0 = BinOp
  local _base_0 = {
    op_name = function()
      return "*"
    end,
    op = function(rand1, rand2)
      return rand1 * rand2
    end
  }
  _base_0.__index = _base_0
  setmetatable(_base_0, _parent_0.__base)
  local _class_0 = setmetatable({
    __init = function(self, ...)
      return _parent_0.__init(self, ...)
    end,
    __base = _base_0,
    __name = "MulOp",
    __parent = _parent_0
  }, {
    __index = function(cls, name)
      local val = rawget(_base_0, name)
      if val == nil then
        return _parent_0[name]
      else
        return val
      end
    end,
    __call = function(cls, ...)
      local _self_0 = setmetatable({}, _base_0)
      cls.__init(_self_0, ...)
      return _self_0
    end
  })
  _base_0.__class = _class_0
  if _parent_0.__inherited then
    _parent_0.__inherited(_parent_0, _class_0)
  end
  MulOp = _class_0
end
local DivOp
do
  local _parent_0 = BinOp
  local _base_0 = {
    op_name = function()
      return "/"
    end,
    op = function(rand1, rand2)
      return rand1 / rand2
    end
  }
  _base_0.__index = _base_0
  setmetatable(_base_0, _parent_0.__base)
  local _class_0 = setmetatable({
    __init = function(self, ...)
      return _parent_0.__init(self, ...)
    end,
    __base = _base_0,
    __name = "DivOp",
    __parent = _parent_0
  }, {
    __index = function(cls, name)
      local val = rawget(_base_0, name)
      if val == nil then
        return _parent_0[name]
      else
        return val
      end
    end,
    __call = function(cls, ...)
      local _self_0 = setmetatable({}, _base_0)
      cls.__init(_self_0, ...)
      return _self_0
    end
  })
  _base_0.__class = _class_0
  if _parent_0.__inherited then
    _parent_0.__inherited(_parent_0, _class_0)
  end
  DivOp = _class_0
end
local FunOp
do
  local _base_0 = {
    op_name = function()
      return "fun"
    end,
    calc = function(self, left, right)
      return Node(self, left, right)
    end
  }
  _base_0.__index = _base_0
  local _class_0 = setmetatable({
    __init = function() end,
    __base = _base_0,
    __name = "FunOp"
  }, {
    __index = _base_0,
    __call = function(cls, ...)
      local _self_0 = setmetatable({}, _base_0)
      cls.__init(_self_0, ...)
      return _self_0
    end
  })
  _base_0.__class = _class_0
  FunOp = _class_0
end
local EqOp
do
  local _base_0 = {
    op_name = function()
      return "=="
    end,
    calc = function(self, left, right)
      local l = left:eval()
      local r = right:eval()
      local ret_left = NameLeaf("left")
      local ret_right = NameLeaf("right")
      return Node(FunOp(), ret_left, Node(FunOp(), ret_right, l.value == r.value and ret_left or ret_right))
    end
  }
  _base_0.__index = _base_0
  local _class_0 = setmetatable({
    __init = function() end,
    __base = _base_0,
    __name = "EqOp"
  }, {
    __index = _base_0,
    __call = function(cls, ...)
      local _self_0 = setmetatable({}, _base_0)
      cls.__init(_self_0, ...)
      return _self_0
    end
  })
  _base_0.__class = _class_0
  EqOp = _class_0
end
local CallOp
do
  local _base_0 = {
    op_name = function()
      return "call"
    end,
    calc = function(self, left, right)
      local fun = left:eval()
      local param = fun.left
      local body = fun.right
      local expression = body:replace(param.name, right)
      print("replace: " .. expression:tostring())
      return expression:eval()
    end
  }
  _base_0.__index = _base_0
  local _class_0 = setmetatable({
    __init = function() end,
    __base = _base_0,
    __name = "CallOp"
  }, {
    __index = _base_0,
    __call = function(cls, ...)
      local _self_0 = setmetatable({}, _base_0)
      cls.__init(_self_0, ...)
      return _self_0
    end
  })
  _base_0.__class = _class_0
  CallOp = _class_0
end
local optable = {
  AddOp(),
  SubOp(),
  MulOp(),
  DivOp(),
  FunOp(),
  CallOp(),
  EqOp()
}
local scanner = require("scanner")
local ops = { }
for _, op in pairs(optable) do
  ops[op.op_name()] = op
end
local parse
parse = function()
  if scanner.has_next_int() then
    return IntLeaf(scanner.next_int())
  elseif scanner.has_next("(") then
    scanner.next()
    local name = scanner.next()
    local left = parse()
    local right = parse()
    scanner.next(")")
    return Node(ops[name], left, right)
  else
    return NameLeaf(scanner.next())
  end
end
run = function()
  while scanner.has_next() do
    local t = parse()
    print(string.format("function: %s\n%s", t:tostring(), t:eval():tostring()))
  end
end
