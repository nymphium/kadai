local scanner = require "scanner"
local uuid = require "uuid" uuid.seed()
local vars = setmetatable({}, {__call = function(self, key, val) self[key] = val end})

IntLeaf = function(value)
	return setmetatable({
		value = value,
		class = "IntLeaf",
		eval = function(self) return self end,
		replace = function(self, name, r) return self end,
		tostring = function(self) return tostring(self.value) end
	}, {__index = IntLeaf})
end

NameLeaf = function(name)
	return setmetatable({
		name = name,
		class = "NameLeaf",
		eval = function(self) return self end,
		replace = function(self, name, r) return name == self.name and r or self end,
		tostring = function(self) return self.name end
	}, {__index = NameLeaf})
end

Node = function(operator, left, right)
	return setmetatable({
		operator = operator,
		left = left,
		right = right,
		class = "Node",
		eval = function(self) return self.operator:calc(self.left, self.right) end,
		replace = function(self, name, r) return Node(self.oerator, self.left:replace(name, r), self.right:replace(name, r)) end,
		tostring = function(self) return string.format("( %s %s %s )", self.operator.op_name(), self.left:tostring(), self.right:tostring()) end
	}, {__index = Node})
end

BinOp = {
	calc = function(self, left, right)
		local l = left:eval().value
		local r = right:eval().value

		return IntLeaf(self.op(l, r))
	end
}

AddOp = setmetatable({
		op_name = function() return "+" end,
		op = function(rand1, rand2) return rand1 + rand2 end
	}, {__index = BinOp}
	)

SubOp = setmetatable({
		op_name = function() return "-" end,
		op = function(rand1, rand2) return rand1 - rand2 end
	}, {__index = BinOp}
	)

MulOp = setmetatable({
		op_name = function() return "*" end,
		op = function(rand1, rand2) return rand1 * rand2 end
	}, {__index = BinOp}
	)

DivOp = setmetatable({
		op_name = function() return "/" end,
		op = function(rand1, rand2) return rand1 / rand2 end
	}, {__index = BinOp}
	)

EqOp = {
	op_name = function() return "==" end,
	calc = function(_, left, right)
		local l = left:eval()
		local r = right:eval()
		local ret_left = NameLeaf("left")
		local ret_right = NameLeaf("right")

		return Node(FunOp, ret_left, Node(FunOp, ret_right, l.value == r.value and ret_left or ret_right))
	end
}

DefOp = {
	op_name = function() return "def" end,
	calc = function(_, left, right)
		local var_name = left:tostring()

		vars(var_name, right)

		return left
	end
}

FunOp = {
	op_name = function() return "fun" end,
	calc = function(self, left, right) return Node(self, left, right) end
}

CallOp = {
	op_name = function() return "call" end,
	calc = function(_, left, right)
		local fun = left:eval()
		local param = fun.left
		local body = fun.right
		local expression = body:replace(param.name, right)

		return expression:eval()
	end
}


local optable = {_G.AddOp, _G.SubOp, _G.MulOp, _G.DivOp, _G.FunOp, _G.CallOp, _G.EqOp, _G.DefOp}
local ops = {}

for _, op in pairs(optable) do
	ops[op.op_name()] = op
end

local rename rename = function(t, rename_from, rename_to)
	if t.class == "Node" then
		local ret = Node(t.operator, rename(t.left, rename_from, rename_to), rename(t.right, rename_from, rename_to))
	elseif t.class == "NameLeaf" then
		local ret = NameLeaf(t.name == rename_from and rename_to or t.name)
	end

	return ret
end

local parse parse = function()
	if scanner.has_next_int() then
		return IntLeaf(scanner.next_int())
	elseif scanner.has_next("(") then
		scanner.next()

		local name = scanner.next()
		local left = parse()
		local right = parse()

		-- if name == "fun" then
			-- local var = left.name
			-- local renameto = uuid()
			-- left = rename(left, var, renameto)
			-- right = rename(right, var, renameto)
		-- end

		scanner.next(")")

		return Node(ops[name], left, right)
	else
		local var = scanner.next()

		return vars[var] or NameLeaf(var)
	end
end

run = function()
	while scanner.has_next() do
		local t = parse()

		-- debugprint(t)

		print(string.format("function: %s\n%s", t:tostring(), t:eval():tostring()))
	end
end

