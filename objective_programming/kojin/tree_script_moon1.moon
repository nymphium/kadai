class IntLeaf
	new: (@value) =>
	eval: => self
	replace: (name, r) => @
	tostring: () => @value

class NameLeaf
	new: (@name) =>
	eval: => @
	replace: (name, r) => name == @name and r or @
	tostring: => @name

class Node
	new: (@operator, @left, @right) =>
	eval: => @operator\calc(@left, @right)
	replace: (name, r) => Node(@operator, @left\replace(name, r), @right\replace(name, r))
	tostring: => string.format "( %s %s %s )", @operator.op_name!, @left\tostring!, @right\tostring!

class BinOp
	calc: (left, right) =>
		l = left\eval!.value
		r = right\eval!.value

		IntLeaf(@op l, r)

class AddOp extends BinOp
	op_name: -> "+"
	op: (rand1, rand2) -> rand1 + rand2

class SubOp extends BinOp
	op_name: -> "-"
	op: (rand1, rand2) -> rand1 - rand2

class MulOp extends BinOp
	op_name: -> "*"
	op: (rand1, rand2) -> rand1 * rand2

class DivOp extends BinOp
	op_name: -> "/"
	op: (rand1, rand2) -> rand1 / rand2

class FunOp
	op_name: -> "fun"
	calc: (left, right) => Node(self, left, right)

class CallOp
	op_name: -> "call"
	calc: (left, right) =>
		fun = left\eval!
		param = fun.left
		body = fun.right
		expression = body\replace(param.name, right)

		print "replace: " .. expression\tostring!

		return expression\eval!

class EqOp
	op_name: -> "=="
	calc: (left, right) =>
		l = left\eval!
		r = right\eval!
		ret_left = NameLeaf "left"
		ret_right = NameLeaf "right"

		Node(FunOp!, ret_left, Node(FunOp!, ret_right, l.value == r.value and ret_left or ret_right))


optable = {AddOp!, SubOp!, MulOp!, DivOp!, FunOp!, CallOp!, EqOp!}
scanner = require "scanner"
ops = {}
for _, op in pairs optable
	ops[op.op_name!] = op


parse = ->
	if scanner.has_next_int!
		IntLeaf scanner.next_int!
	elseif scanner.has_next "("
		scanner.next!
		name = scanner.next!
		left = parse!
		right = parse!
		scanner.next ")"

		Node ops[name], left, right
	else
		NameLeaf scanner.next!

run = ->
	while scanner.has_next!
		t = parse!

		print string.format "function: %s\n%s", t\tostring!, t\eval!\tostring!

run!

