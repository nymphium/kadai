( def fact ( fun n ( call ( call ( == n 0 ) 1 ) ( * n ( call fact ( - n 1 ) ) ) ) ) )

( call fact 20 )
