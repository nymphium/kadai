class ParabolaShape extends Shape {
	double a, b, c;

	ParabolaShape(double a, double b, double c) {
		this.a = a;
		this.b = b;
		this.c = c;
	}

	boolean inside (double x, double y) {
		return a * x * x + b * x + c > y;
	}
}
