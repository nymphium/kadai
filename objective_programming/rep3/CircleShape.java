// ファイルCircleShape.java
class CircleShape extends Shape {
// public class CircleShape {
    double x0, y0, r;  // 中心座標(x0, y0), 半径r
    CircleShape(double x0, double y0, double r) {
        this.x0 = x0;
        this.y0 = y0;
        this.r = r;
    }
    boolean inside(double x, double y) {
        return Math.pow((x - x0), 2) + Math.pow((y - y0), 2) <= r * r;
    }
}
