class DotView implements RobotView {
	public void turn(double oldHeading, double newHeading) {};

	public void move(double oldX, double oldY, double newX, double newY) {
		double xx = (newX - oldX) / 10.;
		double yy = (newY - oldY) / 10.;

		for(int i = 1; i < 11; i++) {
			System.out.println(oldX + xx * i + " " + oldY + yy * i);
		}

		for(int i = 1; i < 11; i++) {
			System.out.println(newX + xx * i + " " + newY + yy * i);
		}
	}
}

