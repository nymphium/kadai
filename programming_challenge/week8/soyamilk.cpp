#include <cstdio>
#include <cmath>
#include <algorithm>
using namespace std;

#define rad(d) d * M_PI / 180
#define printml(q) printf("%.3lf mL\n", q)

int main() {
	double l, w, h, d;

	while(scanf("%lf %lf %lf %lf", &l, &w, &h, &d) != EOF) {
		if(d == 0) printml(l * w * h);
		else {
			auto tand = tan(rad(d));
			if(tand > h / l) printml(w * h * h * tan(rad((int)(90. - d))) / 2);
			else printml(l * w * (h - l * tand / 2));
		}
	}
}
