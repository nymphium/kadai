#include <cstdio>

using namespace std;
int c;

long cache[1000001] = {0};

int main(void) {
	while(1) {
		scanf("%d", &c);

		if(c < 3) break;

		if(cache[c]) printf("%ld\n", cache[c]);
		else {
			for(int i = 4; i <= c; i++) {
				long  sho = (i&1) ? 2L : 1L;
				long  su = (i - 2) / 2;
				long mat = sho + 2 * (su - 1);
				cache[i] = cache[i - 1] + (su * (sho + mat)) / 2L;
			}

			printf("%ld\n", cache[c]);
		}
	}

	return 0;
}
