import sys

for line in sys.stdin:
    a, b = line.split()
    ora, orb = ord(a[0]), ord(b[0])
    a_, b_ = None, None

    if ora > 64:
        a_ = ora - 55
    if orb > 64:
        b_ = orb - 55

    if (a_ is None): # A is number
        if (b_ is None):
            if len(a) + len(b) == 2:
                print("{a} is not equal to {b} in any base 2..36".format(a = a, b = b))
            else:
                inta, intb = int(a), int(b)
                sml, lrg = max(inta, intb), min(inta, intb)

                basea, baesb = a[len(a) - 1], b[len(b) - 1]
                print(basea, baseb)

        else:
            if len(a) == 1:
                print("{a} is not equal to {b} in any base 2..36".format(a = a, b = b))
    else: # a is Alphabet
        if (b_ is None): # B is a number
            if len(b) == 1:
                print("{a} is not equal to {b} in any base 2..36".format(a = a, b = b))
            else:
                inta, intb = a_, int(b)
                sml, lrg = max(inta, intb), min(inta, intb)
                print(sml, lrg)
        else: # A and B are Alphabet
            print("{a} is not equal to {b} in any base 2..36".format(a = a, b = b))

