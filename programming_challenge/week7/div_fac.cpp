#include <cstdio>
using namespace std;

long long int fact(int n) {
	if(n == 1) return 1;
	else return n * fact(n - 1);
}

int main() {
	printf("%lld\n", fact(100));
	return 0;
}
