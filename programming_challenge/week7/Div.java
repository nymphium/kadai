import java.util.*;
import java.math.BigInteger;

class Div {
// class Main {
	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);

		while(s.hasNextInt()) {
			int t = s.nextInt();
			int a = s.nextInt();
			int b = s.nextInt();

			System.out.printf("(%d^%d-1)/(%d^%d-1) ", t,a,t,b);

			if(t == 1 || (a % b != 0 || a < b) || (a - b) * Math.log10(t) > 99) System.out.println("is not an integer with less than 100 digits.");
			else if(a == b) System.out.println("1");
			else {
				BigInteger t_ = new BigInteger(Integer.toString(t));
				BigInteger one = BigInteger.ONE;
				BigInteger bi = t_.pow(a).subtract(one).divide(t_.pow(b).subtract(one));

				if(bi.toString().length() > 99)  System.out.println("is not an integer with less than 100 digits.");
				else System.out.println(bi);
			}
		}
	}
}
