#include <cstdio>
#include <cmath>
#include <string>
#include <iostream>
using namespace std;

int main(void) {
	int t, a, b;

	while(1) {
		if(scanf("%d %d %d", &t, &a, &b) < 2) return 0;

		printf("(%d^%d-1)/(%d^%d-1) ", t,a,t,b);

		if(t == 1 || a == b || a%b > 0 || (a - b) * log10(t) > 99) puts("is not an integer with less than 100 digits.");
		else {
			double f = (pow(t, a) - 1) / (pow(t, b) - 1);
			if((to_string(f).length()) > 100) puts("is not an integer with less than 100 digits.");
			else printf("%.0lf\n", f);
		}
	}

	return 0;
}
