import re

vpat = re.compile(r"(-?\d+)")
pat = re.compile(r"\((\S+)\s(\S+)\s(\S+)\)")

def eval(e):
    if vpat.match(e):
        return int(e)
    else:
        tuple = pat.match(e)
        p, e1, e2 = tuple.group(1, 2, 3)
        pe = float(p)
        ee1 = eval(e1)
        ee2 = eval(e2)
        return pe * (ee1 + ee2) + (1 - pe) * (ee1 - ee2)

while True:
    i = input()

    if i == "()":
        print(value)
        break

    if vpat.match(i):
        print(int(i))
        #  print(eval(i, True))
    else:
        print(eval(i))
        #  value = int(i)

