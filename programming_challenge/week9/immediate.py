import sys

nth = 1
code = []
cnt = 0

for i in sys.stdin:
    i = i.rstrip()

    if i == "9":
        f1 = True
        for x in range(cnt):
            if not f1: break
            for y in range(cnt):
                if x != y:
                    xlen = len(code[x])
                    ylen = len(code[y])
                    if xlen > ylen:
                        if "".join(code[x][0:ylen]) == code[y]:
                            print("Set {n} is not immediately decoded".format(n = nth))
                            f1 = False
                            break
                    if xlen < ylen:
                        if "".join(code[y][0:xlen]) == code[y]:
                            print("Set {n} is not immediately decoded".format(n = nth))
                            f1 = False
                            break
                    if xlen == ylen:
                        if code[x] == code[y]:
                            print("Set {n} is not immediately decoded".format(n = nth))
                            f1 = False
                            break

        if f1: print("Set {n} is immediately decoded".format(n = nth))
        nth += 1
        cnt = 0
        code = []

    code.append(i)
    cnt += 1

