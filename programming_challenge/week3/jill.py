for nth in range(1, int(input()) + 1):
    sum, max, a, z_, a_ = 0, 0, 0, 0, 0

    for i in range(1, int(input())):
        sum += int(input())

        if sum > max or sum == max and i - a > z_ - a_:
            max = sum
            z_ = i
            a_ = a

        if sum < 0:
            a = i
            sum = 0

    if max > 0:
        print("The nicest part of route {r} is between stops {a} and {z}".format(r = nth, a = a_ + 1, z = z_ + 1))
    else:
        print("Route {r} has no nice parts".format(r = nth))

