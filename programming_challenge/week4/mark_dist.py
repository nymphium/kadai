ll = 0
lh = 0

def f(ll, hl, n, t, p, sum, index, dp):
    if sum > t: return 0
    if index == n: return sum == t and 1 or 0

    dp[sum][index] = 0
    ret = 0

    for i in range(ll, hl + 1): ret += f(ll, hl, n, t, p, sum + i, index + 1, dp)

    dp[sum][index] = ret

    return ret

num = int(input())

for _ in range(num):
    (n, t, p) = tuple(map(int, input().split()))

    ll = p
    hl = t - p * (n - 1)

    _dp = [-1] * 70
    dp = [_dp[:]]
    for _ in range(70): dp.append(_dp[:])

    print(f(ll, hl, n, t, p, 0, 0, dp))

