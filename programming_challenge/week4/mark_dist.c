#include <stdio.h>

#define MAXL 71

int dp[MAXL][MAXL];
int n, t, p, ll, hl;

int f(int sum, int index){
	if(sum > t) return 0;
	if(index == n) return sum == t ? 1 : 0;

	dp[sum][index] = 0;
	int ret = 0;

	int i;
	for(i = ll; i <= hl; i++) ret += f(sum + i, index + 1);

	return dp[sum][index] = ret;
}

int main() {
	int cnt;
	scanf("%d", &cnt);

	while(cnt--) {
		scanf("%d %d %d", &n, &t, &p);
		ll = p;
		hl = t - p * (n - 1);

		int i, j;
		for(i = 0; i < t; i++)
			for(j = 0; j < MAXL; j++) dp[i][j] = -1;

		printf("%d\n", f(0, 0));
	}
}

