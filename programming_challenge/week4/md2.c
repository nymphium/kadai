#include <stdio.h>

#define maxn 70

int dp[maxn][maxn];

int f(int n, int t, int p) {
	if(! (n | t)) return 1;
	else if(t < p || n < 0) return 0;
	else if(dp[n][t]+1) return dp[n][t];
	else {
		dp[n][t] = 0;
		int i;
		for(i = p; i <= t; i++) dp[n][t] += f(n - 1, t - i, p);

		return dp[n][t];
	}
}

int main() {
	int cnt;
	scanf("%d", &cnt);

	while(cnt--) {
		int n, t, p;
		scanf("%d %d %d", &n, &t, &p);

		int i, j;
		for(i = 0; i < maxn; i++)
			for(j = 0; j < maxn; j++) dp[i][j] = -1;

		printf("%d\n", f(n, t, p));
	}
}
