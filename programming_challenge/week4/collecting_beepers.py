def dist(a, b):
    return abs(a[0] - b[0]) + abs(a[1] - b[1])


sn = int(input()) # the number of senarios

for _ in range(sn):
    size = tuple(map(int, input().split()))
    startpos = tuple(map(int, input().split()))
    beepers = int(input())
    bc = [] # coordinates of each beeper

    for _ in range(beepers):
        bc.append(tuple(map(int, input().split())))

    pos = startpos
    path = 0

    for _ in range(beepers):
        sh = 0

        for i in range(1, len(bc)):
            sh = dist(pos, bc[sh]) > dist(pos, bc[i]) and i or sh

        path += dist(pos, bc[sh])
        #  print(dist(pos, bc[sh]), pos, bc[sh])
        pos = bc[sh]
        del(bc[sh])

    path += dist(pos, startpos)

    print("The shortest path has length", path)

