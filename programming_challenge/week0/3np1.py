import sys

ar = [None] * 1000000

def thn1(n, acc):
    while True:
        acc += 1
        if n == 1: return acc

        if n % 2 != 0:
            n = 3*n + 1
        else:
            n = n>> 1

def hoge(i, j):
    if i > j: i, j = j, i
    if i == j: j += 1
    m = 0
    for c in range(i, j):
        if ar[c] is not None:
            p = ar[c]
            if p>m:m =p
        else:
            p = thn1(c, 0)
            ar[c] = p
            if p>m:m =p

    return m

for line in sys.stdin:
    st, en = tuple(map(int, line.rstrip().split(" ")))

    print(st, en, hoge(st, en))

