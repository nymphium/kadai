#include <cstdio>
using namespace std;

inline static long thn1(int n, long acc) {
	while(n != 1) {
		if(n % 2) {
			n = 3 * n + 1;
		}else {
			n >>= 1;
		}
		acc++;
	}

	return acc;
}

inline static long hoge(long i, long j) {
	if(i > j) {
		long tmp = i;
		i = j;
		j = tmp;
	}

	long m = 0, p = 0;
	for(long c = i; c <= j; c++) {
		p = thn1(c, 1);
		if(p > m) m = p;
	}

	return m;
}

int main() {
	long i, j;
	while(scanf("%ld %ld", &i, &j) != EOF) {
		printf("%ld %ld %ld\n", i, j, hoge(i, j));
	}
}
