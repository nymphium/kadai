# get lines
# returns a tuple structed with 2 elements, division point :: tuple(int, int) and residence :: tuple(int*)
def get_inputs():
    tuplemapint = lambda t: tuple(map(int,t))
    setnum = int(input())
    # except returns a  (None, None) if K == 0
    if setnum == 0:
        return (None, None)

    divpt = tuplemapint(input().split(" "))
    reside = tuple(tuplemapint(input().split(" ")) for _ in range(setnum))

    return (divpt, reside)

# main
while True:
    divpt, resides = get_inputs()
    if divpt is None:
        break

    for pt in iter(resides):
        c1, c2 = divpt[0] < pt[0], divpt[1] < pt[1]

        if (divpt[0] == pt[0] or (divpt[1] == pt[1])):
            print("divisa")
        else:
            if c1:
                if c2: print("NE")
                else: print("SE")
            else:
                if c2: print("NO")
                else: print("SO")

