from datetime import datetime, timedelta

def get_zodiac(date):
    m, d = date.month, date.day

    if m < 2: return "capricorn" if d < 20 else "aquarius"
    if m < 3: return "aquarius" if d < 19 else "pisces"
    if m < 4: return "pisces" if d < 21 else "aries"
    if m < 5: return "aries" if d < 20 else "taurus"
    if m < 6: return "taurus" if d < 21 else "gemini"
    if m < 7: return "gemini" if d < 22 else "cancer"
    if m < 8: return "cancer" if d < 23 else "leo"
    if m < 9: return "leo" if d < 23 else "virgo"
    if m < 10: return "virgo" if d < 23 else "libra"
    if m < 11: return "libra" if d < 24 else "scorpio"
    if m < 12: return "scorpio" if d < 23 else "sagittarius"
    return "sagittarius" if d < 23 else "capricorn"

setnum = int(input())

for i in range(setnum):
    after40 = datetime.strptime(input(), "%m%d%Y") + timedelta(weeks = 40)

    print(i+1,after40.strftime("%m/%d/%Y"), get_zodiac(after40))

print()

#  def get_zodiac(date):
    #  m, d = date[0], date[1]

    #  if m < 2: return "capricorn" if d < 20 else "aquarius"
    #  if m < 3: return "aquarius" if d < 19 else "pisces"
    #  if m < 4: return "pisces" if d < 21 else "aries"
    #  if m < 5: return "aries" if d < 20 else "taurus"
    #  if m < 6: return "taurus" if d < 21 else "gemini"
    #  if m < 7: return "gemini" if d < 22 else "cancer"
    #  if m < 8: return "cancer" if d < 23 else "leo"
    #  if m < 9: return "leo" if d < 23 else "virgo"
    #  if m < 10: return "virgo" if d < 23 else "libra"
    #  if m < 11: return "libra" if d < 24 else "scorpio"
    #  if m < 12: return "scorpio" if d < 23 else "sagittarius"
    #  return "sagittarius" if d < 23 else "capricorn"

#  def todate_with40(line):
    #  m, d, y = int(line[0]+line[1]), int(line[2]+line[3]), int(line[4]+line[5]+line[6]+line[7])

    #  d += 280

    #  while True:
        #  if (m == 1) or (m == 3) or (m == 5) or (m==7) or (m == 8) or (m == 10) or (m==12):
            #  d -= 31kkk


