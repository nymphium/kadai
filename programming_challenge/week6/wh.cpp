#include <algorithm>
#include<cstdio>

using namespace std;

struct wh{
	int from, to, time;
};

int main() {
	int C,N,M;
	wh W[2000];
	int dist[1000];
	bool reached[1000];
	bool found;

	scanf("%d",&C);

	while(C--) {
		scanf("%d %d",&N,&M);

		for(int i=0;i<M;i++) scanf("%d %d %d",&W[i].from,&W[i].to,&W[i].time);

		dist[0]=0;
		reached[0]=true;
		for(int i=1;i<N;i++) reached[i]=false;

		while(--N) {
			for(int j=0;j<M;j++) {
				if(reached[W[j].from]) {
					if(!reached[W[j].to]) {
						dist[W[j].to] = dist[W[j].from] + W[j].time;
						reached[W[j].to] = true;
					}else dist[W[j].to] = min(dist[W[j].to], dist[W[j].from] + W[j].time);
				}
			}
		}

		found=false;

		for(int i = 0;i < M && !found; i++) if(reached[W[i].from] && (!reached[W[i].to] || dist[W[i].to]>dist[W[i].from]+W[i].time)) found = true;

		if(found) printf("possible\n");
		else printf("not possible\n");
	}
}

