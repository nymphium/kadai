import sys

cases = int(input())

fragments = []

for l in sys.stdin:
    l_ = l.strip()
    if len(l_) > 0:
        fragments.append(l_)

print(fragments)
