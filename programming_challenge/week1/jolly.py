while True:
    try:
        lines = tuple(map(int, input().split()))
    except EOFError:
        break

    num = lines[0]
    set = lines[1:num + 1]

    if num == 1:
        print("Jolly")
    else:
        isjolly = True
        acc = (None,) # differences accumlator
        for i in range(1, num):
            dif = abs(set[i - 1] - set[i]) # diff
            if dif in acc:
                isjolly = False
                break
            acc = acc + (dif,)
            if dif > num - 1 or dif < 1:
                isjolly = False
                break

        if isjolly:
            print("Jolly")
        else:
            print("Not jolly")

