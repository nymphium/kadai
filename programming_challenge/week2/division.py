#  unverwrapstr = lambda s: "".join(sorted(set(s), key = s.index))

oncep  = False

while True:
    try:
        ans = int(input())
        if ans == 0:
            break

        if oncep: print()
        oncep = True
    except EOFError:
        break

    # from 01234 to max of 2

    hasans = False

    for i in range(1234, 48652):
        i_ = str(i)
        if i < 10000: i_ = "0" + i_
        iset = set(i_)

        if i_ == "".join(sorted(iset, key = i_.index)):
            j = ans * i

            if j > 98760:
                break

            j_ = str(j)
            if j < 10000: j_ = "0" + j_
            jset = set(j_)

            if (iset & jset == set()) and (j_ ==  "".join(sorted(jset, key = j_.index))):
                hasans = True
                print("{j} / {i} = {ans}".format(j = j_, i = i_, ans = ans))

    if not hasans:
        print("There are no solutions for {ans}.".format(ans = ans))

