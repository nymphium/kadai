while True:
    i = input()
    if i == "0 0": break

    (dh, nk) = tuple(map(int, i.split()))

    dhl, nkl = [], []
    cost = 0

    for _ in range(dh):
        dhl.append(int(input()))

    for _ in range(nk):
        nkl.append(int(input()))

    doomed = False

    for i in iter(dhl):
        fl = [x for x in iter(nkl) if x >= i]

        if len(fl) == 0:
            doomed = True
            break

        m = min(fl)
        nkl.remove(m)
        cost += m

    if doomed:
        print("Loowater is doomed!")
    else:
        print(cost)

