#include <cstdio>
#include <vector>
#include <list>
#include <algorithm>
using namespace std;

int main() {
	int dh, nk;
	int t;
	list<int> nkl{};
	vector<int> fl{};
	vector<int> tmp{};

	while(true){

		nkl.clear();
		scanf("%d %d", &dh, &nk);
		if(dh == 0 && nk == 0) break;

		int dhl[dh];

		for(int i = 0; i < dh; i++) {
			scanf("%d", &t);
			dhl[i] = t;
		}

		for(int j = 0; j < nk; j++) {
			scanf("%d", &t);
			nkl.push_back(t);
		}

		bool doomed = false;
		int cost = 0;

		nkl.sort();

		for(int i = 0; i < dh; i++) {
			tmp.clear();
			fl.clear();
			for(int j : nkl){
				if(j >= dhl[i]) fl.push_back(j);
			}

			if(fl.empty()) {
				doomed = true;
				break;
			}

			auto m = min_element(fl.begin(), fl.end());
			int pos = distance(nkl.begin(), m);
			nkl[pos] = 0;

			// for(int i = 0; i < nk; i++) {
				// if(i != pos) tmp.push_back(nkl[i]);
			// }

			// nkl.resize(tmp.size());
			// copy(tmp.begin(), tmp.end(), back_inserter(nkl));

			cost += *m;
		}

		if(doomed || cost == 0) {
			puts("Loowater is doomed!");
		}else {
			printf("%d\n", cost);
		}
	}
}

