#include <cstdio>
#include <algorithm>
#include <cstring>
using namespace std;

const int MAX_N = 8;
const int MAX_M = 20;

int N, M;
int as[MAX_M], bs[MAX_M], cs[MAX_M];
int order[MAX_N];

void init(){
	for(int i=0; i<M; i++){
		scanf("%d%d%d", as+i, bs+i, cs+i);
	}
}

bool check(){
	for(int i=0; i<M; i++){
		if(cs[i] < 0){
			if(abs(order[as[i]] - order[bs[i]]) < -cs[i]){
				return false;
			}
		}
		else{
			if(abs(order[as[i]] - order[bs[i]]) > cs[i]){
				return false;
			}
		}
	}

	return true;
}

int solve(){
	for(int i=0; i<N; i++){
		order[i] = i;
	}

	int ans = 0;
	do{
		if(check()){
			ans++;
		}
	}while(next_permutation(order, order+N));

	return ans;
}

int main(){
	while(scanf("%d%d", &N, &M), N){
		init();
		printf("%d\n", solve());
	}

	return 0;
}