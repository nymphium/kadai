#include <GL/glut.h>
#include <math.h>

void ploti(int x, int y);
void line(int x1, int y1, int x2, int y2);
void octant(int r);
void display(void);
void myinit(void);
void reshape(int w, int h);
void keyboard(unsigned char key, int x, int y);

int main(int argc, char** argv){
	glutInit(&argc, argv);
	glutInitWindowSize (600, 600);
	glutInitWindowPosition (10, 10);
	glutInitDisplayMode (GLUT_RGB);
	glutCreateWindow (argv[0]);
	myinit();
	glutDisplayFunc(display);
	glutReshapeFunc(reshape);
	glutKeyboardFunc(keyboard);
	glutMainLoop();
	return 0;
}

void ploti(int x, int y){
	glBegin(GL_POINTS); glVertex2i(x,y); glEnd();
}

void line(int x1, int y1, int x2, int y2){
	int dx = x2 - x1, dy = y2 - y1, d2 = 2 * dx;

	ploti(x1, y1);

	int d = 2 * dy, e = dx * (-1), x = x1, y = y1;

	while(x < x2){
		x++;

		e += d;

		if(e > 0){
			y++;

			e -= d2;
		}

		ploti(x, y);
	}
}

void octant(int r){
	int x = 0, y = r, d = 3 - 2 * r;

	while(x < y){
		if(d < 0){
			d += 4 * x + 6;
		}else{
			d += 4 * (x - y--) + 10;
		}

		ploti(++x, y);
	}
}

void display(void){
	double t, m=400., dr=0.017453;  // dr=π/180.0 度からラジアンへ

	glClear(GL_COLOR_BUFFER_BIT);

	int i = 0;

	while(i <= 45){
		t = (double)i*dr;

		line(0, 0, (int)(m*cos(t)), (int)(m*sin(t)));  // 5度間隔の斜めの線

		i += 5;
	}

	i = 20;

	while(i <= 400){
		octant(i); // 円弧
	
		i += 20;
	}

	glFlush();
}

void myinit(void)   {
	glClearColor (1.0, 1.0, 1.0, 0.0); // 背景色
	glColor3f(0.,0.,0.);               // 描画色
}

void reshape(int w, int h){
	h = (h == 0) ? 1 : h;
	glViewport(0, 0, w, h);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(-100.,(float)w-101.,-100.,(float)h-101.,-1.,1. );
	glMatrixMode(GL_MODELVIEW);
}

// ESCキーやEnterキーが入力されたらプログラムが終了
void keyboard(unsigned char key, int x, int y){
	switch (key){
		case 27:  // ESC code
		case 13: case 10: exit(0);  // RETURN code
		default:  break;
	}
}
