#include <GL/glut.h>
#include <math.h>

void swap(int i, int j){
		int tmp = i;

		i = j;

		j = tmp;
}

void  ploti(int x, int y)
	//　(x,y)：　画面の位置(x,y)に点を描く
{ glBegin(GL_POINTS); glVertex2i(x,y); glEnd(); }

void  linei(int x1, int y1, int x2, int y2)
	// 始点(x1, y1), 終点(x2,y2)を結ぶ直線を描く関数
{
	// ここに任意の方向で線を描く拡張Bresenhamのアルゴリズムのプログラムを入れる
	//　プロットするときは、上で定義された関数 ploti(x,y) を使う

	int h = y2 - y1;

	h = h < 0 ? h * (-1) : h;

	int v = x2 - x1;

	v = v < 0 ? v * (-1) : v;

	int steep = h > v;

	if(steep){
		swap(x1, y1);

		swap(x2, y2);
	}

	if(x1 > x2){
		swap(x1, x2);

		swap(y1, y2);
	}

	int dx = x2 - x1, dy = y2 - y1, d2 = 2 * dx;

	ploti(x1, y1);

	int d = 2 * dy, e = dx * (-1), x = x1, y = y1;

	while(x < x2){
		x++;

		e += d;

		if(e > 0){
			y++;
	
			e -= d2;
		}
	
		if(steep) ploti(y, x);

		else ploti(x, y);
	}
}

void  circlei(int x0, int y0, int r)
	// 中心が (x,y) で半径が r の円を描く関数
{
	// ここに完全な円を描く拡張したMichernerのアルゴリズムのプログラムを入れる
	//　プロットするときは、上で定義された関数 ploti(x,y) を使う

	int x = 0, y = r, d = 3 - 2 * r;

	while(x < y){
		if(d < 0){
			d += 4 * x + 6;
		}else{
			d += 4 * (x - y--) + 10;
		}

		x++;

		ploti(x0 + x, y0 + y);
		
		ploti(x0 + y, y0 + x);
		
		ploti(x0 -x, y0 -y);
		
		ploti(x0 -y, y0 -x);
		
		ploti(x0 -y, y0 + x);
		
		ploti(x0 + x, y0 -y);
		
		ploti(x0 - x, y0 + y);
		
		ploti(x0 + y, y0 -x);
	}
}

void  display(void) {
	int i,ix,iy, r=310;
	double t, dr=0.017453;  // dr = π/180.0　度からラディアンへ

	glClear(GL_COLOR_BUFFER_BIT);
	for( i=0; i<16; i++ ){
		t = (float)i*22.5*dr;
		ix = (int)(300*cos(t)) + r;
		iy = (int)(300*sin(t)) + r;
		linei( r,r, ix,iy );
	}
	circlei( r,r,300 );
	glFlush();
}

void  myinit(void) {
	glClearColor (1.0, 1.0, 1.0, 0.0);
	glColor3f(0.,0.,0.);
}

void  reshape(int w, int h) {
	h = (h == 0) ? 1 : h;
	glViewport(0, 0, w, h);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho( 0.,(float)w-1., 0.,(float)h-1., -1., 1. );
	glMatrixMode(GL_MODELVIEW);
}

void keyboard(unsigned char key, int x, int y) {
	switch (key) {
		case 27:  // ESC code
		case 13: case 10: exit(0);  // RETURN (Enter) code
		default:break;
	}
}

int  main(int argc, char** argv) {
	glutInitWindowSize(620, 620);
	glutInitWindowPosition(10, 10);
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_RGB);
	glutCreateWindow (argv[0]);
	myinit();
	glutDisplayFunc(display);
	glutReshapeFunc(reshape);
	glutKeyboardFunc(keyboard);
	glutMainLoop();
	return 0;
}
