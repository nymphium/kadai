/*
   スペースキー：回転停止／開始
   ESCキー 又は Enterキー: プログラム終了
   */
#define BAR    1
#define COLUMN 2
#define FLAG   3
#define PIN    4
#define AR     0.017453  // π/180.0　：  度からラジアンへ

#include <math.h>     // sin(), cos()
#include <GL/glut.h>

float rote=0., size = 10.;
int   move = 1; // 最初は回転
int   time1=10;  // 10 msec.

void display(void)
{
	glClear(GL_COLOR_BUFFER_BIT);
	glPushMatrix();
	rote += 1.; if( rote > 360. ) rote-=360.;
	glScalef( size,size,1. );
	glColor3f(1.,0.7,0.3);
	glCallList( COLUMN );  // 支柱


	glRotatef( rote,0.,0.,1. );
	glColor3f( 0.7,1.,0.1 );
	glCallList( BAR );     // 棒
	glCallList(PIN);

	glTranslatef( 8.,-2.45,0. );
	glColor3f(0.,0.7,1.);
	glCallList( FLAG );  // 旗
	glTranslatef( -16.,4.9,0. );
	glCallList( FLAG );
	glPopMatrix();
	glutSwapBuffers();
}

void myinit(void)
{
	int i;
	glClearColor(1.,1.,1.,1.);     // 背景は白

	glNewList( COLUMN, GL_COMPILE ); // 支柱
	glBegin(GL_TRIANGLE_STRIP);    // 三角形の連なりで台形を作る
	glColor3f(1.,0.7,0.3);
	glVertex2i(-1,1 ); glVertex2i(-3,-15); glVertex2i(1,1 ); glVertex2i(3,-15);
	glEnd( );
	glEndList();

	glNewList( BAR, GL_COMPILE );  // 棒
	glBegin(GL_TRIANGLE_STRIP);  // 三角形の連なりで長方形を作る
	glVertex2f(-10.,0.5); glVertex2f(-10.,-0.5); glVertex2f(10.,0.5); glVertex2f(10.,-0.5);
	glEnd( );
	glEndList();

	glNewList( FLAG, GL_COMPILE ); // 旗
	glBegin(GL_TRIANGLE_STRIP);  // 三角形の連なりで正方形を作る
	glVertex2i(-2,2); glVertex2i(-2,-2); glVertex2i(2,2); glVertex2i(2,-2);
	glEnd( );
	glEndList();

	glNewList( PIN, GL_COMPILE );  // 回転中心軸
	glColor3f( 0.,0.,0. );
	glBegin(GL_TRIANGLE_FAN);
	glVertex2i(0,0);
	for( i=0; i<=360; i+=30 ) glVertex2f(0.4*cos(AR*(double)i), 0.4*sin(AR*(double)i));
	glEnd();
	glEndList();
}

void timer(int value) // time ミリ秒毎に割込み（コールバック）
{
	if( move == 1 )glutPostRedisplay();
	glutTimerFunc(time1, timer, 0);
}

void key_input(unsigned char key, int x, int y)
{
	switch(key){
		case 27: case 10: case 13: exit(0); // ENTERキーで終了
		case ' ': move = 1 - move; break; // スペースキーで「動く／静止」切り替え
		default: break;
	}
	glutPostRedisplay();
}

void reshape(int w, int h)
{
	h = (h == 0) ? 1 : h;
	glViewport(0, 0, w, h);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho( -w/2.,w/2.,-h/2.,h/2., -h/2.,h/2.);
	if( w > h ) size = (float)h/40.;
	else size = (float)w/40.;
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}

int main(int argc, char **argv)
{
	glutInitWindowSize (600, 600);
	glutInit(&argc, argv);
	glutInitDisplayMode( GLUT_RGBA | GLUT_DOUBLE);
	glutCreateWindow(argv[0]);
	myinit();
	glutDisplayFunc(display);
	glutReshapeFunc(reshape);
	glutKeyboardFunc(key_input);
	glutTimerFunc(time1, timer, 0); // (time) msec間隔で割込み.
	glutMainLoop();
	return 0;
}
