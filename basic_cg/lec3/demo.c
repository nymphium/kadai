// 表示される黒棒（楕円体の中心軸）の先端をマウスでドラッグすると、
// その方向に傾くが、根元は固定されている
//　矢印キーで視線方向を変える

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <GL/glut.h>

#define  GROUND 100           // display listのID
#define  AXIS   101

float    vRot=0.,hRot=0.,zzz; // 視点方向の切り替え変数
int      Axis=0;              // 座標系の表示の有無
float    xbase=0., ybase=0., zbase=0.; //	動かす棒の固定端座標
float    dircos[] = {0.,1.,0.}, bar=5.;// 棒の方向余弦と長さ

float vlength(const float *v)      // ベクトルの長さ
{	return (float)sqrt(v[0]*v[0] + v[1]*v[1] + v[2]*v[2]);}

float	inner(float a[], float b[])  // ベクトルの内積
{ return a[0]*b[0]+a[1]*b[1]+a[2]*b[2]; }

void vcross(const float v1[], const float v2[], float cross[])
{  // ベクトルの外積  
  cross[0] = (v1[1] * v2[2]) - (v1[2] * v2[1]);
  cross[1] = (v1[2] * v2[0]) - (v1[0] * v2[2]);
  cross[2] = (v1[0] * v2[1]) - (v1[1] * v2[0]);
}

void unitvec(float v[])  // 単位ベクトル化
{
  float	d;
  if( (d=vlength(v))==0.0 ){
    printf("vector length is 0\n"); exit(0);
  }
  v[0]/=d;  v[1]/=d; v[2]/=d;
}

// 方向余弦d方向がx軸となるような回転変換行列m[]を求める
// y軸方向、z軸方向は勝手に決める
void getRotMatrix( float d[], float m[] )
{
  float y[]={0.,1.,0.},z[]={0.,0.,1.},my[3],mz[3];

  if( fabs(inner(d,y)) < fabs(inner(z,d)) ){
    vcross(d,y,mz);	unitvec(mz); vcross(mz,d,my); unitvec(my);
  } else {
    vcross(z,d,my); unitvec(my); vcross(d,my,mz); unitvec(mz);
  }
  m[0]=d[0]; m[1]=d[1]; m[2]=d[2]; m[3]=0.; m[4]=my[0];
  m[5]=my[1]; m[6]=my[2]; m[7]=0.; m[8]=mz[0]; m[9]=mz[1];
  m[10]=mz[2]; m[11]=0.; m[12]=m[13]=m[14]=0.; m[15]=1.;
}
	
int getCrossPoint( float a, float b, float c, float d,
  float pnt[], float vec[], float *wx, float *wy, float *wz )
// a,b,c,d 交差を調べる平面方程式の係数 ax+by+cz+d=0とする（入力）, 
//   点pnt[]から方向vec[]に向かう直線（入力）,
//　平面と直線の交点(*wx, *wy, *wz)（出力)
//　返戻値＝１：成功、　＝０：失敗
{
  float len, t;
  len = a*vec[0] + b*vec[1] + c*vec[2];
  if( fabs(len)< 0.00001 ) return 0;
  t = -(a*pnt[0]+b*pnt[1]+c*pnt[2]+d)/(a*vec[0]+b*vec[1]+c*vec[2]);
  *wx = pnt[0] + t*vec[0];
  *wy = pnt[1] + t*vec[1];
  *wz = pnt[2] + t*vec[2];
  return 1;
}

// 画面上の(x,y)点に対応するWorld座標系での視点位置viewpnt[3]と
//    視線方向ベクトルviewvec[3]を求める
void getViewLine( int x, int y, float viewpnt[], float viewvec[] )
{
  double	mvmatrix[16], projmatrix[16];	// 逆投影で使う行列
  double realy, pnt[3], vec[3];
  int viewport[4];

  glGetIntegerv(GL_VIEWPORT, viewport);	          // ビューポート変換行列を得る
  glGetDoublev(GL_MODELVIEW_MATRIX, mvmatrix);    // モデル変換行列を得る
  glGetDoublev(GL_PROJECTION_MATRIX, projmatrix);	// 投影変換行列を得る
  realy = (double)(viewport[3] - y -1);           // y軸方向が上下逆を戻す
  gluUnProject((double)x, realy, 0., mvmatrix, projmatrix,
           viewport, pnt, pnt+1, pnt+2);          // 逆変換
  gluUnProject((double)x, realy, 1., mvmatrix, projmatrix,
           viewport, vec, vec+1, vec+2);          // 逆変換
  viewpnt[0] = (float)pnt[0];                     // viewpnt[]: 視線ベクトルの始点
  viewpnt[1] = (float)pnt[1];
  viewpnt[2] = (float)pnt[2];
  viewvec[0] = (float)vec[0] - (float)pnt[0];	    //　視線方向ベクトルにする
  viewvec[1] = (float)vec[1] - (float)pnt[1];	    //　視線方向ベクトルにする
  viewvec[2] = (float)vec[2] - (float)pnt[2];	    //　視線方向ベクトルにする
}

// World座標系の点(x,y,z)を通って、視線方向と直交する平面の方程式
//　　ax + by + cz + d =0 を求める
void getFacingPlane( float u[], float x1, float y1, float z1,
     float *a, float *b, float *c, float *d )
{
  *a = u[0];	*b = u[1];	*c = u[2];
  *d = - ( u[0]*x1 + u[1]*y1 + u[2]*z1 );
}

void motion(int x, int y)
{
  float	a, b, c, d, x1, y1, z1, near2 = 1.;
  float len, viewvec[3], viewpnt[3];
  float	u,v,w, xb,yb,zb;

  getViewLine( x,y,viewpnt,viewvec );	//  視点位置、視線方向を求める
  xb = bar*dircos[0] + xbase;		// 棒の先端の座標値(xb,yb,zb)を求める 
  yb = bar*dircos[1] + ybase;
  zb = bar*dircos[2] + zbase;
// 棒の先端を含む視線方向に垂直な面の方程式の係数を求める
  getFacingPlane( viewvec, xb, yb, zb, &a, &b, &c, &d );
// 求めた平面と視線との交点の座標値(x1,y1,z1)を求める
  getCrossPoint( a,b,c,d, viewpnt, viewvec, &x1, &y1, &z1 );
  u = xb - x1;    v = yb - y1;   w = zb - z1;
// (xb,yb,zb)と(x1,y1,z1)の距離が近ければ
//　　棒の固定端から(x1,y1,z1)の方向余弦dircos[]を求める
//　　そうすれば、その方向に棒が描かれるから
  if( (u*u + v*v + w*w) < near2 ){
    u = x1 - xbase;   v = y1 - ybase;   w = z1 - zbase;
    len = (float)sqrt((double)(u*u + v*v + w*w));
    dircos[0] = u/len;	dircos[1] = v/len;	dircos[2] = w/len;
    glutPostRedisplay();
  }
}

void display(void)
{
  float mx[16];

  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);		// 画面消去
  glLoadIdentity();              // 現在の射影行列の初期化

  glTranslatef( 0.,0.,-zzz );	
  glRotatef( hRot, 1., 0., 0. ); // 緯度方向に回転
  glRotatef( vRot, 0., 1., 0. ); // 経度方向に回転
  if( Axis == 2 ){               // 軸の表示の選択
    glLineWidth(2);
    glDisable(GL_LIGHTING);
    glCallList(AXIS);
    glEnable(GL_LIGHTING);
  }
  glCallList(GROUND);

// 楕円体の表示
  glPushMatrix();               // 現在の座標変換行列を退避
    getRotMatrix( dircos,mx );  // dircos方向がx軸方向となる回転行列mxを求める
    glMultMatrixf( mx );        // 回転行列mxを現在の座標変換行列に掛ける
    glScalef( bar, 1., 1. );
    glTranslatef( 0.5, 0.,0. );
    glutSolidSphere(0.5, 10., 10. );
  glPopMatrix();                // 座標変換行列を元に戻す

// 棒の表示 (楕円体と重なって表示されている）
  glLineWidth(3);
  glDisable(GL_LIGHTING);
  glColor3f(0.,0.,0.);
  glBegin(GL_LINES);            // 棒の固定端から自由端へ線を描く
    glVertex3f( xbase,ybase,zbase );
    glVertex3f( bar*dircos[0]*1.1, bar*dircos[1]*1.1, bar*dircos[2]*1.1 );
  glEnd();
  glEnable(GL_LIGHTING);
  glutSwapBuffers();}

void make_ground()	// 予め地面を作成しておく
{
  int	i;
  glNewList(GROUND, GL_COMPILE);
    glLineWidth(1.);
    glDisable(GL_LIGHTING);
    glColor3f( 0.9, 0.9, 0.9 );
    glBegin(GL_LINES);
    for( i=-500; i<=500; i+=10 ){
      glVertex3f( -500, 0., i );
      glVertex3f( 500, 0., i );
      glVertex3f( i, 0., -500 );
      glVertex3f( i, 0., 500 );
    }
    glEnd();
    glEnable(GL_LIGHTING);
  glEndList();
}

void make_axis()
{
  float a=10.,b=11.,c=2.;       // 軸を描くための変数 
  glNewList (AXIS, GL_COMPILE); // 軸を予め作成する
    glColor3f( 0., 0., 0. );
    glBegin(GL_LINE_STRIP);
      glVertex3f( a, 0., 0. );  // Axes
      glVertex3f( 0., 0., 0. );
      glVertex3f( 0., a, 0. );
    glEnd();
    glBegin(GL_LINES);
      glVertex3f( 0., 0., 0. );
      glVertex3f( 0., 0., a );
      glVertex3f( b, c, c );    // X
      glVertex3f( b, -c, -c );
      glVertex3f( b, -c, c );
      glVertex3f( b, c, -c );
      glVertex3f( -c, b, c );   // Y
      glVertex3f( 0., b, 0. );
      glVertex3f( 0., b, 0. );
      glVertex3f( c, b, c );
      glVertex3f( 0., b, 0. );
      glVertex3f( 0., b, -c );
    glEnd();
    glBegin(GL_LINE_STRIP);
      glVertex3f( -c, c, b );   // Z
      glVertex3f( c, c, b );
      glVertex3f( -c, -c, b );
      glVertex3f( c, -c, b );
    glEnd();
  glEndList ();                 // 軸の作成終わり
}

void myinit(void)
{
  GLfloat ambient[]   = { 0.2, 0.2, 0.2, 1. };
  GLfloat diffuse[]   = { 1., 1., 1., 1. };
  GLfloat specular[]  = { 1., 1., 1., 1. };	
  GLfloat light_pos0[] = { -20.0, 100.0, 0.0, 1.0 };
  GLfloat light_pos1[] = { 20.0, -10.0, 10.0, 1.0 };
  GLfloat mat_diffuseB[]={1.0,0.85,0.7,1.0};  // body color

  glClearColor(1., 1., 1., 1.);  // 背景色 
  glShadeModel(GL_SMOOTH);

  glEnable(GL_DEPTH_TEST);
  glDepthFunc(GL_LESS);
  glMaterialfv(GL_FRONT,GL_DIFFUSE,mat_diffuseB);

  glLightfv(GL_LIGHT0, GL_AMBIENT, ambient);   
  glLightfv(GL_LIGHT0, GL_DIFFUSE, diffuse);     	             
  glLightfv(GL_LIGHT0, GL_SPECULAR, specular); 
  glLightfv(GL_LIGHT0, GL_POSITION, light_pos0);
  glLightfv(GL_LIGHT1, GL_AMBIENT, ambient);   
  glLightfv(GL_LIGHT1, GL_DIFFUSE, diffuse);     	             
  glLightfv(GL_LIGHT1, GL_SPECULAR, specular); 
  glLightfv(GL_LIGHT1, GL_POSITION, light_pos1);

  glEnable(GL_LIGHT0);    // 光源0を有効にする 
  glEnable(GL_LIGHT1);    // 光源1を有効にする
  glEnable(GL_LIGHTING);  // 光源効果を有効にする
  glEnable(GL_NORMALIZE);

  vRot = -55.;            // 視点を経度方向に回すパラメータ
  hRot = 15.;             // 視点を緯度方向に回すパラメータ
  zzz  = 20.;             // 視点から物体までの距離のパラメータ
  make_axis();            // 座標軸を予め作成
  make_ground();          // 床を予め作成
  glMatrixMode(GL_MODELVIEW);
}

void key_interrupt(unsigned char key, int x, int y)
{
  switch (key) {
    case 'n':  zzz -= 0.5; if( zzz<1.) zzz=1;  break;
    case 'f':  zzz += 0.5;  break;
    case 'z':  Axis++; if( Axis == 3 ) Axis=0;  break;
    case 27: case 13: case 10:  exit(0);
    default: break;
  }
  glutPostRedisplay();
}

void arrow_key(int key, int x, int y)
{
//  int SHIFT,CTRL;
//	if( glutGetModifiers() == GLUT_ACTIVE_CTRL ) CTRL = 1;
//	else CTRL = 0;
//	if( glutGetModifiers() == GLUT_ACTIVE_SHIFT ) SHIFT = 1;
//	else SHIFT = 0;
  switch(key) {
    case GLUT_KEY_LEFT:  vRot-=5.;  break;
    case GLUT_KEY_RIGHT: vRot+=5.;  break;
    case GLUT_KEY_UP:    hRot-=5.;  break;
    case GLUT_KEY_DOWN:  hRot+=5.;  break;
    default: break;
  }
  glutPostRedisplay();
}

void  reshape(int w, int h)
{    
  h = (h == 0) ? 1 : h;
  glViewport(0, 0, w, h);
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
//	glOrtho( -100.,100.,-100.*(float)h/(float)w,100.*(float)h/(float)w,-100.,200.);
  gluPerspective(50.0, (GLfloat) w/(GLfloat) h, 10.0, 200.0);
  glMatrixMode(GL_MODELVIEW);
}

int main(int argc, char** argv)
{   
  glutInit(&argc, argv);
  glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);
  glutInitWindowSize(600, 400);     // 初期ウィンドウサイズ
  glutInitWindowPosition(10, 10);   // 初期ウィンドウ位置
  glutCreateWindow(argv[0]);
  glClearColor(1.,1.,1.,0.);        // 初期画面色
  glShadeModel(GL_FLAT); 
  myinit();
  glutDisplayFunc(display);         // 描画関数
  glutReshapeFunc(reshape);         // 再描画関数
  glutMotionFunc(motion);           // マウス移動割り込み関数
  glutKeyboardFunc(key_interrupt);  // キー割り込み関数
  glutSpecialFunc(arrow_key);
  glutMainLoop();
  return 0;
}
