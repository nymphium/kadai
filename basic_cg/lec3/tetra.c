// ４つの三角形から正四面体を折り曲げてアニメーションをつくる例
//  折り曲げながら移動させていることを繰り返し、折り曲げる操作と
//　描くための変換行列が、後に描く三角形に影響を与えている。
//　後に描く図形に変換行列の影響を与えるために、スタックに変換行列を
//　退避させる必要がない

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <GL/glut.h>

#define TRIANGLE  1
#define D    (sqrt(3.)/6.) 
#define D2   (sqrt(3.)/3.) 

float xrot=0., yrot=0.,rotangle=0.; // 全体の回転角と折り曲げ角

void display(void)
{
   glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); 

   /* 視点位置の計算 */
   glLoadIdentity();
   glRotatef( xrot,1.,0.,0. );
   glRotatef( yrot,0.,1.,0. );
   glScalef( 15.,15.,15. );

   glCallList( TRIANGLE );  // 動かない三角形を描く

   glTranslatef( 0.25,D/2.,0. );	// 移動させる
   glRotatef( -60.,0.,0.,1. );
   glRotatef( rotangle,1.,0.,0. ); // 三角形の底辺位置を折り曲げる
   glTranslatef( 0.,D,0. );	// 移動させる
   glCallList( TRIANGLE ); // ２番目の三角形を描く

   glTranslatef( 0.25,D/2.,0. );	// 移動させる
   glRotatef( -60.,0.,0.,1. );
   glRotatef( rotangle,1.,0.,0. ); // 三角形の底辺位置を折り曲げる
   glTranslatef( 0.,D,0. );	// 移動させる
   glCallList( TRIANGLE ); // ３番目の三角形を描く

   glTranslatef( -0.25,D/2.,0. );	// 移動させる
   glRotatef( 60.,0.,0.,1. );
   glRotatef( rotangle,1.,0.,0. ); // 三角形の底辺位置を折り曲げる
   glTranslatef( 0.,D,0. );	// 移動させる
   glCallList( TRIANGLE ); // ４番目の三角形を描く

   glutSwapBuffers();
}

void myinit(void)/* OPENGL 初期化 */
{
   int i;
   float light_pos0[] = { -5., 10.0, 50.0, 0. };
   float ambient[]={0.5,0.5,0.5,1.};
   float color0[] = {0.7, 0.7, 0.7, 1.}; 
   float color1[] = {0., 1., 0., 1.}; 
   float triangle[][3] ={ 0.,D2,0., 0.5,-D,0., -0.5,-D,0.};
   float norm[]={0.,0.,-1.};

   glLightModeli( GL_LIGHT_MODEL_TWO_SIDE,GL_TRUE );// 両面照光
   glLightfv(GL_LIGHT0, GL_POSITION, light_pos0);
   glLightfv(GL_LIGHT0, GL_AMBIENT, ambient);
   glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); 
   glClearColor(1.,1.,1.,0.);
   glEnable(GL_DEPTH_TEST);
   glEnable(GL_NORMALIZE);
   glEnable(GL_LIGHTING);
   glEnable(GL_LIGHT0);
   glMaterialfv( GL_BACK, GL_DIFFUSE, color0 );// 裏面色
   glMaterialfv( GL_FRONT, GL_DIFFUSE, color1 );//　表面色

   glNewList( TRIANGLE, GL_COMPILE ); /* 三角形を描く */
     glBegin(GL_TRIANGLES);
       glNormal3fv( norm );
       for( i=0; i<3; i++ )glVertex3fv(triangle[i]);
     glEnd();
   glEndList();
}

void reshape(int w, int h)
{
  h = (h == 0) ? 1 : h;
   glViewport(0, 0, w, h);
   glMatrixMode(GL_PROJECTION);
   glLoadIdentity();
   gluPerspective(60.0, (GLfloat) w/(GLfloat) h, 10.0, 100.0);
   glTranslatef( 0.,0.,-50. );
   glMatrixMode(GL_MODELVIEW);
}

void special_input(int key, int x, int y)
{
   switch(key){
     case GLUT_KEY_LEFT:
       yrot -= 2.; if( yrot<0. ) y += 360.; break;
     case GLUT_KEY_RIGHT:
       yrot += 2.; if( yrot>360. ) y-= 360.; break;
     case GLUT_KEY_UP:
       xrot -= 2.; if( xrot <0. ) x += 360.; break;
     case GLUT_KEY_DOWN:
       xrot += 2.; if( xrot > 360. ) x-=360.; break;
   }
   glutPostRedisplay();
}

void key_input(unsigned char key, int x, int y)
{
   switch(key){
     case 'f': rotangle +=1.;// 折り曲げる
       if( rotangle>109.4714 ) rotangle=109.4714;
       glutPostRedisplay(); break;
     case 'u': rotangle -=1.;// 展開する
       if( rotangle<0. ) rotangle=0.;
       glutPostRedisplay(); break;
     case 27:  // ESC
     case 13:  // CR 
     case 10:   exit(0); // LF
     default:   return;
   }
}

int main(int argc, char **argv)
{
   glutInit(&argc, argv);
   glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH);
   glutInitWindowSize (800, 800); 
   glutCreateWindow(argv[0]);
   myinit();
   glutDisplayFunc(display);
   glutReshapeFunc(reshape);
   glutMouseFunc(NULL);
   glutSpecialFunc(special_input);
   glutKeyboardFunc(key_input);
   glutPostRedisplay();  
   glutMainLoop();
   return 0;
}
