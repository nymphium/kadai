#include <math.h>   
#include <stdlib.h> 
#include <stdio.h> 

// 3次元ベクトル
class Vector3d {
public:
	double x, y, z;
	Vector3d() { x = y = z = 0; }
	Vector3d(double _x, double _y, double _z) { x = _x; y = _y; z = _z; }
	void set(double _x, double _y, double _z) { x = _x; y = _y; z = _z; }

	// 長さを1に正規化する
	void normalize() {
		double len = length();
		x /= len; y /= len; z /= len;
	}

	// 長さを返す
	double length() { return sqrt(x * x + y * y + z * z); }

	// s倍する
	void scale( const double s) { x *= s; y *= s; z *= s;}

	// 代入演算の定義
	Vector3d& operator =  ( const Vector3d& v ){ x = v.x; y = v.y; z = v.z; return *this; }

	// 加算代入の定義
	Vector3d& operator+=( const Vector3d& v ) { x += v.x; y += v.y; z += v.z; return( *this ); }

	// 減算代入の定義
	Vector3d& operator-=( const Vector3d& v ) { x -= v.x; y -= v.y; z -= v.z; return( *this ); }

	void debugout() { printf("Vector3d(%f %f %f)\n", x, y, z );}
};

Vector3d operator+( const Vector3d& v1, const Vector3d& v2 ) { return( Vector3d( v1.x+v2.x, v1.y+v2.y, v1.z+v2.z ) );}
Vector3d operator-( const Vector3d& v1, const Vector3d& v2 ) { return( Vector3d( v1.x-v2.x, v1.y-v2.y, v1.z-v2.z ) );}
Vector3d operator-( const Vector3d& v ) { return( Vector3d( -v.x, -v.y, -v.z ) ); }
Vector3d operator*( const double& k, const Vector3d& v ) { return( Vector3d( k*v.x, k*v.y, k*v.z ) );}
Vector3d operator*( const Vector3d& v, const double& k ) { return( Vector3d( v.x*k, v.y*k, v.z*k ) );}
Vector3d operator/( const Vector3d& v, const double& k ) { return( Vector3d( v.x/k, v.y/k, v.z/k ) );}

//----  内積の定義
double operator*( const Vector3d& v1, const Vector3d& v2 ) { return( v1.x*v2.x + v1.y*v2.y + v1.z*v2.z );}
//----  外積の定義
Vector3d  operator%( const Vector3d& v1, const Vector3d& v2 ) { return( Vector3d( v1.y*v2.z - v1.z*v2.y,  v1.z*v2.x - v1.x*v2.z,  v1.x*v2.y - v1.y*v2.x ) );}


int main(int argc, char *argv[]) {
	// 9 - (a)
	Vector3d a(2, 3, 0);

	Vector3d b(5, 2, 7);

	Vector3d c = a + b;

	c.debugout();

	c = 2 * a;

	c.debugout();

	c -= a;

	c.debugout(); // -2, -3, 0

	c = 2 * a + 3 * b;

	c.debugout(); // 19, 12, 21

	c = 3 * a - 6 * b;

	c.debugout(); // -24, -3, -42

	a.normalize();

	a.debugout(); // 0.554700, 0.832050, 0.000000

	return 0;
}
