#include <GL/glut.h>
#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <algorithm>
#include <vector>

// 3次元ベクトル
class Vector3d {
	public:
		double x, y, z;
		Vector3d() { x = y = z = 0; }
		Vector3d(double _x, double _y, double _z) { x = _x; y = _y; z = _z; }
		void set(double _x, double _y, double _z) { x = _x; y = _y; z = _z; }

		// 加算代入の定義
		Vector3d& operator+=( const Vector3d& v ) { x += v.x; y += v.y; z += v.z; return( *this ); }

		// 減算代入の定義
		Vector3d& operator-=( const Vector3d& v ) { x -= v.x; y -= v.y; z -= v.z; return( *this ); }

		void debugout() { printf("Vec3(%f %f %f)\n", x, y, z );}
};

// ボールの状態を表すクラス
class Ball {
	public:
		Vector3d a; // 加速度ベクトル
		Vector3d v; // 速度ベクトル
		Vector3d p; // 位置
		double r;   // 半径

		Ball() {  // 初期状態の設定
			r = 1;
			a.set(0, -0.01, 0);     // 初期化速度の指定
			v.set(0.1, 0, 0);     // 初速度の指定
			p.set(0, 10, 0);    // 初期位置の指定
		}
};

GLfloat ball_color[] = { 0.8, 0.2, 0.2, 1.0 };
GLfloat floor_color[] = { 0.2, 0.2, 1.0, 1.0 };

double rotateAngleH_deg;
double rotateAngleV_deg;
int preMousePositionX;
int preMousePositionY;

Ball ball;
bool bRunning;
bool bBack;

double G = -0.8; // gravitational acceleration

#define FLOOR_SIZE 23 // 床の縦横の寸法をここで指定している

void drawBall(void) {
	glPushMatrix();
	glTranslated(ball.p.x, ball.p.y, ball.p.z);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, ball_color);
	::glutSolidSphere(ball.r, 32, 32);
	glPopMatrix();
}

void drawFloor(void) {
	glPushMatrix();
	glTranslated(0, 0, 0);

	glScaled(FLOOR_SIZE, 0.2, FLOOR_SIZE);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, floor_color);
	::glutSolidCube(1.0f);
	glPopMatrix();
}

void display(void) {
	// 画面クリア
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// モデルビュー変換行列の初期化
	glLoadIdentity();

	// 視点の移動
	glTranslated(0, 0.0, -50);
	glRotated(rotateAngleV_deg, 1.0, 0.0, 0.0);
	glRotated(rotateAngleH_deg, 0.0, 1.0, 0.0);

	// 光源の位置を設定
	static GLfloat lightpos[] = { 3.0, 4.0, 5.0, 0.0 };
	glLightfv(GL_LIGHT0, GL_POSITION, lightpos);

	// シーンの描画
	drawFloor();
	drawBall();

	// 画面の更新
	glFlush();
}

void bound(Ball& b){
	if(std::abs((int)b.p.x) <= FLOOR_SIZE / 2. && b.p.y < b.r){
		b.p.y = b.r;

		b.v.y = bBack ? b.v.y / G : b.v.y * G;
	}
}

void resize(int w, int h) {
	glViewport(0, 0, w, h);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(30.0, (double)w / (double)h, 1.0, 100.0);
	glMatrixMode(GL_MODELVIEW);
}

void keyboard(unsigned char key, int x, int y) {
	switch(key){
		case '\033':

		case 'q': exit(0);

		case 'a': bRunning = !bRunning;

		case 't': bBack = ! bBack;

		case 'h': ball.p.x -= 0.4; break;

		case 'j': ball.p.y -= 0.4; break;

		case 'k': ball.p.y += 0.4; break;

		case 'l': ball.p.x += 0.4; break;

		default: break;
	}
}

void mouse(int button, int state, int x, int y) {
	switch (button) {
		case GLUT_LEFT_BUTTON:
			preMousePositionX = x;
			preMousePositionY = y;
			break;
		case GLUT_MIDDLE_BUTTON:
			break;
		case GLUT_RIGHT_BUTTON:
			break;
		default:
			break;
	}
}


// =======================================================
//  一定時間ごとに呼ばれる。
//  ここで、対象物の物理量（力、加速度、速度、位置）を更新する
// =======================================================
void timer(int value) {

	if(bRunning) { // シミュレーション実行中の処理
		/* 加速度(ball.a)を設定する処理を入れる */

		/* 加速度に基づいて、速度(ball.v)を更新する処理を入れる(単純に現在の速度に加速度を加算すればよい)*/

		if(bBack){
			ball.v -= ball.a;

			ball.p -= ball.v;
		}else{
			ball.v += ball.a;

			ball.p += ball.v;
		}

		bound(ball);
	}

	glutPostRedisplay(); // 描画内容の更新
	glutTimerFunc(10 , timer , 0); // 一定時間経過後に、この関数を再実行する
}

void motion(int x, int y) {
	int diffX = x - preMousePositionX;
	int diffY = y - preMousePositionY;

	rotateAngleH_deg += diffX * 0.1;
	rotateAngleV_deg += diffY * 0.1;

	preMousePositionX = x;
	preMousePositionY = y;

	glutPostRedisplay();
}


void init(void) {
	glClearColor(1.0, 1.0, 1.0, 0.0);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_CULL_FACE);
	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);
}

int main(int argc, char *argv[]) {
	glutInit(&argc, argv);
	glutInitWindowSize(600,600);
	glutInitDisplayMode(GLUT_RGBA | GLUT_DEPTH);
	glutCreateWindow(argv[0]);
	glutDisplayFunc(display);
	glutReshapeFunc(resize);
	glutKeyboardFunc(keyboard);
	glutMouseFunc(mouse);
	glutMotionFunc(motion);
	glutTimerFunc(0 , timer , 0);

	bRunning = true;
	bBack = false;
	init();
	glutMainLoop();
	return 0;
}
