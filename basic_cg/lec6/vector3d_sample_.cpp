#include <math.h>
#include <stdlib.h>
#include <stdio.h>

// 3次元ベクトル
class Vector3d {
public:
	double x, y, z;

	Vector3d() {
		x = y = z = 0;
	}

	Vector3d(double _x, double _y, double _z) {
		x = _x;
		y = _y;
		z = _z;
	}

	// 長さを1に正規化する
	void normalize() {
		double len = length();
		x /= len;
		y /= len;
		z /= len;
	}

	// 長さを返す
	double length() {
		return sqrt(x * x + y * y + z * z);
	}
};


// Vector3d クラスの使用例
/*
実行結果
vecA = (0.000000, 0.000000, 0.000000)
vecA = (1.000000, 2.000000, 3.000000)
length of vecA = 3.741657
length of vecA = 1.000000
vecA = (0.267261, 0.534522, 0.801784)
vecB = (3.000000, 2.000000, 1.000000)
*/

int main(int argc, char** argv) {
	// (0, 0, 0) のベクトルを生成する
	Vector3d vecA;

	// ベクトルのx,y,zの値を確認する
	printf("vecA = (%f, %f, %f)\n", vecA.x, vecA.y, vecA.z);

	// ベクトルのx,y,zの値を変更する
	vecA.x = 1.0; vecA.y = 2.0; vecA.z = 3.0;

	// ベクトルのx,y,zの値を確認する
	printf("vecA = (%f, %f, %f)\n", vecA.x, vecA.y, vecA.z);

	// ベクトルの長さを取得し、その結果を確認する。
	double len = vecA.length();
	printf("length of vecA = %f\n", len);

	// ベクトルを正規化する（長さを1にする）
	vecA.normalize();

	// ベクトルの長さを取得し、その結果を確認する。
	double len1 = vecA.length();
	printf("length of vecA = %f\n", len1);

	// ベクトルのx,y,zの値を確認する
	printf("vecA = (%f, %f, %f)\n", vecA.x, vecA.y, vecA.z);


	// 値を指定してベクトルを生成する
	Vector3d vecB(3.0, 2.0, 1.0);

	// ベクトルのx,y,zの値を確認する
	printf("vecB = (%f, %f, %f)\n", vecB.x, vecB.y, vecB.z);


	return 0;
}

