socket = require'socket'
import dec from require'decode'

port = 8000
server = assert socket.bind("*", port)

ip, p = server\getsockname!

print "build on ip: #{ip}:#{p}"

while true do with client = server\accept!
	\settimeout -1
	line, err = \receive!
	print "info: #{line}"
	print "decode: #{dec line}"

	unless err then \send "ok\n"

	\close!

