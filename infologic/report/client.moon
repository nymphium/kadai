socket = require"socket"
import enc from require "encode"

addr, port = "127.0.0.1", 8000

while true do with _ = io.write "> "
	ok, msg = pcall io.read

	unless ok and msg then break

	if msg\match"[^a-z,?.]"
		io.stderr\write "invalid: message must be made of [a-z,?.]\n"
		continue

	with client = assert socket.connect(addr, port)
		\send ((enc msg) .. "\n")
		res = \receive!
		print "response: #{res}"
		\close!

