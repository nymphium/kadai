binarize = (c) -> switch c
	when "?" then "11011"
	when "," then "11100"
	when "." then "11101"
	else with ret = ""
		cn = (string.byte c) - 96
		for i = 4, 0, -1
			cn_ = cn - 2 ^ i

			if cn_ >= 0
				ret ..= "1"
				cn = cn_
			else
				ret ..= "0"

enc = (str) -> with ret = "" do for c in str\gmatch "." do
	for c_ in (binarize c)\gmatch "." do ret ..= (c_\rep 3)

{:enc}

