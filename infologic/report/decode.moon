revise = (code) -> with ret = "" do for s3 in code\gmatch "..." do switch s3
	when "001", "010", "100" then ret ..= "0"
	when "110", "101", "011" then ret ..= "1"
	else ret ..= s3\match"(.).."

dec = (code) -> with ret = ""
	code, i, byte = (revise code), 4, 0

	for c in code\gmatch"."
		if c == "1" then byte += 2 ^ i

		i -= 1

		if i < 0
			byte = switch byte
				when 27 then byte = 63 -- ?
				when 28 then byte = 44 -- ,
				when 29 then byte = 46 -- .
				else byte += 96 -- [a-z]

			ret ..= string.char byte

			byte = 0
			i = 4

{:dec}

