class Info
	new: (info) =>
		@db = with db = {(string.char i + 96), 0  for i = 1, 26}
			db["?"] = 0
			db[","] = 0
			db["."] = 0

		for c in info\gmatch "."
			@db[c] += 1 if @db[c]

		for c, v in pairs @db
			@db[c] = v / #info

	ctree: (pt) ->
		pt_cut = {k, v for k, v in pairs pt when v > 0}

{:Info}

