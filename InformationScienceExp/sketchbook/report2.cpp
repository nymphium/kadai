int ledRed[10];

int wait = 10;

void setup()
{
  for (int i = 1; i < 11; i+=1) {
  ledRed[i] = i + 2;
  }
  for (int i = 1; i < 11; i+=1) {
  pinMode(ledRed[i], OUTPUT);
  }
}

void loop()
{    
    //1,5
    digitalWrite(ledRed[1], LOW);
    digitalWrite(ledRed[2], HIGH);
    digitalWrite(ledRed[3], HIGH);  
    digitalWrite(ledRed[4], HIGH);
    digitalWrite(ledRed[5], HIGH);
    digitalWrite(ledRed[6], HIGH);
    digitalWrite(ledRed[7], HIGH);
    digitalWrite(ledRed[8], HIGH);
    delay(wait * 2);
    //2,3,4
    digitalWrite(ledRed[1], HIGH);
    digitalWrite(ledRed[2], LOW);
    digitalWrite(ledRed[3], LOW);  
    digitalWrite(ledRed[4], LOW);
    digitalWrite(ledRed[5], HIGH);
    digitalWrite(ledRed[6], LOW);
    digitalWrite(ledRed[7], LOW);
    digitalWrite(ledRed[8], LOW);
    delay(wait * 3);    
}
