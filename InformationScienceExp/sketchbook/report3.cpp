#include <Matrix.h>
#include <Sprite.h>
int sensVal;
int Next = 100;
int sensorPin = 0;

Matrix mtx = Matrix(10, 12, 11);

Sprite breeze = Sprite(
8, 7,
B00110000,
B01001001,
B00000110,
B01100000,
B10010010,
B00001100,
B10000001);

Sprite zero = Sprite(
8, 7,
B01111110,
B01000010,
B01000010,
B01000010,
B01000010,
B01000010,
B01111110);

Sprite one = Sprite(
8, 7,
B00110000,
B00010000,
B00010000,
B00010000,
B00010000,
B00010000,
B11111111);

Sprite two = Sprite(
8, 7,
B00111100,
B01000010,
B01000100,
B00001000,
B00010000,
B00100000,
B11111111);

Sprite three = Sprite(
8, 7,
B11111111,
B00000001,
B00000001,
B11111111,
B00000001,
B00000001,
B11111111);

void setup() {
  mtx.clear();
  Serial.begin(9600);
}

int x = 0;
void loop() {
  mtx.write(0 - x, 0, breeze);
  if(sensVal < Next) {
    mtx.write(8 - x, 0, zero);
  }
  else if(sensVal < (Next * 2)) {
    mtx.write(8 - x, 0, one);
  }
  else if(sensVal < (Next * 3)) {
    mtx.write(8 - x, 0, two);
  }
  else if(sensVal < (Next * 4)) {
    mtx.write(8 - x, 0, three);
  }
  mtx.write(16 - x, 0, breeze);
  delay(100);
  mtx.clear();
  if (x == 16){
    x = 0;
    sensVal = analogRead(sensorPin);
  }
  x++;
}


