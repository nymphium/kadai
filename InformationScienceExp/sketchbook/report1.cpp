int ledRed1 = 9;
int ledRed2 = 10;
int ledRed3 = 11;
int ledRed4 = 12;
int Red1_state = LOW;
int Red2_state = LOW;
int Red3_state = LOW;
int Red4_state = LOW;
int next = 150;
int sensVal;
int sensorPin = 0;


void setup()
{
	pinMode(ledRed1, OUTPUT);
	pinMode(ledRed2, OUTPUT);
	pinMode(ledRed3, OUTPUT);
	pinMode(ledRed4, OUTPUT);

	Serial.begin(9600);
}

void loop()
{
	sensVal = analogRead(sensorPin);

	if (sensVal > 0) {
		digitalWrite(ledRed1, HIGH);
	}
	else {
		digitalWrite(ledRed1, LOW);
	}
	if (sensVal > next) {
		digitalWrite(ledRed2, HIGH);
	}
	else {
		digitalWrite(ledRed2, LOW);
	}
	if (sensVal > next * 2) {
		digitalWrite(ledRed3, HIGH);
	}
	else {
		digitalWrite(ledRed3, LOW);
	}
	if (sensVal > next * 3) {
		digitalWrite(ledRed4, HIGH);
	}
	else {
		digitalWrite(ledRed4, LOW);
	}

	Serial.println(sensVal);
	delay(250);
}

