performance
===
9-1/2-8ルール
initialize

- 全体的に
  + メソッド呼び出し
  + GC
- 局所的
  + JIT compiling
- プロファイリング
  + sampling profiler
    一定時間ごとにやってる処理を取る

## JIT Compiler
<-> Ahead of time

よく呼ばれる処理を検出し､より効率的なコードに変換する

メソッド単位でのJITをMethod/Base-line JIT <-> Tracing JIT

- Tracing JIT
  インライン化みたいな感じ
  効果は大きい､検出が難しい(適用が困難)

### なぜJITがCRubyに入らない?
コンパイル､初期化のコストが高速化の割に合わない

Chromeのv8はすごい
- 最適化､脱最適化を必要に応じて交互におこなう

on-stack replacement
