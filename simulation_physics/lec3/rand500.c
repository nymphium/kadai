#include <stdio.h>
#include <stdlib.h>
#include <math.h>


int main() {
	srand(127);

	int count = 0;

	while(1) {
		double x = 5. - rand() / 1. / ((double)RAND_MAX + 2.) * 10.;
		double y = 5. - rand() / 1. / ((double)RAND_MAX + 2.) * 10.;

		if(pow(x, 2.) + pow(y, 2.) >= 25.) continue;

		printf("%.2f %.2f\n", x, y);

		if(++count > 499) break;
	}
}

