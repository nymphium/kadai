#include <stdio.h>
#include <stdlib.h>

#define H 3.5
#define ORG 50.
#define DT 0.1
#define NK 20
#define LEN 500

int main(int argc, char **argv) {
	FILE *f;
	f = fopen(argv[1], "r");

	double g[LEN][2];
	double g0[LEN][2];
	double v[LEN][2];
	double x, y;
	int ret;
	int count = 0;

	while((ret = fscanf(f, "%lf %lf", &x, &y)) != EOF) {
		g[count][0] = x;
		g[count][1] = y;
		g0[count][0] = x;
		g0[count][1] = y;
		v[count][0] = H * g[count][0];
		v[count][1] = H * g[count][1];

		count++;
	}

	int j; for(j = 0; j < count; j++) {
		g[j][0] = g[j][0] + ORG;
		g[j][1] = g[j][1] + ORG;
	}

	int k; for(k = 0; k < NK; k++) {
		int l; for(l = 0; l < LEN; l++) {
			g[l][0] = g[l][0] + v[l][0] * DT;
			g[l][1] = g[l][1] + v[l][1] * DT;
		}
	}

	int m; for(m = 0; m < count; m++) printf("%.2lf %.2lf\n", g[m][0], g[m][1]);
}

