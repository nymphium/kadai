#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define G 1
#define DX 1
#define DY DX


int main() {
	float phi[4][4];
	float ro[4][4];
	float phi_averaged[4][4];

	for(int i = 0; i < 4; i++) for(int j = 0; j < 4; j++) {
		phi[i][j] = 0.;
		ro[i][j] = 6 * i - 3 * j;
	}

	phi[1][3] = 22.5;
	phi[2][3] = 36.;
	phi[3][1] = 4.5;
	phi[3][2] = 9.;

	memcpy(phi_averaged, phi,  sizeof(int) * 4 * 4);

	for(int k = 0; k < 10000; k++) {
		for(int l = 1; l < 3; l++) for(int m = 1; m < 3; m++) {
			float p1 = phi[(l + k - 1) % 4][(m + k) % 4] + phi[(l + k + 1) % 4][(m + k) % 4] + phi[l][(m + k - 1) % 4] + phi[l][((m + k)+ 1) % 4];
			float p2 = G * ro[l][m] * DX * DY;
			phi[l][m] = p1 / 4. - p2 / 4.;
		}
	}

	for(int p = 1; p < 3; p++) for(int q = 1; q < 3; q++) printf("(%d, %d): %f\n", p, q, phi[p][q]);
}

