#include <stdio.h>
#include <stdlib.h>

#define H 3.5
#define ORG 50.
#define DT 0.1
#define NK 20
#define LEN 500
#define NM 99
#define NI 50
#define G 1
#define M 1
#define DX 1
#define DY DX


int main(int argc, char **argv) {
	FILE *f; f = fopen(argv[1], "r");
	double g[LEN][2];
	double v[LEN][2];
	double x, y;
	int ret;
	int count = 0;
	int nk = atoi(argv[1]);

	while((ret = fscanf(f, "%lf %lf", &x, &y)) != EOF) {
		g[count][0] = x;
		g[count][1] = y;
		v[count][0] = H * g[count][0];
		v[count][1] = H * g[count][1];

		count++;
	}

	fclose(f);

	for(int j = 0; j < count; j++) {
		g[j][0] = g[j][0] + ORG;
		g[j][1] = g[j][1] + ORG;
	}

	// gauss_sidel

	double phi[LEN][LEN]; for(int k = 0; k < LEN; k++) for(int l = 0; l < LEN; l++) phi[k][l] = 0;

	for(int m = 0; m < nk; m++) {
		int ro[LEN][LEN]; for(int m2 = 0; m2 < LEN; m2++) for(int m3 = 0; m3 < LEN; m3++) ro[m2][m3] = 6 * m2 - 3 * m3;

		for(int n = 0; n < 10000; n++) {
			for(int o = 0; o < LEN; o++) for(int p = 0; p < LEN; p++) {
				double p1 = phi[(n + o - 1) % LEN][(n + p) % LEN] + phi[(n + o + 1) % LEN][(n + p) % LEN] + phi[o][(n + p - 1) % LEN] + phi[o][(n + p + 1) % LEN];
				double p2 = G * ro[o][p] * DX * DY;
				phi[o][p] = p1 / 4. - p2 / 4.;
			}
		}

		double fx[LEN][LEN]; for(int q = 0; q < LEN; q++) for(int r = 0; r < LEN; r++) fx[q][r] = (phi[(q + 1) % LEN][r] - phi[q][r]) / DX;

		for(int s = 0; s < LEN; s++) for(int t = 0; t < LEN; t++){
			double fp = M * fx[s][t];
			v[s][0] = v[s][0] + (fp / M) * DT;
			v[s][1] = v[s][1] + (fp / M) * DT;

			g[s][0] = g[s][0] + v[s][0] * DT;
			g[s][1] = g[s][1] + v[s][1] * DT;
		}
	}


	for(int m = 0; m < count; m++) printf("%.2lf %.2lf\n", g[m][0], g[m][1]);
}

