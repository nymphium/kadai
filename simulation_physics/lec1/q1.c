#include <stdio.h>
#include <stdlib.h>
#include <math.h>


int main(){
	FILE *fo;
	char *fname;
	fname = "data.csv";
	fo = fopen(fname, "w");
	srand(127);

	int count = 0;

	while(1){
		double x = rand() / 1. / ((double)RAND_MAX + 2.) * 2. - 1.;
		double y = rand() / 1. / ((double)RAND_MAX + 2.) * 2. - 1;

		if(pow(x, 2) + pow(y, 2) >= 1.) continue;

		fprintf(fo, "%f,%f\n", x, y);

		if(++count > 4999) break;
	}

	fclose(fo);
}

