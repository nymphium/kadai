#include <stdio.h>
#include <stdlib.h>
#include <math.h>


int main(int argc, char* argv[]){
	FILE *fo;
	fo = fopen(argv[1], "r");
	float x = 0.;
	float y = 0.;
	int count = 0;
	double ave = 0;

	while(fscanf(fo, "%f,%f", &x, &y) != EOF){
		if(count < 11){
			ave = (ave + x * y) / ++count * 1.;
		}
	}

	printf("%f %f\n", ave, (3.14 - ave) / 3.14);

	fclose(fo);
}

