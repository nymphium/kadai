puts "\\begin{table}[htb]
		\\begin{center}
			\\begin{tabular}{|l|c|c|c|} \\hline
				乱数の組(点)の数 & 100 & 1000 & 10000 \\\\ \\hline
				面積の平均値(10試行) & #{ARGV[0]} & #{ARGV[2]} & #{ARGV[4]} \\\\ \\hline
				($\\pi - $面積の平均値)$\\div \\pi$ & #{ARGV[1]} & #{ARGV[3]} & #{ARGV[5]} \\\\ \\hline
			\\end{tabular}
		\\end{center}
	\\end{table}
"
