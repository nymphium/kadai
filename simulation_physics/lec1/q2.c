#include <stdio.h>
#include <stdlib.h>
#include <time.h>


int main(int argc, char *argv[]){
	FILE *fo;
	fo = fopen(argv[1], "w");
	srand((unsigned)time(NULL));

	int i; for(i = 0; i < atoi(argv[2]); i++){
		double x = rand() / 1. / ((double)RAND_MAX + 2.) * 2. - 1.;
		double y = rand() / 1. / ((double)RAND_MAX + 2.) * 2. - 1.;

		fprintf(fo, "%f,%f\n", x, y);
	}

	fclose(fo);
}

