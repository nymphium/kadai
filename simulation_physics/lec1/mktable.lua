function hitmiss(x) return x * x end -- function to divide hit/miss randomnum pair

count, size = 0, 0

for l in io.lines(arg[1]) do
	local x = l:gsub("^(.-),.*$", "%1") x = math.abs(tonumber(x))
	local y = l:gsub("^.-,(.*)$", "%1") y = math.abs(tonumber(y))

	if count < 11 and y < hitmiss(x) then -- get (x * y) size average
		count = count + 1
		size = size + x * y
	end
end

size = size / count

io.write(string.format("%f %f ", size, (math.pi - size) / math.pi))

