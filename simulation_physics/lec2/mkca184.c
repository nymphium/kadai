#include <stdio.h>
#include <string.h>


// Param. *ca: parent cell
// Param. len: length(ca)
// Param. num: step number
void mkca184(int *ca, int len, int step) {
	int h; for(h = 0; h < len; h++) printf("%d ", ca[h]);

	puts("");

	int ca_next[len];

	int i; for(i = 0; i < step; i++) {
		int j; for(j = 0; j < len; j++) {
			int p = ca[(len + j - 1) % (len )];
			int q = ca[j];
			int r = ca[(len + j + 1) % len];

			int cell = (p + p * q + q * r) % 2;
			printf("%d ", (ca_next[j] = cell));
		}

		int k; for(k = 0; k < len; k++) ca[k] = ca_next[k];

		puts("");
	}
}

