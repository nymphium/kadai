#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>


int main() {
	srand((unsigned)time(NULL));

	int ran = rand() % 6 + 1;
	int len = 20;
	int ca[len];

	int i; for(i = 0; i < len; i++) ca[i] = 0;

	for(i = 0; i < ran; i++) ca[i] = 1;

	shuffle(ca, len);

	mkca184(ca, len, len);
}

