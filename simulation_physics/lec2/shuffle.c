#include <stdlib.h>
#include <time.h>


// Param. *array: array to shuffle
// Param. len: length(array)
void shuffle(int *array, int len) {
	srand((unsigned)time(NULL));

	int i; for(i = 0; i < len; i++) {
		int j = rand() % len;
		int t = array[i];
		array[i] = array[j];
		array[j] = t;
	}
}

