#include <stdio.h>
#include <stdlib.h>


// Param. density: first density
// Param. len: cell length
// Param. step: CA step number
void space_percaption_map(double density, int len, int step) {
	int ca[len];

	int i; for(i = 0; i < len; i++) ca[i] = 0;

	for(i = 0; i < (int)(len * density); i++) ca[i] = 1;

	shuffle(ca, len);


	mkca184(ca, len, step);
}

int main(int argc, char **argv) {
	double density = atof(argv[1]);

	space_percaption_map(density, 20, 20);
}

