#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define G 1
#define DX 1
#define DY DX


int main() {
	float phi[4][4];
	float ro[4][4];
	float phi_averaged[4][4];

	int i, j; for(i = 0; i < 4; i++) for(j = 0; j < 4; j++) {
		phi[i][j] = 0.;
		ro[i][j] = 6 * i - 3 * j;
	}

	phi[1][3] = 22.5;
	phi[2][3] = 36.;
	phi[3][1] = 4.5;
	phi[3][2] = 9.;

	for(i = 0; i < 4; i++) for(j = 0; j < 4; j++) phi_averaged[i][j] = phi[i][j];

	int k; for(k = 0; k < 4; k++) {
		int l; int m; for(l = 1; l < 3; l++) for(m = 1; m < 3; m++) {
			float p1 = phi[(l + k - 1) % 4][(m + k) % 4] + phi[(l + k + 1) % 4][(m + k) % 4] + phi[l][(m + k - 1) % 4] + phi[l][((m + k)+ 1) % 4];
			float p2 = G * ro[l][m] * DX * DY;
			phi_averaged[l][m] += (p1 / 4. - p2 / 4.);
			phi[l][m] = p1 / 4. - p2 / 4.;
		}

		phi[1][1] = 0.;
		phi[1][2] = 0.;
		phi[2][1] = 0.;
		phi[2][2] = 0.;
	}

	int n; int o; for(n = 1; n < 3; n++) for(o = 1; o < 3; o++) phi_averaged[n][o] /= 4.;

	int p; int q; for(p = 1; p < 3; p++) for(q = 1; q < 3; q++) printf("(%d, %d): %f\n", p, q, phi_averaged[p][q]);
}

