#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>


struct record {
	char word[20];

	int count;

	struct record *next;
};


int read_word(FILE *fp, char *word);

void add_word(char *word);

char upper(char c);

int comp_dic(char *str1, const char *str2);

struct record *head = NULL;

int main(int argc, char *argv[]){
	FILE *fp;

	char word[20];

	struct record *p;

	if(argc != 2){
		printf("missing file argument\n");

		return 1;
	}

	fp = fopen(argv[1], "r");

	if(fp == NULL){
		printf("can't open %s\n", argv[1]);

		return 1;
	}

	while(read_word(fp, word)){
		add_word(word);
	}

	fclose(fp);

	for(p = head; p != NULL; p = p->next) printf("%s %d\n", p->word, p->count);

	return 0;
}


int read_word(FILE *fp, char *word){
	int cur = ftell(fp);

	char w[20];

	char v[20];

	fscanf(fp, "%s", w);

	fscanf(fp, "%s", v);

	fseek(fp, strlen(v) * -1, SEEK_CUR);

	if(ftell(fp) == cur) return 0;

	strcpy(word, w);

	return 1;
}

void add_word(char *word){
	struct record *p, *q, *t;

	t = (struct record *) malloc(sizeof(struct record));

	if(t == NULL){
		puts("out of memory");

		exit(1);
	}

	strcpy(t->word, word);

	t->count = 1;

	q = NULL;

	int cmp;

	int i = 0;

	for(p = head; p != NULL; p = p->next){
		/* if((cmp = comp_dic(p->word, word)) >= 0) break; */
		cmp = strcmp(p->word, word);

		printf("%d compare %s, %s -> %d\n", i++, p->word, word, cmp);
		/* if((cmp = strcmp(p->word, word)) >= 0) break; */
		if(cmp >= 0) break;

		q = p;
	}

	if(q != NULL){
		if(cmp == 0){
			q->count++;
		}else if(cmp > 0){
			q->next = t;

			t->next = p;
		}
	}else{
		puts("hoge");

		head = t;

		t->next = p;
	}
}

char upper(char c){
	return (c >= 'A' && c <= 'Z') ? (c + 'a' - 'A') : c;
}

int comp_dic(char *str1, const char *str2){
	while(upper(*str1) == upper(*str2)){
		if(*str1 == '\0'){
			return 0;
		}

		str1++;

		str2++;
	}

	return (upper(*str1) - upper(*str2));
}

