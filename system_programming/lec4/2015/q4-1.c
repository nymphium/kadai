#include <stdio.h>
#include <stdlib.h>
#include <bsd/string.h>
#include <ctype.h>

#define INITR(ptr, word, cnt, nxt)\
	ptr = (struct record *)malloc(sizeof (struct record)); \
	strlcpy(ptr->word, word, strlen(word) + 1);\
	ptr->count = cnt;\
	ptr->next = nxt;

struct record {
	char word[20];
	int count;
	struct record *next;
};

struct record *head = NULL;

int read_word(FILE *fp, char *word) {
	int LEN = 20;
	memset(word, '\0', LEN);
	char w[LEN];
	int ch;
	int p = 0;

	while ((ch = fgetc(fp)) != EOF) {
		if(isalnum(ch) || ch == 0x27 || ch == 0x2d) {
			w[p++] = ch;
		}else {
			if(p > 0) {
				strlcpy(word, w, p + 1);
				/* puts(word); */
				return 1;
			}else {
				memset(w, '\0', LEN);
				p = 0;
			}
		}
	}

	return 0;
}
void add_word(char *word) {
	struct record *p, *q, *next;
	int c = 0;

	for (p = head; p != NULL; p = p->next) {
		if (strcmp(p->word, word) == 0) {
			p->count++;
			return;
		}
		c++;
		q = p;
	}

	INITR(next, word, 1, NULL);

	if (!c) {
		head = next;
	}else q->next = next;
}

int main(int argc, char *argv[])
{
	FILE *fp;
	char word[20];
	struct record *p;

	if (argc != 2) {
		printf("missing file argument\n");
		return 1;
	}

	fp = fopen(argv[1], "r");
	if (fp == NULL) {
		printf("can't open %s\n", argv[1]);
		return 1;
	}

	while (read_word(fp, word)) {
		add_word(word);
	}

	fclose(fp);

	for (p = head; p != NULL; p = p->next)
		printf("%s %d\n", p->word, p->count);

	return 0;
}
