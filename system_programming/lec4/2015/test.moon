fh = io.open "sys-prog-ex4-data.txt"
fa = fh\read "*a"
ct = {}

for w in fa\gmatch "[a-zA-Z0-9'-]+"
	unless ct[1]
		ct[1] = {word: w, count: 1}
	else
		k = 0

		for i = 1, #ct
			if ct[i].word == w
				ct[i].count += 1
				break
			k += 1

		if k == #ct
			ct[k + 1] = {word: w, count: 1}


for i = 1, #ct
	io.write ct[i].word, " ", ct[i].count, "\n"

