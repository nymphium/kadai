#include <stdio.h>
#include <bsd/string.h>

int main(int argc, char *argv[]) {
	int ch;
	FILE *src, *dst;

	/* コマンドライン引数が1つ指定されているかどうか確認する */
	if (argc != 3) {
		printf("missing file argument\n");
		return 1;
	}

	if ((src = fopen(argv[1], "r")) == NULL) {   /* ファイルのオープン */
		printf("can't open %s\n", argv[1]);
		return 1;
	}

	if ((dst = fopen(argv[2], "w+")) == NULL) {   /* ファイルのオープン */
		printf("can't open %s\n", argv[2]);
		return 1;
	}

	while ((ch = fgetc(src)) != EOF) {   /* 1文字ずつ読み込む */
		if(ch >= 0x61 && ch <= 0x7a) ch -= 0x20;

		fputc(ch, dst);
	}

	fclose(src);     /* ファイルのクローズ */
	fclose(dst);     /* ファイルのクローズ */

	return 0;
}

