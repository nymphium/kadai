#include <stdio.h>

int main(int argc, char *argv[]){
	FILE *sfp, *dfp;

	sfp = fopen(argv[1], "r");

	dfp = fopen(argv[2], "w+");

	int ch;

	while((ch = fgetc(sfp)) != EOF){
		if(ch > 0x60 && ch < 0x7B) ch -= 0x20;

		fputc(ch, dfp);
	}

	fclose(dfp);

	fclose(sfp);

	return 0;
}
