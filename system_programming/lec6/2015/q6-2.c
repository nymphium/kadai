#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct trie {
	struct trie *child;
	struct trie *next;
	char c;
} trie_t;

trie_t *new_trie();
void init_trie(trie_t *ptr) {
	for(;ptr != NULL; ptr = ptr->next) {
		puts("hoge");
		ptr->c = NULL;
	}

	return;
}

trie_t *new_trie() {
	trie_t *ptr;

	ptr = malloc(sizeof(trie_t));
	if (ptr == NULL) exit(1);
	init_trie(ptr);

	return ptr;
}

trie_t *add_to_trie(char *str, trie_t *ptr) {
	if (strlen(str) == 0) {
		if (ptr->next == NULL) {
			ptr->next = new_trie();
			ptr->next->c = '.';
			return ptr->next;
		} else if (ptr->c == '.') {
			return ptr;
		} else {
			return add_to_trie(str, ptr->next);
		}
	}

	if (ptr->c == '\0') {
		ptr->c = '.';
		return ptr;
	} else if (ptr->c == *str) {
		/* do it yourself */
	} else {
		add_to_trie(str + 1, ptr->next);
	}
}

trie_t *find_in_trie(char *str, trie_t *ptr) {
	if (strlen(str) < 1) {
		return ptr->next == NULL ? ptr : NULL;
	}

	return (ptr->c == *str) ? find_in_trie(str + 1, ptr->next) : NULL;
}

int main(int argc, char *argv[]) {
	FILE *f;
	char str[512];
	trie_t root;

	if ((f = fopen(argv[1], "r")) == NULL) exit(1);

	init_trie(&root);
	while (fgets(str, 512, f)) {
		if (str[strlen(str) - 1] == '\n') str[strlen(str) - 1] = '\0';
		add_to_trie(str, &root);
	}

	fclose(f);

	if (find_in_trie(argv[2], &root) != NULL) {
		printf("%s: FOUND in %s\n", argv[0], argv[1]);
	} else {
		printf("%s: not found in %s\n", argv[0], argv[1]);
	}

	return 0;
}

