.data
.text
.global main


main:
	mov $0, %eax

	mov $0, %ebx

	L1:
		add $1, %eax

		add %eax, %ebx

		cmp $10, %eax
			je L2

		jmp L1
	L2:
		call stop
