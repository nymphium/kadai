#include <stdio.h>
#include <bsd/string.h>

void putsrev(char* str) {
	for(int i = strlen(str) - 1; i >= 0; i--) {
		printf("%c", str[i]);
	}

	puts("");
}

int main() {
	putsrev("321");
}

