#include <stdio.h>

#define N 100

int main() {
	double a[N][N], tmp;
	int i, j, k, n;

	printf("input matrix size\n");
	scanf("%d", &n);

	for (j = 0; j < n; j++) {
		for (i = 0; i <= n; i++) {
			scanf("%lf", &a[j][i]);
		}
	}

	for (k = 0; k < n; k++) {
		tmp = a[k][k];

		for(int o = 0; o <= n; o++) a[k][o] /= tmp;

		for(int l = 0; l < n - 1; l++) {
			int next = (k + l + 1) % n;
			double itimes = a[next][k];

			if(!itimes) continue;

			for(int m = 0; m <= n; m++) {
				a[next][m] = a[next][m] / itimes - a[k][m];
			}
		}
	}

	for(k = 0; k < n; k++) {
		a[k][n] /= a[k][k];
	}

	for (j = 0; j < n; j++) {
		printf("%f\n", a[j][n]);
	}

	return 0;
}

