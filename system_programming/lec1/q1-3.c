#include <stdio.h>

int main(){
	char str[100];

	scanf("%s", str);

	int digit = 0;

	while(str[++digit] != '\0'){};

	for(int i = digit - 1; i > -1; i--){
		printf("%c", str[i]);
	}

	printf("\n");

	return 0;
}
