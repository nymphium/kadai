.data
.text
.global fib, main


main: push $10
	call fib
	call stop


fib: push %ebp
	mov %esp, %ebp /* stack frame to ebp */
	push %ebx
	sub $4, %esp
	cmp $1, 8(%ebp) /* if x <= 1 then goto L1 */
	jle L1
	mov 8(%ebp), %eax
	sub $1, %eax /* x = x - 1 */
	sub $12, %esp
	push %eax
	call fib /* fib(x - 1) */
	add $16, %esp
	mov %eax, %ebx
	mov 8(%ebp), %eax
	sub $2, %eax /* x = x - 2 */
	sub $12, %esp
	push %eax
	call fib /* fib(x - 2) */
	add $16, %esp
	add %ebx, %eax
	jmp L2

L1: mov 8(%ebp), %eax

L2: mov -4(%ebp), %ebx
	leave
	/* mov %esp, %ebp */
	/* pop %ebp */
	ret

