.data
.align 4
x: .long 0x00124011, 0x3130ffff
y: .long 0x08001245, 0x12f01348
.text
.global main


main:
	push %ebp
	mov %esp, %ebp
	subl $32, %esp

	movl x, %edx /* x[0] = edx */
	movl y, %eax /* y[0] = eax */
	mul %eax
	movl %edx, -4(%ebp)

	movl x, %edx
	movl y+4, %eax
	mul %eax
	movl %edx, -8(%ebp)

	movl x+4, %edx
	movl y, %eax
	mul %eax
	movl %edx, -12(%ebp)

	movl x+4, %edx
	movl y+4, %eax
	mul %eax
	movl %edx, -16(%ebp)

	movl -4(%ebp), %eax
	movl -8(%ebp), %ebx
	movl -12(%ebp), %ecx
	movl -16(%ebp), %edx

	call stop

