	.file	"glob.c"
	.globl	x
	.data
	.align 4
	.type	x, @object
	.size	x, 8
x:
	.long	1196049
	.long	825294847
	.globl	y
	.align 4
	.type	y, @object
	.size	y, 8
y:
	.long	134222405
	.long	317723464
	.text
	.globl	main
	.type	main, @function
main:
.LFB0:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$16, %esp
	movl	x, %edx
	movl	y, %eax
	imull	%edx, %eax
	movl	%eax, -4(%ebp)
	movl	x, %edx
	movl	y+4, %eax
	imull	%edx, %eax
	movl	%eax, -8(%ebp)
	movl	x+4, %edx
	movl	y, %eax
	imull	%edx, %eax
	movl	%eax, -12(%ebp)
	movl	x+4, %edx
	movl	y+4, %eax
	imull	%edx, %eax
	movl	%eax, -16(%ebp)
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
.LFE0:
	.size	main, .-main
	.ident	"GCC: (GNU) 4.9.2 20141224 (prerelease)"
	.section	.note.GNU-stack,"",@progbits
