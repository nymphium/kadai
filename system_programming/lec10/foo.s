	.file	"foo.c"
	.text
	.globl	foo
	.type	foo, @function
foo:
.LFB0:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$96, %esp
	movl	8(%ebp), %edx
	movl	12(%ebp), %eax
	addl	%edx, %eax
	movl	%eax, -4(%ebp)
	movl	8(%ebp), %eax
	subl	12(%ebp), %eax
	movl	%eax, -8(%ebp)
	movl	-4(%ebp), %eax
	imull	-8(%ebp), %eax
	movl	%eax, -12(%ebp)
	movl	$1, -16(%ebp)
	movl	$1, -20(%ebp)
	movl	$1, -24(%ebp)
	movl	$1, -28(%ebp)
	movl	$1, -32(%ebp)
	movl	$1, -36(%ebp)
	movl	$1, -40(%ebp)
	movl	$1, -44(%ebp)
	movl	$1, -48(%ebp)
	movl	$1, -52(%ebp)
	movl	$1, -56(%ebp)
	movl	$1, -60(%ebp)
	movl	$1, -64(%ebp)
	movl	$1, -68(%ebp)
	movl	$1, -72(%ebp)
	movl	$1, -76(%ebp)
	movl	$1, -80(%ebp)
	movl	$1, -84(%ebp)
	movl	$1, -88(%ebp)
	movl	$1, -92(%ebp)
	movl	$1, -96(%ebp)
	movl	-12(%ebp), %eax
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
.LFE0:
	.size	foo, .-foo
	.ident	"GCC: (GNU) 5.3.0"
	.section	.note.GNU-stack,"",@progbits
