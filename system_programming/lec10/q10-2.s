.data
.text
.global sum


sum:
	push %ebp
	mov %esp, %ebp
	movl $0, -4(%ebp) /* initialize return-value */
	movl $0, -8(%ebp) /* i = 0 */

L1:
	movl -8(%ebp), %eax /* eax = i */
	movl 12(%ebp), %edx /* edx = array */
	movl -4(%ebp), %ecx
	addl (%edx, %eax, 4), %ecx
	movl %ecx, -4(%ebp)
	addl $1, -8(%ebp)

L2:
	movl -8(%ebp), %eax
	cmpl 8(%ebp), %eax
	jl L1
	movl -4(%ebp), %eax
	movl %eax, -12(%ebp)
	pop %ebp
	ret

