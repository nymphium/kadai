.data
.text
.global foo

foo:
	pushl %ebp
	movl %esp, %ebp

	movl 8(%ebp), %eax /* eax = x */
	movl 12(%ebp), %edx /* edx = y */
	movl %eax, -20(%ebp)
	movl %edx, -24(%ebp)
	add %edx, %eax
	mov %eax, -4(%ebp)

	movl -24(%ebp), %edx
	movl -20(%ebp), %eax
	subl %edx, %eax
	movl %eax, -8(%ebp)

	imul -4(%ebp), %eax
	movl %eax, -12(%ebp)
	pop %ebp
	ret

