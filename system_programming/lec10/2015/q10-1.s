.data
.text
.global foo

foo:
	/* init */
	push %ebp
	mov %esp, %ebp
	sub $12, %esp /* int a, b, c // 12 byte */

	// mov 8(%ebp), %ebx /* ebx = (1st arg) */
	// add 12(%ebp), %ebx /* ebx += (2nd arg) */
	// mov %ebx, %eax /* eax = ebx */
	// mov 8(%ebp), %ebx /* ebx = (1st arg) */ 
	// sub 12(%ebp), %ebx /* ebx -= (2nd arg) */
	// imul %ebx, %eax /* eax *= ebx */

	mov 8(%ebp), %ebx /* ebx = x (1st arg) */
	mov 12(%ebp), %ecx /* ecx = y (2nd arg) */

	/* a = x + y // *stack(ebp)[-4] = a */
	mov %ebx, -4(%ebp) /* stack(ebp)[-4] = ebx */
	add %ecx, -4(%ebp) /* stack(ebp)[-4] += ecx */

	/* b = x - y // *stack(ebp)[-8] = b */
	mov %ebx, -8(%ebp) /* stack(ebp)[-8] = ebx */
	sub %ecx, -8(%ebp) /* stack(ebp)[-8] -= ecx */

	/* c = a * b  // *stack(ebp)[-12] = c */
	mov -4(%ebp), %ecx /* ecx = stack(ebp)[-4] */
	mov -8(%ebp), %edx /* edx = stack(ebp)[-8] */
	imul %edx, %ecx /* ecx *= edx */
	mov %ecx, -12(%ebp) /* stack(ebp)[-12] = ecx */
	/* return c */
	mov -12(%ebp), %eax

	/* finalize */
	mov %ebp, %esp
	pop %ebp
	ret

