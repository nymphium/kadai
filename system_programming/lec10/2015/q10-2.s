.data
.text
.global sum

sum:
	/* init */
	push %ebp
	mov %esp, %ebp

	mov 8(%ebp), %ecx /* size */
	mov 12(%ebp), %edx /* array */
	mov $0, %eax /* eax = 0 */

L1:
	sub $1, %ecx /* ecx -= 1 */
	add (%edx, %ecx, 4), %eax /* eax += edx[ecx] */

	cmp $0, %ecx /* if ecx == 0 then goto Fin */
	je Fin
	jmp L1

Fin:
	/* finalize */
	mov %ebp, %esp
	pop %ebp
	ret

