.data
STR: .string "%d\n"
.text
.global sum

sum:
	/* pushl %ebp */
	/* movl %esp, %ebp */
	/* movl 8(%ebp), %eax */
	/* movl %eax, -12(%ebp) */
	/* pop %ebp */
	/* pushl %eax */
	/* pushl 8(%ebp) */

	pushl $8
	pushl $STR

	call printf
	ret

