.text
.globl	sum

sum:
	pushl	%ebp
	movl	%esp, %ebp
	movl	$0, -4(%ebp)
	movl	$0, -8(%ebp)
	jmp	.L2

.L3:
	movl	-8(%ebp), %eax
	leal	0(,%eax,4), %edx
	movl	12(%ebp), %eax
	addl	%edx, %eax
	movl	(%eax), %eax
	addl	%eax, -4(%ebp)
	addl	$1, -8(%ebp)
.L2:
	movl	-8(%ebp), %eax
	cmpl	8(%ebp), %eax
	jl	.L3
	movl	-4(%ebp), %eax
	leave
	/* pop %ebp */
	ret

