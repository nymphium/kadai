int sum(int n, int *array) {
	int ret_sum = 0;

	int i; for(i = 0; i < n; i++) {
		ret_sum += array[i];
	}

	return ret_sum;
}

