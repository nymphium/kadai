.data
.align 4
a: .long 0, 1, 0, 0, 0, 0, 0, 0, 0, 0
.text
.global main


main:
	mov $0, %eax
	mov $a, %ecx

L1:
	cmp $9, %eax
	je L2
	mov (%ecx, %eax, 4), %ebx
	add 4(%ecx, %eax, 4), %ebx
	mov %ebx, 8(%ecx, %eax, 4)
	add $1, %eax
	jmp L1

L2:
	call stop

