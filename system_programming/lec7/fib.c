#include <stdio.h>


int main(){
	int a[11] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0};

	int i;

	for(i = 1; i < 11; i++){
		if(i < 2) a[i] = i;

		else a[i] = a[i - 1] + a[i - 2];

		/* printf("%d\n", a[i]); */
	}

	printf("%d\n", a[10]);

	return 0;
}
