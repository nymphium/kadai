.data
x: .long 0x10000
y: .long 0x7fffffff
.text
.global main


main:
	mov x, %ebx

	add y, %ebx

	jo one

	mov $0, %eax

	call stop

one:
	mov $1, %eax

	call stop
