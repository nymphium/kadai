	.text
	.file	"fib.c"
	.globl	main
	.align	16, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	subq	$56, %rsp
.Ltmp0:
	.cfi_def_cfa_offset 64
	xorps	%xmm0, %xmm0
	movups	%xmm0, 28(%rsp)
	movaps	%xmm0, 16(%rsp)
	movaps	%xmm0, (%rsp)
	movl	$1, 4(%rsp)
	movl	(%rsp), %esi
	incl	%esi
	movl	%esi, 8(%rsp)
	addl	4(%rsp), %esi
	movl	%esi, 12(%rsp)
	addl	8(%rsp), %esi
	movl	%esi, 16(%rsp)
	addl	12(%rsp), %esi
	movl	%esi, 20(%rsp)
	addl	16(%rsp), %esi
	movl	%esi, 24(%rsp)
	addl	20(%rsp), %esi
	movl	%esi, 28(%rsp)
	addl	24(%rsp), %esi
	movl	%esi, 32(%rsp)
	addl	28(%rsp), %esi
	movl	%esi, 36(%rsp)
	addl	32(%rsp), %esi
	movl	%esi, 40(%rsp)
	movl	$.L.str, %edi
	xorl	%eax, %eax
	callq	printf
	xorl	%eax, %eax
	addq	$56, %rsp
	retq
.Ltmp1:
	.size	main, .Ltmp1-main
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"%d\n"
	.size	.L.str, 4


	.ident	"clang version 3.5.0 (tags/RELEASE_350/final)"
	.section	".note.GNU-stack","",@progbits
