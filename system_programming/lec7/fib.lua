function fib(i)
	local t = {}

	for x = 0, i do
		if x < 2 then t[x] = x

		else t[x] = t[x - 1] + t[x - 2]

		end
	end

	return t[i]
end


for i = 1, 10 do
	print(fib(i))
end
