.data
myenv: .long 0, 0, 0, 0, 0, 0
.text

.globl mysetjmp

mysetjmp:
	/* myenv.insert(addr) */
	/* myenv.insert(esp) */
	/* myenv.insert(ebp) */
	/* myenv.insert(ebx) */
	/* myenv.insert(esi) */
	/* myenv.insert(edi) */
	movl $myenv, %ecx
	movl 0(%esp), %edx
	movl %edx, 0(%ecx)
	movl %ebx, 4(%ecx)
	movl %esp, 8(%ecx)
	movl %ebp, 12(%ecx)
	movl %esi, 16(%ecx)
	movl %edi, 20(%ecx)

	ret

.globl mylongjmp

mylongjmp:
	movl $myenv, %ecx
	movl 0(%ecx), %edx
	movl 4(%ecx), %ebx
	movl 8(%ecx), %esp
	movl 12(%ecx), %ebp
	movl 16(%ecx), %esi
	movl 20(%ecx), %edi

	ret

