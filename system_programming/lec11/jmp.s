	.file	"jmp.c"
	.comm	jbuf,156,64
	.comm	touch,4,4
	.text
	.globl	func4
	.type	func4, @function
func4:
.LFB0:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$8, %esp
	movl	$1, touch
	subl	$8, %esp
	pushl	$1
	pushl	$jbuf
	call	longjmp
	.cfi_endproc
.LFE0:
	.size	func4, .-func4
	.section	.rodata
.LC0:
	.string	"func3: %d\n"
	.text
	.globl	func3
	.type	func3, @function
func3:
.LFB1:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$8, %esp
	subl	$8, %esp
	pushl	8(%ebp)
	pushl	$.LC0
	call	printf
	addl	$16, %esp
	subl	$12, %esp
	pushl	8(%ebp)
	call	func4
	addl	$16, %esp
	movl	%eax, 8(%ebp)
	subl	$8, %esp
	pushl	8(%ebp)
	pushl	$.LC0
	call	printf
	addl	$16, %esp
	movl	8(%ebp), %eax
	addl	$1, %eax
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
.LFE1:
	.size	func3, .-func3
	.section	.rodata
.LC1:
	.string	"func2: %d\n"
	.text
	.globl	func2
	.type	func2, @function
func2:
.LFB2:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$8, %esp
	subl	$8, %esp
	pushl	8(%ebp)
	pushl	$.LC1
	call	printf
	addl	$16, %esp
	subl	$12, %esp
	pushl	8(%ebp)
	call	func3
	addl	$16, %esp
	movl	%eax, 8(%ebp)
	subl	$8, %esp
	pushl	8(%ebp)
	pushl	$.LC1
	call	printf
	addl	$16, %esp
	movl	8(%ebp), %eax
	addl	$1, %eax
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
.LFE2:
	.size	func2, .-func2
	.section	.rodata
.LC2:
	.string	"func1: %d\n"
	.text
	.globl	func1
	.type	func1, @function
func1:
.LFB3:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$8, %esp
	subl	$8, %esp
	pushl	8(%ebp)
	pushl	$.LC2
	call	printf
	addl	$16, %esp
	subl	$12, %esp
	pushl	8(%ebp)
	call	func2
	addl	$16, %esp
	movl	%eax, 8(%ebp)
	subl	$8, %esp
	pushl	8(%ebp)
	pushl	$.LC2
	call	printf
	addl	$16, %esp
	movl	8(%ebp), %eax
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
.LFE3:
	.size	func1, .-func1
	.section	.rodata
.LC3:
	.string	"touch: %d\n"
	.text
	.globl	main
	.type	main, @function
main:
.LFB4:
	.cfi_startproc
	leal	4(%esp), %ecx
	.cfi_def_cfa 1, 0
	andl	$-16, %esp
	pushl	-4(%ecx)
	pushl	%ebp
	.cfi_escape 0x10,0x5,0x2,0x75,0
	movl	%esp, %ebp
	pushl	%ecx
	.cfi_escape 0xf,0x3,0x75,0x7c,0x6
	subl	$4, %esp
	movl	touch, %eax
	subl	$8, %esp
	pushl	%eax
	pushl	$.LC3
	call	printf
	addl	$16, %esp
	subl	$12, %esp
	pushl	$jbuf
	call	_setjmp
	addl	$16, %esp
	testl	%eax, %eax
	jg	.L10
	movl	touch, %eax
	testl	%eax, %eax
	jg	.L10
	subl	$12, %esp
	pushl	$0
	call	func1
	addl	$16, %esp
.L10:
	movl	touch, %eax
	subl	$8, %esp
	pushl	%eax
	pushl	$.LC3
	call	printf
	addl	$16, %esp
	movl	-4(%ebp), %ecx
	.cfi_def_cfa 1, 0
	leave
	.cfi_restore 5
	leal	-4(%ecx), %esp
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
.LFE4:
	.size	main, .-main
	.ident	"GCC: (GNU) 4.9.2 20141224 (prerelease)"
	.section	.note.GNU-stack,"",@progbits
