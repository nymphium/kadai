#include <stdio.h>
/* #include <setjmp.h> */


void mysetjmp();
void mylongjmp();

/* jmp_buf jbuf; */
int touch;

int func4(int i) {
	touch++;
	/* mylongjmp(); */
	/* longjmp(jbuf, 1); */

	printf("func4: %d\n", i);

	return i + 1;
}

int func3(int i) {
	printf("func3: %d\n", i);

	i = func4(i);

	printf("func3: %d\n", i);

	return i + 1;
}

int func2(int i) {
	printf("func2: %d\n", i);

	i = func3(i);

	printf("func2: %d\n", i);

	return i + 1;
}

int func1(int i) {
	printf("func1: %d\n", i);

	i = func2(i);

	printf("func1: %d\n", i);

	return i;
}

int main() {
	printf("touch: %d\n", touch);

	mysetjmp();
	if(touch < 1) func1(0);
	/* if(setjmp(jbuf) < 1) func1(0); */

	printf("touch: %d\n", touch);
}

