#include <stdio.h>
#include <stdlib.h>

void switch_to_a(void);
void switch_to_b(void);
void create_thread_b(void(*f)(void), char *stack_bottom);

#define	STACK_SIZE 4096
#define	LOOP_CNT   5
#define	SWTCH_CNT  10

void thread_a(void)
{
	int i, j;
	for (i = 0; i < LOOP_CNT; i++) {
		printf("thread a (%d) ", i);
		for (j = 0; j < SWTCH_CNT; j++) {
			printf("%d ", j);
		}
		printf("\n");
		switch_to_b();
	}
}

void thread_b(void)
{
	int i, j;

	for (i = 0; i < LOOP_CNT; i++) {
		printf("thread b (%d) ", i);
		for (j = 0; j < SWTCH_CNT; j++) {
			printf("%d ", j);
		}
		printf("\n");
		switch_to_a();
	}
}

int
main(int argc, char *argv[])
{
	char *thread_b_stack;
	thread_b_stack = malloc(STACK_SIZE);
	if (thread_b_stack == NULL) {
		perror("malloc");
		exit(1);
	}

	create_thread_b(thread_b, thread_b_stack + STACK_SIZE);
	thread_a();
	printf("main exiting...\n");
}

