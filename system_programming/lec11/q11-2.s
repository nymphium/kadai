.data
env_a: .long 0, 0, 0, 0, 0, 0
env_b: .long 0, 0, 0, 0, 0, 0
.text

.globl switch_to_a

switch_to_a:
	/* store b's environment */
	/* restore a's environment */
	movl $env_b, %ecx
	movl 0(%esp), %edx
	movl %edx, 0(%ecx)
	movl %ebx, 4(%ecx)
	movl %esp, 8(%ecx)
	movl %ebp, 12(%ecx)
	movl %esi, 16(%ecx)
	movl %edi, 20(%ecx)

	movl $env_a, %ecx
	movl 0(%ecx), %edx
	movl 4(%ecx), %ebx
	movl 8(%ecx), %esp
	movl 12(%ecx), %ebp
	movl 16(%ecx), %esi
	movl 20(%ecx), %edi

	ret


.globl switch_to_b
switch_to_b:
	/* store a's environment */
	/* restore b's environment */
	movl $env_a, %ecx
	movl 0(%esp), %edx
	movl %edx, 0(%ecx)
	movl %ebx, 4(%ecx)
	movl %esp, 8(%ecx)
	movl %ebp, 12(%ecx)
	movl %esi, 16(%ecx)
	movl %edi, 20(%ecx)

	movl $env_b, %ecx
	movl 0(%ecx), %edx
	movl 4(%ecx), %ebx
	movl 8(%ecx), %esp
	movl 12(%ecx), %ebp
	movl 16(%ecx), %esi
	movl 20(%ecx), %edi

	ret


.globl create_thread_b
create_thread_b:
	movl $env_b, %ecx
	movl 4(%esp), %edx

	movl 8(%esp), %eax
	subl $4, %eax
	movl %edx, (%eax)
	movl %eax, 8(%ecx)

	ret

