#include <stdio.h>
#include <stdlib.h>
#include <bsd/string.h>

typedef struct {
	char name[16];
	int score;
	int num;
} tuple;


int flines(FILE *fp) {
	int ch, count = 0;

	rewind(fp);

	while ((ch = fgetc(fp)) != EOF) {
		if(ch == '\n') count++;
	}

	rewind(fp);

	return count;
}

int comp(const void *a, const void *b) {
	return ((tuple*)b)->score - ((tuple*)a)->score;
}

int main(int argc, char *argv[]) {
	if (argc != 2) {
		printf("missing file argument\n");
		return 1;
	}

	FILE *fp;

	if ((fp = fopen(argv[1], "r")) == NULL) {
		printf("can't open %s\n", argv[1]);
		return 1;
	}

	int count = flines(fp);
	int i = 0;
	tuple t[count];

	while(fscanf(fp, "%d %s %d", &t[i].num, t[i].name, &t[i].score) != EOF) {
		i++;
	}

	fclose(fp);

	qsort(t, count, sizeof(tuple), comp);

	for(int j = 0; j < count; j++) {
		if(t[j].score >= 60) printf("%d %-16s %d\n", t[j].num, t[j].name, t[j].score);
	}

	return 0;
}

