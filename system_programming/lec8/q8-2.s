.data
.align 4
x: .long 0x87001240,0x00124011,0x8130FFFF,0x1234
y: .long 0x07001245,0x12f01348,0x8230FFFF,0x12
.text
.global main


main:
	mov $x, %eax
	mov $0, %edi
	mov (%eax, %edi, 4), %eax
	mov $y, %esi
	add (%esi, %edi, 4), %eax

	mov $x, %ebx
	mov $1, %edi
	mov (%ebx, %edi, 4), %ebx
	mov $y, %esi
	add (%esi, %edi, 4), %ebx

	mov $x, %ecx
	mov $2, %edi
	mov (%ecx, %edi, 4), %ecx
	mov $y, %esi
	add (%esi, %edi, 4), %ecx

	mov $x, %edx
	mov $3, %edi
	mov (%edx, %edi, 4), %edx
	mov $y, %esi
	add (%esi, %edi, 4), %edx

	call stop

