int fstack[10];
int len = -1;

int main() {
	for(int i = 0; i < 10; i++) {
		if(i > len){
			if(i < 2){
				fstack[i] = i;
			}else fstack[i] = fstack[i - 1] + fstack[i - 2];

			len++;
		}
	}
}
