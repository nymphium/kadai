.data
.align 4
fstack: .long 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
.text
.global main

main:
	mov $0, %eax
	mov $0, %ebx
	jmp L1

L1:
	cmp $11, %eax
	je end
	cmp $2, %eax
	jl L2
	mov %eax, %ecx
	sub $1, %ecx
	mov fstack(, %ecx, 4), %edx
	mov %eax, %ecx
	sub $2, %ecx
	add fstack(, %ecx, 4), %edx
	mov %eax, %ecx
	mov %edx, fstack(, %ecx, 4)
	add $1, %eax
	jmp L1

L2:
	mov %eax, fstack(, %eax, 4)
	add $1, %eax
	jmp L1

end:
	mov $10, %eax
	mov fstack(,%eax, 4), %ebx
	call stop
