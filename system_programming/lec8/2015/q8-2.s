.data
.text
.global main

main:
	mov $1, %eax
	mov $0, %ebx

L1:
	add %eax, %ebx
	add $1, %eax
	cmp $11, %eax
	jne L1
	jmp L2

L2:
	call stop
