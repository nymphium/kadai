	.file	"fib.c"
	.comm	fstack,40,32
	.globl	len
	.data
	.align 4
	.type	len, @object
	.size	len, 4
len:
	.long	-1
	.text
	.globl	main
	.type	main, @function
main:
.LFB0:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movl	$0, -4(%rbp)
	jmp	.L2
.L6:
	movl	len(%rip), %eax
	cmpl	%eax, -4(%rbp)
	jle	.L3
	cmpl	$1, -4(%rbp)
	jg	.L4
	movl	-4(%rbp), %eax
	cltq
	movl	-4(%rbp), %edx
	movl	%edx, fstack(,%rax,4)
	jmp	.L5
.L4:
	movl	-4(%rbp), %eax
	subl	$1, %eax
	cltq
	movl	fstack(,%rax,4), %edx
	movl	-4(%rbp), %eax
	subl	$2, %eax
	cltq
	movl	fstack(,%rax,4), %eax
	addl	%eax, %edx
	movl	-4(%rbp), %eax
	cltq
	movl	%edx, fstack(,%rax,4)
.L5:
	movl	len(%rip), %eax
	addl	$1, %eax
	movl	%eax, len(%rip)
.L3:
	addl	$1, -4(%rbp)
.L2:
	cmpl	$9, -4(%rbp)
	jle	.L6
	movl	$0, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE0:
	.size	main, .-main
	.ident	"GCC: (GNU) 5.3.0"
	.section	.note.GNU-stack,"",@progbits
