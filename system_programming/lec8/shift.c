#include <stdio.h>

int main(){
	int eax = 0;
	int ebx;
	int ecx = 10;
	int edx = 10;

L1: {
		if(edx == 0) goto L4;

		ebx = 1;
		ebx = ebx & edx;

		if(ebx == 1) goto L3;
	}

L2: {
		ecx <<= 1;
		edx >>= 1;

		goto L1;
	}

L3: {
		eax += ecx;

		goto L2;
	}

L4: {
		printf("mul: %d\n", eax);
	}
}

