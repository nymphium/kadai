.data
x: .long 0b111
y: .long 0b11
.text
.global main


main:
/* eax = 0, ecx = x, edx = y */
mov $0, %eax
mov x, %ecx
mov y, %edx

L1:
cmp $0, %edx
je L4

/* edx = 1, edx &= ebx */
mov $1, %ebx
and %edx, %ebx
/* if ebx == 1 then goto: L3: */
cmp $1, %ebx
je L3

L2:
/* ecx >>= 1, edx <<= 1 */
shl $1, %ecx
shr $1, %edx

jmp L1

L3:
/* eax += ecx */
add %ecx, %eax
jmp L2

L4:
call stop

