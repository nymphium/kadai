(* CoreMLRed.v *)
(* 2014/10/31 by Yukiyoshi Kameyama *)

Require Export SfLib.
Require Import Ascii String.
Require Import BinInt.
Require Import CoreMLDef.

Open Scope type_scope.

Notation var_x := (TM_var (Id 0)).
Notation var_y := (TM_var (Id 1)).
Notation var_z := (TM_var (Id 2)).
Notation var_u := (TM_var (Id 3)).
Notation var_v := (TM_var (Id 4)).
Notation var_w := (TM_var (Id 5)).

Fixpoint lookup_env (id1 : id) (E : list (tm * valueT)) : option valueT :=
  match E with
      | (TM_var id2,v2) :: E2 => if (eq_id_dec id1 id2) then Some v2
                                 else lookup_env id1 E2
      | _  => None
  end
.
Definition extend_env (E : envT) id1 v1 := ((id1,v1) :: E).

Definition add1 (v1 v2 : valueT) : option valueT :=
  match (v1,v2) with
  | (V_int n1, V_int n2) => Some (V_int (n1 + n2))
  | _ => None
  end
.

Inductive eval2 : envT -> tm -> valueT -> Prop :=
  | E_var    : forall E id1 v1,
                 (lookup_env id1 E) = Some v1
                 -> eval2 E (TM_var id1) v1
  | E_int    : forall E n1,
                 eval2 E (TM_int n1) (V_int n1)
  | E_bool   : forall E b1,
                 eval2 E (TM_bool b1) (V_bool b1)
  | E_add    : forall E t1 t2 v1 v2 v3,
                  eval2 E t2 v2
               -> eval2 E t1 v1
               -> add1 v1 v2 = Some v3
               -> eval2 E (TM_add t1 t2) v3
  | E_lam    : forall E x t,
                 eval2 E (TM_lam x t) (V_closure x t E)
  | E_app1    : forall E E3 t1 x t2 v2 t3 v4,
                  eval2 E t2 v2
               -> eval2 E t1 (V_closure x t3 E3)
               -> eval2 (extend_env E3 x v2) t3 v4
               -> eval2 E (TM_app t1 t2) v4
  | E_app2    : forall E E3 t1 f x t2 v2 t3 v4,
                  eval2 E t2 v2
               -> eval2 E t1 (V_rclosure f x t3 E3)
               -> eval2 (extend_env (extend_env E3 f (V_rclosure f x t3 E3)) x v2) t3 v4
               -> eval2 E (TM_app t1 t2) v4
  | E_pair   : forall E t1 t2 v1 v2,
                  eval2 E t1 v1
               -> eval2 E t2 v2
               -> eval2 E (TM_pair t1 t2) (V_pair v1 v2)
  | E_left   : forall E t1 v1 v2,
                  eval2 E t1 (V_pair v1 v2)
               -> eval2 E (TM_left t1) v1
  | E_right  : forall E t1 v1 v2,
                  eval2 E t1 (V_pair v1 v2)
               -> eval2 E (TM_right t1) v2
  | E_if1 : forall E t1 t2 t3 v,
                  eval2 E t1 (V_bool true)
               -> eval2 E t2 v
               -> eval2 E (TM_if t1 t2 t3) v
  | E_if2 : forall E t1 t2 t3 v,
                  eval2 E t1 (V_bool false)
               -> eval2 E t2 v
               -> eval2 E (TM_if t1 t2 t3) v
  | E_fix : forall E f x t1,
                  eval2 E (TM_fix f x t1) (V_rclosure f x t1 E)
  | E_let : forall E x t1 t2 v1 v2,
                  eval2 E t1 v1
               -> eval2 (extend_env E x v1) t2 v2
               -> eval2 E (TM_let x t1 t2) v2
.

Notation "E '>>' t1 '-->' t2" := (eval2 E t1 t2) (at level 90).
Definition eval t1 v1 := eval2 ([]: envT) t1 v1.

Theorem ex501 : eval (TM_int 10) (V_int 10).
Proof. apply E_int. Qed.

Theorem ex502 : eval (TM_if (TM_bool true) (TM_int 10) (TM_int 20))
                     (V_int 10).
Proof. apply E_if1. apply E_bool. apply E_int.  Qed.

Theorem ex503 : eval (TM_app (TM_lam var_x var_x)
                             (TM_add (TM_int 10) (TM_int (-20))))
                     (V_int (-10)).
Proof.  apply E_app1 with (E3:=[])
                          (x:=var_x)
                          (v2:=(V_int (-10)))
                          (t3:=var_x).
        apply E_add with (v1:=V_int 10) (v2:=V_int (-20)).
        apply E_int.
        apply E_int.
        unfold add1; simpl. reflexivity.
        apply E_lam.
        unfold extend_env; simpl. apply E_var. reflexivity.
Qed.
 
Theorem ex302 : eval (TM_lam var_x var_x)
                     (V_closure var_x var_x []).
Proof.
  apply E_lam.
Qed.

Theorem ex303 : 
  eval 
    (TM_app (TM_app (TM_lam var_x (TM_lam var_y
                 (TM_app var_x (TM_app var_x var_y))))
              (TM_lam var_z (TM_add var_z (TM_int 10))))
           (TM_int 5))
    (V_int 25).
Proof.
  eapply E_app1. 
  eapply E_int.
  eapply E_app1. 
  apply E_lam.
  apply E_lam.
  unfold extend_env; simpl.
  apply E_lam.
  unfold extend_env; simpl.
  eapply E_app1. 
  eapply E_app1.
  eapply E_var; unfold lookup_env; reflexivity.
  eapply E_var; unfold lookup_env; reflexivity.
  eapply E_add; unfold extend_env.
  apply E_int.
  eapply E_var; unfold lookup_env; reflexivity.
  unfold add1; reflexivity.
  eapply E_var; unfold lookup_env; reflexivity.
  eapply E_add; unfold extend_env.
  eapply E_int.
  eapply E_var; unfold lookup_env; reflexivity.
  unfold add1; reflexivity.
Qed.

(*********************************************************************)

Definition beq_int (n m : Z) : bool :=
  match (Zcompare n m) with
      | Eq => true
      | _  => false
  end.
Definition bgt_int (n m : Z) : bool :=
  match (Zcompare n m) with
      | Gt => true
      | _  => false
  end.
Definition blt_int (n m : Z) : bool :=
  match (Zcompare n m) with
      | Lt => true
      | _  => false
  end.

Fixpoint evaluates2 (Gamma : envT) (t : tm) (step : nat) : option valueT :=
  match step with
    | 0 => None
    | S(step') =>
          match t with
            | TM_var id1   => lookup_env id1 Gamma
            | TM_int n     => Some (V_int n)
            | TM_bool b    => Some (V_bool b)
            | TM_add t1 t2 => 
                match (evaluates2 Gamma t1 step', evaluates2 Gamma t2 step') with
                  | (Some(V_int n1)   ,Some(V_int n2   )) => Some(V_int(n1+n2))
                  | _ => None
                end
            | TM_eq  t1 t2 =>
                match (evaluates2 Gamma t1 step', evaluates2 Gamma t2 step') with
                  | (Some(V_int n1)   ,Some(V_int n2   )) => Some(V_bool(beq_int n1 n2))
                  | _ => None
                end
            | TM_gt  t1 t2 =>
                match (evaluates2 Gamma t1 step', evaluates2 Gamma t2 step') with
                  | (Some(V_int n1)   ,Some(V_int n2   )) => Some(V_bool(bgt_int n1 n2))
                  | _ => None
                end
            | TM_lam x1 t2 => Some(V_closure x1 t2 Gamma)
            | TM_app t1 t2 =>
                match (evaluates2 Gamma t2 step') with
                  | Some v => match (evaluates2 Gamma t1 step') with
                                | Some(V_closure x t1 Gamma') =>
                                    evaluates2 (extend_env Gamma' x v) t1 step'
                                | Some(V_rclosure f x t1 Gamma') =>
                                    evaluates2 (extend_env 
                                                  (extend_env Gamma' x v)
                                                  f (V_rclosure f x t1 Gamma'))
                                               t1 step'
                                | _ => None
                              end
                  | _ => None
                end
            | TM_pair t1 t2 => 
                match (evaluates2 Gamma t1 step',evaluates2 Gamma t2 step') with
                    | (Some v1, Some v2) => Some (V_pair v1 v2)
                    | _ => None
                end
            | TM_left t1 => 
                match (evaluates2 Gamma t1 step') with
                  | Some(V_pair v1 v2) => Some v1
                  | _ => None
                end
            | TM_right t1 => 
                match (evaluates2 Gamma t1 step') with
                  | Some(V_pair v1 v2) => Some v2
                  | _ => None
                end
            | TM_if t1 t2 t3 =>
                match (evaluates2 Gamma t1 step') with
                  | Some(V_bool true)  => evaluates2 Gamma t2 step'
                  | Some(V_bool false) => evaluates2 Gamma t3 step'
                  | _ => None
                end
            | TM_fix f x t1 => Some(V_rclosure f x t1 Gamma)
            | TM_let x1 t1 t2 =>
                match (evaluates2 Gamma t1 step') with
                  | Some(v1) => evaluates2 (extend_env Gamma x1 v1) t2 step'
                  | _ => None
                end
          end
  end
.

Definition evaluates t := evaluates2 [] t 1000.

Eval vm_compute in (evaluates (TM_int 10)).
Eval vm_compute in (evaluates (TM_add (TM_int 10) (TM_int 3))).
Eval vm_compute in (evaluates (TM_add (TM_int 10) (TM_int (-3)))).
Eval vm_compute in (evaluates (TM_add (TM_int (-10)) (TM_int (-3)))).
Eval vm_compute in (evaluates (TM_app (TM_lam var_x (TM_add var_x var_x)) (TM_int (-5)))).
Eval vm_compute in (evaluates 
  (TM_let var_x (TM_lam var_y (TM_lam var_z (TM_app var_y (TM_app var_y var_z))))
          (TM_app (TM_app var_x (TM_lam var_u (TM_add var_u (TM_int 10)))) (TM_int (-3))))).
Definition t0 := (TM_app (TM_fix var_x var_y (TM_int 1)) (TM_int 3)).
Eval vm_compute in (evaluates t0).
Definition t2 := (TM_app (TM_fix var_x var_y (TM_add var_y (TM_int 1))) (TM_int 3)).
Eval vm_compute in (evaluates t2).
Definition t3 := (TM_app (TM_fix var_x var_y (TM_pair var_x var_y)) (TM_int 3)).
Eval vm_compute in (evaluates t3).
Definition t4 := (TM_app (TM_fix var_x var_y (TM_if (TM_eq (TM_int 3) var_y) 
                                                    (TM_int 1)
                                                    (TM_int 2)))
                         (TM_int 3)).
Eval vm_compute in (evaluates t4).
Definition t5 :=
  (TM_fix var_x var_y 
          (TM_if (TM_eq var_y (TM_int 0))
                 (TM_int 3)
                 (TM_app var_x (TM_add var_y (TM_int (-1)))))).
Eval vm_compute in (evaluates (TM_app t5 (TM_int 0))).
Eval vm_compute in (evaluates (TM_app t5 (TM_int 4))).
Definition t6 :=
  (TM_fix var_x var_y 
          (TM_if (TM_eq var_y (TM_int 0))
                 (TM_int 3)
                 (TM_add (TM_int 1) (TM_app var_x (TM_add var_y (TM_int (-1))))))).
Eval vm_compute in (evaluates (TM_app t6 (TM_int 4))).

Definition t10 :=
  (TM_fix var_x var_y 
          (TM_if (TM_eq var_y (TM_int 0))
                 (TM_int 0)
                 (TM_add var_y (TM_app var_x (TM_add var_y (TM_int (-1))))))).
Eval vm_compute in (evaluates (TM_app t10 (TM_int 0))).
Eval vm_compute in (evaluates (TM_app t10 (TM_int 1))).
Eval vm_compute in (evaluates (TM_app t10 (TM_int 2))).
Eval vm_compute in (evaluates (TM_app t10 (TM_int 3))).
Eval vm_compute in (evaluates (TM_app t10 (TM_int 4))).
Eval vm_compute in (evaluates (TM_app t10 (TM_int 5))).

Definition t11 :=
  (TM_let var_x (TM_int 10)
          (TM_let var_y (TM_lam var_z (TM_add var_x var_z))
                  (TM_let var_x (TM_int 20)
                          (TM_app var_y (TM_int 30))))).
Eval vm_compute in (evaluates t11).

Definition beq_int (n m : Z) : bool :=
  match (Zcompare n m) with
      | Eq => true
      | _  => false
  end.
Definition bgt_int (n m : Z) : bool :=
  match (Zcompare n m) with
      | Gt => true
      | _  => false
  end.
Definition blt_int (n m : Z) : bool :=
  match (Zcompare n m) with
      | Lt => true
      | _  => false
  end.

Fixpoint evaluates2 (Gamma : envT) (t : tm) (step : nat) : option valueT :=
  match step with
    | 0 => None
    | S(step') =>
          match t with
            | TM_var id1   => lookup_env id1 Gamma
            | TM_int n     => Some (V_int n)
            | TM_bool b    => Some (V_bool b)
            | TM_add t1 t2 => 
                match (evaluates2 Gamma t1 step', evaluates2 Gamma t2 step') with
                  | (Some(V_int n1)   ,Some(V_int n2   )) => Some(V_int(n1+n2))
                  | _ => None
                end
            | TM_eq  t1 t2 =>
                match (evaluates2 Gamma t1 step', evaluates2 Gamma t2 step') with
                  | (Some(V_int n1)   ,Some(V_int n2   )) => Some(V_bool(beq_int n1 n2))
                  | _ => None
                end
            | TM_gt  t1 t2 =>
                match (evaluates2 Gamma t1 step', evaluates2 Gamma t2 step') with
                  | (Some(V_int n1)   ,Some(V_int n2   )) => Some(V_bool(bgt_int n1 n2))
                  | _ => None
                end
            | TM_lam x1 t2 => Some(V_closure x1 t2 Gamma)
            | TM_app t1 t2 =>
                match (evaluates2 Gamma t2 step') with
                  | Some v => match (evaluates2 Gamma t1 step') with
                                | Some(V_closure x t1 Gamma') =>
                                    evaluates2 (extend_env Gamma' x v) t1 step'
                                | Some(V_rclosure f x t1 Gamma') =>
                                    evaluates2 (extend_env 
                                                  (extend_env Gamma' x v)
                                                  f (V_rclosure f x t1 Gamma'))
                                               t1 step'
                                | _ => None
                              end
                  | _ => None
                end
            | TM_pair t1 t2 => 
                match (evaluates2 Gamma t1 step',evaluates2 Gamma t2 step') with
                    | (Some v1, Some v2) => Some (V_pair v1 v2)
                    | _ => None
                end
            | TM_left t1 => 
                match (evaluates2 Gamma t1 step') with
                  | Some(V_pair v1 v2) => Some v1
                  | _ => None
                end
            | TM_right t1 => 
                match (evaluates2 Gamma t1 step') with
                  | Some(V_pair v1 v2) => Some v2
                  | _ => None
                end
            | TM_if t1 t2 t3 =>
                match (evaluates2 Gamma t1 step') with
                  | Some(V_bool true)  => evaluates2 Gamma t2 step'
                  | Some(V_bool false) => evaluates2 Gamma t3 step'
                  | _ => None
                end
            | TM_fix f x t1 => Some(V_rclosure f x t1 Gamma)
            | TM_let x1 t1 t2 =>
                match (evaluates2 Gamma t1 step') with
                  | Some(v1) => evaluates2 (extend_env Gamma x1 v1) t2 step'
                  | _ => None
                end
          end
  end
.

Definition evaluates t := evaluates2 [] t 1000.

Eval vm_compute in (evaluates (TM_int 10)).
Eval vm_compute in (evaluates (TM_add (TM_int 10) (TM_int 3))).
Eval vm_compute in (evaluates (TM_add (TM_int 10) (TM_int (-3)))).
Eval vm_compute in (evaluates (TM_add (TM_int (-10)) (TM_int (-3)))).
Eval vm_compute in (evaluates (TM_app (TM_lam var_x (TM_add var_x var_x)) (TM_int (-5)))).
Eval vm_compute in (evaluates 
  (TM_let var_x (TM_lam var_y (TM_lam var_z (TM_app var_y (TM_app var_y var_z))))
          (TM_app (TM_app var_x (TM_lam var_u (TM_add var_u (TM_int 10)))) (TM_int (-3))))).
Definition t0 := (TM_app (TM_fix var_x var_y (TM_int 1)) (TM_int 3)).
Eval vm_compute in (evaluates t0).
Definition t2 := (TM_app (TM_fix var_x var_y (TM_add var_y (TM_int 1))) (TM_int 3)).
Eval vm_compute in (evaluates t2).
Definition t3 := (TM_app (TM_fix var_x var_y (TM_pair var_x var_y)) (TM_int 3)).
Eval vm_compute in (evaluates t3).
Definition t4 := (TM_app (TM_fix var_x var_y (TM_if (TM_eq (TM_int 3) var_y) 
                                                    (TM_int 1)
                                                    (TM_int 2)))
                         (TM_int 3)).
Eval vm_compute in (evaluates t4).
Definition t5 :=
  (TM_fix var_x var_y 
          (TM_if (TM_eq var_y (TM_int 0))
                 (TM_int 3)
                 (TM_app var_x (TM_add var_y (TM_int (-1)))))).
Eval vm_compute in (evaluates (TM_app t5 (TM_int 0))).
Eval vm_compute in (evaluates (TM_app t5 (TM_int 4))).
Definition t6 :=
  (TM_fix var_x var_y 
          (TM_if (TM_eq var_y (TM_int 0))
                 (TM_int 3)
                 (TM_add (TM_int 1) (TM_app var_x (TM_add var_y (TM_int (-1))))))).
Eval vm_compute in (evaluates (TM_app t6 (TM_int 4))).

Definition t10 :=
  (TM_fix var_x var_y 
          (TM_if (TM_eq var_y (TM_int 0))
                 (TM_int 0)
                 (TM_add var_y (TM_app var_x (TM_add var_y (TM_int (-1))))))).
Eval vm_compute in (evaluates (TM_app t10 (TM_int 0))).
Eval vm_compute in (evaluates (TM_app t10 (TM_int 1))).
Eval vm_compute in (evaluates (TM_app t10 (TM_int 2))).
Eval vm_compute in (evaluates (TM_app t10 (TM_int 3))).
Eval vm_compute in (evaluates (TM_app t10 (TM_int 4))).
Eval vm_compute in (evaluates (TM_app t10 (TM_int 5))).

Definition t11 :=
  (TM_let var_x (TM_int 10)
          (TM_let var_y (TM_lam var_z (TM_add var_x var_z))
                  (TM_let var_x (TM_int 20)
                          (TM_app var_y (TM_int 30))))).
Eval vm_compute in (evaluates t11).

