(* CoreMLEval.v *)

(* Last Updated on 2015/11/23 by Yukiyoshi Kameyama *)

Require Export SfLib.
Require Import Ascii String.
Require Import ZArith.
Require Import CoreMLDef.
Require Import EvalDef.


(***************)
(*             *)
(*  Exercises  *)
(*             *)
(***************)

Theorem ex501 : initE >> (%10) --> (#10).
Proof. Eint. Qed.

Theorem ex502 : initE >> (%-2) --> (#-2).
Proof. Eint. Qed.

Theorem ex503 : initE >> (_true) --> (#true).
Proof. Ebool. Qed.

Theorem ex504 : initE >> (%10 > %5) --> (#true).
Proof. 
  Ecomp1 (#10) (#5). 
  Eint.
  Eint.
  auto.  (* <-- to resolve an equation like
                "greater_than1 (#10) (#5) = Some true" *)
Qed.

Theorem ex505 : initE >> (%10 > %20) --> (#false).
Proof. 
  Ecomp2 (#10) (#20). 
  Eint.
  Eint.
  auto.  (* <-- to resolve an equation like
                "greater_than1 (#10) (#20) = Some false " *)
Qed.

Theorem ex506 : initE >> (%10 = %20) --> (#false).
Proof. 
  Eeq2 (#10) (#20). 
  Eint.
  Eint.
  auto.  (* <-- to resolve an equation like
                "eq_int1 (#10) (#20) = Some false " *)
Qed.

Theorem ex507 : initE >> (%10 + %20) --> (#30).
Proof. 
  Eplus (#10) (#20). 
  Eint.
  Eint.
  auto.  (* <-- to resolve an equation like
                "add1 (#10) (#20) = Some (#30)" *)
Qed.

Theorem ex508 : initE >> (%10 + (%20 + %30)) --> (#60).
Proof. 
  Eplus (#10) (#50). 
  Eint.
  Eplus (#20) (#30).
  Eint.
  Eint.
  auto.  (* <-- to resolve an equation *)
  auto.  (* <-- to resolve an equation *)
Qed.

Theorem ex509 : initE >> (%10 + (% 20 + (%30 + %40))) --> (# 100).
Proof.
  Eplus (#10) (#90).
  Eint.
  Eplus (#20) (#70).
  Eint.
  Eplus (#30) (#40).
  Eint.
  Eint.
  auto.
  auto.
  auto.
Qed.

Theorem ex510 : initE >> (_if _true then % 10 else % 20) --> (# 10).
Proof. Eif1. Ebool. Eint.  Qed.

Theorem ex511 : initE >> (_if %10 > %20 then %30 else %40)
                     --> (# 40).
Proof. 
  Eif2. 
  Ecomp2 (#10) (#20).
  Eint.  Eint.  auto.
  Eint.
Qed.

Theorem ex512 : initE >> (_if %10 = %20 then %30 else %40)
                     --> (# 40).
Proof. 
  Eif2.
  Eeq2 (#10) (#20).
  Eint.
  Eint.
  auto.
  Eint.
Qed.

(* note that, _lambda (x) %10 reduces to a closure
   and the body in the closure should be %10, not #10.
 *)
Theorem ex520 : initE >> (_lambda (x) %10) --> clos(x, %10, initE).
Proof.
  Elam.
Qed.

Theorem ex521 : initE >> (_lambda (x) x + %2) --> clos(x, x+%2, initE).
Proof.
  Elam.
Qed.

Theorem ex522 : initE >> ((_lambda (x) x) @ % 10) --> (#10).
Proof.  
  Eapp1 (x) (x) (initE) (#10).
  Eint.
  Elam. Evar. 
Qed.

Theorem ex523 : initE >> (_lambda (x) x) @ (% 10 + % (-20)) --> (# (-10)).
Proof.  
  Eapp1 (x) (x) (initE) (# (-10)).
  Eplus (#10) (#-20). Eint. Eint. auto.
  Elam. Evar. 
Qed.

(* Modified on 2015/11/23: the result should be #35, not #20.  *)
Theorem ex524 : initE >> (_lambda (x) x + %30) @ (% 10 + % (-5)) --> (#XX).
Proof.  
  (*Eapp1 (x) (x + %30) (initE) (#5).
  Eplus (#10) (#-5).
  Eint.
  Eint.
  auto.
  Elam.
  Eplus (#5) (#30).񦙦
  Evar.
  Eint.
  auto.*)
  
Qed.

Theorem ex525 : initE >> (_lambda (y) y + y) @ ((_lambda (x) x + %30) @ (% 10 + % (-5))) 
                      --> (#70).
Proof.  
  Eapp1 (y) (y + y) (initE) (#35).
  Eapp1 (x) (x + %30) (initE) (#5).
  Eplus (#10) (#-5).
  Eint.
  Eint.
  auto.
  Elam.
  Eplus (#5) (#30).
  Evar.
  Eint.
  auto.
  Elam.
  Eplus (#35) (#35).
  Evar.
  Evar.
  auto.
Qed.

Theorem ex530 : initE >> 
                  (_let x = %10 in _let y = %20 in y + x)
                     --> (# 30).
Proof.   
  Elet (#10). Eint. 
  Elet (#20). Eint. 
  Eplus (#20) (#10).
  Evar. 
  Evar. 
  auto.
Qed.

Theorem ex531 : initE >> 
                  (_let x = %10 + %20 in _let y = %30 > %40 in 
                     _if y then %0 else x)
                     --> (#30).
Proof.   
  Elet (#30). Eplus (#10) (#20). Eint. Eint. auto.
  Elet (#false). Ecomp2 (#30) (#40).  Eint. Eint. auto.
  Eif2.  Evar.  Evar.
Qed.

Theorem ex532 : initE >> 
                  (_let x = %10 in 
                   _let f = _lambda (y) x + y in
                    f @ %20)
                    --> (#30).
Proof.   
  Elet (#10). Eint. 
  Elet (clos(y,x+y, extend initE x (#10))).
  Elam.
  Eapp1 (y) (x+y) (extend initE x (#10)) (#20).
  Eint.  Evar.  
  Eplus (#10) (#20).
  Evar. Evar. auto.
Qed.

Theorem ex533 : initE >> 
                   (_let x = %1 in
                    _let f = _lambda (y) x + y in
                    _let x = %2 in
                      f @ %10)
                     --> #11.
Proof.
  Elet (#1).
  Eint.
  Elet (clos(y, x + y, extend initE x (#1))).
  Elam.
  Elet (#2).
  Eint.
  Eapp1 (y) (x + y) (extend initE x (#1)) (#10).
  Eint.
  Evar.
  Eplus (#1) (#10).
  Evar. Evar. auto.
Qed.

Theorem ex540 : initE >> 
                   (_fix f (x) 
                     _if (x = %0) then %0 else x + f @ (x + %(-1)))
                     --> 
                   rclos(f, x,
                     _if (x = %0) then %0 else x + f @ (x + %(-1)), initE).
Proof.   
  Efix.
Qed.
 
Theorem ex541 : initE >> 
                   (_fix f (x) 
                     (_if (x = %0) then %0 else (x + f @ (x + %(-1)))))
                     @ (%0)
                     --> #0.
Proof.   
  Eapp2 (f) (x) (_if (x = %0) then %0 else (x + f @ (x + %(-1)))) (initE) (#0).
  Eint.
  Efix.
  Eif1.
  Eeq1 (#0) (#0). Evar. Eint. auto. Eint.
Qed.
 
Theorem ex542 : initE >> 
                   (_fix f (x) 
                     (_if (x = %0) then %0 else (x + f @ (x + %(-1)))))
                     @ (%1)
                     --> #1.
Proof.   
  Eapp2 (f) (x) (_if (x = %0) then %0 else (x + f @ (x + %(-1)))) (initE) (#1).
  Eint.
  Efix.
  Eif2.
  Eeq2 (#1) (#0).
  Evar.
  Eint.
  auto.
  Eplus (#1) (#0).
  Evar.
  Eapp2 (f) (x) (_if (x = %0) then %0 else (x + f @ (x + %(-1)))) (initE) (#0).
  Eplus (#1) (#-1).
  Evar.
  Eint.
  auto.
  Evar.
  Eif1.
  Eeq1 (#0) (#0).
  Evar.
  Eint.
  auto.
  Eint.
  auto.
Qed.

Theorem ex543 : initE >> 
                   (_fix f (x) 
                     (_if (x = %0) then %0 else (x + f @ (x + %(-1)))))
                     @ (%2)
                     --> #3.
Proof.   
  Eapp2 (f) (x) (_if x = %0 then %0 else x + f @ (x + (%-1))) (initE) (#2).
  Eint.
  Efix.
  Eif2.
  Eeq2 (#2) (#0).
  Evar.
  Eint.
  auto.
  Eplus (#2) (#1).
  Evar.
  Eapp2 (f) (x) (_if x = %0 then %0 else x + f @ (x + (%-1))) (initE) (#1).
  Eplus (#2) (#-1).
  Evar.
  Eint.
  auto.
  Evar.
  Eif2.
  Eeq2 (#1) (#0).
  Evar.
  Eint.
  auto.
  Eplus (#1) (#0).
  Evar.
  Eapp2 (f) (x) (_if x = %0 then %0 else x + f @ (x + (%-1))) (initE) (#0).
  Eplus (#1) (#-1).
  Evar.
  Eint.
  auto.
  Evar.
  Eif1.
  Eeq1 (#0) (#0).
  Evar.
  Eint.
  auto.
  Eint.
  auto.
  auto.
Qed.

Theorem ex544 : initE >> 
                   (_fix f (x) 
                     (_if (x = %0) then %0 else (x + f @ (x + %(-1)))))
                     @ (%3)
                     --> #6.
Proof.   
  Eapp2 (f) (x) (_if (x = %0) then %0 else (x + f @ (x + %(-1)))) (initE) (#3).
  Eint.
  Efix.
  Eif2.
  Eeq2 (#3) (#0).
  Evar.
  Eint.
  auto.
  Eplus (#3) (#3).
  Evar.
  Eapp2 (f) (x) (_if (x = %0) then %0 else (x + f @ (x + %(-1)))) (initE) (#2).
  Eplus (#3) (#-1).
  Evar.
  Eint.
  auto.
  Evar.
  Eif2.
  Eeq2 (#2) (#0).
  Evar.
  Eint.
  auto.
  Eplus (#2) (#1).
  Evar.
  Eapp2 (f) (x) (_if (x = %0) then %0 else (x + f @ (x + %(-1)))) (initE) (#1).
  Eplus (#2) (#-1).
  Evar.
  Eint.
  auto.
  Evar.
  Eif2.
  Eeq2 (#1) (#0).
  Evar.
  Eint.
  auto.
  Eplus (#1) (#0).
  Evar.
  Eapp2 (f) (x) (_if (x = %0) then %0 else (x + f @ (x + %(-1)))) (initE) (#0).
  Eplus (#1) (#-1).
  Evar.
  Eint.
  auto.
  Evar.
  Eif1.
  Eeq1 (#0) (#0).
  Evar.
  Eint.
  auto.
  Eint.
  auto.
  auto.
  auto.
Qed.

(* fibonacci *)
Theorem ex545 : initE >> 
                   (_fix f (x) 
                     (_if (x = %0) then %1 else 
                      _if (x = %1) then %1 else 
                        f @ (x + %-1) + f @ (x + %-2)))
                   --> 
                   rclos(f, x,
                      _if (x = %0) then %1 else 
                      _if (x = %1) then %1 else 
                        f @ (x + %-1) + f @ (x + %-2),
                         initE).
Proof.   
  Efix.
Qed.

Theorem ex546 : initE >> 
                   (_fix f (x) 
                     (_if (x = %0) then %1 else 
                      _if (x = %1) then %1 else 
                        f @ (x + %-1) + f @ (x + %-2)))
                   @ %0
                   --> 
                   #1.
Proof.
  Eapp2 (f) (x) (_if (x = %0) then %1 else 
                      _if (x = %1) then %1 else 
                        f @ (x + %-1) + f @ (x + %-2))
        (initE)
        (#0).
  Eint. Efix.
  Eif1.
  Eeq1 (#0) (#0). Evar. Eint. auto.
  Eint.
Qed.

Theorem ex547 : initE >> 
                   (_fix f (x) 
                     (_if (x = %0) then %1 else 
                      _if (x = %1) then %1 else 
                        f @ (x + %-1) + f @ (x + %-2)))
                   @ %1
                   --> 
                   #1.
Proof.
  Eapp2 (f) (x) (_if (x = %0) then %1 else 
                      _if (x = %1) then %1 else 
                        f @ (x + %-1) + f @ (x + %-2))
        (initE)
        (#1).
  Eint. Efix.
  Eif2.
  Eeq2 (#1) (#0). Evar. Eint. auto.
  Eif1.  Eeq1 (#1) (#1).  Evar. Eint. auto.
  Eint.
Qed.

Theorem ex548 : initE >> 
                   (_fix f (x) 
                     (_if (x = %0) then %1 else 
                      _if (x = %1) then %1 else 
                        f @ (x + %-1) + f @ (x + %-2)))
                   @ %2
                   --> 
                   #2.
Proof. 
  Eapp2 (f) (x) (_if x = %0 then %1 else (_if x = %1 then %1 else f @ (x + (%-1)) + f @ (x + (%-2)))) (initE) (#2).
  Eint.
  Efix.
  Eif2.
  Eeq2 (#2) (#0).
  Evar.
  Eint.
  auto.
  Eif2.
  Eeq2 (#2) (#1).
  Evar.
  Eint.
  auto.
  Eplus (#1) (#1).
  Eapp2 (f) (x) (_if x = %0 then %1 else (_if x = %1 then %1 else f @ (x + (%-1)) + f @ (x + (%-2)))) (initE) (#1).
  Eplus (#2) (#-1).
  Evar.
  Eint.
  auto.
  Evar.
  Eif2.
  Eeq2 (#1) (#0).
  Evar.
  Eint.
  auto.
  Eif1.
  Eeq1 (#1) (#1).
  Evar.
  Eint.
  auto.
  Eint.
  Eapp2 (f) (x) (_if x = %0 then %1 else (_if x = %1 then %1 else f @ (x + (%-1)) + f @ (x + (%-2)))) (initE) (#0).
  Eplus (#2) (#-2).
  Evar.
  Eint.
  auto.
  Evar.
  Eif1.
  Eeq1 (#0) (#0).
  Evar.
  Eint.
  auto.
  Eint.
  auto.
Qed.

Theorem ex549 : initE >> 
                   (_fix f (x) 
                     (_if (x = %0) then %1 else 
                      _if (x = %1) then %1 else 
                        f @ (x + %-1) + f @ (x + %-2)))
                   @ %3
                   --> 
                   #3.
Proof. 
  Eapp2 (f) (x) (_if (x = %0) then %1 else _if (x = %1) then %1 else f @ (x + %-1) + f@(x + %-2)) (initE) (#3).
  Eint.
  Efix.
  Eif2.
  Eeq2 (#3) (#0).
  Evar.
  Eint.
  auto.
  Eif2.
  Eeq2 (#3) (#1).
  Evar.
  Eint.
  auto.
  Eplus (#2) (#1).
  Eapp2 (f) (x) (_if (x = %0) then %1 else _if (x = %1) then %1 else f @ (x + %-1) + f@(x + %-2)) (initE) (#2).
  Eplus (#3) (#-1).
  Evar.
  Eint.
  auto.
  Evar.
  Eif2.
  Eeq2 (#2) (#0).
  Evar.
  Eint.
  auto.
  Eif2.
  Eeq2 (#2) (#1).
  Evar.
  Eint.
  auto.
  Eplus (#1) (#1).
  Eapp2 (f) (x) (_if (x = %0) then %1 else _if (x = %1) then %1 else f @ (x + %-1) + f@(x + %-2)) (initE) (#1).
  Eplus (#2) (#-1).
  Evar.
  Eint.
  auto.
  Evar.
  Eif2.
  Eeq2 (#1) (#0).
  Evar.
  Eint.
  auto.
  Eif1.
  Eeq1 (#1) (#1).
  Evar.
  Eint.
  auto.
  Eint.
  Eapp2 (f) (x) (_if (x = %0) then %1 else _if (x = %1) then %1 else f @ (x + %-1) + f@(x + %-2)) (initE) (#0).
  Eplus (#2) (#-2).
  Evar.
  Eint.
  auto.
  Evar.
  Eif1.
  Eeq1 (#0) (#0).
  Evar.
  Eint.
  auto.
  Eint.
  auto.
  Eapp2 (f) (x) (_if (x = %0) then %1 else _if (x = %1) then %1 else f @ (x + %-1) + f@(x + %-2)) (initE) (#1).
  Eplus (#3) (#-2).
  Evar.
  Eint.
  auto.
  Evar.
  Eif2.
  Eeq2 (#1) (#0).
  Evar.
  Eint.
  auto.
  Eif1.
  Eeq1 (#1) (#1).
  Evar.
  Eint.
  auto.
  Eint.
  auto.
Qed.

(* multiplication *)
Theorem ex550 : initE >> 
                   (_lambda (x)
                    _fix f (y) 
                       _if (x = %0) then %0 else 
                         (f @ (y + %-1)) + x)
                   --> 
                   clos(x,
                    _fix f (y) 
                       _if (x = %0) then %0 else 
                         (f @ (y + %-1)) + x,
                        initE).
Proof. 
  Elam.
Qed.

Theorem ex551 : initE >> 
                   ((_lambda (x)
                    _fix f (y) 
                       _if (y = %0) then %0 else 
                         (f @ (y + %-1)) + x)
                    @ %10) @ %0
                   --> 
                   #0.
Proof. 
  Eapp2 (f) (y) (_if (y = %0) then %0 else f @ (y + %-1) + x)
        (extend initE x (#10))
        (#0).
  Eint. 
  Eapp1 (x) (_fix f (y) _if (y = %0) then %0 else (f @ (y + %-1)) + x) 
        (initE) (#10).
  Eint.  Elam.  Efix.
  Eif1.    
  Eeq1 (#0) (#0).  Evar. Eint.   auto.  Eint.
Qed.

Theorem ex552 : initE >> 
                   ((_lambda (x)
                    _fix f (y) 
                       _if (y = %0) then %0 else 
                         (f @ (y + %-1)) + x)
                    @ %10) @ %1
                   --> 
                   #10.
Proof. 
  Eapp2 (f) (y) (_if y = %0 then %0 else (f @ (y + (%-1)) + x)) (extend initE x (#10)) (#1).
  Eint.
  Eapp1 (x) (_fix f (y) _if (y = %0) then %0 else (f@(y + %-1)) + x) (initE) (#10).
  Eint.
  Elam.
  Efix.
  Eif2.
  Eeq2 (#1) (#0).
  Evar.
  Eint.
  auto.
  Eplus (#0) (#10).
  Eapp2 (f) (y) (_if y = %0 then %0 else (f @ (y + (%-1)) + x)) (extend initE x (#10)) (#0).
  Eplus (#1) (#-1).
  Evar.
  Eint.
  auto.
  Evar.
  Eif1.
  Eeq1 (#0) (#0).
  Evar.
  Eint.
  auto.
  Eint.
  Evar.
  auto.
Qed.

Theorem ex553 : initE >> 
                   ((_lambda (x)
                    _fix f (y) 
                       _if (y = %0) then %0 else 
                         (f @ (y + %-1)) + x)
                    @ %10) @ %2
                   --> 
                   #20.
Proof. 
  Eapp2 (f) (y) (_if y = %0 then %0 else (f @ (y + (%-1)) + x)) (extend initE x (#10)) (#2).
  Eint.
  Eapp1 (x) (_fix f (y) _if (y = %0) then %0 else (f@(y + %-1)) + x) (initE) (#10).
  Eint.
  Elam.
  Efix.
  Eif2.
  Eeq2 (#2) (#0).
  Evar.
  Eint.
  auto.
  Eplus (#10) (#10).
  Eapp2 (f) (y) (_if y = %0 then %0 else (f @ (y + (%-1)) + x)) (extend initE x (#10)) (#1).
  Eplus (#2) (#-1).
  Evar.
  Eint.
  auto.
  Evar.
  Eif2.
  Eeq2 (#1) (#0).
  Evar.
  Eint.
  auto.
  Eplus (#0) (#10).
  Eapp2 (f) (y) (_if y = %0 then %0 else (f @ (y + (%-1)) + x)) (extend initE x (#10)) (#0).
  Eplus (#1) (#-1).
  Evar.
  Eint.
  auto.
  Evar.
  Eif1.
  Eeq1 (#0) (#0).
  Evar.
  Eint.
  auto.
  Eint.
  Evar.
  auto.
  Evar.
  auto.
Qed.


Inspect 50.
