(* CoreMLEval.v *)
(* 2015/11/16 by Yukiyoshi Kameyama *)

Require Export SfLib.
Require Import Ascii String.
Require Import ZArith.
Require Import CoreMLDef.

Fixpoint lookup_env (id1 : id) (E : list (tm * valueT)) : option valueT :=
  match E with
      | (TM_var id2,v2) :: E2 => if (eq_id_dec id1 id2) then Some v2
                                 else lookup_env id1 E2
      | _  => None
  end
.
Definition extend (E : envT) id1 v1 := ((id1,v1) :: E).

Definition add1 (v1 v2 : valueT) : option valueT :=
  match (v1,v2) with
  | (V_int n1, V_int n2) => Some (V_int (n1 + n2))
  | _ => None
  end
.

Definition greater_than1 (v1 v2 : valueT) : option bool :=
  match (v1,v2) with
  | (V_int n1, V_int n2) => Some (Z.gtb n1 n2) 
  | _ => None
  end
.

Definition eq_int1 (v1 v2 : valueT) : option bool :=
  match (v1,v2) with
  | (V_int n1, V_int n2) => Some (Z.eqb n1 n2) 
  | _ => None
  end
.

Open Scope type_scope.

(* terms
   % 10       ... integer 10  (for some reason, we need %)
   _true      ... boolean literal "true"  
   _false     ... boolean literal "false" 
   t1 + t2
   t1 = t2
   t1 > t2
   (t1,t2)
   _left t1   ... left t1  (you can also write _left(t1) )
   _right t1  ... right t1
   _lambda (x) t1  ...   lambda x. t1
   _lam (x) t1     ...   lambda x. t1  
   t1 @ t2
   _if t1 then t2 else t3  ... if t1 then t2 else t3  
   _let x = t1 in t2   ...   let x = t1 in t2
   _fix x (y) t1       ...   fix x.y. t1

   Note that, we need to add "_" just before the keyword,
   e.g.,  we write _left t, and not left t.
   Also, 
       _if t1 then t2 else t3   is ok, but
       if t1 then t2 else t3   is not good, and
       _if t1 _then t2 _else t3   is not good.
*)

Notation "% t1" := (TM_int t1) (at level 50).
Notation _true := (TM_bool true).
Notation _false := (TM_bool false).
Notation "t1 + t2" := (TM_add t1 t2) (at level 50, left associativity).
Notation "t1 = t2" := (TM_eq  t1 t2) (at level 70).
Notation "t1 > t2" := (TM_gt  t1 t2) (at level 70).
Notation "( t1 , t2 )" := (TM_pair t1 t2) (at level 0).
Notation "'_left' t1 " := (TM_left t1) (at level 50).
Notation "'_right' t1 " := (TM_right t1) (at level 50).
Notation "'_lambda' ( x ) t1" := (TM_lam x t1) (at level 60).
Notation "t1 @ t2" := (TM_app t1 t2) (at level 41, left associativity).
Notation "'_if' t1 'then' t2 'else' t3" := (TM_if t1 t2 t3) (at level 73).
Notation "'_let' x = t1 'in' t2" := (TM_let x t1 t2) (at level 72, x at level 0).
Notation "'_fix' f ( x ) t1" := (TM_fix f x t1) (at level 72, f at level 0, x at level 0, left associativity).

(* values *)
Notation "# t1" := (V_int t1) (at level 50).
Notation "'#true'" := (V_bool true) (at level 50).
Notation "'#false'" := (V_bool false) (at level 50).
Notation "'#(' t1 , t2 ')'" := (V_pair t1 t2).
Notation "clos( t1 , t2 , t3 )" := (V_closure t1 t2 t3).
Notation "rclos( t1 , t2 , t3 , t4 )" := (V_rclosure t1 t2 t3 t4).

Inductive eval2 : envT -> tm -> valueT -> Prop :=
  | E_var    : forall E id1 v1,
                 (lookup_env id1 E) = Some v1
                 -> eval2 E (TM_var id1) v1
  | E_int    : forall E n1,
                 eval2 E (TM_int n1) (V_int n1)
  | E_bool   : forall E b1,
                 eval2 E (TM_bool b1) (V_bool b1)
  | E_plus    : forall E t1 t2 v1 v2 v3,
                  eval2 E t1 v1
               -> eval2 E t2 v2
               -> add1 v1 v2 = Some v3
               -> eval2 E (TM_add t1 t2) v3
  | E_comp1    : forall E t1 t2 v1 v2,
                  eval2 E t1 v1
               -> eval2 E t2 v2
               -> greater_than1 v1 v2 = Some true
               -> eval2 E (TM_gt t1 t2) (V_bool true)
  | E_comp2    : forall E t1 t2 v1 v2,
                  eval2 E t1 v1
               -> eval2 E t2 v2
               -> greater_than1 v1 v2 = Some false
               -> eval2 E (TM_gt t1 t2) (V_bool false)
  | E_eq1    : forall E t1 t2 v1 v2,
                  eval2 E t1 v1
               -> eval2 E t2 v2
               -> eq_int1 v1 v2 = Some true
               -> eval2 E (TM_eq t1 t2) (V_bool true)
  | E_eq2    : forall E t1 t2 v1 v2,
                  eval2 E t1 v1
               -> eval2 E t2 v2
               -> eq_int1 v1 v2 = Some false
               -> eval2 E (TM_eq t1 t2) (V_bool false)
  | E_if1 : forall E t1 t2 t3 v,
                  eval2 E t1 (V_bool true)
               -> eval2 E t2 v
               -> eval2 E (TM_if t1 t2 t3) v
  | E_if2 : forall E t1 t2 t3 v,
                  eval2 E t1 (V_bool false)
               -> eval2 E t3 v
               -> eval2 E (TM_if t1 t2 t3) v
  | E_pair   : forall E t1 t2 v1 v2,
                  eval2 E t1 v1
               -> eval2 E t2 v2
               -> eval2 E (TM_pair t1 t2) (V_pair v1 v2)
  | E_left   : forall E t1 v1 v2,
                  eval2 E t1 (V_pair v1 v2)
               -> eval2 E (TM_left t1) v1
  | E_right  : forall E t1 v1 v2,
                  eval2 E t1 (V_pair v1 v2)
               -> eval2 E (TM_right t1) v2
  | E_lam    : forall E x t,
                 eval2 E (TM_lam x t) (V_closure x t E)
  | E_fix : forall E f x t1,
                  eval2 E (TM_fix f x t1) (V_rclosure f x t1 E)
  | E_let : forall E x t1 t2 v1 v2,
                  eval2 E t1 v1
               -> eval2 (extend E x v1) t2 v2
               -> eval2 E (TM_let x t1 t2) v2
  | E_app1    : forall E E3 t1 x t2 v2 t3 v4,
                  eval2 E t2 v2
               -> eval2 E t1 (V_closure x t3 E3)
               -> eval2 (extend E3 x v2) t3 v4
               -> eval2 E (TM_app t1 t2) v4
  | E_app2    : forall E E3 t1 f x t2 v2 t3 v4,
                  eval2 E t2 v2
               -> eval2 E t1 (V_rclosure f x t3 E3)
               -> eval2 (extend (extend E3 f (V_rclosure f x t3 E3)) x v2) t3 v4
               -> eval2 E (TM_app t1 t2) v4
.

Notation x := (TM_var (Id 0)).
Notation y := (TM_var (Id 1)).
Notation z := (TM_var (Id 2)).
Notation u := (TM_var (Id 3)).
Notation v := (TM_var (Id 4)).
Notation w := (TM_var (Id 5)).
Notation f := (TM_var (Id 6)).
Notation g := (TM_var (Id 7)).


Ltac Evar := apply (E_var); auto.
Ltac Eint := apply (E_int).
Ltac Ebool := apply (E_bool).
Ltac Eplus V1 V2 := apply (E_plus _ _ _ V1 V2 _).
Ltac Ecomp1 V1 V2 := apply (E_comp1 _ _ _ V1 V2).
Ltac Ecomp2 V1 V2 := apply (E_comp2 _ _ _ V1 V2).
Ltac Eeq1 V1 V2 := apply (E_eq1 _ _ _ V1 V2).
Ltac Eeq2 V1 V2 := apply (E_eq2 _ _ _ V1 V2).
Ltac Eif1 := apply (E_if1).
Ltac Eif2 := apply (E_if2).
Ltac Epair V1 V2 := apply (E_pair).
Ltac Eleft V1 := apply (E_left _ _ V1 _).
Ltac Eright V2 := apply (E_right _ _ V2 _).
Ltac Elam := apply (E_lam).
Ltac Elambda := apply (E_lam).
Ltac Efix := apply (E_fix).
Ltac Eapply1 var body env argv 
  := apply (E_app1 _ env _ var _ argv body _); try (unfold extend).
Ltac Eapp1   var body env argv 
  := apply (E_app1 _ env _ var _ argv body _); try (unfold extend).
Ltac Eapp2   fvar argvar body env argv 
  := apply (E_app2 _ env _ fvar argvar _ argv body _); try (unfold extend).
Ltac Eapply2 fvar argvar body env argv 
  := apply (E_app2 _ env _ fvar argvar _ argv body _); try (unfold extend).
Ltac Elet v1 := apply (E_let _ _ _ _ v1 _); try (unfold extend).

Notation "E '>>' t1 '-->' t2" := (eval2 E t1 t2) (at level 90).
Notation initE := ([] : envT).
Definition eval t1 v1 := eval2 initE t1 v1.

