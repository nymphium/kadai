#!/usr/bin/env ruby
# encoding: utf-8
require 'cgi'
require './votesystem2'
require './sqlite3'
cgi = CGI.new
res = Hash.new(0)
q, sel = makeqs QF

dboneshot(DBF) {|db|
	sel.each {|x, _|
		res[x] = db.get_first_value("select count(*) from #{DB} where name=?;", x)
	}
}

print <<EOF
#{cgi.header("text/html; charset=utf-8")}
<html><body>
	<p> #{q.chomp}</p>
EOF

res.each {|k, v|
	puts "	<p>#{k} #{v}</p>"
}

print <<EOF
	<a href="enquete_form2.rb">back</a>
</body></html>
EOF

