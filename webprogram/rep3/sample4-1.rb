#!/usr/bin/env ruby
# encoding: utf-8
# このCGIを実行する前にプリントのRubyスクリプトからINSERT文を使う例を実行しろ

require 'cgi'
require './sqlite3'

cgi = CGI.new

begin
	number = cgi['number']
	rows = '' #検索結果

	if number.empty?
		raise "put your gakuseki number"
	end

	db = SQLite3::Database.new("test.db")
	db.transaction() {
		rows = db.execute("SELECT * FROM mytbl WHERE id like ?;", number)
	}
	db.close

	print <<-EOB
#{cgi.header("text/html; charset=UTF-8")}
	<html>
		<head>
			<meta http-equiv="Content-Type" conten="text/html; charset=UTF-8">
		</head>
		<body>
			<h1>検索結果</h1>
	EOB

	if rows.size > 0
		rows.each {|row|
			puts "<div>gakuseki num = #{row[0]}, name = #{row[1]}</div>"
		}
	else
		puts "no one has such gakuseki number"
	end

	print <<-EOB
		</body>
	</html>
	EOB
rescue => ex
	print <<-EOB
#{cgi.header("text/html; charset=utf-8")}
	<html>
		<head>
			<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		</head>
		<body>
			<h1>#{ex.message}</h1>
			<pre>
#{CGI.escapeHTML(ex.backtrace.join("\n"))}
			</pre>
		</body>
	</html>
	EOB

end
