QF = "question.txt"
DBF = "report1023.db"
DB = "votes"

def makeqs qf
	qf = open(qf, "r:UTF-8")

	question = (qf.gets).chomp
	sel = Hash.new

	while line = qf.gets
		sel[line.chomp!] = 0
	end

	qf.close

	return question, sel
end

def dboneshot dbf
	db = SQLite3::Database.new dbf
	db.transaction() {
		yield db
	}
	db.close
end
