QF = "question.txt"
VF = "vote_result.txt"

def makeqs qf
	qf = open(qf, "r:UTF-8")

	question = (qf.gets).chomp
	sel = Hash.new

	while line = qf.gets
		sel[line.chomp!] = 0
	end

	return question, sel
end

def flblock f, ba
	f.flock(File::LOCK_EX)
	yield ba
	f.flock(File::LOCK_UN)
end

