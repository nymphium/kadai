#!/usr/bin/env ruby
# encoding: utf-8
require 'cgi'
require './votesystem'
cgi = CGI.new
res = Hash.new(0)
vres = open(VF, "r:UTF-8")
q = open(QF, "r:UTF-8").gets

while line = vres.gets
	res[line.chomp!] += 1
end

vres.close

print <<EOF
#{cgi.header("text/html; charset=utf-8")}
<html><body>
	<p> #{q.chomp}</p>
EOF

res.each{|k, v|
	puts "	<p>#{k} #{v}</p>"
}

print <<EOF
	<a href="enquete_form.rb">back</a>
</body></html>
EOF

