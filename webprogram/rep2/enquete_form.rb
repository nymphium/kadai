#!/usr/bin/env ruby
# encoding: utf-8
require 'cgi'
require './votesystem'
cgi = CGI.new
q, s = makeqs QF

print <<EOF
#{cgi.header("text/html; charset=utf-8")}
<html><body>
	<p>
		投票システム<br>#{q}<br>
		<form action='vote.rb' method='get'>
EOF

s.each{|k, _|
	print <<EOF
			<div>#{k}<input type='checkbox' name='name' value='#{k}'></div>
EOF
}

print <<EOF
			<input type='submit' value='send'>
			<input type='reset' value='clear'>
		</form>
	</p>
	<br>
	<a href=".">back</a>
</body></html>
EOF

