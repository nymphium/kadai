def makeqs qf
	qf = open(qf, "r:UTF-8")

	question = (qf.gets).chomp
	sel = Hash.new

	while line = qf.gets
		line.chomp!
		sel[line] = 0
	end

	return question, sel
end

