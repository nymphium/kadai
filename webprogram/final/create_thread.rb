#!/usr/bin/env ruby
# encoding: utf-8
require'cgi'
require'./bbses'
cgi = CGI.new
thread = CGI.escapeHTML cgi["thread"]

begin
	printf <<-EOL
#{cgi.header("text/html; charset=utf-8")}
<html>
#{head}
	<body>
		#{(tag"h1")["Create Thread"]}
	EOL

	if thread.length > 0
		if File.exists?("bbsdata#{thread}.txt")
			puts (t 2) + (tag"p")["the thread \"#{thread}\" already exists."]
		else
			begin
				open("bbsdata#{thread}.txt", "w+:UTF-8"){}
				open("threadsname.txt", "a:UTF-8") {|f| f << thread + "\n"}
				puts (tag"p")["Thank you! The Thread has been built."]
			rescue
				puts (tag"div",:class=>"emph")["!!Internal Error!!"]
			end
		end
	else
		puts (t 3) +
			(tag"form",:class=>"submitForm",:action=>"create_thread.rb",:method=>"get")[(tn 3) +
				(tag"p")[(tn 4) +
					(tag"div", :class=>"inputs")["スレッド名 " +
					(tag"input", :class=>"validate",:type=>"text",:name=>"thread")[]] +
					(tn 4) + "<p/>" + (tn 4) +
					(tag"input",:type=>"submit",:value=>"submit")[] +
					(tag"input",:type=>"reset",:value=>"clear")[] +
					(tn 3)] + (tn 2)]
	end

	printf <<-EOL
		#{(tag"p")[(tag"a",:href=>"index.rb")["back"]]}
	</body>
</html>
	EOL
rescue Exception => e
	puts cgi.header("text/html; charset=utf-8")
	puts "Internal Error: "
	puts e.message
	puts e.backtrace.inspect
end

