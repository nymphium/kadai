#!/usr/bin/env ruby
# encoding: utf-8
require 'cgi'
require'./bbses'
cgi = CGI.new
threadname = CGI.escapeHTML cgi["thread"]
LT = "bbsdata#{threadname}.txt"
logfile = nil
err = false

begin
	begin
		logfile = open(LT, "r:UTF-8")
	rescue
		err = true
	end

	print <<-EOL
#{cgi.header("text/html; charset=utf-8")}
<html>
#{head [(tag"title")[threadname + " ようこそ.com"]]}
	<body>
	EOL

	if err
		printf <<-EOL
		#{(tag"h3")["!!Error"]}
		No such thread #{threadname}
		EOL
	else
		puts (t 2) + (tag"h1")[threadname] +
			(tn 2) + (tag"form",:class=>"submitForm",:action=>"update.rb",:method=>"get")[(tn 3) +
				(tag"p")[(tn 4) + (tag"div",:class=>"inputs")[(tn 5) +
					(tag"input",:type=>"submit",:value=>"submit")[" 名前: "] + (tn 5) +
					(tag"input",:name=>"name")["<p/>"] + (tn 5) +
					(tag"input",:class=>"validate",:size=>"30rem",:name=>"message")[] + (tn 5) +
					(tag"input",:id=>"thread",:name=>"thread",:value=>threadname,:style=>"display:none;")[] +
					(tn 4)] + (tn 3)] + (tn 2)] +
			(tn 2) + '<div class="thread">'

		cnt = 0

		while l = logfile.gets
			l = l.split DELIM

			puts (t 3) + (tag"div",:class=>"athread",:id=>"res#{cnt+=1}")[(tn 4) +
				(tag"p")[(tag"a",:name=>"res#{cnt}",:href=>"#res#{cnt}")[cnt] + " 名前: " +
				(tag"font",:color=>"green")[(l[0].length > 0) ? (CGI.escapeHTML l[0]) : "annonymous"]] + (tn 4) +
				(tag"p",:class=>"content")[((CGI.escapeHTML l[1]).to_s)
					.gsub(/&gt;&gt;(\d)+/, (->(x){(tag"a", {:id=>"res#{x}",:href=>"#res#{x}"})["&gt;&gt;#{x}"]})['\1'])
					.chomp!] + (tn 3)]
		end

		if cnt < 1
			puts "#{t 3}<p/>"
		end

		logfile.close
		
		puts "#{t 2}</div>"
	end

	printf <<-EOL
		#{(tag"p")[(tag"a",:href=>"index.rb")["back"]]}
	</body>
</html>
	EOL
rescue Exception => e
	puts cgi.header("text/html; charset=utf-8")
	puts "Internal Error: "
	puts e.message
	puts e.backtrace.inspect
end
