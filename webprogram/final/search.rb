#!/usr/bin/env ruby
# encoding: utf-8
require 'cgi'
require './bbses'
cgi = CGI.new
fhash = Hash.new {|h, k| h[k] = []}

def fn_to_bbsn fn
	fn.gsub /bbsdata(.+)\.txt$/, '\1'
end

begin
	searchstr = CGI.escapeHTML cgi['search']
	bbsdatas = Dir::entries(".").select {|fn| fn.match /bbsdata.*\.txt$/}
	printf <<-EOL
#{cgi.header("text/html; charset=utf-8")}
<html>
#{head [(tag "title")["検索: #{searchstr} ようこそ.com"]]}
	<body>
		<h1>検索: #{searchstr}</h1>
		EOL

		bbsdatas.each{|fn|
			cnt = 1
			File.open(fn, "r:UTF-8").each {|fl|
				if fl.match /#{searchstr}/
					name , res = fl.split '<>'
					fhash[fn] << {:name=>name, :res=>res, :num=>cnt}
				end

				cnt += 1
			}

			if fhash[fn].length > 0
				bbsname = CGI.escapeHTML fn_to_bbsn fn

				puts (tn 2) +
					->(tn){(tag"h3", :class=>"emph")[(tag "a", {:href=>"bbs.rb?thread=#{tn}"})[tn]]}[bbsname] + (tn 2) +
					'<div class="thread">'

				fhash[fn].each {|l|
					puts (t 3) + (tag"div",:class=>"athread")[
						(tn 4) + (tag"p")[
							(tag"a",:href=>"bbs.rb?thread=#{(bbsname).force_encoding"UTF-8"}#res#{l[:num]}")[l[:num]] +
							" 名前: " +
							(tag"font",:color=>"green")[((CGI.escapeHTML l[:name]).encode "UTF-8")
								.gsub("#{searchstr}", (tag"span",:class=>"highlight")[searchstr])]] +
						(tn 4) + (tag"p",:class=>"content")[
							(((CGI.escapeHTML l[:res]).encode "UTF-8")
								.gsub(/&gt;&gt;(\d)+/, (->(x){(tag"a", {:id=>"res#{x}",:href=>"#res#{x}"})["&gt;&gt;#{x}"]})['\1']))
								.gsub("#{searchstr}", (tag"span",:class=>"highlight")[searchstr])
								.chomp!] + (tn 3)]
				}

				puts"#{tn 2}</div>"
			end
		}

	printf <<-EOL
		#{(tag"p")[(tag"a",:href=>"index.rb")["back"]]}
	</body>
</html>
	EOL
rescue Exception => e
	puts cgi.header("text/html; charset=utf-8")
	puts "Internal Error: "
	puts e.message
	puts e.backtrace.inspect
end
