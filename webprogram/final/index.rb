#!/usr/bin/env ruby
# encoding: utf-8
require'cgi'
require'./bbses'
cgi = CGI.new
TH = "threadsname.txt"
threads = nil
toc = []

begin
	threads = open(TH, "r:UTF-8")

	while t = threads.gets
		t.chomp!
		toc << CGI.escapeHTML(t)
	end

	threads.close
rescue
	open(TH, "a:UTF-8"){}
end

begin
	printf <<-EOL
#{cgi.header("text/html; charset=utf-8")}
<html>
#{head [(tag "title")["ようこそ.com"]]}
	<body>
		#{
			(tag"h1")["ようこそ.com"] + (tn 2) +
			(tag"h3")["search"] + (tn 2) +
			(tag"form",:class=>"submitForm",:action=>"search.rb",:method=>"get")[(tn 3) +
				(tag"p")[(tn 4) +
					(tag"input", :class=>"validate", :name=>"search")[] + (tn 4) +
					(tag"input",:type=>"submit",:value=>"search")[] +
					(tn 3)] + (tn 2)] + (tn 2) +
			(tag"p")[(tag"a", :class=>"emph", :href=>"create_thread.rb")["create thread"]] + (tn 2) +
			(tag"h3")["Threads"]
		}
		<table>
	EOL

	toc.each{|i|
		puts (t 3) + ->(x){(tag"tr")[(tag"td")[(tag"a", :href=>"bbs.rb?thread=#{x}")[x]]]}[CGI.escapeHTML i]
	}

	printf <<-EOL
		</table>
	</body>
</html>
	EOL
rescue Exception => e
	puts cgi.header("text/html; charset=utf-8")
	puts "Internal Error: "
	puts e.message
	puts e.backtrace.inspect
end

