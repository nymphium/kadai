#!/usr/bin/env ruby
# encoding: utf-8
require 'cgi'
require './bbses'
cgi = CGI.new
threadname = cgi["thread"]
LT = "bbsdata#{threadname}.txt"
data = nil

begin
	data = open(LT, "a:UTF-8")
rescue
	open(LT){}
	data = open(LT, "a:UTF-8")
end

name = cgi["name"] + DELIM + cgi["message"] + "\n"

begin
	data.write(name)
	data.close

	puts cgi.header("text/html; charset=utf-8"),
		(tag"html")[(tn 1) + head + (tag"title")["Thank you"] +
			(tag"body")[(tag"p",:class=>"emph")["Thank you"] +
				(tag"p")["written to #{threadname}"] +
				(tag"a",:href=>"bbs.rb?thread=#{threadname}")["back"]]]
rescue Exception => e
	puts cgi.header("text/html; charset=utf-8")
	puts "Internal Error: "
	puts e.message
	puts e.backtrace.inspect
end
