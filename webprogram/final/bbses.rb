# encoding: utf-8

DELIM = "<>"

def tag t, a = []
	atr = ""

	a.each {|x, y|
		atr += " #{x}=\"#{y}\""
	}

	->(c = "") {"<#{t}#{atr}>#{c}</#{t}>"}
end

def t n
	"	" * n
end

def tn n
	"\n" + (t n)
end

def head t = []
	etags = ""

	t.each {|x|
		etags += x + " "
	}

	<<-EOL
	<head>
		#{(tag"script", :src=>"http://code.jquery.com/jquery-2.1.4.min.js")[]}
		#{(tag"script", :src=>"validate.js")[]}
		#{(tag"link",:rel=>"stylesheet",:href=>"style.css",:type=>"text/css")[]}
		#{(tag"link",:rel=>"shortcut icon",:type=>"image/x-icon",:href=>"favicon.ico")[]}
		#{etags}
	</head>
	EOL
end

