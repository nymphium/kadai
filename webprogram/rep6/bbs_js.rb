#!/usr/bin/env ruby
# encoding: utf-8
require 'cgi'
cgi = CGI.new
data = open("bbsdata.txt", "r:UTF-8")

print <<EOF
#{cgi.header("text/html; charset=utf-8")}
<html>
<head>
<script src="http://code.jquery.com/jquery-2.1.4.min.js"></script>
<script>
$(function(){
	$("form#submitForm").submit(function(){
		if($("input#message").val() == "" || $("input#name").val() == ""){
			alert("insert your name and message");
			return false;
		}

		if($("input#message").val().match(/<.+>/) || $("input#name").val().match(/<.+>/)){
			alert("you can't use HTML tag");
			return false;
		}

		return true;
	})
});
</script>
</head>
<body>
	<form id="submitForm" action="update.rb" method="get">
		<p>
			メッセージをどうぞ
			<div>お名前<input id="name" type="text" name="name"></div>
			<div>メッセージ<input id="message" type="text" name="message"></div>
			<br>
			<input type="submit" value="submit">
			<input type="reset" value="clear">
		</p>
	</form>
	<pre>#{CGI.escapeHTML(data.read)}</pre>
	<a href=".">back</a>
</body></html>
EOF

