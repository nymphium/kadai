package chap3;
import java.io.*;
import stone.*;

public class FileLexerRunner {
	public static void main(String[] args) throws ParseException, FileNotFoundException {
		Lexer l = new Lexer(CodeDialog.file());

		for(Token t; (t = l.read()) != Token.EOF;)
			System.out.println("=> " + t.getText());
	}
}
