package stone;
import static stone.Parser.rule;
import java.util.HashSet;
import stone.ast.*;

public class ForParser extends ClosureParser {
	public ForParser() {
		Parser breakline = rule().sep(Token.EOL);
		reserved.add("for");

		statement.insertChoice(rule(ForStmt.class)
				.sep("for")
				.ast(expr).repeat(breakline).sep(";")
				.ast(expr).repeat(breakline).sep(";")
				.ast(expr).repeat(breakline)
				.ast(block));
	}
}

