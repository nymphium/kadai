package stone;
import static stone.Parser.rule;
import stone.Parser.Operators;
import stone.ast.*;

public class MyBasicParser extends BasicParser {
	public MyBasicParser() {
		statement0.reset();

		Parser elst0 = rule();
		Parser elst = elst0.repeat(rule(IfStmnt.class).sep("elsif").ast(expr).ast(block)
				.or(elst0, rule().option(rule().sep("else").ast(block))));

		statement = statement0.or(
				rule(IfStmnt.class).sep("if").ast(expr).ast(block).ast(elst),
				rule(WhileStmnt.class).sep("while").ast(expr).ast(block),
				simple);

		program = rule().or(statement, rule(NullStmnt.class))
			.sep(";", Token.EOL);
	}
}
