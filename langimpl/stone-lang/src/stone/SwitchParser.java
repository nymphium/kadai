package stone;
import static stone.Parser.rule;
import java.util.HashSet;
import stone.ast.*;

public class SwitchParser extends ClosureParser {
	private Parser breakline = rule().sep(Token.EOL);

	private Parser arm = rule().sep("case").number(NumberLiteral.class)
		.repeat(breakline)
		.ast(block)
		.repeat(breakline);

	public SwitchParser() {
		reserved.add("switch");
		reserved.add("case");
		statement.insertChoice(rule(SwitchStmt.class)
				.sep("switch").ast(expr).repeat(breakline)
				.sep("{").repeat(breakline)
				.ast(arm)
				.repeat(arm)
				.repeat(breakline)
				.sep("}"));
	}
}

