package stone.ast;
import java.util.List;
import stone.*;

public class ForStmt extends ASTList {
	public ForStmt(List<ASTree> c) { super(c); }
	public ASTree start() { return child(0); }
	public ASTree end() { return child(1); }
	public ASTree degree() { return child(2); }
	public ASTree body() { return child(3); }
	public String toString() {
		return "(for " + start() + " " + end() + " " + degree() + " " + body() + ")";
	}
}

