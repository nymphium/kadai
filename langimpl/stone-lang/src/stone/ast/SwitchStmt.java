package stone.ast;
import java.util.*;
import stone.*;
import stone.ast.NumberLiteral;

public class SwitchStmt extends ASTList {
	public class CaseArm {
		public int pattern;
		public ASTree body;
		public CaseArm(int c, ASTree b) {
			pattern = c;
			body = b;
		}

		public String toString() {
			return "(case " + pattern + " " + body + ")";
		}
	}

	public SwitchStmt(List<ASTree> c) { super(c); }
	public ASTree condition() { return child(0); }
	public List<CaseArm> cases() {
		List<CaseArm> ret = new ArrayList<CaseArm>();
		for(int i = 1; i < numChildren(); i++) {
			int pat = Integer.parseInt(child(i).child(0).toString());
			ASTree body = child(i).child(1);
			ret.add(new CaseArm(pat, body));
		}

		return ret;
	}

	private String casesToString() {
		List<CaseArm> cs = cases();
		int size = cs.size();
		String ret = "";

		for(int i = 0; i < size; i++) {
			ret += cs.get(i);
			if(i < (size - 1)) {
				ret += " ";
			}
		}

		return ret;
	}

	public String toString() {
		return "(switch " + condition() + " " + casesToString() + ")";
	}
}

