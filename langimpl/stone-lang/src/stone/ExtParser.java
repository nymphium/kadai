package stone;
import static stone.Parser.rule;
import java.util.HashSet;
import stone.ast.*;

public class ExtParser extends ClosureParser {
	public ExtParser() {
		Parser breakline = rule().sep(Token.EOL);

		Parser arm = rule().sep("case").number(NumberLiteral.class)
			.repeat(breakline)
			.ast(block)
			.repeat(breakline);

		reserved.add("for");
		reserved.add("switch");
		reserved.add("case");

		statement.insertChoice(rule(ForStmt.class)
				.sep("for")
				.ast(expr).repeat(breakline).sep(";")
				.ast(expr).repeat(breakline).sep(";")
				.ast(expr).repeat(breakline)
				.ast(block));

		statement.insertChoice(rule(SwitchStmt.class)
				.sep("switch").ast(expr).repeat(breakline)
				.sep("{").repeat(breakline)
				.ast(arm)
				.repeat(arm)
				.repeat(breakline)
				.sep("}"));
	}
}

