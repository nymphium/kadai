package stone;
import static stone.Parser.rule;
import stone.ast.*;

public class VariableParser extends FuncParser {
	Parser vardef = rule(VariableStmnt.class)
		.sep("variable").identifier(reserved);
    public VariableParser() {
        program.insertChoice(vardef);
    }
}
