package chap4;

import java.util.*;
import stone.*;
import stone.ast.*;

public class TreeBuilder {
	public static String ops = "[\\+\\*-/]";

	public static void main(String[] args) throws ParseException {
		Lexer l = new Lexer(new CodeDialog());
		LinkedList<ASTree> stack = new LinkedList<ASTree>();
		for (Token t; (t = l.read()) != Token.EOF; ) {
			if (t.isNumber()) {
				stack.push(new NumberLiteral(t));
			} else {
				if (t.getText().matches(ops)) {
					ArrayList<ASTree> acc = new ArrayList<ASTree>();
					ASTree tl = stack.pop();
					ASTree tr = stack.pop();
					acc.add(tr);
					acc.add(new ASTLeaf(t));
					acc.add(tl);
					stack.push(new BinaryExpr(acc));
				}
			}
		}
		for (ASTree tree : stack) {
			System.out.println("=> " + tree);
		}
	}
}
