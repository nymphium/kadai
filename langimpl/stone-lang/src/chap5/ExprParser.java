package chap5;
import static stone.Parser.rule;

import stone.*;
import stone.ast.*;

public class ExprParser {
	Parser expr0  = rule();

	Parser factor = rule().or(rule().token("-").number(), rule().number(), rule().sep("(").ast(expr0).sep(")"));
	Parser term   = rule().ast(factor).repeat(rule().option(rule().token("*", "/").ast(factor)));
	Parser expr   = expr0.ast(term).repeat(rule().option(rule().token("+", "-").ast(term)));
	Parser eol    = rule().sep(Token.EOL);
	Parser top    = rule().ast(expr).repeat(rule().ast(eol));

	public ASTree parse(Lexer lexer) throws ParseException {
		return top.parse(lexer);
	}
}
