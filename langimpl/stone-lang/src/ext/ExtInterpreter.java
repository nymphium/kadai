package ext;
import stone.ExtParser;
import stone.ParseException;
import chap6.BasicInterpreter;
import chap7.NestedEnv;
import chap8.Natives;

public class ExtInterpreter extends BasicInterpreter {
	public static void main(String[] args) throws ParseException {
		run(new ExtParser(),
				new Natives().environment(new NestedEnv()));
	}
}
