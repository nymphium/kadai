package ext;
import javassist.gluonj.util.Loader;
import chap7.ClosureEvaluator;
import chap8.NativeEvaluator;

public class ExtRunner {
	public static void main(String[] args) throws Throwable {
		Loader.run(ExtInterpreter.class, args, ExtEvaluator.class);
	}
}
