package ext3;
import stone.ast.*;
import stone.*;
import stone.ForParser;

public class ForParserRunner {
	public static void main(String[] args) throws ParseException {
		Lexer lex = new Lexer(new CodeDialog());
		BasicParser bp = new ForParser();
		while (lex.peek(0) != Token.EOF) {
			ASTree ast = bp.parse(lex);
			System.out.println("=> " + ast.toString());
		}
	}
}

