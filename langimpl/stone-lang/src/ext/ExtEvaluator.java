package ext;
import java.util.*;
import stone.StoneException;
import stone.ast.*;
import javassist.gluonj.*;
import chap6.Environment;
import chap6.BasicEvaluator.*;
import chap8.NativeEvaluator;

@Require(NativeEvaluator.class)
@Reviser public class ExtEvaluator {
	public static final int TRUE = 1;
	public static final int FALSE = 0;

	@Reviser public static class ForEx extends ForStmt {
		public ForEx(List<ASTree> c) { super(c); }
		public Object eval(Environment env) {
			Object start = ((ASTreeEx)start()).eval(env);
			Object result = 0;

			for(;;) {
				Object c = ((ASTreeEx)end()).eval(env);

				if (c instanceof Integer && ((Integer)c).intValue() == FALSE) {
					return result;
				}

				result = ((ASTreeEx)body()).eval(env);
				((ASTreeEx)degree()).eval(env);
			}
		}
	}

	@Reviser public static class SwitchEx extends SwitchStmt {
		public SwitchEx(List<ASTree> c) { super(c); }
		public Object eval(Environment env) {
			Object cond = ((ASTreeEx)condition()).eval(env);
			if (!(cond instanceof Integer)) {
				throw new StoneException("switchee expression must be a number");
			}

			List<CaseArm> arms = cases();

			for(CaseArm c : arms) {
				if (c.pattern == (int)cond) {
					return ((ASTreeEx)c.body).eval(env);
				}
			}

			return 0;
		}
	}
}

