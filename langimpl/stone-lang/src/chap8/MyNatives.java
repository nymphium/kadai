package chap8;
import java.lang.reflect.Method;
import javax.swing.JOptionPane;
import stone.StoneException;
import chap6.Environment;

public class MyNatives extends Natives {
	public Environment environment(Environment env) {
		appendNatives(env);
		append(env, "charAt", MyNatives.class, "charAt", String.class, int.class);
		append(env, "nativeFib", MyNatives.class, "nativeFib", int.class);
		return env;
	}

	public static String charAt(String s, int i) {
		return "" + s.charAt(i);
	}

	public static int nativeFib(int i) {
		if (i < 2) return i;
		return nativeFib(i - 1) + nativeFib(i - 2);
	}
}
