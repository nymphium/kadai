package chap7;
import javassist.gluonj.*;
import stone.VariableParser;
import stone.ast.*;
import java.util.*;
import chap6.Environment;
import chap7.NestedEnv;
import chap7.FuncEvaluator.*;

@Require(FuncEvaluator.class)
@Reviser public class VariableEvaluator {
    @Reviser public static class VariableStmntEx extends VariableStmnt {
		public VariableStmntEx(List<ASTree> c) {super(c);}
		public Object eval(Environment env) {
			((NestedEnv)env).putNew(name(), 0);
			return name();
		}
    }
}
