% - list.pl

append([], L, L).
append([X|L1], L2, [X|L3]) :- append(L1, L2, L3).

% ?- append([1,2,3], [4,5,6], X).
% ?- append(X, Y, [1,2,3,4,5,6]).

% slow reverse
reverse1([], []).
reverse1([X|L1], L3) :- reverse1(L1, L2), append(L2, [X], L3).

% ?- reverse1([1,2,3], X).
% ?- reverse1(X, [1,2,3]).
% ?- reverse1([1,2,3], X), reverse1(X, Y).

% fast reverse
reverse2(L1, L2) :- reverse2sub(L1, [], L2).
reverse2sub([], L, L).
reverse2sub([X|L1], L2, L3) :- reverse2sub(L1, [X|L2], L3).

% perhaps very slow quick-sort
qsort([], []).
qsort([X|Lin], Lout) :- partition(X, Lin, L1, L2), 
                        qsort(L1, L1sorted), qsort(L2, L2sorted), 
                        append(L1sorted,[X|L2sorted],Lout).

partition(_, [], [], []).
partition(X, [Y|L1], [Y|L2], L3) :- greaterthan(X, Y), partition(X,L1,L2,L3).
partition(X, [Y|L1], L2, [Y|L3]) :- lessorequal(X, Y), partition(X,L1,L2,L3).

greaterthan(s(_),0).
greaterthan(s(X),s(Y)) :- greaterthan(X,Y).

lessorequal(0,_).
lessorequal(s(X),s(Y)) :- lessorequal(X,Y).

% quick-sort using "cut" operator

qsort2([], []).
qsort2([X|Lin], Lout) :- partition2(X, Lin, L1, L2), 
                        qsort2(L1, L1sorted), qsort2(L2, L2sorted), 
                        append(L1sorted,[X|L2sorted],Lout).

partition2(_, [], [], []).
partition2(X, [Y|L1], [Y|L2], L3) :- greaterthan(X, Y), !, partition2(X,L1,L2,L3).
partition2(X, [Y|L1], L2, [Y|L3]) :- partition2(X,L1,L2,L3).

% ?- qsort2([s(s(s(0))),s(0),s(s(0)),0],X).

