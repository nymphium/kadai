add(0,Y,Y).
add(s(X),Y,s(Z)):-add(X,Y,Z).

subtract(X,0,X).
subtract(s(X),s(Y),Z) :- subtract(X,Y,Z).
subtract(0,_,0).

times(0,_,0).
times(s(X),Y,Z) :- times(X,Y,W), add(W,Y,Z).

% int -> s(..s(0)..)
gen_num(X,Y):-X = 0, !, Y=0.
gen_num(X,s(Y1)):-X1 is X - 1, gen_num(X1, Y1).

% s(..s(0)..) -> int
gen_int(0,0).
gen_int(s(X),Y1) :-gen_int(X,Y), Y1 is Y + 1.


% X < Y
less(0,s(_)).
less(s(X),s(Y)) :- less(X,Y).

div(X,Y,0) :- less(X,Y).
div(X,X,s(0)).
div(X,Y,s(W)) :- less(Y,X),subtract(X,Y,Z),div(Z,Y,W).

% 1-1
isAdd3(X,Y):-add(X,Y,s(s(s(0)))),add(Y,X,s(s(s(0)))).
% 1-2
isTimes6(X):-times(X,s(s(s(0))),s(s(s(s(s(s(0))))))),!.
% 1-3
isAdd5Times6(X,Y):-add(X,Y,s(s(s(s(s(0)))))),times(X,s(s(s(0))),s(s(s(s(s(s(0))))))).

% 2-1
mul3(X,Y,Z,U):-times(X,Y,W),times(W,Z,U).

% 2-2
pow(_,0,s(0)).
pow(X,s(Y),Z):-pow(X,Y,Zm),times(X,Zm,Z).

% 2-3
% X % Y = Z
mod(_,s(0),0).
mod(X,Y,Z):-div(X,Y,Z1),times(Y,Z1,Z2),subtract(X,Z2,Z).

gcd(X,0,X).
gcd(X,X,X).
gcd(X,Y,Z):-mod(X,Y,Y2),gcd(Y,Y2,Z).

% gcdAll(_,s(0)).
gcdAll(X,s(s(0))):-gcd(X,s(s(0)),s(0)).
gcdAll(X,s(s(Y))):-gcd(X,s(s(Y)),Z),!,less(Z,s(s(0))),gcdAll(X,Y).

isPrime(s(s(0))).
isPrime(s(s(s(0)))).
isPrime(s(X)):-mod(s(X),s(s(0)),s(0)),gcdAll(s(X),X),!.

% nextPrime(0,s(s(0))).
nextPrime(s(s(0)),s(s(s(0)))).
nextPrime(P,s(Pn)):-less(P,s(Pn)),isPrime(P),isPrime(s(Pn)),gen_int(s(P),Pint),gen_int(Pn,Pnint),forall(between(Pint,Pnint,I),(gen_num(I,X),\+(isPrime(X)))).

% X is Yth prime number.
prime(s(0),s(s(0))).
prime(s(X),P):-nextPrime(Pb,P),prime(X,Pb),!.
