%
%  prime.ml
%

lt(0,s(_)).
lt(s(X),s(Y)) :- lt(X,Y).

ge(_,0).
ge(s(X),s(Y)) :- ge(X,Y).

add(X,0,X).
add(X,s(Y),s(Z)) :- add(X,Y,Z).

sub(X,0,X).
sub(s(X),s(Y),Z) :- sub(X,Y,Z).

mod(X,Y,X) :- lt(X,Y).
mod(X,Y,Z) :- ge(X,Y),sub(X,Y,X1),mod(X1,Y,Z).

%%%%%%%%%% Vesion 1 %%%%%%%%%%%%%

isNonDivisible(_,Y,Y).
isNonDivisible(X,Y,s(Z)) :- mod(X,Z,s(_)),isNonDivisible(X,Y,Z).
isPrime(s(s(X))) :- isNonDivisible(s(s(X)),s(s(0)),s(s(X))).

isDivisible(X,Y,s(Z)) :- lt(Y,s(Z)),mod(X,Z,0).
isDivisible(X,Y,s(Z)) :- isDivisible(X,Y,Z).
isNonPrime(s(s(X))) :- isDivisible(s(s(X)),s(s(0)),s(s(X))).

nthPrime(s(0),s(s(0))).
nthPrime(s(s(X)),s(s(s(Y)))) :- nthPrime(s(X),s(s(Y))),isPrime(s(s(s(Y)))).
nthPrime(s(s(X)),s(s(s(Y)))) :- nthPrime(s(s(X)),s(s(Y))),isNonPrime(s(s(s(Y)))).

% nthPrime(s(0),X)  
% X = s(s(0)) 
% nthPrime(s(s(0)),X)  
% X = s(s(s(0))) 
% nthPrime(s(s(s(0))),X)  
% X = s(s(s(s(s(0))))) 
% nthPrime(s(s(s(s(0)))),X) 
% does not terminate

%%%%%%%%%% Vesion 2 %%%%%%%%%%%%%

nextPrime(X,X) :- isPrime(X).
nextPrime(X,Y) :- isDivisible(X,s(s(0)),X),nextPrime(s(X),Y).

nthPrime2(s(0),s(s(0))).
nthPrime2(s(s(X)),W) :- nthPrime2(s(X),s(s(Y))),nextPrime(s(s(s(Y))),W).

% ?- nthPrime2(s(0),X).
% X = s(s(0)) 
% 
% ?- nthPrime2(s(s(0)),X).
% X = s(s(s(0))) 
% 
% ?- nthPrime2(s(s(s(0))),X).
% X = s(s(s(s(s(0))))) 
% 
% ?- nthPrime2(s(s(s(s(0)))),X).
% X = s(s(s(s(s(0))))) 
%
% ?- nthPrime2(s(s(s(s(0)))),X).
% X = s(s(s(s(s(s(s(0))))))) 
% 
% ?- nthPrime2(s(s(s(s(s(0))))),X).
% X = s(s(s(s(s(s(s(s(s(s(...)))))))))) 
% 
% ?- nthPrime2(s(s(s(s(s(s(0)))))),X).
% X = s(s(s(s(s(s(s(s(s(s(...)))))))))) 
%  ...
% ?- nthPrime2(s(s(s(s(s(s(s(s(s(s(0)))))))))),X).
% |    X = s(s(s(s(s(s(s(s(s(s(...)))))))))) w
% X = s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(s(0))))))))))))))))))))))))))))) 
% (10th prime is 29)
% typing "w" lets the system to print the whole term

