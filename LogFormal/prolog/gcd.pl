%
% 
%

leq(0,X).
leq(s(X),s(Y)) :- leq(X,Y).

greater(s(X),0).
greater(s(X),s(Y)) :- greater(X,Y).

subtract(X,0,X).
subtract(s(X),s(Y),Z) :- subtract(X,Y,Z).

gcd(0,Y,Y).
gcd(X,0,X).
gcd(s(X),s(Y),Z) :- leq(X,Y), subtract(Y,X,W), gcd(s(X),W,Z).
gcd(s(X),s(Y),Z) :- greater(X,Y), subtract(X,Y,W), gcd(W,s(Y),Z).

%  gcd(s(s(s(s(s(0))))),s(0),X).
%  gcd(s(s(s(s(s(s(0)))))),s(s(0)),X).
%  gcd(s(s(s(s(s(s(s(s(s(s(s(s(0)))))))))))),s(s(s(s(s(s(s(s(s(0))))))))),X).
