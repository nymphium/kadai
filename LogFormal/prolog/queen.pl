% queen.pl - N queen problem

choose([X|L1], X, L1).
choose([X|L1], Y, [X|L2]) :- choose(L1, Y, L2).

permute([], []).
permute(L1, [X|L3]) :- choose(L1, X, L2), permute(L2, L3).

ok([]). 
ok([X|L1]) :- no_catch1(X, L1), no_catch2(X, L1), ok(L1).

neq(0,s(_)).
neq(s(_),0).
neq(s(X),s(Y)) :- neq(X,Y).

no_catch1(_, []).
no_catch1(X, [Y|L1]) :- neq(s(X),Y), no_catch1(s(X), L1).

no_catch2(_, []).
no_catch2(0, _).
no_catch2(s(X), [Y|L2]) :- neq(X,Y), no_catch2(X, L2).

queen(L1, L2) :- permute(L1, L2), ok(L2).

% ?- queen([0,s(0),s(s(0)),s(s(s(0))),s(s(s(s(0)))),s(s(s(s(s(0))))),s(s(s(s(s(s(0)))))),s(s(s(s(s(s(s(0)))))))],X).


% some cosmetics

gen_num(X, Y) :- X = 0, !, Y=0. 
gen_num(X, s(Y1) ) :- X1 is X - 1, gen_num(X1, Y1).

gen_nums([],[]).
gen_nums([X|L1],[Y|L2]) :- gen_num(X,Y), gen_nums(L1,L2).

gen_int(0,0).
gen_int(s(X),Y1) :-gen_int(X,Y), Y1 is Y + 1.

gen_ints([],[]).
gen_ints([X|L1],[Y|L2]) :- gen_int(X,Y), gen_ints(L1,L2).

queen2(L1, L2) :- gen_nums(L1, L11), queen(L11, L22), gen_ints(L22, L2).

% ?- queen2([0,1,2,3,4,5,6,7],X).
