% magic.pl - 3*3 magic square

% the following definitino doesn't work
% add3(X,Y,Z,W) :- add(X,Y,XY), add(Z,XY,W).

% so directly define add3
add3(0,0,Z,Z).
add3(0,s(Y),Z,s(W)) :- add3(0,Y,Z,W).
add3(s(X),Y,Z,s(W)) :- add3(X,Y,Z,W).

% "not equal"
neq(0,s(_)).
neq(s(_),0).
neq(s(X),s(Y)) :- neq(X,Y).

% the main predicate
magic_square(X11,X12,X13,X21,X22,X23,X31,X32,X33) :-
add3(X11,X12,X13,s(s(s(s(s(s(s(s(s(s(s(s(0))))))))))))),
add3(X21,X22,X23,s(s(s(s(s(s(s(s(s(s(s(s(0))))))))))))),
add3(X31,X32,X33,s(s(s(s(s(s(s(s(s(s(s(s(0))))))))))))),
add3(X11,X21,X31,s(s(s(s(s(s(s(s(s(s(s(s(0))))))))))))),
add3(X12,X22,X32,s(s(s(s(s(s(s(s(s(s(s(s(0))))))))))))),
add3(X13,X23,X33,s(s(s(s(s(s(s(s(s(s(s(s(0))))))))))))),
add3(X11,X22,X33,s(s(s(s(s(s(s(s(s(s(s(s(0))))))))))))),
add3(X13,X22,X31,s(s(s(s(s(s(s(s(s(s(s(s(0))))))))))))),
neq(X11,X12),
neq(X11,X13),
neq(X11,X21),
neq(X11,X22),
neq(X11,X23),
neq(X11,X31),
neq(X11,X32),
neq(X11,X33),
neq(X12,X13),
neq(X12,X21),
neq(X12,X22),
neq(X12,X23),
neq(X12,X31),
neq(X12,X32),
neq(X12,X33),
neq(X13,X21),
neq(X13,X22),
neq(X13,X23),
neq(X13,X31),
neq(X13,X32),
neq(X13,X33),
neq(X21,X22),
neq(X21,X23),
neq(X21,X31),
neq(X21,X32),
neq(X21,X33),
neq(X22,X23),
neq(X22,X31),
neq(X22,X32),
neq(X22,X33),
neq(X23,X31),
neq(X23,X32),
neq(X23,X33),
neq(X31,X32),
neq(X31,X33),
neq(X32,X33).

% test run
%
% magic_square(s(0),s(s(s(s(s(s(s(s(0)))))))),s(s(s(0))),s(s(s(s(s(s(0)))))),s(s(s(s(0)))),s(s(0)),s(s(s(s(s(0))))),0,s(s(s(s(s(s(s(0)))))))).
%
% magic_square(A,B,C,D,s(s(s(s(0)))),F,G,H,I).
%
% magic_square(A,B,C,D,E,F,G,H,I).
%

% some cosmetics

gen_int(0,0).
gen_int(s(X),Y1) :-gen_int(X,Y), Y1 is Y + 1.

gen_ints([],[]).
gen_ints([X|L1],[Y|L2]) :- gen_int(X,Y), gen_ints(L1,L2).

magic_square2(_) :- magic_square(A1,B1,C1,D1,E1,F1,G1,H1,I1),
                    gen_ints([A1,B1,C1,D1,E1,F1,G1,H1,I1], [A,B,C,D,E,F,G,H,I]),
                    write(A),write(' '),write(B),write(' '),write(C), nl,
                    write(D),write(' '),write(E),write(' '),write(F), nl,
                    write(G),write(' '),write(H),write(' '),write(I), nl.

% magic_square2(0).

