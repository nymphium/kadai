%
% linear.pl
%

eq(X,X).

add(0,Y,Y).
add(s(X),Y,s(Z)) :- add(X,Y,Z).

times(0,_,0).
times(s(X),Y,Z) :- times(X,Y,W), add(W,Y,Z).

% nat -> s(s(...)) 
gen_num(X, Y) :- X = 0, !, Y=0. 
gen_num(X, s(Y1) ) :- X1 is X - 1, gen_num(X1, Y1).

% s(s(...)) -> nat
gen_int(0,0).
gen_int(s(X),Y1) :-gen_int(X,Y), Y1 is Y + 1.


% linear equations
% ( 1 3 ) (X) = ( 4 )
% ( 2 4 ) (Y)   ( 6 )

solve(X,Y) :-
eq(A11,s(0)),
eq(A12,s(s(s(0)))),
eq(A21,s(s(0))),
eq(A22,s(s(s(s(0))))),
eq(B1,s(s(s(s(0))))),
eq(B2,s(s(s(s(s(s(0))))))),
times(A11,X,Z11),
times(A12,Y,Z12),
add(Z11,Z12,B1),
times(A21,X,Z21),
times(A22,Y,Z22),
add(Z21,Z22,B2).

% ?- solve(X,Y).
% X = s(0),
% Y = s(0)

