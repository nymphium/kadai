%
% basic.pl
%

%  「%」の記号から後はコメントである。

human(alice).
human(bob).
human(chris).

%  Goal の例。
%    ?- human(alice).
%    ?- human(X).
%    ?- human(kameyama).

% adj(X,Y) means X is adjacent to Y.
% reach(X,Y) means X is reachable from Y.

adj(tokyo,ibaraki).  
adj(tokyo,saitama).
adj(saitama,tochigi).
adj(ibaraki,saitama).
adj(ibaraki,tochigi).
adj(ibaraki,fukushima).
adj(tochigi,fukushima).
reach(X,Y) :- adj(X,Y).
reach(X,Z) :- adj(X,Y), reach(Y,Z).

%  Goal の例。
%    ?- reach(tokyo,fukushima).
%    ?- reach(tokyo,Y).
%    ?- reach(X,fukusihma).
%    ?- reach(X,Y).
%    ?- reach(bob,alice).

add(0,Y,Y).
add(s(X),Y,s(Z)) :- add(X,Y,Z).

%  Goal の例。
%    ?- add(0,0,0).                          %%% 0+0 = 0
%    ?- add(s(s(0)), s(s(s(0))), X).         %%% 2+3 = X
%    ?- add(s(s(0)), X, s(s(s(s(s(0)))))).   %%% 2+X = 5
%    ?- add(X, Y, s(s(s(s(s(0)))))).         %%% X+Y = 5
times(0,_,0).
times(s(X),Y,Z) :- times(X,Y,W), add(W,Y,Z).

%  Goal の例。
%    ?- times(s(s(0)), s(s(s(0))), X).            %%% 2*3 = X
%    ?- times(s(s(0)), X, s(s(s(s(s(s(0))))))).   %%% 2*X = 6
%    ?- times(X, Y, s(s(s(s(s(s(0))))))).         %%% X*Y = 6  %%% may not terminate


% convert a natural number (0,1,2,3,...) to s(s(..(0)..)) 
gen_num(X, Y) :- X = 0, !, Y=0. 
gen_num(X, s(Y1) ) :- X1 is X - 1, gen_num(X1, Y1).

% convert s(s(..(0)..)) to a natural number (0,1,2,3,...)
gen_int(0,0).
gen_int(s(X),Y1) :-gen_int(X,Y), Y1 is Y + 1.

% ?- gen_num(12,X).
% ?- gen_int(s(s(s(0))),X).

% less than
less(0,s(_)).
less(s(X),s(Y)) :- less(X,Y).

% ?- less(s(s(s(0))), s(s(0))).           ( 3 < 2 ?)
% ?- less(s(s(s(0))), s(s(s(s(0))))).     ( 3 < 4 ?)

% subtract 
subtract(X,0,X).
subtract(s(X),s(Y),Z) :- subtract(X,Y,Z).
subtract(0,_,0).

% ?- subtract(s(s(s(0))), s(s(0)), s(0)).        ( 3 - 2 = 1)

% div
div(X,Y,0) :- less(X,Y).
div(X,X,s(0)).
div(X,Y,s(W)) :- less(Y,X),subtract(X,Y,Z),div(Z,Y,W).

% ?- div(s(s(s(s(s(s(s(0))))))), s(s(0)), s(s(s(0)))).  (7 / 2 = 3)
