pow(N, 0, 1).
pow(N, M, X) :- M > 0, M1 is M - 1, pow(N, M1, X1), X is X1 * N.
