cflags-y := -Wall
DIR = /lib/modules/$(shell uname -r)/build
MAKE = make

TARGET ?= devone

CFILES = $(TARGET).c
OBJFILE = $(TARGET).o

obj-m += $(OBJFILE)
sample-objs := $(CFILES:.c=.o)

all:
	$(MAKE) -C $(DIR) M=$(PWD) modules

clean:
	make -C /lib/modules/$(shell uname -r)/build M=$(PWD) clean
