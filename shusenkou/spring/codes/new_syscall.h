#ifndef __LINUX_NEW_SYSCALL_H
#define __LINUX_NEW_SYSCALL_H
#include <unistd.h>
#include <sys/syscall.h>
#include <asm/unistd.h>
#define new_syscall(x, y) syscall(__NR_new_syscall, x, y)
#endif
