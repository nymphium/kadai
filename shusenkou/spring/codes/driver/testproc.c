#include <linux/module.h>
#include <linux/init.h>
#include <linux/proc_fs.h>
#include <linux/seq_file.h>
#include <asm/uaccess.h>

MODULE_LICENSE("Dual BSD/GPL");

#define PROCNAME "driver/testproc"
static int nonzero_flag = 0;

#define SIZE 32

static ssize_t testproc_write(struct file *file, const char *buffer , size_t count, loff_t *data) {
	char buf[SIZE];
	unsigned long len = count;
	int n;

	if(len >= SIZE) len = SIZE - 1;
	printk(KERN_INFO "%d (%s)\n", (int)len, __func__);

	if(copy_from_user(buf, buffer, len)) return -EFAULT;

	buf[len] = '\0';
	n = simple_strtol(buf, NULL, 10);
	if(n == 0) nonzero_flag = 0;
	else nonzero_flag = 1;

	return (len);
}

static int nonzero_flag_show(struct seq_file *m, void *v) {
	seq_printf(m, "%d\n", nonzero_flag);
	return 0;
}

static int testproc_read(struct inode *inode, struct  file *file) {
	return single_open(file, nonzero_flag_show, NULL);
}

struct file_operations proc_fops = {
	.owner = THIS_MODULE,
	.open = testproc_read,
	.read = seq_read,
	.write = testproc_write,
};

static int testproc_init(void) {
	struct proc_dir_entry *entry;

	entry = proc_create(PROCNAME, S_IRUGO | S_IWUGO, NULL, &proc_fops);

	if(entry == NULL) {
		printk(KERN_WARNING" "PROCNAME": unable to create /proc entry\n");
	}

	printk("driver loaded\n");

	return 0;
}

static void testproc_exit(void) {
	remove_proc_entry(PROCNAME, NULL);

	printk(KERN_ALERT "driver unloaded\n");
}

module_init(testproc_init);
module_exit(testproc_exit);
