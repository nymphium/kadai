#include <stdio.h>
#include "new_syscall.h"
#include <bsd/string.h>

int main() {
	char str[128];
	strlcpy(str, "aiueo01234", 11);
	new_syscall(str, 128);

	printf("new_syscall(str, 128) -> %s\n", str);

	return 0;
}
