# kernel hack tips
[TOC]

## 構成
ArchLinux 64bit (archlinux-2016.04.01-dual.iso) on VMWare Workstation 12(host: ArchLinux 64bit)
を**`pacman -Syu`せずに**運用
+ package
 - base base-devel
 - bison
 - bc
 - neovim
 - openssh
 - open-vm-tools
 - linux-headers
 - grub
 - wget
 - distcc
 - gdb
 - strace
 - libbsd
 - git
+ system
 * Host
   - RAM
      DDR 3 8GB
   - Storage
     256GB SSD
     * 60GB RAID 0 F2FS (/)
     * 58.5GB RAID 0 F2FS (/home)
     * 1GB FAT32 (/boot)
 * VM
   - RAM
    1024MB
   - Storage
    40GB
     * 39GB ext4
     * 1G ext4

## kernel
Linux 4.6.3-1
`/usr/src`上でビルド

bitbucketに.config(など)を入れる

## 課題1
実行時間を見る｡

|ver.|time|
|:--|--|
|default|105m|
|shave device|71m|

## 課題2
1. `/path/to/kernel/vmlinux`と`/path/to/arch/ARCH/boot/bzImage`をローカルにコピー
2. `gdb`で`file vmlinux` `file vmlinuz`
3. `tar rem:8864`
4. `b sys_SYSCALL`
5. ホストで適当に何かして`SYSCALL`が呼ばれると止まる

## 課題3
システムコールを追加する｡

### 3-1.
`arch/x86/kernel/new_syscall.c`に以下を追加する｡

```
#include <linux/syscalls.h>

SYSCALL_DEFINE1(new_syscall, int, x) {
                printk("new_syscall added!!");
                return x;
}
```

### 3-2.
`arch/x86/kernel/Makefile`を書き換える｡

```
...

CFLAGS_irq.o := -I$(src)/../include/asm/trace

obj-y                   := process_$(BITS).o signal.o
obj-y                   += new_syscall.o                         # これを追加
obj-$(CONFIG_COMPAT)    += signal_compat.o
obj-y                   += traps.o irq.o irq_$(BITS).o dumpstack_$(BITS).o
obj-y                   += time.o ioport.o dumpstack.o nmi.o
obj-$(CONFIG_MODIFY_LDT_SYSCALL)        += ldt.o
obj-y                   += setup.o x86_init.o i8259.o irqinit.o jump_label.o
...
```

### 3-3.
システムコールの番号を表`arch/x86/entry/syscalls/syscall_64.tbl`の**最後**に追加する｡

```
323     common  userfaultfd             sys_userfaultfd
324     common  membarrier              sys_membarrier
325     common  mlock2                  sys_mlock2
326     common  copy_file_range         sys_copy_file_range
327     common  new_syscall             sys_new_syscall # これを追加
```

最後とは書いたがx32用のシステムコール番号も書かれており､それの上(x86用の最後ということ)｡

### 3-4.
カーネルビルドし､そのカーネルを起動する｡

### 3-5.
ゲストにて､`arch/x86/include/generated/uapi/asm/unistd_64.h`をユーザランドの適当な場所にコピーする｡

おなじディレクトリに次のようなファイルを作る｡

```
//new_syscall.h
#ifndef __LINUX_NEW_SYSCALL_H
#define __LINUX_NEW_SYSCALL_H
#include "unistd_64.h"
#include <asm/unistd.h>
#define new_syscall(x) syscall(__NR_new_syscall, x)
#endif
```

```
//new_syscall.c
#include <stdio.h>
#include "new_syscall.h"

int main()
{
        printf("new_syscall(10) -> %d\n",new_syscall(10));
        return 0;
}
```

これをコンパイルすると､以下のような結果が得られる｡

```
$ gcc new_syscall.c
$ ./a.out
new_syscall(10) -> 10
$ dmesg | tail -n 1
[  182.378978] new_syscall added!!
```
### 3-6.
`new_syscall.c`を書き換える｡

```C
#include <linux/syscalls.h>
#include <asm/uaccess.h>
#include <linux/string.h>

SYSCALL_DEFINE2(new_syscall, char *, x, int, size) {
        char _x[128];

        if(copy_from_user(_x, x, size)) {
                printk("failed to copy from user");
                return -EFAULT;
        }

        memcpy(_x, "hello", 5);

        copy_to_user(x, _x, sizeof(_x));
        return x;
}
```

カーネルコンパイル｡

### 3-7.
3-5. からと同じ｡ユーザー空間の`new_syscall.h`､`new_syscall.c`を書き換える｡
```
//new_syscall.c
#include <stdio.h>
#include "new_syscall.h"
#include <bsd/string.h>

int main() {
        char str[128];

        strlcpy(str, "aiueo01234", 11);
        new_syscall(str, 128);
        printf("new_syscall(str, 128) -> %s\n", str);
        return 0;
}
```

```
#ifndef __LINUX_NEW_SYSCALL_H
#define __LINUX_NEW_SYSCALL_H
#include <unistd.h>
#include "unistd_64.h"
#include <sys/syscall.h>
#include <asm/unistd.h>
#define new_syscall(x, f) syscall(__NR_new_syscall, x, f)
#endif
```

### 3-8.
`include/linux/syscalls.h`に以下を追加


```
......
asmlinkage long sys_membarrier(int cmd, int flags);
asmlinkage long sys_copy_file_range(int fd_in, loff_t __user *off_in,
                                    int fd_out, loff_t __user *off_out,
                                    size_t len, unsigned int flags);

asmlinkage long sys_mlock2(unsigned long start, size_t len, int flags);
asmlinkage long sys_new_syscall(char* x, int size);   // この行

#endif
```

### 3-9.
コンパイルして実行

```
$ gcc new_syscall.c -lbsd
$ ./a.out
new_syscall(str, 128) -> hello01234
```

## 課題4 デバイスの作成
ユーザー空間でプログラムを作成していく｡カーネルコンパイルの必要はない｡

Linux デバイスドライバ プログラミングに沿っていく｡

### 4-1.
#### 4-1-1.
open/closeのあるシンプルなデバイスを作ってみる｡

```C
#include <linux/init.h>
#include <linux/module.h>
#include <linux/types.h>
#include <linux/kernel.h>
#include <linux/fs.h>
#include <linux/cdev.h>
#include <linux/sched.h>
#include <asm/current.h>
#include <asm/uaccess.h>

MODULE_LICENSE("Dual BSD/GPL");

#define DRIVER_NAME "devone"

static unsigned int devone_major = 0;
module_param(devone_major, uint, 0);

static int devone_open(struct inode *inode, struct file *file) {
	printk("%s: major %d minor %d (pid %d)\n", __func__,
			imajor(inode),
			iminor(inode),
			current->pid
			);

	inode->i_private = inode;
	file->private_data = file;

	printk("  i_private=%p private_data=%p\n",
			inode->i_private, file->private_data);

	return 0; // success
}

static int devone_close(struct inode *inode, struct file *file) {
	printk("%s: major %d minor %d (pid %d)\n", __func__,
			imajor(inode),
			iminor(inode),
			current->pid
			);
	
	printk("  i_private=%p private_data=%p\n",
				inode->i_private, file-> private_data
			);

	return 0; // success
}

struct file_operations devone_fops = {
	.open = devone_open,
	.release = devone_close,
};

static int devone_init(void) {
	int major;
	int ret = 0;

	major = register_chrdev(devone_major, DRIVER_NAME, &devone_fops);
	if((devone_major > 0 && major != 0) ||
			(devone_major == 0 && major < 0) ||
			major < 0) {
		printk("%s driver registeration error\n", DRIVER_NAME);
		ret = major;
		goto error;
	}
	if(devone_major == 0 ) {
		devone_major = major;
	}

	printk("%s driver(major %d) installed.\n", DRIVER_NAME, devone_major);

error:
	return (ret);
}

static void devone_exit(void) {
	unregister_chrdev(devone_major, DRIVER_NAME);
	printk("%s driver removed.\n", DRIVER_NAME);
}

module_init(devone_init);
module_exit(devone_exit);
```

メジャー番号は`devone_major`に格納される｡初期値が0になっているが､`module_param`マクロを用いているため､後述の`insmod`コマンド使用時に`devone_major=MAJOR`というオプションを渡すことでメジャー番号を`MAJOR`にすることができる｡

#### 4-1-2.
Makefileを作成する｡

```
cflags-y := -Wall # 書によると `CFLAGS = ...` だが､makeに怒られるため､変更
CFILES = devone.c

obj-m += sample.o
sample-objs := $(CFILES:.c=.o)

all:
	make -C /lib/modules/$(shell uname -r)/build M=$(PWD) modules

clean:
	make -C /lib/modules/$(shell uname -r)/build M=$(PWD) clean
```

#### 4-1-3.
`make` すると､`sample.ko` (カーネルオブジェクト)が得られる｡

```
$  ls
devone.c  Makefile
$ make
make -C /lib/modules/4.5.2-MOD-master-g30e53ae-dirty/build M=/home/nymphium/codes/driver modules
make[1]: Entering directory '/usr/src/linux-4.5.2'
  CC [M]  /home/nymphium/codes/driver/devone.o
  LD [M]  /home/nymphium/codes/driver/sample.o
  Building modules, stage 2.
  MODPOST 1 modules
  CC      /home/nymphium/codes/driver/sample.mod.o
  LD [M]  /home/nymphium/codes/driver/sample.ko
make[1]: Leaving directory '/usr/src/linux-4.5.2'
$  ls
devone.c  devone.o  Makefile  modules.order  Module.symvers  sample.ko  sample.mod.c  sample.mod.o  sample.o
```

#### 4-1-4.
`insmod` でカーネルモジュール(sample.ko)をロード､`mknod`でスペシャルファイル(`/dev/devone`)を作成

```
$ sudo insmod sample.ko
# ドライバーのメジャー番号を確認
$ grep devone /proc/devices
247 devone
# 一般ユーザーでread/writeできるようにパーミッションを666にしておく
$ sudo mknod --mode=666 /dev/devone c $(awk '{print $1}' <(grep devone /proc/devices)) 0
$ ls /dev/devone
/dev/devone
```

#### 4-1-5.
次のようなサンプルアプリケーションを作成する｡

```
// simple.c
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>

#define DEVFILE "/dev/devone"

int open_file(void) {
        int fd = open(DEVFILE, O_RDWR);
        if (-fd) {
                perror("open");
        }

        return fd;
}

void close_file(int fd) {
        if (close(fd) != 0) {
                perror("close");
        }
}

int main(void) {
        int fd = open_file();

        sleep(20);
        close_file(fd);
        return 0;
}
```

これを2回実行すると､カーネルバッファに以下のようにドライバのメッセージが得られる｡

```
$ ./a.out && ./a.out
open: Success
close: Success
open: Success
close: Success
$ dmesg | tail
[ 2821.278574] devone_open: major 247 minor 0 (pid 2392)
[ 2821.278579]   i_private=ffff880036d05a08 private_data=ffff88003beaae00
[ 2841.278998] devone_close: major 247 minor 0 (pid 2392)
[ 2841.279001]   i_private=ffff880036d05a08 private_data=ffff88003beaae00
[ 2857.972970] devone_open: major 247 minor 0 (pid 2400)
[ 2857.972975]   i_private=ffff880036d05a08 private_data=ffff880036da5100
[ 2877.973425] devone_close: major 247 minor 0 (pid 2400)
[ 2877.973433]   i_private=ffff880036d05a08 private_data=ffff880036da5100
```

### 4-2.
#### 4-2-1.
マイナー番号の制限をかけてみる｡

```
// cdevone.c
#include <linux/init.h>
#include <linux/module.h>
#include <linux/types.h>
#include <linux/kernel.h>
#include <linux/fs.h>
#include <linux/cdev.h>
#include <linux/sched.h>
#include <asm/current.h>
#include <asm/uaccess.h>

MODULE_LICENSE("Dual BSD/GPL");

#define DRIVER_NAME "cdevone"

static int devone_devs = 2; // device count
static int devone_major = 0;
module_param(devone_major, uint, 0);
static struct cdev devone_cdev;

static int devone_open(struct inode *inode, struct file *file) {
	printk("%s: major %d minotr %d (pid %d)\n",
				__func__,
				imajor(inode),
				iminor(inode),
				current->pid
			);

	inode->i_private = inode;
	file->private_data = file;

	printk(" i_private=%p private_data=%p\n", inode->i_private, file->private_data);

	return 0;
}

static int devone_close(struct inode *inode, struct file *file) {
	printk("%s: major %d minotr %d (pid %d)\n",
			__func__,
			imajor(inode),
			iminor(inode),
			current->pid
			);
	printk("  i_private=%p private_data=%p\n", inode->i_private, file->private_data);

	return 0;
}

struct file_operations devone_fops = {
	.open = devone_open,
	.release = devone_close
};

static int devone_init(void) {
	dev_t dev = MKDEV(devone_major, 0);
	int alloc_ret = 0;
	int major;
	int cdev_err = 0;

	alloc_ret = alloc_chrdev_region(&dev, 0, devone_devs, DRIVER_NAME);

	if(alloc_ret) goto error;

	devone_major = major = MAJOR(dev);

	cdev_init(&devone_cdev, &devone_fops);

	devone_cdev.owner = THIS_MODULE;

	cdev_err = cdev_add(&devone_cdev, MKDEV(devone_major, 0), devone_devs);

	if(cdev_err) goto error;

	printk(KERN_ALERT "%s driver(major %d) installed.\n", DRIVER_NAME, major);

	return 0;

error:
	if(cdev_err == 0) cdev_del(&devone_cdev);

	if(alloc_ret == 0) unregister_chrdev_region(dev, devone_devs);

	return -1;
}

static void devone_exit(void) {
	dev_t dev = MKDEV(devone_major, 0);

	cdev_del(&devone_cdev);

	unregister_chrdev_region(dev, devone_devs);

	printk(KERN_ALERT "%s driver removed.\n", DRIVER_NAME);
}

module_init(devone_init);
module_exit(devone_exit);
```

`devone_devs` がマイナー番号の数となる｡`2`ということは､0〜1までのデバイスファイル*のみ*が有効となる｡

Makefileも書き換える｡

```
cflags-y := -Wall
CFILES ?= devone.c
DIR = /lib/modules/$(shell uname -r)/build
MAKE = make

obj-m += sample.o
sample-objs := $(CFILES:.c=.o)

all:
	$(MAKE) -C $(DIR) M=$(PWD) modules

cdevone:
	$(MAKE) CFILES=cdevone.c


clean:
	make -C /lib/modules/$(shell uname -r)/build M=$(PWD) clean
```

`make cdevone` でマイナー番号に制限のあるデバイス`cdevone`のカーネルオブジェクトができる｡

#### 4-2-2.
アッピケーションは以下のようになる｡

```
// simple2.c
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>

#define DEVFILE "/dev/cdevone"
#define DEVCOUNT 4

int open_file(char *filename) {
	int fd = open(filename, O_RDWR);

	if(fd == -1) {
		perror("open");
	}

	return fd;
}

void close_file(int fd) {
	if(close(fd) != 0) {
		perror("close");
	}
}

int main(void) {
	int fd[DEVCOUNT];
	int i;
	char file[BUFSIZ];

	for(i = 0; i < DEVCOUNT; i++) {
		snprintf(file, sizeof(file), "%s%d", DEVFILE, i);
		printf("%s\n", file);
		fd[i] = open_file(file);
	}

	sleep(5);

	for(i = 0; i < DEVCOUNT; i++) {
		printf("closing fd[%d]\n", i);
		close_file(fd[i]);
	}

	return 0;
}
```

#### 4-2-3.
デバイスを作成していく｡

```
# 上記でロードしたものを一旦消す
$ sudo rmmod sample
$ sudo insmod sample.ko
$  for ((i=0;i<4;i++)){
sudo mknod --mode=666 /dev/cdevone${i} c $(awk '{print $1}' <(grep cdevone /proc/devices)) ${i}
}
$ ls -l /dev/cdevone?
crw-rw-rw- 1 root root 247, 0 Jun 18 03:09 /dev/cdevone0
crw-rw-rw- 1 root root 247, 1 Jun 18 03:09 /dev/cdevone1
crw-rw-rw- 1 root root 247, 2 Jun 18 03:09 /dev/cdevone2
crw-rw-rw- 1 root root 247, 3 Jun 18 03:09 /dev/cdevone3
$ gcc simple2.c
$ ./a.out
/dev/cdevone0
/dev/cdevone1
/dev/cdevone2
open: No such device or address
/dev/cdevone3
open: No such device or address
closing fd[0]
closing fd[1]
closing fd[2]
close: Bad file descriptor
closing fd[3]
close: Bad file descriptor
$ dmesg | tail
[10666.725198] devone_open: major 247 minotr 0 (pid 4746)
[10666.725201]  i_private=ffff88002f9aa438 private_data=ffff880036ecfa00
[10666.725208] devone_open: major 247 minotr 1 (pid 4746)
[10666.725209]  i_private=ffff88002f9adf28 private_data=ffff880036ece800
[10671.730302] devone_close: major 247 minotr 0 (pid 4746)
[10671.730308]   i_private=ffff88002f9aa438 private_data=ffff880036ecfa00
[10671.730319] devone_close: major 247 minotr 1 (pid 4746)
[10671.730322]   i_private=ffff88002f9adf28 private_data=ffff880036ece800
```

マイナー番号0〜3のデバイスを作成した｡しかしユーザープロセスからはマイナー番号2以上のデバイスファイルをオープンできない｡
`cdev_add()`により`devone_devs`にマイナー番号を制限していることがわかる｡

### 4-3.
#### 4-3-1.
read/writeハンドラを使ったドライバを作成する｡

```
// rwdevone.c
#include <linux/init.h>
#include <linux/module.h>
#include <linux/types.h>
#include <linux/kernel.h>
#include <linux/fs.h>
#include <linux/cdev.h>
#include <linux/sched.h>
#include <linux/slab.h>
#include <asm/current.h>
#include <asm/uaccess.h>

MODULE_LICENSE("Dual BSD/GPL");

#define DRIVER_NAME "rwdevone"

static int devone_devs = 1; // device count
static int devone_major = 0;
module_param(devone_major, uint, 0);
static struct cdev devone_cdev;

struct devone_data {
	unsigned char val;
	rwlock_t lock;
};

ssize_t devone_write(struct file *filp, const char __user *buf, size_t count, loff_t *f_pos) {
	struct devone_data *p = filp->private_data;
	unsigned char val;
	int retval = 0;

	printk("%s: count %d pos %lld\n", __func__, count, *f_pos);

	if(count >= 1) {
		if(copy_from_user(&val, &buf[0], 1)) {
			retval = -EFAULT;
			goto out;
		}

		write_lock(&p->lock);
		p->val = val;
		write_unlock(&p->lock);
		retval = count;
	}

out:
	return retval;
}

ssize_t devone_read(struct file *filp, char __user *buf, size_t count, loff_t *f_pos) {
	struct devone_data *p = filp->private_data;
	int i;
	unsigned char val;
	int retval;

	read_lock(&p->lock);
	val = p->val;
	read_unlock(&p->lock);

	printk("%s: const %d pos %lld\n", __func__, count, *f_pos);

	for(i = 0; i < count; i++) {
		if(copy_to_user(&buf[i], &val, 1)) {
			retval = -EFAULT;
			goto out;
		}
	}
	retval = count;

out:
	return retval;
}

static int devone_open(struct inode *inode, struct file *file) {
	struct devone_data *p;

	printk("%s: major %d minor %d (pod %d)\n", __func__,
			imajor(inode),
			iminor(inode),
			current->pid
			);

	p = kmalloc(sizeof(struct devone_data), GFP_KERNEL);

	if(p == NULL) {
		printk("%s: Not memory\n", __func__);
		return -ENOMEM;
	}

	p->val = 0xff;
	rwlock_init(&p->lock);

	file->private_data = p;

	return 0;
}

static int devone_close(struct inode *inode, struct file *file) {
	printk("%s: major %d minor %d (pod %d)\n", __func__,
			imajor(inode),
			iminor(inode),
			current->pid
			);

	if(file->private_data) {
		kfree(file->private_data);
		file->private_data = NULL;
	}

	return 0;
}

struct file_operations devone_fops = {
	.open = devone_open,
	.release = devone_close,
	.read = devone_read,
	.write = devone_write,
};

static int devone_init(void) {
	dev_t dev = MKDEV(devone_major, 0);
	int alloc_ret = 0;
	int major;
	int cdev_err = 0;

	alloc_ret = alloc_chrdev_region(&dev, 0, devone_devs, DRIVER_NAME);
	if(alloc_ret) goto error;

	devone_major = major = MAJOR(dev);

	cdev_init(&devone_cdev, &devone_fops);
	devone_cdev.owner = THIS_MODULE;

	cdev_err = cdev_add(&devone_cdev, MKDEV(devone_major, 0), devone_devs);

	if(cdev_err) goto error;

	printk(KERN_ALERT "%s driver(major %d) installed.\n", DRIVER_NAME, major);

	return 0;

error:
	if(cdev_err == 0) cdev_del(&devone_cdev);

	if(alloc_ret == 0) unregister_chrdev_region(dev, devone_devs);

	return -1;
}

static void devone_exit(void) {
	dev_t dev = MKDEV(devone_major, 0);

	cdev_del(&devone_cdev);
	unregister_chrdev_region(dev, devone_devs);

	printk(KERN_ALERT "%s driver removed.\n", DRIVER_NAME);
}

module_init(devone_init);
module_exit(devone_exit);

```

サンプルアプリケーションは以下のようになる｡

```
//test.c
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>

#define DEVFILE "/dev/rwdevone"

int open_file(char *filename) {
	int fd;

	fd = open(filename, O_RDWR);

	if(fd == -1) {
		perror("open");
	}

	return fd;
}

void close_file(int fd) {
	if(close(fd) != 0) {
		perror("close");
	}
}

void read_file(int fd) {
	unsigned char buf[8], *p;
	size_t ret;

	ret = read(fd, buf, sizeof(buf));
	if(ret > 0) {
		p =buf;
		while(ret--) {
			printf("%02x ", *p++);
		}
	}else {
		perror("read");
	}

	puts("");
}

void write_file(int fd, unsigned char val) {
	ssize_t ret;

	ret = write(fd, &val, 1);

	if(ret <= 0) {
		perror("write");
	}
}

int main(void) {
	int fd;
	int i;

	for(i = 0; i < 2; i++) {
		printf("No. %d\n", i+1);

		fd = open_file(DEVFILE);

		read_file(fd);

		write_file(fd, 0x00);
		read_file(fd);

		write_file(fd, 0xc0);
		read_file(fd);

		close_file(fd);
	}

	return 0;
}
```

#### 4-3-2.
Makefileの更新

```
cflags-y := -Wall
CFILES ?= devone.c
DIR = /lib/modules/$(shell uname -r)/build
MAKE = make

obj-m += sample.o
sample-objs := $(CFILES:.c=.o)

all:
	$(MAKE) -C $(DIR) M=$(PWD) modules

cdevone:
	$(MAKE) CFILES=cdevone.c

rwdevone:
	$(MAKE) CFILES=rwdevone.c

clean:
	make -C /lib/modules/$(shell uname -r)/build M=$(PWD) clean
```

#### 4-3-3.
デバイスを作成､サンプルプログラムを実行する｡

```
$ make rwdevone
make CFILES=rwdevone.c
make[1]: Entering directory '/home/nymphium/codes/driver'
make -C /lib/modules/4.5.2-MOD-master-g30e53ae-dirty/build M=/home/nymphium/codes/driver modules
make[2]: Entering directory '/usr/src/linux-4.5.2'
  CC [M]  /home/nymphium/codes/driver/rwdevone.o
/home/nymphium/codes/driver/rwdevone.c: In function ‘devone_write’:
/home/nymphium/codes/driver/rwdevone.c:31:9: warning: format ‘%d’ expects argument of type ‘int’, but argument 3 has type ‘size_t {aka long unsigned int}’ [-Wformat=]
  printk("%s: count %d pos %lld\n", __func__, count, *f_pos);
         ^
/home/nymphium/codes/driver/rwdevone.c: In function ‘devone_read’:
/home/nymphium/codes/driver/rwdevone.c:59:9: warning: format ‘%d’ expects argument of type ‘int’, but argument 3 has type ‘size_t {aka long unsigned int}’ [-Wformat=]
  printk("%s: const %d pos %lld\n", __func__, count, *f_pos);
         ^
  LD [M]  /home/nymphium/codes/driver/sample.o
  Building modules, stage 2.
  MODPOST 1 modules
  CC      /home/nymphium/codes/driver/sample.mod.o
  LD [M]  /home/nymphium/codes/driver/sample.ko
make[2]: Leaving directory '/usr/src/linux-4.5.2'
make[1]: Leaving directory '/home/nymphium/codes/driver'
$ sudo rmmod sample
$ sudo insmod sample.ko
$ sudo mknod --mode=666 /dev/rwdevone c $(awk '{print $1}' <(grep rwdevone /proc/devices)) 0
$ gcc test.c && ./a.out
No. 1
ff ff ff ff ff ff ff ff
00 00 00 00 00 00 00 00
c0 c0 c0 c0 c0 c0 c0 c0
No. 2
ff ff ff ff ff ff ff ff
00 00 00 00 00 00 00 00
c0 c0 c0 c0 c0 c0 c0 c0
$ dmesg | tail -20
[78072.436678] devone_read: const 8192 pos 0
[78072.438126] devone_close: major 247 minor 0 (pod 13014)
[78172.718549] rwdevone driver removed.
[78213.874490] rwdevone driver(major 247) installed.
[78246.184141] rwdevone driver removed.
[78263.203714] rwdevone driver(major 247) installed.
[78303.156847] devone_open: major 247 minor 0 (pod 14262)
[78303.156859] devone_read: const 8 pos 0
[78303.157003] devone_write: count 1 pos 0
[78303.157005] devone_read: const 8 pos 0
[78303.157012] devone_write: count 1 pos 0
[78303.157013] devone_read: const 8 pos 0
[78303.157017] devone_close: major 247 minor 0 (pod 14262)
[78303.157023] devone_open: major 247 minor 0 (pod 14262)
[78303.157025] devone_read: const 8 pos 0
[78303.157028] devone_write: count 1 pos 0
[78303.157029] devone_read: const 8 pos 0
[78303.157032] devone_write: count 1 pos 0
[78303.157033] devone_read: const 8 pos 0
[78303.157037] devone_close: major 247 minor 0 (pod 14262)
```

### 4-4.
#### 4-4-1.
状態をdmesgに吐き出す`IOCTL_VALDMP`を追加する｡

```
#include <linux/init.h>
#include <linux/module.h>
#include <linux/types.h>
#include <linux/kernel.h>
#include <linux/fs.h>
#include <linux/cdev.h>
#include <linux/device.h>
#include <linux/poll.h>
#include <linux/slab.h>
#include <asm/uaccess.h>
#include "devone_ioctl.h"

MODULE_LICENSE("Dual BSD/GPL");
#define DEVNAME "ledevone"

static int devone_devs = 1;
static int devone_major = 0;
static int devone_minor = 0;
static struct cdev devone_cdev;
static struct class *devone_class = NULL;
static dev_t devone_dev;

struct devone_data {
	rwlock_t lock;
	unsigned char val;
};

long devone_ioctl(struct file *filp, unsigned int cmd, unsigned long arg) {
	struct devone_data *dev = filp->private_data;
	int retval = 0;
	unsigned char val;
	struct ioctl_cmd data;

	memset(&data, 0, sizeof(data));

	switch(cmd) {
		case IOCTL_VALSET:
			if(!capable(CAP_SYS_ADMIN)) {
				retval = -EPERM;
				goto done;
			}
			if(!access_ok(VERIFY_READ, (void __user *)arg, _IOC_SIZE(cmd))) {
				retval = -EFAULT;
				goto done;
			}
			if(copy_from_user(&data, (int __user *)arg, sizeof(data))) {
				retval = -EFAULT;
				goto done;
			}

			printk("IOCTL_cmd.val %u (%s)\n", data.val, __func__);

			write_lock(&dev->lock);
			dev->val = data.val;
			write_unlock(&dev->lock);

			break;
		case IOCTL_VALGET:
			if(!access_ok(VERIFY_WRITE, (void __user *)arg, _IOC_SIZE(cmd))) {
				retval = -EFAULT;
				goto done;
			}
			read_lock(&dev->lock);
			val = dev->val;
			read_unlock(&dev->lock);
			data.val = val;
			if(copy_to_user((int __user *)arg, &data, sizeof(data))) {
				retval = -EFAULT;
				goto done;
			}
			break;
		case IOCTL_VALDMP:
			if(!capable(CAP_SYS_ADMIN)) {
				retval = -EPERM;
				goto done;
			}
			if(!access_ok(VERIFY_READ, (void __user *)arg, _IOC_SIZE(cmd))) {
				retval = -EFAULT;
				goto done;
			}
			if(copy_from_user(&data, (int __user *)arg, sizeof(data))) {
				retval = -EFAULT;
				goto done;
			}
			read_lock(&dev->lock);
			val = dev->val;
			read_unlock(&dev->lock);

			printk("current_data: %c\n", val);
			break;
		default:
			retval = -ENOTTY;
			break;
	}

done:
	return retval;
}

ssize_t devone_read(struct file *filp, char __user *buf, size_t count, loff_t *f_pos) {
	struct devone_data *dev = filp->private_data;
	unsigned char val;
	int retval;
	int i;

	read_lock(&dev->lock);
	val = dev->val;
	read_unlock(&dev->lock);

	for(i = 0; i < count; i++) {
		if(copy_to_user(&buf[i], &val, 1)) {
			retval = -EFAULT;
			goto out;
		}
	}

	retval = count;

out:
	return retval;
}

int devone_close(struct inode *inode, struct file *filp) {
	struct devone_data *dev = filp->private_data;

	if(dev) {
		kfree(dev);
	}

	return 0;
}

int devone_open(struct inode *inode, struct file *filp) {
	struct devone_data *dev = kmalloc(sizeof(struct devone_data), GFP_KERNEL);

	if(dev == NULL) {
		return -ENOMEM;
	}

	rwlock_init(&dev->lock);
	dev->val = 0xff;

	filp->private_data = dev;

	return 0;
}

struct file_operations devone_fops = {
	.owner = THIS_MODULE,
	.open = devone_open,
	.release = devone_close,
	.read = devone_read,
	.unlocked_ioctl = devone_ioctl,
};

static int devone_init(void) {
	dev_t dev = MKDEV(devone_major, 0);
	int alloc_ret = 0;
	int major;
	int cdev_err = 0;
	struct device *class_dev = NULL;

	alloc_ret = alloc_chrdev_region(&dev, 0, devone_devs, DEVNAME);

	if(alloc_ret) {
		goto error;
	}

	devone_major = major = MAJOR(dev);

	cdev_init(&devone_cdev, &devone_fops);

	devone_cdev.owner = THIS_MODULE;
	devone_cdev.ops = &devone_fops;
	cdev_err = cdev_add(&devone_cdev, MKDEV(devone_major, devone_minor), 1);
	if(cdev_err) {
		goto error;
	}

	devone_class = class_create(THIS_MODULE, DEVNAME);
	if(IS_ERR(devone_class)) {
		goto error;
	}

	devone_dev = MKDEV(devone_major, devone_minor);

	class_dev = device_create(
			devone_class,
			NULL,
			devone_dev,
			NULL,
			DEVNAME"%d",
			devone_minor);

	printk(KERN_ALERT DEVNAME" driver(major %d) installed.\n", major);

	return 0;

error:
	if(cdev_err == 0) {
		cdev_del(&devone_cdev);
	}

	if(alloc_ret == 0) {
		unregister_chrdev_region(dev, devone_devs);
	}

	return -1;
}

static void devone_exit(void) {
	dev_t dev = MKDEV(devone_major, 0);

	device_destroy(devone_class, devone_dev);

	class_destroy(devone_class);

	cdev_del(&devone_cdev);
	unregister_chrdev_region(dev, devone_devs);

	printk(KERN_ALERT DEVNAME" driver removed.\n");
}

module_init(devone_init);
module_exit(devone_exit);
```

## 4-4-2.
Makefileをガッツリ書き換える

```
cflags-y := -Wall
# CFILES ?= devone.c
DIR = /lib/modules/$(shell uname -r)/build
MAKE = make

TARGET ?= devone

CFILES = $(TARGET).c
OBJFILE = $(TARGET).o

obj-m += $(OBJFILE)
sample-objs := $(CFILES:.c=.o)

all:
	$(MAKE) -C $(DIR) M=$(PWD) modules

cdevone:
	$(MAKE) TARGET=cdevone

rwdevone:
	$(MAKE) TARGET=rwdevone

iodevone:
	$(MAKE) TARGET=iodevone

ledevone:
	$(MAKE) TARGET=ledevone

clean:
	make -C /lib/modules/$(shell uname -r)/build M=$(PWD) clean
```

## 4-4-3.
アプリケーションを考える｡

```
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include "devone_ioctl.h"

#define DEVFILE "/dev/ledevone0"

void read_buffer(int fd) {
	unsigned char buf[64];
	int ret;
	int i;
	ret = read(fd, buf, sizeof(buf));
	if(ret == -1) {
		perror("read");
	}

	for(i = 0; i < ret; i++) {
		printf("%02x ", buf[i]);
	}

	puts("");
}

int main(void) {
	struct ioctl_cmd cmd;
	int ret;
	int fd;

	fd = open(DEVFILE, O_RDWR);

	if(fd == -1) {
		perror("open");
		exit(1);
	}

	memset(&cmd, 0, sizeof(cmd));
	ret = ioctl(fd, IOCTL_VALGET, &cmd);
	if(ret == -1) {
		printf("errno %d\n", errno);
		perror("ioctl");
	}

	printf("val %d\n", cmd.val);

	read_buffer(fd);

	memset(&cmd, 0, sizeof(cmd));
	cmd.val = 0xcc;
	ret = ioctl(fd, IOCTL_VALSET, &cmd);
	if(ret == -1) {
		printf("errno %d\n", errno);
		perror("ioctl");
	}

	read_buffer(fd);

	memset(&cmd, 0, sizeof(cmd));
	ret = ioctl(fd, IOCTL_VALGET, &cmd);
	if(ret == -1) {
		printf("errno %d\n", errno);
		perror("ioctl");
	}

	memset(&cmd, 0, sizeof(cmd));
	ret = ioctl(fd, IOCTL_VALDMP, &cmd);
	if(ret == -1) {
		printf("errno %d\n", errno);
		perror("ioctl");
	}

	printf("val %d\n", cmd.val);

	close(fd);

	return 0;
}
```

## 4-4-4.
デバイスを作成､実行する｡

```
$ make ledevone
make TARGET=ledevone
make[1]: Entering directory '/home/nymphium/codes/driver'
make -C /lib/modules/4.6.3-MOD-master-g73b530d-dirty/build M=/home/nymphium/codes/driver modules
make[2]: Entering directory '/usr/src/linux-4.6.3'
  CC [M]  /home/nymphium/codes/driver/ledevone.o
  Building modules, stage 2.
  MODPOST 1 modules
  LD [M]  /home/nymphium/codes/driver/ledevone.ko
make[2]: Leaving directory '/usr/src/linux-4.6.3'
make[1]: Leaving directory '/home/nymphium/codes/driver'
S insmod ledevone.ko
S mknod --mode=666 /dev/ledevone0 c "$(awk '{print $1}' <(grep ledevone /proc/devices))" 0
$ gcc ledapp.c
$ sudo ./a.out
val 255
ff ff ff ff ff ff ff ff ff ff ff ff ff ff ff ff ff ff ff ff ff ff ff ff ff ff ff ff ff ff ff ff ff ff ff ff ff ff ff ff ff ff ff ff ff ff ff ff ff ff ff ff ff ff ff ff ff ff ff ff ff ff ff ff
cc cc cc cc cc cc cc cc cc cc cc cc cc cc cc cc cc cc cc cc cc cc cc cc cc cc cc cc cc cc cc cc cc cc cc cc cc cc cc cc cc cc cc cc cc cc cc cc cc cc cc cc cc cc cc cc cc cc cc cc cc cc cc cc
val 0
$ dmesg | tail -n3
[  228.908997] ledevone driver(major 247) installed.
[  329.995984] IOCTL_cmd.val 204 (devone_ioctl)
[  329.996005] current_data: \xffffffcc
```

## 参考文献
[Windows 7上のVMware Playerにgdbでリモート接続する方法](http://kzlog.picoaccel.com/windows-7%E4%B8%8A%E3%81%AEvmware-player%E3%81%ABgdb%E3%81%A7%E3%83%AA%E3%83%A2%E3%83%BC%E3%83%88%E6%8E%A5%E7%B6%9A%E3%81%99%E3%82%8B%E6%96%B9%E6%B3%95/)
