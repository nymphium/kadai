\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {開発環境}}{2}{chapter.1}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {課題1}カーネルのコンパイルとパラメタの設定}{4}{chapter.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.1}ソースコードの展開}{4}{section.1.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.2}デフォルトパラメーターの設定}{4}{section.1.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.3}パラメーターの設定}{5}{section.1.3}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.4}コンパイル時間の計測}{5}{section.1.4}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {課題2}カーネルのリモート・デバッグ}{6}{chapter.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.1}ゲストOSのカーネルパラメーター設定}{6}{section.2.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.2}コンパイル、ブートローダーへの設定}{6}{section.2.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.3}gdb}{7}{section.2.3}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {課題3}システムコールの追加}{9}{chapter.3}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.1}システムコールの番号の割り当て}{9}{section.3.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.2}関数の定義}{9}{section.3.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.3}Makefileの書き換え}{10}{section.3.3}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.4}カーネルコンパイル}{10}{section.3.4}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.5}アプリケーションの作成}{11}{section.3.5}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.6}アプリケーションの実行}{11}{section.3.6}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {課題4}デバイス・ドライバの作成}{13}{chapter.4}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.1}機能}{13}{section.4.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.2}デバイスドライバの実装}{13}{section.4.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.3}Makefileの作成}{19}{section.4.3}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.4}カーネルオブジェクトの作成}{20}{section.4.4}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.5}アプリケーションの作成}{21}{section.4.5}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.6}アプリケーションの実行}{23}{section.4.6}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {課題5}読み書きのできるprocfsの作成}{24}{chapter.5}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.1}機能}{24}{section.5.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.2}procfsの実装}{24}{section.5.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.3}カーネルオブジェクトの作成}{26}{section.5.3}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.4}動作の確認}{26}{section.5.4}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {感想}}{28}{chapter.6}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {作成時間}}{29}{chapter.7}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {LinuxCon レポート}}{30}{chapter.8}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {}1日目}{30}{section.8.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {}3日目}{30}{section.8.2}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {参考文献}}{31}{chapter.9}
