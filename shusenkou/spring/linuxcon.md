[toc]
# day 1
## keynote 1

## keynote 2: Hyperledger Project
blockchain let anyone confirm the commit contrast with the traditional DB

Hyperledger, advanced blocchain technology
cross-industry open standard for distributed ledgers that can transform the way *business transactions*

modular framework

## keynote 3: embedded linux in industry and civil infrastructure system
Linux id widely used in industry, transport infra, energy infra, other services, and other.
they require:
- reliability
- security
- realtime capability
- longterm support

world is changing
- industrial IoT Archtectures
- automotion and operation
- longterm availability
- new features

to be done
- join forces for commodity components
- share maintenance costs
- innovate for future technology

things change, stay the same ->  ***Civil Infrastructure pratfor***

## keyonote 4: the rise of open source program office
todo, strategy -> governance -> operations
explict your starategy
OSSは最高

## keynote 5: the kernel report
1. eliminating 脆弱性
  - post-init readonly memory
  - use of gcc plugins
  - kernel stack hardening
  - hardened user-copy
  - reference-count hardening
2. security-related code has tradeoff: performance, user-space compatibility
3. new tech
  memory
  - huge
  - persistent in reboot
  - directly addressable for CPU
4. role of the kernel
  high level networking protocol, in the user space (TOU, QUIC)
  
## keynote 6: 
Fujitsu contributes to the kernel, kvm, container and openstack
why contribute?
- Just for fun
- to add enterprise features and qualities?
- to decrease development costs?
- *to provide long time support*, it need open source ecosystem which is developed in the communities

enterprise features and qualities in communities -> talented engineers grow in fujitsu



## fail fast, fail often
## KASan(Kernel Adressing Sanitizer) in a Bare-Metal Hypervisor
KAsan
- dynamic memory error detector for the kernel
- AdressSanitizer project由来

shadowing, 8 bytes has 9 states
- 0 if access to all 8 bytes is valid
- N if access only to first N bytes is valid
- Negative value(poisoned) if access to all 8 bytes is invalid

when memory accessing , gcc calls `__asan_load##size()` or `__aan_store##size()`

bare-metal hyprevisor
1. page tables for shadow
  hypervisor memory (~200MB)
  KASan shadow memory (~25 MB)
  choosing KASAN_SHADOW_OFFSET is tricky
  check whether hypervisor can access the foreign memory
2. sanitize *a single* source file
3. track global variables
4. track heap
5. poison shadow by default
6. track stack
7. design a noKASan API
  allow memory access without KASan check in
8. 9,10. applyto the whole proje 

UBSan (Unidefined Behavior Sanitizer)

## networking containers in ultra low latency envirnment
SR-IOV 大勝利
- metal - always performs best
- direct network++ - macvlan is your friend
- others - roughly similar, careful of weave

## machine intelligence at google scale: tensorflow and cloud machine learning

# day 2
## keynote 1
web scale
develop, deploy on container

## keynote 2: 5 takeways from 20 years of open source compliance

## keynote 3: 対談
## resouce allocation: Intel RDT
CAT(Cache Allocation Technology)
CDP(Code and Data Prioritization),extension of CAT

## container standardization
isolated tech with each container vendors
container issues:
- users hard to choose the ebst tools to build the best applications
- users locked into any technology vendor for the long run

solution: standardization
goal -> 
- make open industry standards for container
- help users to choose container-based solutions

OCI(Open Container Initiative)
- runtime-spec
- image-spec
- runc
- ocitools

## reproduce and verify filesystems
compressed file consistencym

## mesos scheduler Amazon ECS

# day3
## dynamically hacking the kernel with Containers
### motivation
tradeoff, higher performance, lower isonation between VM and container
### terms
- OS container
  + vm like
  + no layered fs
  + ex: lxc, openbz, bsd jails , solaris zones
- app container
  - for single services
  - layered fs
  - ex: docker

## dimensions
- system software
   os container
- orchestlation
system- application - orchestration

between the app and sytem ... ?
large empty hole

kernel detouring: like a live patching
normal process -> func() in kernel
kernel.func() refer userspace.container.func()
that's why detouring

demo: run FreeBSD app on Linux
- remapping syscall number FreeBSD to Linux (ex: fork -> open)
- live patching missing remapping(ex: exit) in container
- right

other binary compatibility work
- Wine
- FreeBSD, Win 10

compromise between lower isonation and higher performance
VM -> hyper v
container -> kernel detouring
(lower isolation) -> new paradigms(multi-kernel, unikernels)

## yocto

## last keynote1
## last keynote 2
