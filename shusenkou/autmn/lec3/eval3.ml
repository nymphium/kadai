let string_of_env env =
    let rec string_of_env' env acc =
        match env with
        | [] when acc = "" -> "[]"
        | []    -> Printf.sprintf "[%s]" acc
        | (varname, e)::es ->
                match e with
                | IntVal i -> string_of_env' es (acc ^ (Printf.sprintf "(%S, IntVal %d); " varname i))
                | BoolVal b -> string_of_env' es (acc ^ (Printf.sprintf "(%S, BoolVal %B); " varname b))
    in
    string_of_env' env "";;

let rec eval3 e env =           (* env を引数に追加 *)
  let binop f e1 e2 env =       (* binop の中でも eval3 を呼ぶので env を追加 *)
    match (eval3 e1 env, eval3 e2 env) with
    | (IntVal(n1),IntVal(n2)) -> IntVal(f n1 n2)
    | _ -> failwith "integer value expected"
  in 
  match e with
  | Var(x)       -> lookup x env
  | IntLit(n)    -> IntVal(n)
  | BoolLit(b)   -> BoolVal(b)
  | Plus(e1,e2)  -> binop (+) e1 e2 env     (* env を追加 *)
  | Times(e1,e2) -> binop ( * ) e1 e2 env   (* env を追加 *)
  | Eq(e1,e2)    -> 
    begin
        match (eval3 e1 env, eval3 e2 env) with
        | (IntVal(n1),IntVal(n2)) -> BoolVal(n1=n2)
        | (BoolVal(b1),BoolVal(b2)) -> BoolVal(b1=b2)
        | _ -> failwith "wrong value"
    end
  | If(e1,e2,e3) ->
    begin
      match (eval3 e1 env) with          (* env を追加 *)
      | BoolVal(true)  -> eval3 e2 env   (* env を追加 *)
      | BoolVal(false) -> eval3 e3 env   (* env を追加 *)
      | _ -> failwith "wrong value"
    end
  | Let(x,e1,e2) ->
      print_string ((string_of_env env) ^ " -> ");
      let env1 = ext env x (eval3 e1 env)
      in 
      print_string ((string_of_env env1) ^ "\n");
      eval3 e2 env1
  | _ -> failwith "unknown expression"
