open Mysyntax;;

let rec lookup x env =
   match env with
   | [] -> None
   | (y,v)::tl -> if x=y then Some v 
                  else lookup x tl 

let isk = function
    | IntLit _ -> true
    | BoolLit _ -> true
    | _ -> false

(* exp ~~> exp[xexp/x] *)
let rec bind_free x xexp exp env =
    match exp with
    | Var v ->
            if x = v then
                begin match lookup v env with
                | Some _ -> exp
                | None   -> xexp
                end
            else exp
    | Let (v, eb, exp') ->
            let env' = (v, eb)::env in
            Let (v, (bind_free x xexp eb env'), bind_free x xexp exp' env')
    | App (e1, e2) -> App (bind_free x xexp e1 env, bind_free x xexp e2 env)
    | Plus(e1, e2) -> Plus(bind_free x xexp e1 env, bind_free x xexp e2 env)
    | Minus(e1, e2) -> Minus(bind_free x xexp e1 env, bind_free x xexp e2 env)
    | Times(e1, e2) -> Times(bind_free x xexp e1 env, bind_free x xexp e2 env)
    | Div(e1, e2) -> Div(bind_free x xexp e1 env, bind_free x xexp e2 env)
    | Eq(e1, e2) -> Eq(bind_free x xexp e1 env, bind_free x xexp e2 env)
    | Neq(e1, e2) -> Neq(bind_free x xexp e1 env, bind_free x xexp e2 env)
    | Less(e1, e2) -> Less(bind_free x xexp e1 env, bind_free x xexp e2 env)
    | Greater(e1, e2) -> Greater(bind_free x xexp e1 env, bind_free x xexp e2 env)
    | LetRec (fname, fargname, body, exp') ->
            if fname = x then exp
            else
                let  env' = (fname, IntLit 0)::env in  (* dummy *)
                if fargname = x then
                    LetRec (fname, fargname, body, bind_free x xexp exp' env')
                else
                    LetRec (fname, fargname, (bind_free x xexp body env'), bind_free x xexp exp' env')
    | Fun (argname, body) ->
            if x = argname then exp
            else
                let env' = (argname, IntLit 0)::env in (* dummy *)
                Fun (argname, bind_free x xexp body env')
    | If (cond, body, elsebody) ->
            If (bind_free x xexp cond env, bind_free x xexp body env, bind_free x xexp elsebody env)
    | _ -> exp


let debruijn_index exp =
    let rec debruijn_index exp env idx =
        match exp with
        | Var v ->
                begin match lookup v env with
                | Some i -> Var (string_of_int i)
                | None -> failwith "unknown variable"
                end
        | Let (v, eb, exp') ->
                let env' = (v, idx)::env in
                Let (string_of_int idx, debruijn_index eb env (idx + 1), debruijn_index exp' env' (idx + 1))
        | LetRec (fname, fargname, body, exp') ->
                let env' = (fname, idx)::env in
                let env'' = (fargname, idx + 1)::env' in
                LetRec (string_of_int idx, string_of_int (idx + 1), debruijn_index body env'' (idx + 2), debruijn_index exp' env' (idx + 2))
        | Fun (x, body) ->
                let env' = (x, idx)::env in
                Fun (string_of_int idx, debruijn_index body env' (idx + 1))
        | App (e1, e2) -> App (debruijn_index e1 env (idx + 1), debruijn_index e2 env (idx + 1))
        | Plus (e1, e2) -> Plus (debruijn_index e1 env (idx + 1), debruijn_index e2 env (idx + 1))
        | Minus (e1, e2) -> Minus (debruijn_index e1 env (idx + 1), debruijn_index e2 env (idx + 1))
        | Times (e1, e2) -> Times (debruijn_index e1 env (idx + 1), debruijn_index e2 env (idx + 1))
        | Div (e1, e2) -> Div (debruijn_index e1 env (idx + 1), debruijn_index e2 env (idx + 1))
        | Eq (e1, e2) -> Eq (debruijn_index e1 env (idx + 1), debruijn_index e2 env (idx + 1))
        | Neq (e1, e2) -> Neq (debruijn_index e1 env (idx + 1), debruijn_index e2 env (idx + 1))
        | Greater (e1, e2) -> Greater (debruijn_index e1 env (idx + 1), debruijn_index e2 env (idx + 1))
        | Less (e1, e2) -> Less (debruijn_index e1 env (idx + 1), debruijn_index e2 env (idx + 1))
        | If (cond, body, elsebody) ->
                If (debruijn_index cond env (idx + 1), debruijn_index body env (idx + 1), debruijn_index elsebody env (idx + 1))
        | _ -> exp
    in debruijn_index exp [] 0

let rec cst_prop exp env =
    match exp with
    | Let (v, eb, exp') ->
            let eb' = cst_prop eb env in
            if isk eb' then
                let env' = (v, eb')::env in
                Let (v, eb', (cst_prop exp' env'))
            else
                Let (v ,eb', (cst_prop exp' env))
    | Var v ->
            begin match lookup v env with
            | Some e -> cst_prop e env
            | None -> exp
            end
    | Plus(e1, e2) -> Plus(cst_prop e1 env, cst_prop e2 env)
    | Minus(e1, e2) -> Minus(cst_prop e1 env, cst_prop e2 env)
    | Times(e1, e2) -> Times(cst_prop e1 env, cst_prop e2 env)
    | Div(e1, e2) -> Div(cst_prop e1 env, cst_prop e2 env)
    | Eq(e1, e2) -> Eq(cst_prop e1 env, cst_prop e2 env)
    | Neq(e1, e2) -> Neq(cst_prop e1 env, cst_prop e2 env)
    | Less(e1, e2) -> Less(cst_prop e1 env, cst_prop e2 env)
    | Greater(e1, e2) -> Greater(cst_prop e1 env, cst_prop e2 env)
    | LetRec (fname, fargname, body, exp') -> LetRec (fname, fargname, (cst_prop body env), cst_prop exp' env)
    | Fun (x, body) -> Fun (x, cst_prop body env)
    | If (cond, body, elsebody) -> If(cst_prop cond env, cst_prop body env, cst_prop elsebody env)
    | _ -> exp

let rec cst_fold exp env =
    let binop e1 e2 op =
            let (IntLit ev1, IntLit ev2) = (e1, e2) in
            IntLit (op ev1 ev2)
    in
    match exp with
    | Let (v, eb, exp') ->
            let eb' = cst_fold eb env in
            if isk eb' then
                let env' = (v, eb')::env in
                Let (v, eb', (cst_fold exp' env'))
            else
                Let (v, eb', (cst_fold exp' env))
    | Var v ->
            begin match lookup v env with
            | Some e -> cst_fold e env
            | None -> exp
            end
    | Plus (e1, e2) ->
        let e1' = cst_fold e1 env in
        let e2' = cst_fold e2 env in 
        if isk e1' && isk e2' then
            binop e1' e2' ( + )
        else exp
    | Minus (e1, e2) ->
       let e1' = cst_fold e1 env in
        let e2' = cst_fold e2 env in 
        if isk e1' && isk e2' then
            binop e1' e2' ( - )
        else exp
    | Times (e1, e2) ->
        let e1' = cst_fold e1 env in
        let e2' = cst_fold e2 env in 
        if isk e1' && isk e2' then
            binop e1' e2' ( * )
        else exp
    | Div (e1, e2) ->
        let e1' = cst_fold e1 env in
        let e2' = cst_fold e2 env in 
        if isk e1' && isk e2' then
            binop e1' e2' ( / )
        else exp
    | Eq (e1, e2) -> Eq (cst_fold e1 env, cst_fold e2 env)
    | Neq (e1, e2) -> Neq (cst_fold e1 env, cst_fold e2 env)
    | Greater (e1, e2) -> Greater (cst_fold e1 env, cst_fold e2 env)
    | Less (e1, e2) -> Less (cst_fold e1 env, cst_fold e2 env)
    | LetRec (fname, fargname, body, exp') -> LetRec (fname, fargname, cst_fold body env, cst_fold exp' env)
    | Fun (x, body) -> Fun (x, cst_fold body env)
    | If (cond, body, elsebody) -> If (cst_fold cond env, cst_fold body env, cst_fold elsebody env)
    | _ -> exp

let rec func_inline exp env =
    let rec func_inline' fn arg env =
        match fn with
        | Var v ->
                begin match lookup v env with
                | Some fn' -> func_inline' fn' arg ((v, fn)::env)
                | None   -> App (fn, arg)
                end
        | Fun (x, body) -> bind_free x (func_inline arg env) (func_inline body env) env
        | LetRec (_, _, _, _) -> App (fn, arg)
        | _ -> failwith "invalid applied"
    in
    match exp with
    | App (e1, e2) -> func_inline' e1 e2 env
    | Let (v, eb, exp') ->
            let eb' = func_inline eb env in
            let env' = (v, eb')::env in
            Let (v, eb', func_inline exp' env')
    | Plus(e1, e2) -> Plus(func_inline e1 env, func_inline e2 env)
    | Minus(e1, e2) -> Minus(func_inline e1 env, func_inline e2 env)
    | Times(e1, e2) -> Times(func_inline e1 env, func_inline e2 env)
    | Div(e1, e2) -> Div(func_inline e1 env, func_inline e2 env)
    | Eq(e1, e2) -> Eq(func_inline e1 env, func_inline e2 env)
    | Neq(e1, e2) -> Neq(func_inline e1 env, func_inline e2 env)
    | Less(e1, e2) -> Less(func_inline e1 env, func_inline e2 env)
    | Greater(e1, e2) -> Greater(func_inline e1 env, func_inline e2 env)
    | LetRec (fname, fargname, body, exp') -> LetRec (fname, fargname, func_inline body env, func_inline exp' env)
    | Fun (x, body) -> Fun(x, func_inline body env)
    | If (cond, body, elsebody) -> If (func_inline cond env, func_inline body env, func_inline elsebody env)
    | _ -> exp

let optimizer exp = cst_fold (func_inline (cst_prop (debruijn_index exp) []) []) []

