open Mysyntax;;
open Tostring;;

type binary_result =
    | IntResult of int
    | BoolResult of bool;;

type 'a fntyp =
    | IntFun of (int -> int -> int)
    | BoolFun1 of (int -> int -> bool)
    | BoolFun2 of (bool -> bool -> bool)
    | EqFun of ('a -> 'a -> bool);;

let (^>) = function
    | IntFun f -> fun (IntVal i) (IntVal j) -> IntVal (f i j)
    | BoolFun1 f -> fun (IntVal i) (IntVal j) -> BoolVal (f i j)
    | BoolFun2 f -> fun (BoolVal i) (BoolVal j) -> BoolVal (f i j)

let emptyenv () = []
let ext env x v = (x,v) :: env

let rec lookup x env =
   match env with
   | [] -> failwith ("unbound variable: " ^ x)
   | (y,v)::tl -> if x=y then v 
                  else lookup x tl 

let eval e env =
    let rec eval6 e env =     
      let binop f e1 e2 env = 
          (^>)f (eval6 e1 env) (eval6 e2 env)
      in 
      match e with
      (* | Semicol(e1, e2) -> let _ = eval6 e1 env in eval6 e2 env *)
      | Var(x)       -> lookup x env
      | IntLit(n)    -> IntVal(n)
      | BoolLit(b)   -> BoolVal(b)
      | Plus(e1,e2)  -> binop (IntFun (+)) e1 e2 env     (* env を追加 *)
      | Minus(e1, e2) -> binop (IntFun (-)) e1 e2 env
      | Times(e1,e2) -> binop (IntFun ( * )) e1 e2 env  (* env を追加 *)
      | Less(e1, e2) -> binop (BoolFun1 ( < )) e1 e2 env
      | Greater(e1, e2) -> binop (BoolFun1 ( > )) e1 e2 env
      (* | Eq (e1, e2) -> binop (EqFun (=)) e1 e2 env *)
      (* | Neq (e1, e2) -> binop (EqFun (<>)) e1 e2 env *)
  | Eq(e1,e2)    -> 
    begin
        match (eval6 e1 env, eval6 e2 env) with
        | (IntVal(n1),IntVal(n2)) -> BoolVal(n1=n2)
        | (BoolVal(b1),BoolVal(b2)) -> BoolVal(b1=b2)
        | _ -> failwith "wrong value"
    end
  | Neq(e1,e2)    -> 
    begin
        match (eval6 e1 env, eval6 e2 env) with
        | (IntVal(n1),IntVal(n2)) -> BoolVal(n1<>n2)
        | (BoolVal(b1),BoolVal(b2)) -> BoolVal(b1<>b2)
        | _ -> failwith "wrong value"
    end
     
     | If(e1,e2,e3) ->
        begin
          match (eval6 e1 env) with          (* env を追加 *)
          | BoolVal(true)  -> eval6 e2 env   (* env を追加 *)
          | BoolVal(false) -> eval6 e3 env   (* env を追加 *)
          | _ -> failwith "wrong value"
        end
      | Let(x,e1,e2) ->
          let env1 = ext env x (eval6 e1 env)
          in 
          eval6 e2 env1
        | LetRec(f,x,e1,e2) ->
          let env1 = ext env f (RecFunVal (f, x, e1, env))
          in eval6 e2 env1
      | Fun(x, e1) -> FunVal(x, e1, env)
      | App(e1,e2) ->
          let funpart = (eval6 e1 env) in
          let arg = (eval6 e2 env) in
            begin
             match funpart with
             | FunVal(x,body,env1) ->
                let env2 = (ext env1 x arg) in
                eval6 body env2
             | RecFunVal(f,x,body,env1) ->
                let env2 = (ext (ext env1 x arg) f funpart) in
                eval6 body env2
             | _ -> failwith "wrong value in App"
            end
      | e -> failwith ("unknown expression: " ^ (string_of_exp e))
    in eval6 e env

