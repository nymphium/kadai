open Mysyntax ;;
(* open Cps;; *)

(* type ir = *)
    (* | ParseFailed of string *)
    (* | IR          of exp *)

(* let parse str =  *)
    (* try IR(Myparser.parse Mylexer.token (Lexing.from_string str)) *)
    (* with Failure msg -> ParseFailed msg *)

(* let rec acc_line () = *)
    (* let l = read_line () in *)
    (* let len = String.length l in *)

    (* if l = ";;" then "" *)
    (* else if len >= 2 && (String.sub l (len - 2) 2) = ";;" then *)
        (* String.sub l 0 (len - 2) *)
    (* else l ^ " " ^ (acc_line ()) *)

(* let rec repl () = *)
    (* print_string "!> "; *)
    (* ( *)
        (* try acc_line () |> parse *)
        (* with End_of_file -> failwith "EOL" *)
    (* ) |> (function *)
        (* | ParseFailed msg -> print_string msg *)
        (* | IR e            -> print_string (Tostring.string_of_value (eval_cps e (Stdlib.standardlib ()) id))); *)
    (* repl (print_newline ()) *)


let parse str = Myparser.parse Mylexer.token (Lexing.from_string str)
