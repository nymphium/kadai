(* syntax.ml *)

(* 式の型 *)
type exp = 
  | Var       of string
  | IntLit    of int
  | BoolLit   of bool
  (* | StringLit of string *)
  (* | UnitLit *)
  (* | Semicol   of exp * exp *)
  | If        of exp * exp * exp
  | Let       of string * exp * exp
  | LetRec    of string * string * exp * exp
  | Fun       of string * exp
  | App       of exp * exp
  | Eq        of exp * exp
  | Neq       of exp * exp
  | Greater   of exp * exp
  | Less      of exp * exp
  | Plus      of exp * exp
  | Minus     of exp * exp
  | Times     of exp * exp
  | Div       of exp * exp
  (* | Empty     *)
  (* | Match     of exp * ((exp * exp) list) *)
  (* | Cons      of exp * exp *)
  (* | Head      of exp *)
  (* | Tail      of exp *)
  (* | Try       of exp * exp *)
  (* | Callcc    of exp *)

(* 値の型 *)
type value = 
  | IntVal    of int        (* integer value e.g. 17 *)
  | BoolVal   of bool       (* booleanvalue e.g. true *)
  (* | StringVal of string *)
  (* | UnitVal *)
  (* | ListVal   of value list (* list value e.g. [1;2;3] *) *)
  | FunVal    of string * exp * env
  | RecFunVal of string * string * exp * env
                          (* recursive function value: solution-2 *)
                          (* let rec f x = e1 in e2 *)
  (* | ErrorVal of string (* for try-with, ErrorVal("unknown expression") *) *)
  | StdFn    of (value -> value)
  (* | ContVal  of (value -> value) *)
and
  env = (string * value) list

