open Mysyntax;;
open Tostring;;

let standardlib () = [
    ("print_string", StdFn(function
        | StringVal s -> print_string s; UnitVal
        | v ->  ErrorVal("print_string error " ^ (string_of_value v))));
    ("print_int", StdFn(function
        | IntVal i -> print_int i; UnitVal
        | v -> ErrorVal("print_int error " ^ (string_of_value v))));
    ("string_of_int", StdFn(function
        | IntVal i -> StringVal (string_of_int i)
        | v -> ErrorVal("string_of_int error " ^ (string_of_value v))));
]
