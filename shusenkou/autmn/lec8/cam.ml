type cam_instr =
    | CAM_Ldi of int
    | CAM_Ldb of bool
    | CAM_Access of int
    | CAM_Closure of cam_code
    | CAM_Apply
    | CAM_Return
    | CAM_Let
    | CAM_EndLet
    | CAM_Test of cam_code * cam_code
    | CAM_Add
    | CAM_Minus
    | CAM_Times
    | CAM_Div
    | CAM_Eq
    | CAM_Neq
    | CAM_Greater
    | CAM_Less

and cam_code = cam_instr list

type cam_value =
    | CAM_IntVal  of int
    | CAM_BoolVal of bool
    | CAM_ClosVal of cam_code * cam_env
and cam_stack = cam_value list
and cam_env = cam_value list


let rec eval (code: cam_code) (env: cam_env) (stack: cam_stack) =
    let binop f c env stack =
        let ((CAM_IntVal v1))::stack' = stack in
        let ((CAM_IntVal v2))::stack'' = stack' in
        eval c env (CAM_IntVal(f v2 v1)::stack'')
    in
    match code with
    | [] -> let h::_ = stack in h
    | (CAM_Ldi n)::c -> eval c env ((CAM_IntVal n)::stack)
    | (CAM_Ldb b)::c -> eval c env ((CAM_BoolVal b)::stack)
    | (CAM_Access i)::c -> eval c env ((List.nth env i)::stack)
    | (CAM_Closure c')::c -> eval c env ((CAM_ClosVal(c', env))::stack)
    | CAM_Apply::c ->
        let (CAM_ClosVal(c', env'))::stack' = stack in
        let v::stack'' = stack' in
        eval c' (v::(CAM_ClosVal(c', env'))::env') (CAM_ClosVal(c, env)::stack'')
    | CAM_Return::c ->
        let v::stack' = stack in
        let (CAM_ClosVal(c', env'))::stack'' = stack' in
        eval c' env' (v::stack'')
    | CAM_Let::c ->
        let v::stack' = stack in
        eval c (v::env) stack'
    | CAM_EndLet::c ->
        let v::env' = env in
        eval c env' stack
    | CAM_Test(c1, c2)::c ->
        let (CAM_BoolVal vbool)::stack' = stack in
        eval c env ((eval (if vbool then c1 else c2) env stack')::stack')
    | CAM_Add::c -> binop (+) c env stack
    | CAM_Minus::c -> binop (-) c env stack
    | CAM_Times::c -> binop ( * ) c env stack
    | CAM_Div::c -> binop (/) c env stack
    | CAM_Eq::c ->
        let n1::stack' = stack in
        let n2::stack'' = stack' in
        begin
            match (n1, n2) with
            | (CAM_IntVal(v1), CAM_IntVal(v2)) -> eval c env (CAM_BoolVal(v1 = v2)::stack'')
            | (CAM_BoolVal(v1), CAM_BoolVal(v2)) -> eval c env (CAM_BoolVal(v1 = v2)::stack'')
            | _ -> failwith "eq error"
        end
    | CAM_Neq::c ->
        let n1::stack' =  stack in
        let n2::stack'' = stack' in
        begin
            match (n1, n2) with
            | (CAM_IntVal(v1), CAM_IntVal(v2)) -> eval c env (CAM_BoolVal(v1 <> v2)::stack'')
            | (CAM_BoolVal(v1), CAM_BoolVal(v2)) -> eval c env (CAM_BoolVal(v1 <> v2)::stack'')
            | _ -> failwith "not eq error"
        end
    | CAM_Greater::c ->
        let n1::stack' = stack in
        let n2::stack'' = stack' in
        begin
            match (n1, n2) with
            | (CAM_IntVal(v1), CAM_IntVal(v2)) -> eval c env (CAM_BoolVal(v2 > v1)::stack'')
            | (CAM_BoolVal(v1), CAM_BoolVal(v2)) -> eval c env (CAM_BoolVal(v2 > v1)::stack'')
            | _ -> failwith "greater error"
        end
    | CAM_Less::c ->
        let n1::stack' = stack in
        let n2::stack'' = stack' in
        begin
            match (n1, n2) with
            | (CAM_IntVal(v1), CAM_IntVal(v2)) -> eval c env (CAM_BoolVal(v2 < v1)::stack'')
            | (CAM_BoolVal(v1), CAM_BoolVal(v2)) -> eval c env (CAM_BoolVal(v2 < v1)::stack'')
            | _ -> failwith "less error"
        end

