open Mysyntax
open Cam

let rec position (x : string) (venv : string list) : int =
    match venv with
      | [] -> failwith "no matching variable in environment"
      | y::venv2 -> if x=y then 0 else (position x venv2) + 1

let rec compile v venv =
    match v with
    | Var(x) -> [CAM_Access (position x venv)]
    | Fun(x, e) -> [CAM_Closure((compile e (x::""::venv)) @ [CAM_Return])]
    | App(e1, e2) -> (compile e2 venv)@(compile e1 venv)@[CAM_Apply]
    | Let(x, e1, e2) -> (compile e1 venv)@CAM_Let::(compile e2 (x::venv))@[CAM_EndLet]
    | LetRec(f, x, e1, e2) -> (CAM_Closure((compile e1 (x::f::venv))@[CAM_Return]))::CAM_Let::(compile e2 (f::venv))@[CAM_EndLet]
    | IntLit(i) -> [CAM_Ldi i]
    | BoolLit(b) -> [CAM_Ldb b]
    | Plus(e1, e2) -> (compile e1 venv)@(compile e2 venv)@[CAM_Add]
    | Minus(e1, e2) -> (compile e1 venv)@(compile e2 venv)@[CAM_Minus]
    | Times(e1, e2) -> (compile e1 venv)@(compile e2 venv)@[CAM_Times]
    | Div(e1, e2) -> (compile e1 venv)@(compile e2 venv)@[CAM_Div]
    | Eq(e1, e2) -> (compile e1 venv)@(compile e2 venv)@[CAM_Eq]
    | Neq(e1, e2) -> (compile e1 venv)@(compile e2 venv)@[CAM_Neq]
    | Greater(e1, e2) -> (compile e1 venv)@(compile e2 venv)@[CAM_Greater]
    | Less(e1, e2) -> (compile e1 venv)@(compile e2 venv)@[CAM_Less]
    | If(cond, body, elsebody) -> (compile cond venv)@[CAM_Test((compile body venv), (compile elsebody venv))]

