let rec tcheck2 (te : tyenv) =
    let rec lookup x = function
        | [] -> failwith ("not found " ^ x)
        | (y, t) :: tl ->
            if  x =  y then t
            else lookup x tl
    in
    let tbinop e1 e2 te =
        match (tcheck2 te e1, tcheck2 te e2) with
        | (TInt, TInt) -> TInt
        | (_, _) -> failwith "binary operator error"
    in
    let tcomp e1 e2 te =
        match (tcheck2 te e1, tcheck2 te e2) with
        | (TInt, TInt) -> TBool
        | (TBool, TBool) -> TBool
        | (_, _)  -> failwith "compare error"
    in
    (* match e with *)
    function
    | IntLit _ -> TInt
    | BoolLit _ -> TBool
    | Semicol(_, e) -> tcheck2 te e
    | Var(s)    -> lookup s te
    | Plus(e1,e2)  ->  tbinop e1 e2 te
    | Minus(e1, e2) -> tbinop e1 e2 te
    | Div(e1, e2) -> tbinop e1 e2 te
    | Times(e1, e2) -> tbinop e1 e2 te
    | Eq(e1, e2) -> tcomp e1 e2 te
    | Neq(e1, e2) -> tcomp e1 e2 te
    | Greater(e1, e2) -> tcomp e1 e2 te
    | Less(e1, e2) -> tcomp e1 e2 te
    | If(cond, body, elsebody) ->
        begin
            match tcheck2 te cond with
            | TBool ->
                    match (tcheck2 te body, tcheck2 te elsebody) with
                    | (TInt, TInt) -> TInt
                    | (TBool, TBool) -> TBool
                    | _ -> failwith "body and elsebody doesn't match"
            | _ -> failwith "conditional error"
        end

