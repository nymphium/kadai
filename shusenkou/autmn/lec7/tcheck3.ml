let rec tcheck3 (te : tyenv) =
    let rec lookup x = function
        | [] -> failwith ("not found " ^ x)
        | (y, t) :: tl ->
            if  x =  y then t
            else lookup x tl
    in
    let ext env x t = (x, t)::env in
    let tbinop e1 e2 te =
        match (tcheck3 te e1, tcheck3 te e2) with
        | (TInt, TInt) -> TInt
        | _ -> failwith "binary operator error"
    in
    let tcomp e1 e2 te =
        match (tcheck3 te e1, tcheck3 te e2) with
        | (TInt, TInt) -> TBool
        | (TBool, TBool) -> TBool
        | _  -> failwith "compare error"
    in
    (* match e with *)
    function
    | IntLit _ -> TInt
    | BoolLit _ -> TBool
    | Semicol(_, e) -> tcheck3 te e
    | Var(s)    -> lookup s te
    | Plus(e1,e2)  ->  tbinop e1 e2 te
    | Minus(e1, e2) -> tbinop e1 e2 te
    | Div(e1, e2) -> tbinop e1 e2 te
    | Times(e1, e2) -> tbinop e1 e2 te
    | Eq(e1, e2) -> tcomp e1 e2 te
    | Neq(e1, e2) -> tcomp e1 e2 te
    | Greater(e1, e2) -> tcomp e1 e2 te
    | Less(e1, e2) -> tcomp e1 e2 te
    | If(cond, body, elsebody) ->
        begin
            match tcheck3 te cond with
            | TBool ->
                let tb = tcheck3 te body in
                let teb = tcheck3 te elsebody in
                if tb = teb then tb
                else failwith "body and elsebody error"
            | _ -> failwith "conditional error"
        end
    | Fun(x, e) ->
            let t1 = lookup x te in
            let t2 = tcheck3 te e in
            TArrow(t1, t2)
    | App(e1, e2) ->
            let t1 = tcheck3 te e1 in
            let t2 = tcheck3 te e2 in
            begin
                match t1 with
                | TArrow(t10, t11) ->
                    if t2 = t10 then t11
                    else failwith "type error in App"
                | _ -> failwith "type error in Ap"
            end
    | Let(x, e1, e2) -> tcheck3 (ext te x (tcheck3 te e1)) e2
    | _ -> failwith "unknown expression"

