open Myparsing;;
open Eval;;

let rec repl () =
    let rec acc_line () =
        let l = read_line () in
        let len = String.length l in

        if l = ";;" then ""
        else if len >= 2 && (String.sub l (len - 2) 2) = ";;" then
            String.sub l 0 (len - 2)
        else l ^ " " ^ (acc_line ())
    in

    print_string "!> ";
    (
        try acc_line () |> parse
        with End_of_file -> exit 0
    ) |> (function
        | ParseFailed msg -> print_string msg
        | IR e            -> print_string ("- : " ^ (Tostring.string_of_value (eval_cps e [] id))));
    repl (print_newline ());;

(* repl () *)

