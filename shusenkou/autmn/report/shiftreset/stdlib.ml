open Mysyntax;;
open Tostring;;

let standardlib () = [
    ("print_endline", StdFn(function
        | StringVal s -> print_endline s; UnitVal
        | v ->  ErrorVal("print_endline error " ^ (string_of_value v))));
    ("print_string", StdFn(function
        | StringVal s -> print_string s; UnitVal
        | v ->  ErrorVal("print_string error " ^ (string_of_value v))));
    ("print_int", StdFn(function
        | IntVal i -> print_int i; UnitVal
        | v -> ErrorVal("print_int error " ^ (string_of_value v))));
    ("print_newline", StdFn(function
        | UnitVal -> print_newline (); UnitVal
        | v -> ErrorVal("print_newline error " ^ (string_of_value v))));
    ("string_of_int", StdFn(function
        | IntVal i -> StringVal (string_of_int i)
        | v -> ErrorVal("string_of_int error " ^ (string_of_value v))));
]
