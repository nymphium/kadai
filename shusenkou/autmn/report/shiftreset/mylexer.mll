{
open Myparser
open Lexing
}

let space = [' ' '\t' '\n' '\r']
let digit = ['0'-'9']
let alpha = ['A'-'Z' 'a'-'z' '_']
let alnum = digit | alpha | '\''

rule token = parse
  | digit+
    { INT (int_of_string (lexeme lexbuf)) }
  | "true"    { TRUE }
  | "false"   { FALSE }
  | '+'       { PLUS }
  | '-'       { MINUS }
  | '*'       { ASTERISK }
  | '/'       { SLASH }
  | '^'       { HAT }
  | '='       { EQUAL }
  | "<>"      { NEQUAL }
  | '<'       { LESS }
  | '>'       { GREATER }
  | ';'       { SEMICOL }
  | '"'       { DQUOT }
  | '('       { LPAREN }
  | ')'       { RPAREN }
  | '['       { LBRA }
  | ']'       { RBRA }
  | "->"      { ARROW }
  | "if"      { IF }
  | "then"    { THEN }
  | "else"    { ELSE }
  | "let"     { LET }
  | "rec"     { REC }
  | "in"      { IN }
  | "fun"     { FUN }
  | "shift"   { SHIFT }
  | "reset"   { RESET }
  | alpha alnum*
    { VAR (lexeme lexbuf) }
  | space+    { token lexbuf }
  | eof       { EOF }
  | _
    {
      let message = Printf.sprintf
        "unknown token %s near characters %d-%d"
        (lexeme lexbuf)
        (lexeme_start lexbuf)
        (lexeme_end lexbuf)
      in
      failwith message
    }

