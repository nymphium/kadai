%{
open Mysyntax
%}

%token <string> VAR
%token <int> INT
%token TRUE
%token FALSE

%token DQUOT

%token PLUS
%token MINUS
%token ASTERISK
%token SLASH
%token HAT

%token EQUAL
%token NEQUAL
%token LESS
%token GREATER

%token SEMICOL

%token LPAREN
%token RPAREN

%token LBRA
%token RBRA

%token IF
%token THEN
%token ELSE

%token LET
%token REC
%token IN

%token ARROW
%token FUN

%token SHIFT
%token RESET

%token EOF 

%nonassoc IN ELSE ARROW
%left EQUAL GREATER LESS
%left PLUS MINUS HAT
%left ASTERISK SLASH
%left SEMICOL
%nonassoc UNARY
%nonassoc SHIFT
%nonassoc RESET
%nonassoc FUN
%left VAR INT TRUE FALSE LBRA LPAREN

%start parse
%type <Mysyntax.exp> parse

%%

parse: exp EOF { $1 } ;

arg_exp:
  | VAR { Var $1 }
  | INT { IntLit $1 }
  | DQUOT VAR DQUOT { StringLit $2 }
  | TRUE { BoolLit true }
  | FALSE { BoolLit false }
  | LPAREN exp RPAREN { $2 }
  | LPAREN RPAREN { UnitLit }
;

exp:
  | arg_exp { $1 }
  | exp arg_exp
    { App ($1, $2) }
  | MINUS exp %prec UNARY
    { Minus (IntLit 0, $2) }
  | exp PLUS exp
    { Plus ($1, $3) }
  | exp MINUS exp
    { Minus ($1, $3) }
  | exp ASTERISK exp
    { Times ($1, $3) }
  | exp SLASH exp
    { Div ($1, $3) }
  | exp HAT exp
    { Concat ($1, $3) }
  | exp EQUAL exp
    { Eq ($1, $3) }
  | exp NEQUAL exp
    { Neq ($1, $3) }
  | exp LESS exp
    { Less ($1, $3) }
  | exp GREATER exp
    { Greater ($1, $3) }
  | exp SEMICOL exp
    { Semicol ($1, $3) }
  | FUN VAR ARROW exp
    { Fun ($2, $4) }
  | LET VAR EQUAL exp IN exp
    { Let ($2, $4, $6) }
  | LET REC VAR VAR EQUAL exp IN exp
    { LetRec ($3, $4, $6, $8) }
  | IF exp THEN exp ELSE exp
    { If ($2, $4, $6) }
  | SHIFT exp { Shift $2 }
  | RESET exp { Reset $2 }
;

