open Mysyntax;;

type ir =
    | ParseFailed of string
    | IR          of exp

let parse str = 
    try IR(Myparser.parse Mylexer.token (Lexing.from_string str))
    with Failure msg -> ParseFailed msg

