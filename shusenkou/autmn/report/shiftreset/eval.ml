open Mysyntax;;
open Tostring;;
open Stdlib;;

let stdlib = standardlib();;

let rec lookup x = function
    | []           -> None
    | (y, t) :: tl ->
        if x = y then Some t
        else lookup x tl

let binops = [("+", ( + )); ("-", ( - )); ("*", ( * )); ("/", ( / ))]

let emptyenv () = []
let ext env x t = (x, t) :: env

let id o = o
let rec eval_cps e env k =
    let binop_cps f e1 e2 env k =
        eval_cps e1 env (fun t1 ->
        eval_cps e2 env (fun t2 ->
        match (t1, t2) with
        | (IntVal(n1), IntVal(n2)) -> 
            k (match lookup f binops with
            | Some fn -> IntVal (fn n1 n2)
            | None -> ErrorVal ("undefined function: " ^ f))
            | _   -> k (ErrorVal(Printf.sprintf "integer value expected: %s %s %s" (string_of_value t1) f (string_of_value t2)))))
    in
    match e with
    | Var(x)                       ->
        k (match lookup x env with
        | Some v -> v
        | None ->
            begin match lookup x stdlib with
            | Some fn -> fn
            | None -> ErrorVal ("unbound variable: " ^ x)
            end)
    | IntLit(n)                    -> k (IntVal(n))
    | BoolLit(b)                   -> k (BoolVal(b))
    | StringLit(s)                 -> k (StringVal s)
    | UnitLit                      -> k UnitVal
    | Plus(e1, e2)                 -> binop_cps "+" e1 e2 env k
    | Minus(e1, e2)                -> binop_cps "-" e1 e2 env k
    | Times(e1, e2)                -> binop_cps "*" e1 e2 env k
    | Div(e1, e2)                  -> binop_cps "/" e1 e2 env k
    | Concat (e1, e2)              ->
        eval_cps e1 env (fun t1 ->
        eval_cps e2 env (fun t2 ->
        k (match (t1, t2) with
        | (StringVal(s1), StringVal(s2)) -> k (StringVal (s1 ^ s2))
        | _ -> k (ErrorVal (Printf.sprintf "string value expected: %s ^ %s"
            (string_of_value t1) (string_of_value t2))))))
    | Let(x, e1, e2)               -> eval_cps e1 env (fun t1 -> eval_cps e2 (ext env x t1) k)
    | LetRec(fname, arg, body, e2) -> eval_cps e2 (ext env fname (RecFunVal(fname, arg, body, env))) k
    | Semicol(e1, e2)              ->
        eval_cps e1 env (fun t ->
        match t with
        | ErrorVal msg -> k t
        | _            -> eval_cps e2 env k)
    | If(cond, body, elsebody)     ->
        eval_cps cond env (function
        | BoolVal b when b -> eval_cps body env k
        | BoolVal b        -> eval_cps elsebody env k
        | _         -> k (ErrorVal "invalid conditional"))
    | Eq(e1, e2)                   ->
        eval_cps e1 env (fun t1 ->
        eval_cps e2 env (fun t2 ->
        match (t1, t2) with
        | (IntVal(n1), IntVal(n2))   -> k (BoolVal(n1 = n2))
        | (BoolVal(b1), BoolVal(b2)) -> k (BoolVal(b1 = b2))
        | _                          -> k (ErrorVal "eq error")))
    | Neq(e1, e2)                    ->
        eval_cps e1 env (fun t1 ->
        eval_cps e2 env (fun t2 ->
        match (t1, t2) with
        | (IntVal(n1),  IntVal(n2))  -> k (BoolVal(n1 <> n2))
        | (BoolVal(b1), BoolVal(b2)) -> k (BoolVal(b1 <> b2))
        | _                          -> k (ErrorVal "eq error")))
    | Greater(e1, e2)                ->
        eval_cps e1 env (fun t1 ->
        eval_cps e2 env (fun t2 ->
        match (t1, t2) with
        | (IntVal(n1), IntVal(n2)) -> k (BoolVal(n1 > n2))
        | _                        -> k (ErrorVal "greater error")))
    | Less(e1, e2)                 ->
        eval_cps e1 env (fun t1 ->
        eval_cps e2 env (fun t2 ->
        match (t1, t2) with
        | (IntVal(n1), IntVal(n2)) -> k (BoolVal(n1 < n2))
        | _                        -> k (ErrorVal "less error")))
    | Fun(x, e1)                   -> k (FunVal(x, e1, env))
    | App(e1, e2)                  ->
        eval_cps e1 env (fun f   ->
        eval_cps e2 env (fun arg ->
        app f arg k))
    | Shift fn ->
        eval_cps fn env (function
        | FunVal (var, body, env') -> eval_cps body (ext env' var (ContVal k)) id
        | e -> k (ErrorVal ("wrong value in Reset " ^ (string_of_value e))))
    | Reset fn ->
        k (eval_cps fn env (function
            | FunVal (_, body, env') -> eval_cps body env' id
            | e -> (ErrorVal ("wrong value in Reset " ^ (string_of_value e)))))
and app f arg k =
    match f with
    | FunVal(x, body, env1)           -> eval_cps body (ext env1 x arg) k
    | RecFunVal(fname, x, body, env1) -> eval_cps body (ext (ext env1 x arg) fname f)  k
    | ContVal k'  -> k (k' arg)
    | StdFn(fn)                       -> k (fn arg)
    | t                               -> k (ErrorVal("wrong value in App " ^ (string_of_value t)))

let eval e = eval_cps e [] id

