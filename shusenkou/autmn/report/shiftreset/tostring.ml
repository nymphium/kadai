open Mysyntax;;

let rec string_of_exp =
    let rec string_of_explist = function
        | []    -> ""
        | [e]   -> string_of_exp e
        | e::es -> (string_of_exp e) ^ ", " ^ (string_of_explist es)
    in
    function
    | Var(e)               ->  "Var(\"" ^ e ^ "\")"
    | IntLit(e)            ->  "IntLit(" ^ (string_of_int e) ^ ")"
    | BoolLit(e)           ->  "BoolLit(" ^ (string_of_bool e) ^ ")"
    | StringLit(e)         ->  "StringLit(\""^ e ^ "\")"
    | UnitLit              -> "UnitLit"
    | Plus(e1, e2)         -> "Plus(" ^ (string_of_explist [e1; e2]) ^ ")"
    | Minus(e1, e2)        -> "Minus(" ^ (string_of_explist [e1; e2]) ^ ")"
    | Times(e1, e2)        -> "Times(" ^ (string_of_explist [e1; e2]) ^ ")"
    | Div(e1, e2)          -> "Div(" ^ (string_of_explist [e1; e2]) ^ ")"
    | Concat(e1, e2)       -> "Concat(" ^ (string_of_explist [e1; e2]) ^ ")"
    | Let(e1, e2, e3)      -> "Let(" ^ e1 ^ (string_of_explist [e2; e3]) ^ ")"
    | LetRec(f, v, e1, e2) -> "LetRec(" ^ f ^ ", " ^ v ^ ", " ^ (string_of_explist [e1; e2]) ^ ")"
    | Semicol(e1, e2)      -> "Semicol(" ^ (string_of_explist [e1; e2]) ^ ")"
    | If(e1, e2, e3)       -> "If(" ^ (string_of_explist [e1; e2; e3]) ^ ")"
    | Eq(e1, e2)           -> "Eq(" ^ (string_of_explist [e1; e2]) ^ ")"
    | Neq(e1, e2)          -> "Neq(" ^ (string_of_explist [e1; e2]) ^ ")"
    | Greater(e1, e2)      -> "Greater(" ^ (string_of_explist [e1; e2]) ^ ")"
    | Less(e1, e2)         -> "Less(" ^ (string_of_explist [e1; e2]) ^ ")"
    | Fun(e1, e2)          -> "Fun(\"" ^ e1 ^ "\", " ^ (string_of_exp e2) ^ ")"
    | App(e1, e2)          -> "App(" ^ (string_of_explist [e1; e2]) ^ ")"
    | Shift e -> "Shift " ^ (string_of_exp e)
    | Reset e -> "Reset " ^ (string_of_exp e)

let rec string_of_value =
    let rec string_of_env e =
        let rec string_of_envval = function
        | []         -> ""
        | [(s, v)]   -> s ^ ": " ^ (string_of_value v)
        | (s, v)::ls -> s ^ ": " ^ (string_of_value v) ^ "; " ^ (string_of_envval ls)
        in "[" ^ (string_of_envval e) ^ "]"
    in
    function
    | StdFn _                       -> "StdFn <fun>"
    | UnitVal                       -> "UnitVal"
    | IntVal i                      -> "IntVal(" ^ (string_of_int i) ^ ")"
    | BoolVal b                     -> "BoolVal(" ^ (string_of_bool b) ^ ")"
    | StringVal s                   -> "StringVal(\"" ^ s ^ "\")"
    | ErrorVal e                    -> "ErrorVal(\""  ^ e ^ "\")"
    | FunVal(fname, arg, env)       -> "FunVal(\"" ^ fname ^ "\", " ^ (string_of_exp arg) ^ ", " ^ (string_of_env env) ^ ")"
    | RecFunVal(fname, arg, e, env) -> "RecFunVal(\"" ^ fname ^ "\", \"" ^ arg ^ "\", " ^ (string_of_exp e) ^ (string_of_env env) ^ ")"
    | ContVal v                     -> "ContVal(value -> value)"

let rec unparse =
    let braceor e = 
        match e with
        | Var v -> v
        | IntLit i -> string_of_int i
        | BoolLit b -> string_of_bool b
        | StringLit s -> "\"" ^ s ^ "\""
        | UnitLit -> "()"
        | _     -> "(" ^ (unparse e) ^ ")"
    in
    function
    | Var v -> v
    | IntLit i -> string_of_int i
    | BoolLit b -> string_of_bool b
    | StringLit s -> "\"" ^ s ^ "\""
    | UnitLit -> "()"
    | Plus (e1, e2) -> (unparse e1) ^ " + " ^ (unparse e2)
    | Minus (e1, e2) -> (unparse e1) ^ " - " ^ (unparse e2)
    | Times (e1, e2) -> (braceor e1) ^ " * " ^ (braceor e2)
    | Div(e1, e2) -> (braceor e1) ^ " / " ^ (braceor e2)
    | Concat(e1, e2) -> (braceor e1) ^ " ^ " ^ (braceor e2)
    | Let (v, eb, exp') -> "let " ^ v ^ " = " ^ (unparse eb) ^ " in " ^ (unparse exp')
    | LetRec (f, v, eb, exp') -> "let rec " ^ f ^ " " ^ v ^ " = " ^ (unparse eb) ^ " in " ^ (unparse exp')
    | Semicol (e1, e2) -> (unparse e1) ^ "; " ^ (unparse e2)
    | If (cond, body, elsebody) -> "if " ^ (unparse cond) ^ " then " ^ (unparse body) ^ " else " ^ (unparse elsebody)
    | Eq (e1, e2) -> (braceor e1) ^ " = " ^ (braceor e2)
    | Neq (e1, e2) -> (braceor e1) ^ " <> " ^ (braceor e2)
    | Less (e1, e2) -> (braceor e1) ^ " < " ^ (braceor e2)
    | Greater (e1, e2) -> (braceor e1) ^ " > " ^ (braceor e2)
    | Fun (v, body) -> "fun " ^ v ^ " -> " ^ (unparse body)
    | App (f, a) -> (unparse f) ^ " " ^ (braceor a)
    | Shift fn -> "shift " ^ (unparse fn)
    | Reset fn -> "reset " ^ (unparse fn)

