type token =
  | VAR of (string)
  | INT of (int)
  | TRUE
  | FALSE
  | DQUOT
  | PLUS
  | MINUS
  | ASTERISK
  | SLASH
  | HAT
  | EQUAL
  | NEQUAL
  | LESS
  | GREATER
  | SEMICOL
  | LPAREN
  | RPAREN
  | LBRA
  | RBRA
  | IF
  | THEN
  | ELSE
  | LET
  | REC
  | IN
  | ARROW
  | FUN
  | SHIFT
  | RESET
  | EOF

val parse :
  (Lexing.lexbuf  -> token) -> Lexing.lexbuf -> Mysyntax.exp
