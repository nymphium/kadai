type exp = 
  | Var       of string
  | IntLit    of int
  | BoolLit   of bool
  | StringLit of string
  | UnitLit
  | Semicol   of exp * exp
  | If        of exp * exp * exp
  | Let       of string * exp * exp
  | LetRec    of string * string * exp * exp
  | Fun       of string * exp
  | App       of exp * exp
  | Eq        of exp * exp
  | Neq       of exp * exp
  | Greater   of exp * exp
  | Less      of exp * exp
  | Plus      of exp * exp
  | Minus     of exp * exp
  | Times     of exp * exp
  | Div       of exp * exp
  | Concat    of exp * exp
  | Shift     of exp
  | Reset     of exp

type value = 
  | IntVal    of int 
  | BoolVal   of bool
  | StringVal of string
  | UnitVal
  | FunVal    of string * exp * env
  | RecFunVal of string * string * exp * env
  | ContVal of (value -> value)
  | StdFn    of (value -> value)
  | ErrorVal of string (* for try-with, ErrorVal("unknown expression") *)
and
  env = (string * value) list

