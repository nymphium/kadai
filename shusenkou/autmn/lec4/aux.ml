open Syntax;;

let string_of_value = function
    | IntVal  v -> Printf.sprintf "%d" v
    | BoolVal b -> Printf.sprintf "%B" b
    (* | ListVal l when l = [] -> "[]" *)
    (* ... *)

let print_exp e = Printf.printf "%s\n" (string_of_value e)
