type exp =
  |  IntLit of int
  |  Plus of exp * exp 
  |  Times of exp * exp
  |  BoolLit of bool        (* 追加分; 真理値リテラル, つまり trueや false  *)
  |  If of exp * exp * exp  (* 追加分; if-then-else式 *)
  |  Eq of exp * exp        (* 追加分; e1 = e2 *)
  |  Greater of exp * exp
  |  Var of string
  |  Let of string * exp * exp

type value =
  | IntVal  of int          (* 整数の値 *)
  | BoolVal of bool         (* 真理値の値 *)
