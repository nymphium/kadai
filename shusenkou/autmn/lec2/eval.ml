type exp =
  |  IntLit of int
  |  Plus of exp * exp 
  |  Minus of exp * exp
  |  Times of exp * exp
  |  Div of exp * exp;;

let rec eval1 e =
  match e with
  | IntLit(n) -> n
  | Plus(e1,e2) -> (eval1 e1) + (eval1 e2)
  | Minus(e1,e2) -> (eval1 e1) - (eval1 e2)
  | Times(e1,e2) -> (eval1 e1) * (eval1 e2)
  | Div(e1,e2) ->
          let e1', e2' = eval1 e1, eval1 e2 in
          if e2' == 0 then failwith "Division by zero"
          else e1' / e2'
  | _ -> failwith "unknown expression" ;;
