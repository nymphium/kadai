type exp =
    IntLit of int
  | Plus of exp * exp
  | Minus of exp * exp
  | Times of exp * exp
  | Div of exp * exp;;

let rec allabs e =
    match e with
    | IntLit n -> IntLit (abs n)
    | Plus (n, m) -> Plus (allabs n, allabs m)
    | Minus (n, m) -> Minus (allabs n, allabs m)
    | Times (n, m) -> Times (allabs n, allabs m)
    | Div (n, m) -> Div (allabs n, allabs m);;
