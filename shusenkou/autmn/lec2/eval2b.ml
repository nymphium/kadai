let rec eval2b e =
    (* let binop f e1 e2 = *)
        (* match (eval2b e1, eval2b e2) with *)
        (* | (IntVal(n1), IntVal(n2)) -> IntVal (f n1 n2) *)
        (* | _ -> failwith "integer values expected" *)
    (* in *)
    match e with
    | IntLit(n)  -> IntVal(n)
    | Plus(e1,e2) -> binop (+) e1 e2
    | Times(e1,e2) -> binop ( * ) e1 e2
    | Eq(e1,e2) ->
        begin
            match (eval2b e1, eval2b e2) with
            | (IntVal(n1),IntVal(n2)) -> BoolVal(n1=n2)
            | (BoolVal(b1),BoolVal(b2)) -> BoolVal(b1=b2)
            | _ -> failwith "wrong value"
        end
    | Greater(e1,e2) ->
        begin
            match (eval2b e1, eval2b e2) with
            | (IntVal(n1),IntVal(n2)) -> BoolVal(n1>n2)
            | _ -> failwith "wrong value"
        end
    | BoolLit(b) -> BoolVal(b)
    | If(e1,e2,e3) ->
        begin
            match (eval2b e1) with
            | BoolVal(true) -> eval2b e2
            | BoolVal(false) -> eval2b e3
            | _ -> failwith "wrong value"
        end
    | _ -> failwith "unknown expression"
and binop f e1 e2 =
    match (eval2b e1, eval2b e2) with
    | (IntVal(n1), IntVal(n2)) -> IntVal (f n1 n2)
    | _ -> failwith "integer values expected"

