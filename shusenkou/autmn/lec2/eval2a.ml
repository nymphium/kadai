let rec eval2a e =
    let binop flag e1 e2 =
        match (eval2a e1, eval2a e2) with
        | (IntVal(n1),IntVal(n2)) -> 
            if flag = 1 then IntVal(n1 + n2)
            else IntVal(n1 * n2)
        | _ -> failwith "integer values expected"
    in
    match e with
    | IntLit(n)    -> IntVal(n)
    | Plus(e1,e2)  -> binop 1 e1 e2
    | Times(e1,e2) -> binop 2 e1 e2
    | _ -> failwith "unknown expression";;
