open Mysyntax;;
open Tostring;;

let emptyenv () = []
let ext env x t = (x, t) :: env

let id o = o
let rec eval_cps e env k =
    let rec lookup x = function
        | []           -> ErrorVal ("unbound variable: " ^ x)
        | (y, t) :: tl ->
            if x = y then t
            else lookup x tl
    in
    let binop_cps f e1 e2 env k =
        eval_cps e1 env (fun t1 ->
        eval_cps e2 env (fun t2 ->
        match (t1, t2) with
        | (IntVal(n1), IntVal(n2)) -> k (IntVal(f n1 n2))
        | _                        -> k (ErrorVal("integer value expected: " ^ (string_of_value t1) ^ " + " ^ (string_of_value t2)))))
    in
    let rec matcher m es env k =
        match es with
        | []            -> k (ErrorVal "empty match")
        | (me, ex)::es' ->
            eval_cps me env (function
            | mv when mv = m || mv = StringVal("_") -> eval_cps ex env k
            | _                                     -> matcher m es' env k)
    in
    match e with
    | Var(x)                       -> k (lookup x env)
    | IntLit(n)                    -> k (IntVal(n))
    | BoolLit(b)                   -> k (BoolVal(b))
    | StringLit(s)                 -> k (StringVal s)
    | UnitLit                      -> k UnitVal
    | Empty                        -> k (ListVal [])
    | Fun(x, e1)                   -> k (FunVal(x, e1, env))
    | Plus(e1, e2)                 -> binop_cps (+) e1 e2 env k
    | Minus(e1, e2)                -> binop_cps (-) e1 e2 env k
    | Times(e1, e2)                -> binop_cps ( * ) e1 e2 env k
    | Div(e1, e2)                  -> binop_cps (/) e1 e2 env k
    | Let(x, e1, e2)               -> eval_cps e1 env (fun t1 -> eval_cps e2 (ext env x t1) k)
    | LetRec(fname, arg, body, e2) -> eval_cps e2 (ext env fname (RecFunVal(fname, arg, body, env))) k
    | Callcc e                     -> eval_cps e env (fun funpart -> app funpart (ContVal k) k)
    | Match(e1, es)                -> eval_cps e1 env (fun t1 -> matcher t1 es env k)
    | Head(e)                      ->
        eval_cps e env (function 
        | ListVal(h::_) -> k h
        | t             -> k (ErrorVal("Failure \"hd\" " ^ string_of_value t)))
    | Tail(e)                      ->
        eval_cps e env (function
        | ListVal(h::rs) -> k (ListVal(rs))
        | _              -> k (ErrorVal "Failure \"tl\""))
    | Semicol(e1, e2)              ->
        eval_cps e1 env (fun t ->
        match t with
        | ErrorVal msg -> k t
        | _            -> eval_cps e2 env k)
    | App(e1, e2)                  ->
        eval_cps e1 env (fun f   ->
        eval_cps e2 env (fun arg ->
        app f arg k))
    | Greater(e1, e2)                ->
        eval_cps e1 env (fun t1 ->
        eval_cps e2 env (fun t2 ->
        match (t1, t2) with
        | (IntVal(n1), IntVal(n2)) -> k (BoolVal(n1 > n2))
        | _                        -> k (ErrorVal "greater error")))
    | Less(e1, e2)                 ->
        eval_cps e1 env (fun t1 ->
        eval_cps e2 env (fun t2 ->
        match (t1, t2) with
        | (IntVal(n1), IntVal(n2)) -> k (BoolVal(n1 < n2))
        | _                        -> k (ErrorVal "less error")))
    | If(cond, body, elsebody)     ->
        eval_cps cond env (function
        | BoolVal b ->
            if b then eval_cps body env k
            else eval_cps elsebody env k
        | _         -> k (ErrorVal "invalid conditional"))
    | Cons(e1, e2)                 ->
        eval_cps e1 env (fun t1 ->
        eval_cps e2 env (function
        | ListVal [] -> k (ListVal [t1])
        | ListVal l  -> k (ListVal (t1::l))
        | _          -> k (ErrorVal "cons error")))
    | Eq(e1, e2)                   ->
        eval_cps e1 env (fun t1 ->
        eval_cps e2 env (fun t2 ->
        match (t1, t2) with
        | (IntVal(n1), IntVal(n2))   -> k (BoolVal(n1 = n2))
        | (BoolVal(b1), BoolVal(b2)) -> k (BoolVal(b1 = b2))
        | _                          -> k (ErrorVal "eq error")))
    | Neq(e1, e2)                    ->
        eval_cps e1 env (fun t1 ->
        eval_cps e2 env (fun t2 ->
        match (t1, t2) with
        | (IntVal(n1),  IntVal(n2))  -> k (BoolVal(n1 <> n2))
        | (BoolVal(b1), BoolVal(b2)) -> k (BoolVal(b1 <> b2))
        | _                          -> k (ErrorVal "eq error")))
    | Try(e1, e2)                  ->
        eval_cps e1 env (function
        | ErrorVal(msg) ->
            begin
                match e2 with
                | Fun(x, body) -> eval_cps body (ext env x (StringVal msg)) k
                | e            -> eval_cps e env k
            end
        | t1            -> k t1)

and app f arg k =
    match f with
    | FunVal(x, body, env1)           -> eval_cps body (ext env1 x arg) k
    | RecFunVal(fname, x, body, env1) -> eval_cps body (ext (ext env1 x arg) fname f)  k
    | ContVal k'                      -> k' arg
    | StdFn(fn)                       -> k (fn arg)
    | t                               -> k (ErrorVal("wrong value in App " ^ (string_of_value t)))

let eval e = eval_cps e [] id

