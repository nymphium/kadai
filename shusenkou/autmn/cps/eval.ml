open Mysyntax;;

let emptyenv () = []
let ext env x v = (x,v) :: env

let rec lookup x env =
   match env with
   | [] -> failwith ("unbound variable: " ^ x)
   | (y,v)::tl -> if x=y then v 
                  else lookup x tl 
(* let string_of_env env = *)
    (* let rec string_of_env' env acc = *)
        (* match env with *)
        (* | [] when acc = "" -> "[]" *)
        (* | []    -> Printf.sprintf "[%s]" acc *)
        (* | (varname, e)::es -> *)
                (* match e with *)
                (* | IntVal i -> string_of_env' es (acc ^ (Printf.sprintf "(%S, IntVal %d); " varname i)) *)
                (* | BoolVal b -> string_of_env' es (acc ^ (Printf.sprintf "(%S, BoolVal %B); " varname b)) *)
    (* in *)
    (* string_of_env' env "";; *)


let rec eval6 e env =           (* env を引数に追加 *)
  let binop f e1 e2 env =       (* binop の中でも eval6 を呼ぶので env を追加 *)
    match (eval6 e1 env, eval6 e2 env) with
    | (IntVal(n1),IntVal(n2)) -> IntVal(f n1 n2)
    | _ -> failwith "integer value expected"
  in 
  match e with
  | Semicol(e1, e2) -> let _ = eval6 e1 env in eval6 e2 env
  | Var(x)       -> lookup x env
  | IntLit(n)    -> IntVal(n)
  | BoolLit(b)   -> BoolVal(b)
  | Plus(e1,e2)  -> binop (+) e1 e2 env     (* env を追加 *)
  | Minus(e1, e2) -> binop (-) e1 e2 env
  | Times(e1,e2) -> binop ( * ) e1 e2 env   (* env を追加 *)
  | Eq(e1,e2)    -> 
    begin
        match (eval6 e1 env, eval6 e2 env) with
        | (IntVal(n1),IntVal(n2)) -> BoolVal(n1=n2)
        | (BoolVal(b1),BoolVal(b2)) -> BoolVal(b1=b2)
        | _ -> failwith "wrong value"
    end
  | Neq(e1,e2)    -> 
    begin
        match (eval6 e1 env, eval6 e2 env) with
        | (IntVal(n1),IntVal(n2)) -> BoolVal(n1<>n2)
        | (BoolVal(b1),BoolVal(b2)) -> BoolVal(b1<>b2)
        | _ -> failwith "wrong value"
    end
  | If(e1,e2,e3) ->
    begin
      match (eval6 e1 env) with          (* env を追加 *)
      | BoolVal(true)  -> eval6 e2 env   (* env を追加 *)
      | BoolVal(false) -> eval6 e3 env   (* env を追加 *)
      | _ -> failwith "wrong value"
    end
  | Let(x,e1,e2) ->
      (* print_string ((string_of_env env) ^ " -> "); *)
      let env1 = ext env x (eval6 e1 env)
      in 
      (* print_string ((string_of_env env1) ^ "\n"); *)
      eval6 e2 env1
    | LetRec(f,x,e1,e2) ->
      let env1 = ext env f (RecFunVal (f, x, e1, env))
      in eval6 e2 env1
  | Fun(x, e1) -> FunVal(x, e1, env)
  (* | App(e1, e2) -> *)
          (* begin *)
              (* let arg = (eval6 e2 env) in *)
              (* match (eval6 e1 env) with *)
              (* | FunVal(x, body, env1) -> eval6 body (ext env1 x arg) *)
              (* | _ -> failwith "function value expected" *)
              (* (* match (eval6 e1 env) with *) *)
              (* (* | FunVal(x, body, env1) -> *) *)
                      (* (* let arg = (eval6 e2 env) *) *)
                      (* (* in eval6 body (ext env1 x arg) *) *)
              (* (* | _ -> failwith "function value expected" *) *)
          (* end *)
  | App(e1,e2) ->
      let funpart = (eval6 e1 env) in
      let arg = (eval6 e2 env) in
        begin
         match funpart with
         | FunVal(x,body,env1) ->
            let env2 = (ext env1 x arg) in
            eval6 body env2
         | RecFunVal(f,x,body,env1) ->
            let env2 = (ext (ext env1 x arg) f funpart) in
            eval6 body env2
         | _ -> failwith "wrong value in App"
        end
  | _ -> failwith "unknown expression"

