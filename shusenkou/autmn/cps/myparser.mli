type token =
  | VAR of (string)
  | INT of (int)
  | PLUS
  | MINUS
  | ASTERISK
  | SLASH
  | EQUAL
  | NEQUAL
  | LESS
  | GREATER
  | SEMICOL
  | COLCOL
  | LPAREN
  | RPAREN
  | LBRA
  | RBRA
  | ARROW
  | VBAR
  | TRUE
  | FALSE
  | FUN
  | LET
  | REC
  | IN
  | IF
  | THEN
  | ELSE
  | MATCH
  | TRY
  | WITH
  | HEAD
  | TAIL
  | CALCC
  | EOF

val parse :
  (Lexing.lexbuf  -> token) -> Lexing.lexbuf -> Mysyntax.exp
