open Mysyntax;;

let rec string_of_exp =
    let rec string_of_explist = function
        | []    -> ""
        | [e]   -> string_of_exp e
        | e::es -> (string_of_exp e) ^ ", " ^ (string_of_explist es)
    in
    let rec string_of_matchlist = function
        | []            -> ""
        | [(e1, e2)]    -> (string_of_exp e1) ^ " -> " ^ (string_of_exp e2)
        | (e1, e2)::ees -> (string_of_exp e1) ^ " -> " ^ (string_of_exp e2) ^ "; " ^ (string_of_matchlist ees)
    in
    function
    | Var(e)               ->  "Var(\"" ^ e ^ "\")"
    | IntLit(e)            ->  "IntLit(" ^ (string_of_int e) ^ ")"
    | BoolLit(e)           ->  "BoolLit(" ^ (string_of_bool e) ^ ")"
    | StringLit(e)         ->  "StringLit(\""^ e ^ "\")"
    | UnitLit              -> "UnitLit"
    | Semicol(e1, e2)      -> "Semicol(" ^ (string_of_explist [e1; e2]) ^ ")"
    | If(e1, e2, e3)       -> "If(" ^ (string_of_explist [e1; e2; e3]) ^ ")"
    | Let(e1, e2, e3)      -> "Let(" ^ e1 ^ (string_of_explist [e2; e3]) ^ ")"
    | Fun(e1, e2)          -> "Fun(\"" ^ e1 ^ "\", " ^ (string_of_exp e2) ^ ")"
    | App(e1, e2)          -> "App(" ^ (string_of_explist [e1; e2]) ^ ")"
    | Eq(e1, e2)           -> "Eq(" ^ (string_of_explist [e1; e2]) ^ ")"
    | Neq(e1, e2)          -> "Neq(" ^ (string_of_explist [e1; e2]) ^ ")"
    | Greater(e1, e2)      -> "Greater(" ^ (string_of_explist [e1; e2]) ^ ")"
    | Less(e1, e2)         -> "Less(" ^ (string_of_explist [e1; e2]) ^ ")"
    | Plus(e1, e2)         -> "Plus(" ^ (string_of_explist [e1; e2]) ^ ")"
    | Minus(e1, e2)        -> "Minus(" ^ (string_of_explist [e1; e2]) ^ ")"
    | Times(e1, e2)        -> "Times(" ^ (string_of_explist [e1; e2]) ^ ")"
    | Div(e1, e2)          -> "Div(" ^ (string_of_explist [e1; e2]) ^ ")"
    | Empty                -> "Empty"
    | Cons(e1, e2)         -> "Cons(" ^ (string_of_explist [e1; e2]) ^ ")"
    | Head(e)              ->  "Head(" ^ (string_of_exp e) ^ ")"
    | Tail(e)              ->  "Tail(" ^ (string_of_exp e) ^ ")"
    | Try(e1, e2)          -> "Try(" ^ (string_of_explist [e1; e2]) ^ ")"
    | LetRec(f, v, e1, e2) -> "LetRec(" ^ f ^ ", " ^ v ^ ", " ^ (string_of_explist [e1; e2]) ^ ")"
    | Match(e, ls)         -> "Match(" ^ (string_of_exp e) ^ ", [" ^ (string_of_matchlist ls) ^ "])"
    | Callcc e             -> "Callcc(" ^ (string_of_exp e) ^ ")"

let rec string_of_value =
    let rec string_of_listvalval = function
        | []    -> ""
        | [s]   -> string_of_value s
        | h::rs -> (string_of_value h) ^ "; " ^ string_of_listvalval rs
    in
    let rec string_of_env e =
        let rec string_of_envval = function
        | []         -> ""
        | [(s, v)]   -> s ^ ": " ^ (string_of_value v)
        | (s, v)::ls -> s ^ ": " ^ (string_of_value v) ^ "; " ^ (string_of_envval ls)
        in "[" ^ (string_of_envval e) ^ "]"
    in
    function
    | StdFn _                       -> "StdFn <fun>"
    | UnitVal                       -> "UnitVal"
    | IntVal i                      -> "IntVal(" ^ (string_of_int i) ^ ")"
    | BoolVal b                     -> "BoolVal(" ^ (string_of_bool b) ^ ")"
    | StringVal s                   -> "StringVal(\"" ^ s ^ "\")"
    | ErrorVal e                    -> "ErrorVal(\""  ^ e ^ "\")"
    | ListVal ls                    -> "ListVal([" ^ (string_of_listvalval ls) ^ "])"
    | FunVal(fname, arg, env)       -> "FunVal(\"" ^ fname ^ "\", " ^ (string_of_exp arg) ^ ", " ^ (string_of_env env) ^ ")"
    | RecFunVal(fname, arg, e, env) -> "RecFunVal(\"" ^ fname ^ "\", \"" ^ arg ^ "\", " ^ (string_of_exp e) ^ (string_of_env env) ^ ")"
    | ContVal v                     -> "ContVal(value -> value)"

let rec unparse =
    let braceor e = 
        match e with
        | Var v -> v
        | IntLit i -> string_of_int i
        | BoolLit b -> string_of_bool b
        | StringLit s -> "\"" ^ s ^ "\""
        | UnitLit -> "()"
        | Empty -> "[]"
        | _     -> "(" ^ (unparse e) ^ ")"
    in
    let rec list_of_cons c =
        match c with
        | Empty -> []
        | Cons (e1, Empty) -> [e1]
        | Cons (e1, e2) -> e1::(list_of_cons e2)
        | _ -> failwith  "list_of_cons error"
    in
    let unparse_list ls =
        let rec unparse_list =
            function
            | [] -> ""
            | e::[] -> unparse e
            | e1::ls' -> (unparse e1) ^ "; " ^ (unparse_list ls')
        in "[" ^ (unparse_list ls) ^ "]"
    in
    let rec unparse_match =
        function
        | [] -> ""
        | (m, f)::ls' -> " | " ^ (unparse m) ^ " -> " ^  (unparse f) ^ (unparse_match ls')
    in
    function
    | Var v -> v
    | IntLit i -> string_of_int i
    | BoolLit b -> string_of_bool b
    | StringLit s -> "\"" ^ s ^ "\""
    | UnitLit -> "()"
    | Empty -> "[]"
    | Cons (e1, e2) -> unparse_list (list_of_cons (Cons (e1, e2)))
    | Head ls -> "List.head " ^ (unparse_list (list_of_cons ls))
    | Tail ls -> "List.tail " ^ (unparse_list (list_of_cons ls))
    | Fun (v, body) -> "fun " ^ v ^ " -> " ^ (unparse body)
    | Semicol (e1, e2) -> (unparse e1) ^ "; " ^ (unparse e2)
    | Let (v, eb, exp') -> "let " ^ v ^ " = " ^ (unparse eb) ^ " in " ^ (unparse exp')
    | LetRec (f, v, eb, exp') -> "let rec " ^ f ^ " " ^ v ^ " = " ^ (unparse eb) ^ " in " ^ (unparse exp')
    | If (cond, body, elsebody) -> "if " ^ (unparse cond) ^ " then " ^ (unparse body) ^ " else " ^ (unparse elsebody)
    | App (f, a) -> (unparse f) ^ " " ^ (braceor a)
    | Plus (e1, e2) -> (unparse e1) ^ " + " ^ (unparse e2)
    | Minus (e1, e2) -> (unparse e1) ^ " - " ^ (unparse e2)
    | Times (e1, e2) -> (braceor e1) ^ " * " ^ (braceor e2)
    | Div(e1, e2) -> (braceor e1) ^ " / " ^ (braceor e2)
    | Eq (e1, e2) -> (braceor e1) ^ " = " ^ (braceor e2)
    | Neq (e1, e2) -> (braceor e1) ^ " <> " ^ (braceor e2)
    | Less (e1, e2) -> (braceor e1) ^ " < " ^ (braceor e2)
    | Greater (e1, e2) -> (braceor e1) ^ " > " ^ (braceor e2)
    | Try (e1, e2) -> "try " ^ (braceor e1) ^ " with " ^ (braceor e2)
    | Match (e1, ls) -> "match " ^ (unparse e1) ^ " with " ^ (unparse_match ls)
    | Callcc e -> "call/cc " ^ (unparse e)

