let rec tinf1 te tv =
    let (tenv, e)::te' = te in
    match e with
    | IntLit _ -> TInt
    | BoolLit _ -> TBool
    | Semicol(_, e1) -> tinf (tenv, e1)::te' tw
    (* | Eq (e1, e2) -> *)
        (* let  *)

