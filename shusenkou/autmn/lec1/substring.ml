let substring s1 s2 =
    let explode s =
        let rec exp i l =
            if i < 0 then l
            else exp (i - 1) (s.[i]::l) in
        exp (String.length s - 1) [] in
    let rec substring' s1 s2 acc =
        if s2 = [] then -1
        else if s1 = [] then -1
        else
            let x::xs = s2 in
            let y::ys = s1 in
            if x = y then
                if xs = [] then acc
                else substring' ys xs acc
            else substring' ys s2 (acc + 1) in
    substring' (explode s1) (explode s2) 0;;

