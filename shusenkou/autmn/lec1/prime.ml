let prime n =
    let is_prime n =
        let n' = abs n in
        let rec is_not_divisor d =
            d * d > n' || (n' mod d <> 0 && is_not_divisor (d + 1)) in
        n' <> 1 && is_not_divisor 2 in
    let rec prime' n prim acc =
        if n = acc then prim - 1
        else
            let acc' = if is_prime prim then acc + 1 else acc in
            prime' n (prim + 1) acc' in
    prime' n 2 0;;

