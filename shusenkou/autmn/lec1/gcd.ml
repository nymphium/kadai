(* let rec gcd x y = *)
    (* if x < 0 then failwith (Printf.sprintf "Accept only positive numer (x: %d)" x) *)
    (* else if y < 0 then failwith (Printf.sprintf "Accept only positive numer (y: %d)" y) *)
    (* else *)
        (* if x = y then x *)
            (* else if x > y then gcd (x - y) y *)
            (* else gcd x (y - x);; *)
let rec gcd =
    function
        | x when x < 0 ->  failwith (Printf.sprintf "Accept only positive numer (x: %d)" x)
        | x -> function
            | y when y < 0 ->  failwith (Printf.sprintf "Accept only positive numer (y: %d)" y)
            | y when x = y -> x
            | y when x > y -> gcd (x - y) y
            | y -> gcd x (y - x)

