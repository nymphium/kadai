(* let rec fib n = *)
    (* match n with *)
    (* | 1 -> 1 *)
    (* | 2 -> 1 *)
    (* | _ -> fib (n - 1) + fib (n - 2);; *)

let rec fib = function
    | 1 -> 1
    | 2 -> 2
    | n -> fib(n - 1) + fib(n - 2);;

let fib' n =
    (* let rec fib'' n a b = *)
        (* if n = 0 then a *)
        (* else *)
            (* fib'' (n - 1) b (a + b) in *)
    let rec fib'' a b = function
        | 0 -> a
        | n -> fib'' b (a + b) (n - 1) in
    fib'' 0 1 n



