let rec qsort l = 
    match l with
    | [] -> []
    | pivot::xs ->
        let div_piv x (ls, bs) = if x < pivot then (x::ls, bs) else (ls, x::bs) in 
        let littles, bigs = List.fold_right div_piv xs ([], []) in
        qsort littles @ pivot::(qsort bigs);;

