package com.example.nymphium.scanit;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.customtabs.CustomTabsIntent;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;
import android.widget.Toast;

import com.googlecode.tesseract.android.TessBaseAPI;
import com.isseiaoki.simplecropview.CropImageView;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

public final class MainActivity extends Activity {
    private static final String DEFAULT_LANGUAGE = "jpn";
    private static final String trainDataFile = DEFAULT_LANGUAGE + ".traineddata";
    private static final String dataDir = "tessdata/";
    private static final String wikiUrl = "http://yugioh-wiki.net/index.php?";
    private static final String wikiCardStartBrace = "%A1%D4";
    private static final String wikiCardEndBrace = "%A1%D5";
    private static final int RESULT_CAMERA = 1001;
    private static String defaultResultText;
    private TextView resultTextView;
    private static String resultText = null;

    private static String filepath;
    private static CropImageView mCropView;
    private static Bitmap bitmap = null;
    private static Uri imageUri;
    private static TessBaseAPI tessBaseAPI;

    private static final int BOUND_RATIO = 500;
    private static final double MIN_BOUND = 0.6;
    private double bound = MIN_BOUND;

    private TextView boundTextView;

    @SuppressLint("StaticFieldLeak")
    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // permission check {{{
        int permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA);
        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            int REQUEST_CODE = 1;
            ActivityCompat.requestPermissions(this, new String[]{
                    Manifest.permission.CAMERA,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.READ_EXTERNAL_STORAGE,
            }, REQUEST_CODE);
        }
        // }}}

        String dataDirPre = "/tesseract/";
        filepath = getFilesDir() + dataDirPre;

        checkFile(new File(filepath + dataDir));
        tessBaseAPI = new TessBaseAPI();
        tessBaseAPI.init(filepath, DEFAULT_LANGUAGE);
        defaultResultText = getString(R.string.defaultText);
        mCropView = findViewById(R.id.cropImageView);
        mCropView.setCropMode(CropImageView.CropMode.FREE);
        mCropView.setGuideShowMode(CropImageView.ShowMode.NOT_SHOW);

        resultTextView = findViewById(R.id.resultText);
        SeekBar seeker = findViewById(R.id.seekBar);
        boundTextView = findViewById(R.id.boundTextView);
        boundTextView.setText(String.format("bound value: %.3f", bound));

        findViewById(R.id.cameraButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String filename = System.currentTimeMillis() + ".png";
                ContentValues values = new ContentValues();
                values.put(MediaStore.Images.Media.TITLE, filename);
                values.put(MediaStore.Images.Media.MIME_TYPE, "image/png");
                imageUri = getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);

                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                intent.setAction(MediaStore.ACTION_IMAGE_CAPTURE);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
                startActivityForResult(intent, RESULT_CAMERA);
            }
        });

        findViewById(R.id.cropButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bitmap = mCropView.getCroppedBitmap();
                mCropView.setImageBitmap(bitmap);
            }
        });

        findViewById(R.id.browseButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (resultText != null && !resultText.isEmpty()) {
                    String url = buildYugiohUrl(resultText);

                    if (url != null && !url.isEmpty()) {
                        CustomTabsIntent.Builder builder = new CustomTabsIntent.Builder();
                        CustomTabsIntent customTabsIntent = builder.build();
                        customTabsIntent.launchUrl(MainActivity.this, Uri.parse(url));
                    }
                }
            }
        });

        findViewById(R.id.button).setOnClickListener(new View.OnClickListener() {
            @SuppressLint("StaticFieldLeak")
            @Override
            public void onClick(View view) {
                if (bitmap == null) return;

                Log.d("info", "button clicked");
                final ProgressDialog dialog = new ProgressDialog(MainActivity.this);
                dialog.setMessage("解析中...");
                dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                dialog.setCancelable(true);
                dialog.show();

                new AsyncTask<Void, Void, String>() {
                    @Override
                    protected String doInBackground(Void... p) {
                        if (bitmap == null) {
                            return null;
                        } else {
                            bitmap = BitmapUtils.binarize(bitmap, bound);
                            tessBaseAPI.setImage(bitmap);
                            return tessBaseAPI.getUTF8Text();
                        }
                    }

                    @SuppressLint("ShowToast")
                    @Override
                    protected void onPostExecute(String ok) {
                        if (ok != null && !ok.isEmpty()) {
                            resultText = ok.replaceAll("\\s", "");
                            resultTextView.setText(resultText);
                            Toast.makeText(MainActivity.this, "解析完了", Toast.LENGTH_LONG);
                        } else {
                            Toast.makeText(MainActivity.this, "わからん…", Toast.LENGTH_LONG);
                        }

                        mCropView.setImageBitmap(bitmap);
                        dialog.hide();
                    }
                }.execute();
            }
        });

        seeker.setProgress(seeker.getMax() / 2);
        seeker.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekbar, int progress, boolean fromUser) {
                bound = MIN_BOUND + (double)(progress - 50) / BOUND_RATIO;
                boundTextView.setText(String.format("bound value: %.3f", bound));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekbar) {}

            @Override
            public void onStopTrackingTouch(SeekBar seekbar) { }
        });
    }

    @SuppressLint("StaticFieldLeak")
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == RESULT_CAMERA && imageUri != null) {
            resultText = defaultResultText;
            resultTextView.setText(resultText);

            try {
                bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), imageUri);
                mCropView.setImageBitmap(bitmap);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    // file utils {{{
    private void checkFile(File file) {
        if (!file.exists() && file.mkdirs()) {
            copyFiles();
        }
        if (file.exists()) {
            String datapath = String.format("%s/%s%s", filepath, dataDir, trainDataFile);
            File datafile = new File(datapath);

            if (!datafile.exists()) {
                copyFiles();
            }
        }
    }

    private void copyFiles() {
        try {
            String datapath = String.format("%s/%s%s", filepath, dataDir, trainDataFile);
            InputStream instream = getAssets().open(dataDir + trainDataFile);
            OutputStream outstream = new FileOutputStream(datapath);

            byte[] buffer = new byte[1024];
            int read;
            while ((read = instream.read(buffer)) != -1) {
                outstream.write(buffer, 0, read);
            }

            outstream.flush();
            outstream.close();
            instream.close();

            File file = new File(datapath);
            if (!file.exists()) {
                throw new FileNotFoundException();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    // }}}

    private static String buildYugiohUrl(String cardName) {
        try {
            String eucEncoded = URLEncoder.encode(cardName, "EUC-JP");
            return String.format("%s%s%s%s", wikiUrl, wikiCardStartBrace, eucEncoded, wikiCardEndBrace);
        } catch (UnsupportedEncodingException e) {
            Log.e("error", e.toString());
            return null;
        }
    }
}
