スマホをdistccサーバとして分散コンパイルに参加することを考える｡
現在多くの人がスマホを所持しており､その多くがいつでもインターネットに接続できる状況となっている｡
一方でスマホ利用者の多くはスマホ上で高負荷の計算をおこなう場合は少なく､計算資源を余らせている｡
distccという､コンピュータネットワーク上で分散コンパイルをおこなうことができるソフトウェアがある｡
Linuxカーネルなどの大きなプログラムをコンパイルしたいdistccクライアントが､分散コンパイルをおこなうdistccサーバにそれぞれプログラム片を渡し結果をクライアントに渡してマージするという動作をする｡
このdistccサーバとしてスマホを使いたい｡
distccサーバとして使うことを許可したスマホをVPNなどのネットワークに参加させる｡
distccクライアントはなにかをコンパイルするときに､ネットワーク上で近いdistccサーバをいくつか選び､コンパイルに参加させる｡
これにより､スマホの計算資源を無駄にすることなく､またdistccクライアントは高速にプログラムのコンパイルをおこなうことができる｡
distccクライアントは､通信量に応じてdistccサーバに報酬を支払うシステムを作ることで､サービスとして稼働させていくことができる｡
