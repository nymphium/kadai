WikipediaでWGコーパスを評価した
newswireよりもWikipediaのほうが難しいNERのドメインであることがわかった｡
ウィキピジャの自動アノテーションはWGと強く一致し､学習データとして使うとneswireモデルよりも7.7%効率が上昇した｡

# What this paper is about

## Introduction and Objective

This training data, including English newswire from the MUC-6, MUC-7, and CONLL03 competitive evaluation tasks, and the BBN Pronoun Coreference and Entity Type Corpus, is critical to the success of these approaches.
Recently, Wikipedias markup has been exploited to automatically derive NE annotated text for training statistical models.
We present the first evaluation of Wikipedia-trained models on Wikipedia: the C&C NER tagger trained on automatically annotated Wikipedia text extracted by Nothman et al.; and traditional newswire NER corpora.

# What you can learn
## Results
## Discussion and Conclusions
We annotated a corpus of Wikipedia articles with gold-standard NE tags.
However, we found that all four models performed significantly worse on the WG corpus than they did on news text, suggesting that Wikipedia as a textual domain is more difficult for NER.
We found that WP2 agreed with our final WG corpus to a high degree, demonstrating that Wikipedia is a viable source of automatically annotated NE annotated data, reducing our dependence on expensive manual annotation for training NER systems.

