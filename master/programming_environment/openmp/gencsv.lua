local N = 500

local f0 = io.open("size.h", "w")
assert(f0:write(([[#define N %d]]):format(N)))
assert(f0:close())

local function gen_mat()
	local ret = {}

	for i = 1, N do
		ret[i] = {}

		for j = 1, N do
			ret[i][j] = math.floor(math.random() * 100)
		end
	end

	return ret
end

local function to_s(t)
	local tt = {}

	for i = 1, #t do
		tt[i] = {}

		for j = 1, #(t[i]) do
			tt[i][j] = tostring(t[i][j])
		end

		tt[i] = table.concat(tt[i], ",")
	end

	return table.concat(tt, "\n")
end

math.randomseed(100)
local mat1 = gen_mat()
local f1 = io.open("mat1.csv", "w")
assert(f1:write(to_s(mat1)))
assert(f1:close())

math.randomseed(200)
local mat2 = gen_mat()
local f2 = io.open("mat2.csv", "w")
assert(f2:write(to_s(mat2)))
assert(f2:close())

