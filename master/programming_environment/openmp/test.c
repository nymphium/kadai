#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>

#include "size.h"

#define LINE 2048

#define REPEAT 5000

int A[N][N];
int B[N][N];
int C[N][N];

void get_from_csv(char *file, int sort) {
	FILE *fp;

	char lines[LINE] = {'\0'};
	char *tp;

	fp = fopen(file, "r");

	int l = 0;
	while (fgets(lines, LINE, fp) != NULL) {
		l++;

		tp = strtok(lines, ",");

		int i = 0;
		do {
			switch(sort) {
				case 0: 
					A[l][i++] = atoi(tp);
					break;
				case 1:
					B[l][i++] = atoi(tp);
					break;
			}

			tp = strtok(NULL,",");
		} while (tp != NULL);
	}

	fclose(fp);
}

volatile void calc(void) {
#ifdef OMP
	printf("omp with %d threads\n", THREAD);
#else
	puts("default");
#endif

	for (int c = 0; c < REPEAT; c++) {
#ifdef OMP
#pragma omp parallel for num_threads(THREAD)
		for(int i = 0; i < N; ++i) {
#pragma omp parallel for num_threads(THREAD)
			for(int j = 0; j < N; ++j) {
				C[i][j] = A[i][j] * B[i][j];
			}
		}

#else
		for(int i = 0; i < N; i++) {
			for(int j = 0; j < N; j++) {
				C[i][j] = A[i][j] + B[i][j];
			}
		}
#endif
	}
}


int main(void) {
	struct timespec start, end;

	get_from_csv("mat1.csv", 0);
	get_from_csv("mat2.csv", 1);

	clock_gettime(CLOCK_REALTIME, &start);
	calc();
	clock_gettime(CLOCK_REALTIME, &end);

    printf("elapsed time = ");
    if (end.tv_nsec < start.tv_nsec) {
      printf("%5ld.%09ld", end.tv_sec - start.tv_sec - 1,
             end.tv_nsec + (long int)1.0e+9 - start.tv_nsec);
    } else {
      printf("%5ld.%09ld", end.tv_sec - start.tv_sec,
             end.tv_nsec - start.tv_nsec);
    }
    printf("(sec)\n");

	return 0;
}
