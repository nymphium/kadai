let rec map f l =
    match l with
    | [] -> []
    | x::rest -> f x::(map f rest);;

