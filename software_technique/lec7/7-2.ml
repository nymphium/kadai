exception Zero;;


let preprod l =
    let rec preprod l prod =
        match l with
        | [] -> 0
        | x::[] -> if x = 0 then raise(Zero) else prod * x
        | head::rest -> if head = 0 then raise(Zero) else preprod rest head * prod
    in preprod l 1;;


let prod l =
    try preprod l with
    | Zero -> 0
    | _ -> preprod l;;

