let rec filter pred l =
    match l with
    | [] -> []
    | head::tail ->
        if pred head then
            head::(filter pred tail)
        else
            filter pred tail;;


let rec map f l =
    match l with
    | [] -> []
    | head::tail -> (f head)::(map f tail);;


let rec exist x l =
    match l with
    | [] -> false
    | head::tail ->
        if head = x then
            true
        else
            (exist x tail);;


let inter l1 l2 =
    filter (fun x -> exist x l1) l2;;


let pair v l =
    map (fun x -> (v, x)) l;;


let rec prod l1 l2 =
    match l1 with
    | [] -> []
    | head::tail -> (pair head l2)@(prod tail l2);;

