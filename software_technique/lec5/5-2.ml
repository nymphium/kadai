let rec qsort_func func l =
    match l with
    | [] -> []
    | h::t ->
        let rec split l =
            match l with
            | [] -> ([], [])
            | h_::t_ -> let (l1, l2) = split t_ in
        if (func h_ h) then (h_::l1, l2) else (l1, h_::l2) in
    let (l1, l2) = split t
    in qsort_func func l1@(h::(qsort_func func l2));;

