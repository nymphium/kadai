type 'a tree =
    | Lf
    | Br of 'a * 'a tree * 'a tree;;


let rec add x t =
    match t with
    | Lf -> Br(x, Lf, Lf)
    | Br(y, lt, rt) ->
            if y > x then
                Br(y, add x lt, rt)
            else
                Br(y, lt, add x rt);;


let rec min_elt t =
    match t with
    | Br(x, Lf, _) -> x
    | Br(_, lt, _) -> min_elt lt;;


let rec max_elt t =
    match t with
    | Br(_, _, Lf) -> t
    | Br(_, _, rt) -> max_elt rt


let left t = match t with Br(_, lt, _) -> lt;;


let node_val t = match t with Br(n, _, _) -> n;;


let rec delete x t =
    match t with
    | Br(y, lt, rt) ->
            if y > x then Br(y, (delete x lt), rt)
            else if y < x then Br(y, lt, (delete x rt))
            else if lt <> Lf then
                Br((node_val maxt) , (left (max_elt lt)), rt)
            else Lf ;;

