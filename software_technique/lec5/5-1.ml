let rec summation f x =
    match x with
    | 0 -> f(0)
    | n -> f(x) + summation f (n - 1);;


let rec summation2 g (m, n) =
    (summation (fun x -> g(m, x)) n) + (summation (fun y -> g(y, n)) m);;
