let rec num_of(l, x)=
    match l with
    (* | h::t when h = x -> 1 + num_of(t, x) *)
    | h::t -> if h = x then 1 + num_of(t, x) else num_of(t, x)
    (* | _::t -> num_of(t, x) *)
    | [] -> 0;;



let num_of_rec(l, x) =
    let rec sub_f(ls, a) =
        match ls with
        | h::t when h = x -> sub_f(t, a + 1)
        | _::t -> sub_f(t, a)
        | [] -> a
        in sub_f(l, 0);;

print_int (num_of_rec([1; 2; 2; 3], 2));;
