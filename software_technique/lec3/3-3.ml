let split_list l =
    let rec split_list(l, d1, d2) =
        match l with
        | [] -> d1, d2
        | t::h -> if List.length h mod 2 <> 0 then split_list(h, t::d1, d2) else split_list(h, d1, t::d2)
    in split_list(l, [], []);;


let rec merge(l1, l2) =
    match (l1, l2) with
    | ([], []) -> []
    | (l, []) -> l
    | ([], l) -> l
    | (t1::h1, t2::h2) -> if t1 > t2 then t2::merge(l1, h2) else t1::merge(h1, l2);;


let rec merge_sort l =
    match l with
    | [] -> []
    | h::[] -> h::[]
    | e -> let l1, l2 = split_list l in
        merge((merge_sort l1), (merge_sort l2));;


let l = [-3; 3; 0; 12; 9; -1];;

List.iter (fun i -> Printf.printf("%d ") i) (merge_sort l);;

