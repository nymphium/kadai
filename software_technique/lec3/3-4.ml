let fib n =
    let rec fib (i, a1, a2) =
        if i = 0 then a1 else fib(i - 1, a1 + a2, a1)
    in fib(n, 1, 0);;


