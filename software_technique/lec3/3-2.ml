let sum_pair l =
    let rec sum_pair(l, pos, neg) =
        match l with
        | [] -> pos, neg
        | t::h -> if t >= 0 then sum_pair(h, pos + 1, neg) else sum_pair(h, pos, neg + 1)
    in sum_pair(l, 0, 0);;

(* List.iter (fun i -> Printf.printf "%d, %d" i) (sum_pair [-1; 2; 3; 0; -4]);; *)
let a, b = sum_pair [-1; 2; 3; 0; -4];;

Printf.printf("%d %d") a b;;
