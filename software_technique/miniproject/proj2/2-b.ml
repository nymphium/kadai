let rec mul_mono_poly l1 l2 =
    match l2 with
    | head::rest -> if rest <> [] then (mul_mono l1 head)::(mul_mono_poly l1 rest)
        else [mul_mono l1 head]
    | _ -> [(0, [0;])];;

