let rec mul_poly l1 l2 =
    match l1 with
    | h::r -> let l1' = mul_mono_poly h l2 in add_poly l1' (mul_poly r l2)
    | _     -> []

