type exp =
    | Int of int
    | Var of string
    | Add of exp * exp
    | Mul of exp * exp;;


let rec expise c nl el =
    match nl, el with
    | h1::r1, h2::r2 -> if h1 = c then (h2 + 1)::r2 else h2::expise c r1 r2
    | _, _ -> el;;


let rec makevar nl =
    match nl with
    | [] -> [0]
    | h::r -> if r <> [] then 0::makevar r else [0];;


let rec exp2poly exp xs =
    let var = makevar xs in
    let cale2p e = exp2poly e xs in
    let calexp c = expise c xs var in
    match exp with
    | Int n -> [(n, var)]
    | Var s -> [(1, calexp s)]
    | Add (e1, e2) -> add_poly (cale2p e1) (cale2p e2)
    | Mul (e1, e2) -> mul_poly (cale2p e1) (cale2p e2);;

