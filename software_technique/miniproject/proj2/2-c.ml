let rec mono_le m1 m2 =
    match (m1, m2) with
    | ([], []) -> true
    | (n1::m1', n2::m2') -> n1 < n2 || (n1 = n2 && mono_le m1' m2')
    | _                    -> false;;


let rec add_poly l1 l2 =
    match l1, l2 with
    | _, []          -> l1
    | [], _          -> l2
    | h::r, h'::r' ->
            match h, h' with
            | (c1, m1), (c2, m2) when m1 = m2  -> (c1 + c2, m1)::(add_poly r r') 
            | (_, m1), (_, m2)                    -> if mono_le m1 m2 then h::(add_poly r l2)
                else h'::(add_poly l1 r')

