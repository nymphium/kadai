let mul_mono l1 l2 =
    let rec sync l1' l2' =
        match l1', l2' with
        | [], _ -> []
        | _, [] -> []
        | h1::r1, h2::r2 -> (h1 + h2)::(sync r1 r2)
    in match l1, l2 with
    | (ln1, l1'), (ln2, l2') -> (ln1 * ln2, sync l1' l2');;

