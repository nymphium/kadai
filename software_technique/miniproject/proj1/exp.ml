type exp =
    | Var of string
    | Int of int
    | Add of exp * exp
    | Mul of exp * exp;;


let rec deriv exp key =
    let calder e = (fun k -> deriv e k) key
    in match exp with
    | Var s          -> if s = key then Int 1 else Int 0
    | Int _          -> Int 0
    | Add (e1, e2) -> Add ((calder e1), (calder e2))
    | Mul (e1, e2) -> Add (Mul (e1, (calder e1)), Mul (e2, (calder e2)));;


let rec mul x y =
    let detect i =
        match i with
        | Add (e1, e2) -> add e1 e2
        | Mul (e1, e2) -> mul e1 e2
        | _              -> i
    in
    let x = detect x in
    let y = detect y in
    match x, y with
    | Int n1, Int n2 -> Int (n1 * n2)
    | Int n, _         -> if n = 0 then x else if n = 1 then y else Mul (x, y)
    | _, Int n         -> if n = 0 then y else if n = 1 then x else Mul (x, y)
    | _, _             -> Mul (x, y)
and add x y =
    let detect i =
        match i with
        | Add (e1, e2) -> add e1 e2
        | Mul (e1, e2) -> mul e1 e2
        | _              -> i
    in
    let x = detect x in
    let y = detect y in
    match x, y with
    | Int n1, Int n2 -> Int (n1 + n2)
    | Int n, _         -> if n = 0 then y else Add (x, y)
    | _, Int n         -> if n = 0 then x else Add (x, y)
    | _, _             -> Add (x, y);;


(* 1 - (b) *)
let rec deriv' exp key =
    let calder' e = (fun k -> deriv' e k) key
    in match exp with
    | Var s          -> if s = key then Int 1 else Int 0
    | Int _          -> Int 0
    | Add (e1, e2) -> add (calder' e1) (calder' e2)
    | Mul (e1, e2) -> add (mul e1 (calder' e1)) (mul e2 (calder' e2));;

