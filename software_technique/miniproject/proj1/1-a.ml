type exp =
    | Var of string
    | Int of int
    | Add of exp * exp
    | Mul of exp * exp;;


let rec deriv exp key =
    let calder e = (fun k -> deriv e k) key in
    match exp with
    | Var s          -> if s = key then Int 1 else Int 0
    | Int _          -> Int 0
    | Add (e1, e2) -> Add ((calder e1), (calder e2))
    | Mul (e1, e2) -> Add (Mul (e1, (calder e1)), Mul (e2, (calder e2)));;

