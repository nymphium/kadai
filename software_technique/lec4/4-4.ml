type 'a ftree = FBr of 'a * 'a ftree list;;


let max(a, b) =
    if a > b then a else b;;


let rec fdepth t =
    match t with
    | FBr(_, ts) -> 1 + fdepth_ts(ts)

and fdepth_ts ts =
    match ts with
    | [] -> 0
    | h::tl -> max (fdepth(h), fdepth_ts(tl));;


let f1 = FBr(1, [FBr(2, []); FBr(3, [FBr(4, [])])]);;

