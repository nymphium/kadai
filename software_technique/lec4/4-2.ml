type 'a bintree =
    | Lf
    | Br of 'a * 'a bintree * 'a bintree;; 


let rec depth t =
    match t with
    | Lf -> 0
    | Br(_, l, r) -> 1 + depth (max l r)
and max a b =
    if (depth a) > (depth b) then a else b;;

(* depth Br(Lf, Lf) *)
(* - : int = 1 *)


let rec comptree(n, x) =
    match n with
    | 0 -> Lf
    | m -> Br(x, comptree(m - 1, x), comptree(m - 1, x));;
(* comptree(X, 0) *)
(* - : X-type bintree = Lf *)


let bintree1 = Br(1, Br(1, Lf, Lf), Br(1, Br(1, Lf, Lf), Lf));;
