type 'a tree =
    | Lf
    | Br of 'a * 'a tree * 'a tree;;


let inorder t =
    let rec inorder (t, l) =
    match t with
    | Lf -> l
    | Br(x, lt, rt) -> inorder(lt, l)@[x]@inorder(rt, l)
    in inorder(t, []);;


let postorder t =
    let rec postorder(t, l) =
        match t with
        | Lf -> l
        | Br(x, lt, rt) -> postorder(lt, l)@postorder(rt, l)@[x]
        in postorder(t, []);;

let bintree1 = Br(1, Br(3, Lf, Lf), Br(6, Br(9, Lf, Lf), Lf));;

