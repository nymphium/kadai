type figure =
    | Circle of float 
    | Rectangle of (float * float)
    | Square of float;;


let area fig =
    match fig with
    | (Circle x) -> x *. x *. 4.0 *. atan 1.0
    | (Rectangle (x, y)) -> x *. y
    | (Square x) -> x *. x;;

