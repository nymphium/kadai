type primop =
    | PLUSop
    | MULop

type exp = 
    | INTexp of int
    | FLOATexp of float
    | VARexp of string
    | LETexp of string * exp * exp
    | PRIMexp of primop * exp * exp


let rec exp2string exp =
    match exp with
    | INTexp n -> string_of_int n
    | FLOATexp f -> string_of_float f
    | VARexp s -> s
    | LETexp(s, e1, e2) -> "let " ^ s ^ "=" ^ (exp2string e1) ^ " in " ^ (exp2string e2)
    | PRIMexp(p, e1, e2) -> let op =
            match p with
            | PLUSop -> "+"
            | MULop -> "*"
            in "(" ^ (exp2string e1) ^ op ^ (exp2string e2) ^ ")";;


(* type value = *)
    (* | INTval of int *)
    (* | FLOATval of float;; *)


(* let rec eval env exp = *)
    (* match exp with *)
    (* | INTexp n -> INTval n *)
    (* | FLOATexp f -> FLOATval f *)
    (* | PRIMexp(p, e1, e2) -> let x1 = *)
        (* match e1 with *)
        (* | INTexp n -> n *)
        (* | FLOATexp f -> f *)
        (* in let x2 = match e2 with *)
        (* | INTexp n -> n *)
        (* | FLOATexp f -> f *)
        (* in match p with *)
        (* | PLUSop -> FLOATval (x1 +. x2) *)
        (* | MULop -> FLOATval (x1 *. x2) *)
    (* | LETexp(s, e1, e2) -> let x =  *)
        (* match e1 with *)
        (* | INTexp n -> n *)
        (* | FLOATexp f -> f *)
        (* in match e2 with *)
            (* |  *)


