type primop =
    | PLUSop
    | MINUSop;;

type exp =
    | INTexp of int
    | FNexp of string * exp
    | PRIMexp of primop * exp * exp
    | IFZEROexp of exp * exp * exp
    | APPexp of exp * exp;;

type value =
    | INTval of int
    | CLOSUREval of string * exp * (string * value) list;;


let rec eval l exp =
    
