open List


let rec drop (l, i) =
    match l with
    | [] -> []
    | _::_ -> if i <= 0 then l else drop (tl l, i - 1);;


let forprint l =
    let () = iter (fun i -> Printf.printf "%d\n" i) l in

    print_endline "";;


let l = [1; 2; 3; 5; 8];;

forprint l;;

let ll = (drop (l, 2));;

forprint ll;;

let ll = (drop (ll, 2));;

forprint ll;;
