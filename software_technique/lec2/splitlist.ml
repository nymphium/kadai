open List


let rec split_list l =
    match l with
    | [] -> ([], [])
    | x::rest ->
            let (pos, neg) = split_list rest in
            if x >= 0 then (x::pos, neg) else (pos, x::neg);;


let l1 = [1; -2; 3; -4];;

let l = (split_list l1);;

let p = (fun (x, _) -> x) l;;

let n = (fun (_, x) -> x) l;;


iter (fun i -> Printf.printf "%d, " i) p;;

    print_endline "";

iter (fun i -> Printf.printf "%d, " i) n;;
