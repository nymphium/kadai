let rec sum_list l =
	match l with
	| [] -> 0
	| h::res -> (sum_list (List.tl l)) + h;;


let ave_list l =
	(sum_list l) / (List.length l);;

let l = [1; 2; 3];;

print_int (sum_list l);;

print_endline "";;

print_int (ave_list l);;
