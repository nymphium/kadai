open List


let flatten l =
	let ret = [] in

	let rec flatten_ l =
		match l with
	| [] -> []
	| t::res -> let ret = ret@t in
	flatten_ (res) in

	ret;;


let l = (flatten [[1; 2]; [3]]);;
