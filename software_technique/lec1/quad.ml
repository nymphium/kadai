let namRoots a b c =
    let eq = b * b - 4 * a * c in

    if eq > 0 then 2 else if eq = 0 then 1 else 0;;

print_int (namRoots 1 2 1);;

print_endline "";;

print_int (namRoots 1 5 3);;

print_endline "";;

print_int (namRoots 1 1 1);;
