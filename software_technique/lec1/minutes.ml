let minute_to_time m =
    let date = m / 24 / 60 in

    let hour = m / 60 mod 24 in

    let minute = m mod 60 in

    date, hour, minute;;


let time_to_minute (d, h, m) =
    (d * 24 + h) * 60 + m;;


let date_sum ((d1, h1, m1), (d2, h2, m2)) =
    minute_to_time (time_to_minute (d1, h1, m1) + time_to_minute (d2, h2, m2));;


let print_trituple (a, b, c) =
    let tuple_to_str = Printf.sprintf("%d %d %d") a b c in

    print_endline tuple_to_str;;


print_trituple (minute_to_time 8000);;

print_int (time_to_minute (5, 13, 20));;

print_endline "";;

print_trituple (date_sum ((5, 13, 20), (5,13,20)));;
