// ファイルState.java
public interface State {
    public char processChar(FilterIterator f, char ch);
}
