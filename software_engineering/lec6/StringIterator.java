import java.util.Iterator;

public class StringIterator implements Iterator<Character> {
	private String orig;
	private int pos = 0;
	public StringIterator(String s) {
		this.orig = s;
	}

	public boolean hasNext() {
		return pos < orig.length();
	}

	public Character next() {
		return orig.charAt(pos++);
	}
}

