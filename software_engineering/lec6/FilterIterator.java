// ファイルFilterIterator.java
import java.util.Iterator;

// public class FilterIterator implements Iterator<Character> {
	// private Iterator<Character> original;
	// public FilterIterator(Iterator<Character> original) {
		// this.original = original;
	// }
	// public boolean hasNext() {
		// return original.hasNext();
	// }
	// final int AFTER_SPACE = 1, OTHER = 2;
	// private int state = AFTER_SPACE;
	// public void setState(int newState) {
		// state = newState;
	// }
	// public Character next() {
		// char ch = original.next();
		// switch (state) {
			// case AFTER_SPACE:
				// if (! Character.isWhitespace(ch)) {
					// setState(OTHER);
				// }
				// return ch;
			// default:
				// if (Character.isWhitespace(ch)) {
					// setState(AFTER_SPACE);
					// return ch;
				// }
				// return '.';
		// }
	// }

	// public void remove() {original.remove();}
// }

public class FilterIterator implements Iterator<Character> {
	private Iterator<Character> original;
	private State state = AfterSpace.getInstance();
	private FilterIterator filter;

	public FilterIterator(Iterator<Character> original) {
		this.original = original;
		this.filter = this;
	}

	public boolean hasNext() {
		return this.original.hasNext();
	}

	public void setState(State newState) {
		this.state = newState;
	}

	public Character next() {
		return this.state.processChar(this, this.original.next());
	}
}

