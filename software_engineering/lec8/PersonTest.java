import junit.framework.TestCase;

public class PersonTest extends TestCase {
	private String name = "Bob";
	private String company = "workcenter";
	private String officeAC = "0x";
	private String officeNum = "ffff";
	private Person person;
	
	public void setUp(){
		person = new Person(name, company, officeAC, officeNum);
	}

	public void testGetOfficeTelephoneNumber() {
		assertEquals(person.getOfficeTelephoneNumber(), "0x-ffff");
	}
}
