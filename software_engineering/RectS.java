public class RectS implements Shape {
	protected double SIZE[] = {5.0, 3.0};
	protected double XRANGE[] = {0.0, 7,0};
	protected double YRANGE[] = {0.0, 6.0};
	protected double XSTEP = 0.25;
	protected double YSTEP = 0.5;
	protected double REF[] = {2.0, 2.0};

	public boolean inside(double x, double y) {
		return REF[0] <= x && x <= (REF[0] + SIZE[0]) &&
			REF[1] <= y && y <= (REF[1] + SIZE[1]);
	}

	public XYRange getRange() {
		return new XYRange(XRANGE[0], XRANGE[1], XSTEP, YRANGE[0], YRANGE[1], YSTEP);
	}
}
