
// ファイルCommand.java
public interface Command {
    public abstract void run(Message msg);
}