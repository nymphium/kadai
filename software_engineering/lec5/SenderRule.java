public class SenderRule extends Rule {
	private String sender;
	public SenderRule(Command command, String sender) {
		super(command);
		this.sender = sender;
	}

	protected boolean check(Message msg) {
		return msg.getSender().equals(this.sender);
	}
 }
