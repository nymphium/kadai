
// ファイルMain.java

public class Main {
	public static void main(String[] args) {
		Rule rule;
		/* ここでルールを組み立てる． */

		rule = new SenderRule(new SaveCommand(), "alice");
		rule.setNext(new SenderRule(new DiscardCommand(), "bob")).setNext(new AnyRule(new PrintCommand()));

		Message[] msgs = {  // Messageの配列
			new Message("alice", "me", "Hello, this is Alice."),
			new Message("bob", "me", "Hello from Bob."),
			new Message("charlie", "me", "Hi, it's Charlie.")
		};

		for (Message m : msgs) {
			rule.handle(m);
		}
	}
}
