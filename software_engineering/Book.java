class Book {
	String author;
	String title;
	int pages;

	Book(String bookAuthor, String bookTitle) {
		author = bookAuthor;
		title = bookTitle;
	}

	void setPages(int n) {
		this.pages = n;
	}

	String getAuthor() {
		return this.author;
	}

	String getTitle() {
		return this.title;
	}

	int getPages() {
		return this.pages;
	}

	void printAuthor() {
		System.out.println(this.getAuthor());
	}

	void printTitle() {
		System.out.println(this.getTitle());
	}

	void printPages() {
		System.out.println(this.getPages());
	}

	void printDetails() {
		this.printAuthor();
		this.printTitle();
		this.printPages();
	}

	public static void main(String[] args) {
     Book b = new Book("夏目漱石", "坊っちゃん");
     b.setPages(500);
     b.printDetails();
     Book b2 = new Book("Charles Dickens", "A Tale of Two Cities");
     b2.setPages(400);
     b2.printDetails();
	}
}
