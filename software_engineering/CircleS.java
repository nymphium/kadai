public class CircleS implements Shape {
	protected double RAD = 3.0;
	protected double XRANGE[] = {-5.0, 5.0};
	protected double YRANGE[] = {-5.0, 5.0};
	protected double XSTEP = 0.25;
	protected double YSTEP = 0.5;

	public boolean inside(double x, double y){
		return (x*x + y*y) <= RAD * RAD;
	}

	public XYRange getRange() {
		return new XYRange(XRANGE[0], XRANGE[1], XSTEP, YRANGE[0], YRANGE[1], YSTEP);
	}
}

