public class PlotDot implements Plot{
	public void plotInside() {
		System.out.print("*");
	}

	public void plotOutside() {
		System.out.print(" ");
	}

	public void nextLine() {
		System.out.println();
	}
}
