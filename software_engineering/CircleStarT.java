public class CircleStarT extends FigureTemplate {
	protected double RAD = 3.0;
	protected double XRANGE[] = {-5.0, 5.0};
	protected double YRANGE[] = {-5.0, 5.0};
	protected double XSTEP = 0.25;
	protected double YSTEP = 0.5;

	protected boolean inside(double x, double y){
		return (x*x + y*y) <= RAD * RAD;
	}

	protected XYRange getRange() {
		return new XYRange(XRANGE[0], XRANGE[1], XSTEP, YRANGE[0], YRANGE[1], YSTEP);
	}

	protected void plotInside() {
		System.out.print("★");
	}

	protected void plotOutside() {
		System.out.print(" ");
	}

	protected void nextLine() {
		System.out.println();
	}

	public static void main(String[] args) {
		(new CircleStarT()).draw();
	}
}
