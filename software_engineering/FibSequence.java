// ファイルFibSequence.java
import java.util.Iterator;

public class FibSequence implements Iterable<Integer> {
	// 長さnumのフィボナッチ数列を表すクラス
	int num;
	public FibSequence(int num) { this.num = num; }
	public Iterator<Integer> iterator() { return new FibIterator(num); }
}

class FibIterator implements Iterator<Integer> {
	private int num, i, j;
	FibIterator(int num) { 
		this.num = num;
		i = 0; j = 1;
	}
	public boolean hasNext() {
		return num > 0;
	}
	public Integer next() {
		// iとjを使って次の値を計算して返す
		// i, j, numを更新する
		num--;

		if(i == 0) {
			// fib 0
			return i++;
		}else if(j == 1) {
			// fib 1
			return j++;
		}else if(i + j < 4){
			// fib 2
			j++;
			return i++;
		}else {
			int i1 = j;
			int j2 = i + i1;
			i = i1;
			j = j2;
			return j2 - i1;
		}
	}
	public void remove() { } // 何もしない
}
