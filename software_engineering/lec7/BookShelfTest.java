import junit.framework.TestCase;

public class BookShelfTest extends TestCase {
	private BookShelf bs;
	public void setUp() {
		bs = new BookShelf(3);
		bs.appendBook(new Book("hoge"));
		bs.appendBook(new Book("fuga"));
		bs.appendBook(new Book("piyo"));
	}
	public void testGetLength() {
		assertEquals(3, bs.getLength());
	}
	public void testIterator() {
		int i = 0;
		String[] c = {"hoge", "fuga", "piyo"};
		for(Book b : bs) {
			assertEquals(c[i++], b.getName());
		}
	}

	public void testRemoveAt() {
		bs.removeAt(1);
		assertEquals(2, bs.getLength());
		assertEquals("hoge", bs.getBookAt(0).getName());
		assertEquals("piyo", bs.getBookAt(1).getName());
		bs.removeAt(0);
		bs.removeAt(0);
		assertEquals(0, bs.getLength());
	}
}
