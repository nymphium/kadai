public class IndentObserver implements Observer<Entry> {
	public void update(Entry e) {
		printPreorder(e, 0);
	}

	private void printPreorder(Entry e, int indent) {
		if(e != null) {
			for(int i = 0; i < indent; i++) {System.out.print("  ");}
			System.out.println(e.getName());
			e.getChildren().forEach(c -> printPreorder(c, indent + 1));
		}
	}
}
