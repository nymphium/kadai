// ファイル Directory.java
import java.util.List;
import java.util.LinkedList;

public class Directory extends Entry implements Subject<Entry>{
	public List<Observer<Entry>> observerList = new LinkedList<Observer<Entry>>();

	public Directory(String name) {
		super(name);
	}

	private List<Entry> children = new LinkedList<Entry>();
	public List<Entry> getChildren() {
		return children;
	}
	public void add(Entry e) {
		children.add(e);
		notifyObservers();
	}

	public void addObserver(Observer<Entry> o) {
		observerList.add(o);
	}
	public void notifyObservers(){
		observerList.forEach(o -> o.update(this));
	}
}
