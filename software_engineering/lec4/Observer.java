
// ファイルObserver.java
public interface Observer<E> {
    public void update(E e);
}