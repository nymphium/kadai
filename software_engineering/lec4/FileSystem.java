public abstract class FileSystem {
    private static FileSystem instance = null;
    public static FileSystem getInstance() {
		if(instance != null) {
			return instance;
		}else {
			return new SimpleFileSystem();
		}
    }
    public abstract Directory createDirectory(String name);
    public abstract File createFile(String name);
}
