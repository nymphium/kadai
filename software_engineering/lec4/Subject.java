
// ファイルSubject.java
public interface Subject<E> {
    public void addObserver(Observer<E> o);
    public void notifyObservers();
}
