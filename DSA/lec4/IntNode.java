class IntNode {
	protected int val;
	protected IntNode left, right;

	IntNode(int val, IntNode left, IntNode right) {
		this.val = val;
		this.left = left;
		this.right = right;
	}
}

