class IntNode2 {
	protected int val, height;
	protected IntNode2 left, right;

	IntNode2(int val, IntNode2 left, IntNode2 right) {
		this.val = val;
		this.left = left;
		this.right = right;
		resetHeight();
	}

	void resetHeight() {
		this.height = Math.max(height(left), height(right)) + 1;
		if(this.left != null) this.left.resetHeight();
		if(this.right != null) this.right.resetHeight();
	}

	private static int height(IntNode2 n) {
		return n == null ? 0 : n.height;
	}
}

