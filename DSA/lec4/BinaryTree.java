class BinaryTree {
	static void preOrder(Node n) {
		preO(n);

		System.out.println();
	}

	private static void preO(Node n) {
		System.out.printf(n.val + " ");

		if(n.left != null) preO(n.left);
		if(n.right != null) preO(n.right);
	}

	static void inOrder(Node n) {
		inO(n);

		System.out.println();
	}

	private static void inO(Node n) {
		if(n.left != null) inO(n.left);
		System.out.printf(n.val + " ");
		if(n.right != null) inO(n.right);
	}

	static void postOrder(Node n) {
		postO(n);

		System.out.println();
	}

	private static void postO(Node n) {
		if(n.left != null) postO(n.left);
		if(n.right != null) postO(n.right);
		System.out.printf(n.val + " ");
	}

	static void breathFirst(Node n) {
		QueueArray q = new QueueArray((int)Math.pow(2, height(n)) - 1);

		q.enqueue(n);

		while(q.stored > 0) {
			Node pop = q.dequeue();

			System.out.printf(pop.val + " ");

			if(pop.left != null) q.enqueue(pop.left);
			if(pop.right != null) q.enqueue(pop.right);
		}

		System.out.println();
	}

	static int height(Node n) {
		return n == null ? 0 : 1 + (Math.max(height(n.left), height(n.right)));
	}

	static void display(Node n) {
		d(n);

		System.out.println();
	}

	private static void d(Node n) {
		if (n == null) System.out.printf("null");
		else{
			System.out.printf("%s(", n.val);
			d(n.left);
			System.out.printf(",");
			d(n.right);
			System.out.printf(")");
		}
	}

	static void displayImproved(Node n) {
		System.out.println("\\documentclass[a4paper]{article}");
		System.out.println("\\def\\pgfsysdriver{pgfsys-dvipdfmx.def}");
		System.out.println("\\usepackage{tikz}");
		System.out.println("\\usetikzlibrary{trees}");
		System.out.println("\\thispagestyle{empty}");
		System.out.println("\\begin{document}");
		System.out.println("\\tikzstyle{level 1}=[sibling distance=" + (height(n) < 3 ? 3 : height(n) )+ ".8cm]");
		System.out.println("\\tikzstyle{level 2}=[sibling distance=" + (height(n) < 3 ? 1 : height(n) - 1) + ".8cm]");
		System.out.println("\\tikzstyle{level 3}=[sibling distance=" + (height(n) < 3 ? 1 : height(n) - 2) + "cm]");
		System.out.println("\\tikzstyle{edge from parent}=[draw,-]");
		System.out.println("\\tikzstyle{every node}=[distance=1cm,draw,circle,thick,minimum size=2em]");
		System.out.println("\\centering");
		System.out.println("\\begin{tikzpicture}");
		System.out.println("\\node");

		di(n);

		System.out.println(";");
		System.out.println("\\end{tikzpicture}");
		System.out.println("\\end{document}");
	}

	private static void di(Node n) {
		if(n != null) {
			System.out.printf("{%s}\n", n.val);
			System.out.println("child {node");
			di(n.left);
			System.out.println("}");
			System.out.println("child {node");
			di(n.right);
			System.out.println("}");
		} else System.out.printf("{null}");
	}

	public static void main(String[] args) {
		Node I = new Node("I", null, null);
		// Node H = new Node("H", null, null);
		// Node G = new Node("G", null, null);
		// Node F = new Node("F", H, G);
		// Node E = new Node("E", null, I);
		// Node D = new Node("D", null, null);
		// Node C = new Node("C", D, E);
		// Node B = new Node("B", F, null);
		// Node A = new Node("A", C, B);

		Node J = new Node("J", null, null);
		Node E = new Node("E", null, null);
		Node H = new Node("H", null, null);

		Node F = new Node("F", J, I);
		Node B = new Node("B", E, null);
		Node D = new Node("D", H, null);
		Node G = new Node("G", B, F);
		Node C = new Node("C", null, D);
		Node A = new Node("A", C, G);


		display(A);
		preOrder(A);
		inOrder(A);
		postOrder(A);
		breathFirst(A);
		System.out.println(height(A));

		// displayImproved(A);
	}
}

