class BinarySearchTree2 {
	static IntNode2 root;

	static int minBst() {
		return minBst(root);
	}

	static int minBst(IntNode2 n) {
		return n.left == null ? n.val : minBst(n.left);
	}

	private static int maxLeftBst(IntNode2 n) {
		return n.right == null ? n.val : maxLeftBst(n.right);
	}

	static boolean searchBst(int d) {
		return searchBst(root, d);
	}

	private static boolean searchBst(IntNode2 n, int d) {
		return (n == null) ? false : (n.val == d) ? true : searchBst((n.val > d ? n.left : n.right), d);
	}

	static void insertBst(int d) {
		insertBst(root, d);
		root.resetHeight();
	}

	private static void insertBst(IntNode2 n, int d) {
		if (root == null) {
			root = new IntNode2(d, null, null);
	
			return;
		}

		if(n.val > d) {
			if(n.left == null) {
				n.left = new IntNode2(d, null, null);
			}
			else insertBst(n.left, d);
		}else {
			if(n.right == null) {
				n.right = new IntNode2(d, null, null);
			}
			else insertBst(n.right, d);
		}
	}

	static void deleteBst(int d) {
		deleteBst(root, d);
		root.resetHeight();
	}

	static void deleteBst(IntNode2 n, int d) {
		if(n == null || ! searchBst(d)) return;

		if(root.val == d) {
			if(root.left == null && root.right == null) root = null;
			else if(root.left == null && root.right != null) root = root.right;
			else if(root.left != null && root.right == null) root = root.left;
			else {
				int lmax = maxLeftBst(root.left);
				deleteBst(lmax);
				root.val = lmax;
			}

			return;
		}

		if(n.left != null && n.left.val == d) {
			if(n.left.left == null && n.left.right == null) n.left = null;
			else if(n.left.left == null && n.left.right != null) n.left = n.left.right;
			else if(n.left.left != null && n.left.right == null) n.left = n.left.left;
			else {
				int lmax = maxLeftBst(n.left.left);
				deleteBst(lmax);
				n.left.val = lmax;
			}
		} else if(n != null && n.right.val == d) {
			if(n.right.left == null && n.right.right == null) n.right = null;
			else if(n.right.left == null && n.right.right != null) n.right = n.right.right;
			else if(n.right.left != null && n.right.right == null) n.right = n.right.left;
			else {
				int lmax = maxLeftBst(n.right.left);
				deleteBst(lmax);
				n.right.val = lmax;
			}
		}

		deleteBst(n.val > d ? n.left : n.right, d);
	}

	static void display() {
		display(root);

		System.out.println();
	}

	private static void display(IntNode2 n) {
		if (n == null) System.out.printf("null");
		else {
			System.out.printf("%s#%d(", n.val, n.height);
			display(n.left);
			System.out.printf(",");
			display(n.right);
			System.out.printf(")");
		}
	}

	public static void main(String[] args) {
		insertBst(20);
		insertBst(10);
		insertBst(26);
		insertBst(14);
		insertBst(13);
		insertBst(5);
		display();

		if (searchBst(14)) System.out.println("Found!");
		else System.out.println("Not found!");

		if (searchBst(7)) System.out.println("Found!");
		else System.out.println("Not found!");

		deleteBst(20);
		display();
		deleteBst(10);

		System.out.println(root.height);

		display();
		System.out.println("Minimum: "+minBst());
	}
}

