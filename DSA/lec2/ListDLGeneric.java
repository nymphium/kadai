public class ListDLGeneric {
	private ListDLGeneric prev, next;
	private Object data;

	@SafeVarargs
	<T> ListDLGeneric(T... data) {
		this.data = data.length < 1 ? null : data[0];
		this.prev = this;
		this.next = this;
	}

	void insertPrev(ListDLGeneric l) {
		this.prev.next = l;
		l.next = this;
		l.prev = this.prev;
		this.prev = l;
	}

	void insertNext(ListDLGeneric l) {
		this.next.prev = l;
		l.prev = this;
		l.next = this.next;
		this.next = l;
	}

	void deletePrev() {
		if(this.next == this.prev) {
			System.out.println("WARN: this cell is not linked");

			return;
		}else {
			if(this.prev.data == null) {
				this.prev.data = this.prev.next.data;
				this.prev.next = this.prev.next.next;
				this.prev.next.prev = this.prev;
			}else {
				this.prev = this.prev.prev;
				this.prev.next = this;
			}
		}
	}

	void deleteNext() {
		if(this.next.data == null) {
			this.next.next.deletePrev();
		}else {
			this.next = this.next.next;
			this.next.prev = this;
		}
	}

	<T> ListDLGeneric search(T i, ListDLGeneric... args) {
		ListDLGeneric l = args.length < 1 ? this : args[0];

		return l.data == i ? l : l.next == this ? null : search(i, l.next);
	}

	void display(ListDLGeneric... args) {
		ListDLGeneric l = args.length < 1 ? this : args[0];

		if(l.next == this) System.out.println(l.data);
		else {
			System.out.printf(l.data + " ");

			display(l.next);
		}
	}
}

