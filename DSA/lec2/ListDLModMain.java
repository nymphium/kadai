public class ListDLModMain {
	public static void main(String[] args) {
		ListDLMod head = new ListDLMod();
		ListDLMod elem;

		head.insertPrev(new ListDLMod(4));
		head.insertNext(new ListDLMod(2));
		head.insertNext(new ListDLMod(1));
		head.insertPrev(new ListDLMod(5));
		head.display();

		elem = head.search(2);
		elem.insertNext(new ListDLMod(3));
		head.display();

		elem = head.search(2);
		elem.deletePrev();
		head.display();

		head.deleteNext();
		head.display();

		ListDLMod fromArray = ListDLMod.readFromArray(new Integer[] {1, 2, 3, 4, 5});
		fromArray.display();

		(new ListDLMod()).deletePrev();

		ListDLMod test = head.search(8);

		fromArray.writeToFile();

		(ListDLMod.readFromFile()).display();
	}
}

