import java.util.stream.Stream;
import java.util.*;
import java.io.*;


public class ListDLMod {
	private ListDLMod prev, next;
	private Integer data;

	ListDLMod(Integer... data) {
		this.data = data.length < 1 ? null : data[0];
		this.prev = this;
		this.next = this;
	}

	void insertPrev(ListDLMod l) {
		this.prev.next = l;
		l.next = this;
		l.prev = this.prev;
		this.prev = l;
	}

	void insertNext(ListDLMod l) {
		this.next.prev = l;
		l.prev = this;
		l.next = this.next;
		this.next = l;
	}

	void deletePrev() {
		if(this.next == this.prev) System.out.println("WARN: this cell is not linked");
		else {
			if(this.prev.data == null) {
				this.prev.data = this.prev.next.data;
				this.prev.next = this.prev.next.next;
				this.prev.next.prev = this.prev;
			}else {
				this.prev = this.prev.prev;
				this.prev.next = this;
			}
		}
	}

	void deleteNext() {
		if(this.next.data == null) this.next.next.deletePrev();
		else {
			this.next = this.next.next;
			this.next.prev = this;
		}
	}

	ListDLMod search(Integer i, ListDLMod... args) {
		ListDLMod l = args.length < 1 ? this : args[0];

		return l.data == i ? l : l.next == this ? null : search(i, l.next);
	}

	void display(ListDLMod... args) {
		ListDLMod l = args.length < 1 ? this : args[0];

		if(l.next == this) System.out.println(l.data);
		else {
			System.out.printf(l.data + " ");

			display(l.next);
		}
	}

	static ListDLMod readFromArray(Integer[] elem) {
		ListDLMod ret = new ListDLMod();
		Stream<Integer> elSt = Stream.of(elem);

		elSt.forEach(e -> ret.insertPrev(new ListDLMod(e)));

		ret.prev.deleteNext(); // delete empty cell

		return ret;
	}

	Integer[] writeToArray() {
		return writeToArray(new ArrayList<>(), this);
	}

	Integer[] writeToArray(ArrayList<Integer> ar, ListDLMod l) {
		ar.add(l.data);

		return l.next == this ? ar.toArray(new Integer[0]) : writeToArray(ar, l.next);
	}

	static ListDLMod readFromFile() {
		ListDLMod ret = new ListDLMod();

		try {
			BufferedReader br = new BufferedReader(new FileReader(new File("elements")));
			String line;

			while((line = br.readLine()) != null) {
				ret.insertPrev(new ListDLMod(Integer.parseInt(line)));
			}
		}catch(Exception e) {
			System.err.println(e);
		}

		ret.prev.deleteNext(); // delete empty cell

		return ret;
	}

	void writeToFile() {
		try {
			PrintWriter pw = new PrintWriter(new BufferedWriter(new FileWriter(new File("elements"))));

			writeToFile(pw, this);

			pw.close();
		}catch(Exception e) {
			System.err.println(e);
		}
	}

	void writeToFile(PrintWriter pw, ListDLMod l) {
		pw.println(l.data);
		if(l.next == this) return;
		writeToFile(pw, l.next);
	}
}

