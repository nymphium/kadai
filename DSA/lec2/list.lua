List = {}

function List:constructor(l, data)
	local ls = {next = l or nil, data = data or nil}

	return setmetatable(ls, {__index = List})
end

setmetatable(List, {__call = List.constructor, __mode = "kv"})

head = List()


function insert_cell(ls, d)
	ls.next = List(ls.next, d)
end

function insert_cell_top(d)
	local function ict(ls, d)
		if ls.next then
			ict(ls.next, d)
		else
			ls.next = List(nil, d)
		end
	end

	ict(head, d)
end

function delete_cell(ls)
	ls.next = ls.next.next
end

function delete_cell_top()
	local function dct(ls)
		if ls.next then
			dct(ls)
		else
			ls.data = nil
		end
	end

	dct(head)
end

function display()
	local function d(ls)
			io.write(ls.data or "", " ")

		if ls.next then
			d(ls.next)
		else
			print()
		end
	end

	d(head.next)
end


-- insert_cell(head, 3)
insert_cell_top(1)

