class List {
	static List head;
	List next;
	int data;

	List(List next, int data) {
		this.next = next;
		this.data = data;
	}

	static void insertCell(List p, int d) {
			p.next = new List(p.next, d);
	}

	static void insertCellTop(int d) {
			head = new List(head, d);
	}

	static void deleteCell(List p) {
		p.next = p.next.next;
	}

	static void deleteCellTop() {
		head = head.next;
	}

	static void display() {
		List l = head;

		do {
			System.out.printf(l.data + " ");
		}while((l = l.next) != null);

		System.out.println();

	}


	public static void main(String[] args) {
		insertCellTop(1);
		insertCell(head, 3);
		insertCell(head, 2);
		display();
		deleteCell(head);
		display();
		deleteCellTop();
		display();
		insertCell(head, 4);
		insertCell(head, 5);
		insertCellTop(8);
		display();
	}
}

