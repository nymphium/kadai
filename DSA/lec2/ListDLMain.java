public class ListDLMain {
	public static void main(String[] args) {
		ListDL head = new ListDL();
		ListDL elem;

		head.insertPrev(new ListDL(4));
		head.insertNext(new ListDL(2));
		head.insertNext(new ListDL(1));
		head.insertPrev(new ListDL(5));
		head.display();

		head.search(9);

		elem = head.search(2);
		if(elem != null) elem.insertNext(new ListDL(3));
		head.display();

		elem = head.search(5);
		if(elem != null) elem.deleteNext();
		head.display();

		head.deletePrev();
		head.display();

		(new ListDL()).deleteNext();
	}
}

