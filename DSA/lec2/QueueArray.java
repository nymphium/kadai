import java.util.ArrayList;


class QueueArray {
	Integer length, top, stored;
	ArrayList<Integer> queue;

	QueueArray(Integer len) {
		queue = new ArrayList<>();
		length = len;
		top = 0;
		stored = 0;
	}

	void enqueue(Integer val) {
		if(stored == length) {
			System.err.println("ERROR: this queue is not empty.");
	
			return;
		}

		queue.add(val);
		stored++;

		if (++top == length) top = 0;
	}

	Integer dequeue() {
		if(queue.isEmpty()) {
			System.err.println("EROR: Queue is empty.");
			
			return null;
		}

		Integer ret = queue.get(0);
		queue.remove(0);
		stored--;

		return ret;
	}

	void display() {
		if(queue.isEmpty()) System.out.printf("EMPTY");

		queue.stream().forEach(q -> System.out.printf("%s ", q != null ? q : "null"));

		System.out.println();
	}


	public static void main(String[] args) {
		QueueArray queue = new QueueArray(9);

		queue.enqueue(1);
		queue.enqueue(2);
		queue.enqueue(3);
		queue.enqueue(4);
		queue.enqueue(5);
		queue.display();
		System.out.println(queue.dequeue());
		System.out.println(queue.dequeue());
		System.out.println(queue.dequeue());
		System.out.println(queue.dequeue());
		System.out.println(queue.dequeue());
		queue.display();

		queue.enqueue(1);
		queue.enqueue(2);
		queue.enqueue(3);
		queue.enqueue(4);
		queue.enqueue(5);
		queue.enqueue(6);
		queue.enqueue(7);
		queue.enqueue(8);
		queue.enqueue(9);
		queue.enqueue(10);
		queue.enqueue(11);
		queue.enqueue(12);
		queue.display();
		System.out.println(queue.dequeue());
		queue.display();
		queue.enqueue(10);
		queue.display();

		queue = new QueueArray(15);

		queue.enqueue(1);
		queue.display();
	}
}

