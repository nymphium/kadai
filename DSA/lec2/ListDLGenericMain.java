class ListDLGenericMain {
	public static void main(String[] args) {
		ListDLGeneric head = new ListDLGeneric();
		ListDLGeneric elem;

		head.insertPrev(new ListDLGeneric("a"));
		head.insertNext(new ListDLGeneric(1));
		head.insertNext(new ListDLGeneric(new ListDLGeneric()));
		head.insertPrev(new ListDLGeneric(true));
		head.display();

		elem = head.search(1);
		if(elem != null) elem.insertNext(new ListDLGeneric(new Object()));
		head.display();

		elem = head.search(true);
		if(elem != null) elem.deleteNext();
		head.display();

		elem = head.search(true);
		if(elem != null) elem.deletePrev();
		head.display();

		(new ListDLGeneric()).deleteNext();
	}
}

