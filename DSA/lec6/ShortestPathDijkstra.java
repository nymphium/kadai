public class ShortestPathDijkstra{
	static final int M = Integer.MAX_VALUE;

	// static final int N = 6;

	// static int w[][] ={
	// {0, M,  M, 8, 15, M},
	// {0, 0, 24, M,  8, M},
	// {M, M,  0, M,  M, 6},
	// {M, M,  M, 0,  5, M},
	// {M, M, 12, M,  0, 7},
	// {M, M,  3, M,  M, 0}};

	static final int N = 4;

	// static int w[][] = {
	// {0, 4, M, M, 3, M, M, M, M},
	// {M, 0, M, M, 0, M, M, M, M},
	// {M, 12, 0, M, M, M, M, M, M},
	// {M, M, 7, 0, M, M, M, 20, M},
	// {M, M, M, M, 0, 7, M, M, M},
	// {M, 13, M, M, M, 0, 6, M, M},
	// {M, 2, M, 4, M, M, 0, 8, M},
	// {M, M, M, M, M, M, M, 0, 1},
	// {M, M, 9, M, M, M, 8, M, 0}
	// };

	static int w[][] = {
		{0, 10, 30, M},
		{M, 0, 5, 15},
		{M, M, 0, 40},
		{10, M, M, 0}
	};


	static boolean S[] = new boolean[N];

	static int d[] = new int[N];

	static int parent[] = new int[N];


	static void add(int u, boolean[] S){
		S[u] = true;
	}

	static boolean remain(){
		for(boolean b : S) if(!b) return true;

		return false;
	}

	static int selectMin(){
		int ret = M;

		for(int k = N - 1; k >= 0 ; k--) ret = (!S[k] && d[k] < ret) ? k : ret;

		return ret;
	}

	static boolean member(int u, int x){
		return (w[u][x] != M && u != x);
	}

	static void dijkstra(int p){
		add(p, S);

		for(int i = 0; i < N; i++){
			d[i] = w[p][i];

			parent[i] = p;
		}

		while(remain()){
			int u = selectMin();

			if(u == M) break;

			add(u, S);

			for(int x = 0; x < N; x++){
				if(member(u, x)){
					int k = d[u] + w[u][x];

					if(k < d[x]){
						d[x] = k;

						parent[x] = u;
					}
				}
			}
		}

		displayPath(p);
	}

	static void display(String name, int[] ary){
		String stdout = name + ":";

		for(int i : ary) stdout += i < M ? " " + i  : " M";

		System.out.println(stdout);
	}

	static void displayPath(int p){
		for(int i = 0; i < N; i++){
			System.out.print(p + " to " + i + " : ");

			String stdout = "";

			if(d[i] == M || i == p){
				System.out.println(i != p ? "unreachable" : "X");

				continue;
			}

			int tmp = parent[i];

			while(tmp != p){
				stdout = " -> " + Integer.toString(tmp) + stdout;

				tmp = parent[tmp];
			}

			System.out.println(p + stdout + " -> " + i);
		}
	}


	public static void main(String[] args){
		if(args.length != 1){
			System.err.println("Usage: java ShortestPathDijkstra <出発点>");

			System.exit(1);
		}

		int p = Integer.parseInt(args[0]);

		if(p >= N){
			System.err.println("WORNING : You can ONLY select startpoint [0 ~  " +  (N - 1) + "]");

			System.exit(1);
		}

		for(int i = 0; i < N; i++) S[i] = false;

		dijkstra(p);

		display("Result", d);
	}
}
