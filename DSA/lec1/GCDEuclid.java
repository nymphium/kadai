class GCDEuclid{
	static int gcd(int m, int n){
		int tmp;

		while(n > 0){
			tmp = n;
			n = m % n;
			m = tmp;
		}

		return m;
	}

	public static void main(String[] args){
		int m = Integer.parseInt(args[0]);
		int n = Integer.parseInt(args[1]);

		System.out.printf("The GCD of %d and %d is %d.\n", m, n, gcd(m, n));
	}
}

