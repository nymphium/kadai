class GCDRecursive{
	static int gcd(int m, int n){
		return m % n < 1 ? n : gcd(n, m % n);
	}

	public static void main(String[] args){
		int m = Integer.parseInt(args[0]);
		int n = Integer.parseInt(args[1]);

		System.out.printf("The GCD of %d and %d is %d.\n", m, n, gcd(m, n));
	}
}

