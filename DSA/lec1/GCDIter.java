public class GCDIter{
	public static void main(final String[] args){
		if(args.length != 2){
			System.err.println("Usage:java GCDIter <int1> <int2>");
			System.exit(1);
		}

		int n = Integer.parseInt(args[0]);
		int m = Integer.parseInt(args[1]);

		int gcd = gcd(n, m);
		System.out.println("The GCD of " + m + "and " + n + "is " + gcd + ".");
	}

	static int gcd(int n, int m){
		if(n > m){
			int tmp = m;
			m = n;
			n = tmp;
		}

		int gcd = 1;
		int i = 1;
		while (i <= n) {
			if (n % i == 0 && m % i == 0)
				// gcd = i;
			{gcd = i;System.out.printf("%d\n", i);}
			i++;
		}
		return gcd;
	}
}
