class DictDoubleHash {
	private DictData[] d;
	private int q;

	DictDoubleHash(int len) {
		d = new DictData[len];
		q = prim(d.length);

		for(int i = 0; i < len; i++) {
			d[i] = new DictData();
		}
	}

	private int prim(int i) {
		for(int j = 2; j < d.length; j++) {
			if(i % j != 0) return j;
		}

		return 1;
	}

	private int hash(int i, int j) {
		int len = d.length;

		return (i % len  + j * (q - i % q)) % len;
	}

	int searchHash(int i) {
		for(int j = 0; j < d.length; j++) {
			int h = hash(i, j);

			if(d[h].state == DictData.State.OCCUPIED && d[h].data == i) return h;
		}

		return -1;
	}

	void insertHash(int i) {
		for(int j = 0; j < d.length; j++) {
			int h = hash(i, j);

			if(d[h].state != DictData.State.OCCUPIED) {
				d[h].data = i;
				d[h].state = DictData.State.OCCUPIED;

				return;
			}
		}

		System.err.println("ERROR: CANNOT insert new data, dictionry is full");
	}

	void deleteHash(int i) {
		try {
			d[searchHash(i)].state = DictData.State.DELETED;
		}catch(Exception e) {}
	}

	void display() {
		String line = "------";

		System.out.println(line);

		for( int i = 0; i < d.length; i++ ) {
			if(d[i].state == DictData.State.OCCUPIED) System.out.println(i + " " + d[i].data);
		}

		System.out.println(line);
	}
}

