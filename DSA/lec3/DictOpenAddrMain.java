class DictOpenAddrMain {
	public static void main(String[] args) {
		DictOpenAddr dict = new DictOpenAddr(20);

		dict.insertHash(1);
		dict.insertHash(2);
		dict.insertHash(3);
		dict.insertHash(21);
		dict.insertHash(50);
		dict.insertHash(7);
		dict.insertHash(21);
		dict.insertHash(98);
		dict.insertHash(51230);
		dict.insertHash(72);
		dict.insertHash(498);
		dict.insertHash(500);
		dict.insertHash(932);
		dict.insertHash(841);
		dict.insertHash(6);
		dict.insertHash(21);
		dict.insertHash(8);
		dict.insertHash(9);
		dict.insertHash(0);
		dict.insertHash(0);
		dict.insertHash(0x0);
		dict.display();

		System.out.println("Search 1 ...\t"  + dict.searchHash(1));
		System.out.println("Search 2 ...\t"  + dict.searchHash(2));
		System.out.println("Search 21 ...\t" + dict.searchHash(21));
		System.out.println("Search 5 ...\t"  + dict.searchHash(11));

		dict.deleteHash(3);
		dict.display();

		dict.deleteHash(11);
		dict.display();
	}
}

