class DictOpenAddr {
	private DictData[] d;

	DictOpenAddr(int len) {
		d = new DictData[len];

		for(int i = 0; i < len; i++) {
			d[i] = new DictData();
		}
	}

	private int hash(int i, int j) {
		return (i + j) % d.length;
	}

	int searchHash(int i) {
		for(int j = 0; j < d.length; j++) {
			int h = hash(i, j);

			if(d[h].state == DictData.State.OCCUPIED && d[h].data == i) return h;
		}

		return -1;
	}

	void insertHash(int i) {
		for(int j = 0; j < d.length; j++) {
			int h = hash(i, j);

			if(d[h].state != DictData.State.OCCUPIED) {
				d[h].data = i;
				d[h].state = DictData.State.OCCUPIED;
				return;
			}
		}

		System.err.println("ERROR: CANNOT insert new data, dictionry is full");
	}

	void deleteHash(int i) {
		try {
			d[searchHash(i)].state = DictData.State.DELETED;
		}catch(Exception e) {}
	}

	void display() {
		String line = "------";

		System.out.println(line);

		for( int i = 0; i < d.length; i++ ) {
			if(d[i].state == DictData.State.OCCUPIED) System.out.println(i + " " + d[i].data);
		}

		System.out.println(line);
	}
}

