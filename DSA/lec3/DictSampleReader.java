import java.io.*;

class DictSampleReader {
	public static void main(String[] args) {
		int fileSize = Integer.parseInt(args[0]);
		String line;
		String fileName = "dict-sample.txt";
		String usedHashAlg;
		long start, end;

		try {
			FileReader fr = new  FileReader(fileName);
			BufferedReader br = new BufferedReader(fr);

			if(args.length > 1) {
				usedHashAlg = "Double Hash";
				DictDoubleHash dictDH = new DictDoubleHash(fileSize);
				start = System.nanoTime();

				while((line = br.readLine()) != null) {
					dictDH.insertHash(new Integer(line));
				}

				end = System.nanoTime();
			}else {
				usedHashAlg = "Open Address";
				DictOpenAddr dictOA = new DictOpenAddr(fileSize);
				start = System.nanoTime();

				while((line = br.readLine()) != null) {
					dictOA.insertHash(new Integer(line));
				}

				end = System.nanoTime();
			}

			System.out.println("Used hash algorithm: " + usedHashAlg + " Time(sec): " + (end - start) / 100000000.0);

			fr.close();
		} catch(Exception e) {
			System.err.println("ERROR: invalid file name");
		}
	}
}

