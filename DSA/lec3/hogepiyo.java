public class DictOpenAddr{
	DictData[] H;
	int B;

	enum State{
		EMPTY, DELETED, OCCUPIED
	}

	DictOpenAddr(int len){
		B = len;

		H = new DictData[B];

		for(int i = 0; i < B; i++){
			H[i] = new DictData();

			H[i].name = 0;

			H[i].state = State.EMPTY;
		}
	}

	int hash(int d){
		return d % B;
	}

	void insert_hash(int d){
		int i;

		for(i = 0; i < B; i++){
			int n = hash(d + i);
			if(H[n].state != State.OCCUPIED){
				H[n].name = d;

				H[n].state = State.OCCUPIED;

				break;
			}
		}

		if(i >= B) System.err.println("ERR: no empty array left.");
	}

	int search_hash(int d){

		int i;

		for(i = 0; i <= B; i++){
		int n = hash(d + i);
			if(H[n].state == State.OCCUPIED && H[n].name == d){
				return n;
			}
		}

		return -1;
	}

	void delete_hash(int d){
		int n = hash(d);


		int i;

		for(i = 1; i < B; i++){
			if(H[n].state == State.OCCUPIED && H[n].name == d) H[n].state = State.DELETED;

			n = hash(d + i);
		}
	}

	void display(){
		for(int i = 0; i < B; i++){
			String OUT = H[i].state != State.OCCUPIED ? "null" : Integer.toString(H[i].name);

			System.out.println(OUT);
		}
	}


	public class DictData{
		int name;

		State state;

		DictData(){
			this.name = 0;

			this.state = State.EMPTY;
		}
	}


	public static void main(String[] args){
		DictOpenAddr dict = new DictOpenAddr(10);

		dict.insert_hash(1);
		dict.insert_hash(2);
		dict.insert_hash(3);
		dict.insert_hash(11);
		dict.insert_hash(12);
		dict.insert_hash(21);
		dict.display();

		System.out.println("Search 1 ...\t" + dict.search_hash(1));
		System.out.println("Search 2 ...\t" + dict.search_hash(2));
		System.out.println("Search 21 ...\t" + dict.search_hash(21));
		System.out.println("Search 5 ...\t" + dict.search_hash(5));

		dict.delete_hash(3);
		dict.display();

		dict.delete_hash(11);
		dict.display();
	}
}
