import java.util.ArrayList;


class QueueArray {
	Integer length, top, stored;
	ArrayList<Integer> queue;

	QueueArray(Integer len) {
		queue = new ArrayList<>();
		length = len;
		top = 0;
		stored = 0;
	}

	void enqueue(Integer val) {
		if(stored == length) {
			System.err.println("ERROR: this queue is not empty.");

			return;
		}

		queue.add(val);
		stored++;

		if (++top == length) top = 0;
	}

	Integer dequeue() {
		if(queue.isEmpty()) return null;

		Integer ret = queue.get(0);
		queue.remove(0);
		stored--;

		return ret;
	}

	void display() {
		if(queue.isEmpty()) System.out.printf("EMPTY");

		queue.stream().forEach(q -> System.out.printf("%s ", q != null ? q : "null"));

		System.out.println();
	}
}

