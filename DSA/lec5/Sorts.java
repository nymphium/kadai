import java.util.Random;
import java.util.Scanner;
import java.util.stream.Stream;
import java.util.stream.IntStream;
import java.lang.StringBuilder;

class Sorts {
	static boolean isVerbose = false;
	static private long compareCount = 0;

	static private class Util {
		static void swap(int[] array, int a, int b) {
			int tmp = array[a];
			array[a] = array[b];
			array[b] = tmp;

			if(isVerbose) System.out.printf("swap %d, %d\n", a, b);
		}

		static int compare(int i, int j) {
			compareCount++;

			if(isVerbose) System.out.printf("compare %d, %d : %b | %b | %b\n", i, j, i < j, i == j, i > j);

			// < --> -1
			// == -> 0
			// > --> 1
			return i < j ? -1 : i == j ? 0 : 1;
		}

		static long getCompareCounter() {
			return compareCount;
		}

		static void reset() {
			compareCount = 0;
		}

		static  void shiftDown(int[] array, int i, int len) {
			i -= (i + 1) % 2;
			int parentIndex = (i + 1) / 2 - 1;
			int biggerIndex = i > len - 1 || array[i] >= array[i + 1] ? i : i + 1;

			if(Util.compare(array[parentIndex], array[biggerIndex]) == -1) Util.swap(array, parentIndex, biggerIndex);
		}

		static void buildHeap(int[] array, int len) {
			for(int i = len; i > 0; i-=2) shiftDown(array, i, len);
		}

		static int partition(int[] array, int left, int right) {
			int l = left;
			int r = right - 1;

			while(true) {
				while(Util.compare(array[l], array[right]) == -1) l++;
				while(l <= r && Util.compare(array[r], array[right]) > -1) r--;

				if(l < r) Util.swap(array, l, r);
				else break;
			}

			Util.swap(array, l, right);

			return l;
		}

		static int digit(int num, int dig) {
			String numStr = Integer.toString(num);

			if(numStr.length() < dig) return 0;

			return Character.getNumericValue(((new StringBuilder(numStr)).reverse()).toString().charAt(dig - 1));
		}
	}

	static void heap(int[] array) {
		for(int i = array.length - 1; i > 0; i--) {
			Util.buildHeap(array, i);

			Util.swap(array, 0, i);
		}
	}

	static void quick(int[] array) {
		quick(array, 0, array.length - 1);
	}

	static void quick(int[] array, int left, int right) {
		if(left < right) {
			int p = Util.partition(array, left, right);

			quick(array, left, p - 1);
			quick(array, p + 1, right);
		}
	}

	static void insert(int[] array) {
		for(int i = 0; i < array.length - 1; i++) {
			int next = i + 1;
			if(Util.compare(array[next], array[0]) == -1) {
				for(int j = next; j > 0; j--) Util.swap(array, j, j - 1);
			}else {
				int k = next;
				while(Util.compare(array[k], array[k - 1]) == -1) {

					Util.swap(array, k, --k);
				}
			}
		}
	}

	static void bucket(int[] array, int div) {
		QueueArray b[] = new QueueArray[10];

		for(int i = 0; i < 10; i++) b[i] = new QueueArray(10000);

		for(int j = 0; j < array.length; j++) b[Util.digit(array[j], div)].enqueue(array[j]);

		int cnt = 0;
		Integer pop;

		for(int k = 0; k < 10; k++) {
			b[k].display();
			while((pop = b[k].dequeue()) != null) array[cnt++] = pop;
		}

		System.out.println("------");
	}

	static void radix(int[] array) {
		IntStream.rangeClosed(1,3).forEach(i -> bucket(array, i));
	}

	static class TestUtil {
		static int[] makeIntArray(int... num) {
			int l = num.length < 1 ? 100 : num[0];

			int[] array = new int[l];

			for(int i = 0; i < array.length; i++){
				Random rnd = new Random();

				array[i] = rnd.nextInt(l * 127 + 1) / 7 + 1;
			}

			return array;
		}

		static void putsVal(int[] array) {
			for(int i: array){
				System.out.print(i + "\n");
			}
		}

		static boolean isSorted(int[] array) {
			int tmp = array[0];

			for(int i : array) {
				if(tmp > i) return false;
				tmp = i;
			}

			return true;
		}

		static void selectRunAlgorithm(String algName, int[] array) {
			if(isVerbose) System.out.println("Algorithm: " + algName);

			switch(algName) {
				case "heap":
					heap(array);
					break;

				case "insert":
					insert(array);
					break;

				case "quick":
					quick(array);
					break;

				case "radix":
					radix(array);
					break;

				default:
					System.err.println("ERROR: invalid Algorithm name");
					System.exit(1);
			}
		}
	}


	public static void main(String[] args) {
		boolean isMeasure = false;
		boolean isWorst = false;
		int n = 0;
		String algName = "heap";

		for(int i = 0; i < args.length; i++) {
			try {
				if(args[i].equals("-v")) isVerbose = true;

				if(args[i].equals("-t")) isMeasure = true;

				if(args[i].equals("-a")) algName = args[i + 1];

				if(args[i].equals("--worst")) isWorst = true;

				n = Integer.parseInt(args[i]);
			}catch(Exception __) {}
		}

		if(isWorst) {

			for(int h = 1; h <= 10; h++) {
				int j = 1000 * h;
				int worstArray[] = new int[j];
				for(int i = 0; i < j; i++) worstArray[j - 1- i] = i;

				TestUtil.selectRunAlgorithm("quick", worstArray);

				System.out.printf("%d %d\n", j, Util.getCompareCounter());

				Util.reset();
			}

			System.exit(0);
		}

		if(algName.equals("radix")) {
			int array[] = {143, 322, 246, 755, 123, 563, 514, 522};

			TestUtil.selectRunAlgorithm(algName, array);

			TestUtil.putsVal(array);

			System.exit(0);
		}

		if(n > 0) {
			Scanner scan = new Scanner(System.in);
			int[] a = new int[n];

			System.out.println(n+"個の整数を入力してください．");

			for (int i = 0; i < n; i++) a[i] = scan.nextInt();

			TestUtil.selectRunAlgorithm(algName, a);

			System.out.println("整列結果");

			TestUtil.putsVal(a);

			System.out.println("比較回数 " + Util.getCompareCounter());
		}else {
			double start = 0;
			double end = 0;

			for(int i = 1; i <= 10; i++) {
				int j = i * 1000;
				int array[] = TestUtil.makeIntArray(j);

				if(isMeasure) start = System.nanoTime();

				TestUtil.selectRunAlgorithm(algName, array);

				if(isMeasure) end = System.nanoTime();

				System.out.printf("%d %d%s\n",j, Util.getCompareCounter(), isMeasure ? (" Time: " + Double.toString((end - start) / Math.pow(10, 8))) : "");

				Util.reset();
			}
		}
	}
}

