public class DPKnapsack2{
	static int[] v = {0, 250, 380, 420, 520};

	static int[] w = {0, 1, 2, 4, 3};

	static int SIZE=10000;

	static int[][] dp = new int[SIZE][SIZE];

	public static boolean[] knapsack(int[] v, int[] w, int k, int i){
		boolean[] done = new boolean[SIZE];
	
		for(int l = 0; l <= k; l++){
			for(int m = 0; m <= i; m++){
				dp[l + 1][m] = (w[l] <= m && dp[l][m] < dp[l][m - w[l]] + v[l]) ? dp[l][m - w[l]] + v[l] : dp[l][m];
			}
		}

		int m = i;

		for(int l = k; l >= 0; l--){
			if(w[l] <= m && dp[l][m] < dp[l][m - w[l]] + v[l]){
				done[l] = true;

				m -= w[l];
			}
		}

		return done;
	}

	public static void main(String[] args){
		if(args.length == 2){
			int k = Integer.parseInt(args[0]);

			int i = Integer.parseInt(args[1]);

			boolean done[]  = knapsack(v, w, k, i);

			int total = 0;

			for(int j = 0; j <= k; j++){
				if(done[j]){
					System.out.printf("重さ %d 価値 %d\n", w[j], v[j]);
			
					total += v[j];
				}
			}

			System.out.println("合計価格 " + total);
		}else System.out.println("2個の引数を与えてください");
	}
}
