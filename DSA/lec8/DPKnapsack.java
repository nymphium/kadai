import java.util.Random;


public class DPKnapsack{
	static int[] v = {0, 250, 380, 420, 520};

	static int[] w = {0, 1, 2, 4, 3};

	static int[][] dp = new int[10000][10000];

	public static int knapsack(int[] v, int[] w, int k, int i){
		for(int l = 0; l <= k; l++){
			for(int m = 0; m <= i; m++){
				dp[l + 1][m] = (w[l] <= m && dp[l][m] < dp[l][m - w[l]] + v[l]) ? dp[l][m - w[l]] + v[l] : dp[l][m];
			}
		}

		return dp[++k][i];
	}

	public static void main(String[] args){
		if(args.length == 2){
			int k = Integer.parseInt(args[0]);

			int i = Integer.parseInt(args[1]);

			System.out.println(knapsack(v, w, k, i));
		}else if(args.length == 1){
			int n = Integer.parseInt(args[0]);

			int[] v = new int[n+1]; 

			int[] w = new int[n+1]; 

			Random rnd = new Random();

			for(int i = 0; i < n; i++){ 
				v[i] = rnd.nextInt(100);

				w[i] = rnd.nextInt(10)+1;
			}

			long t1 = System.nanoTime(); 

			int i = knapsack(v, w, n, n*5);

			long t2 = System.nanoTime(); 

			System.out.println(i);

			System.out.println();

			System.out.println(((t2-t1)/1000000)+"ミリ秒");
		}else System.out.println("１～２個の引数を与えてください");
	}
}
