import java.io.File;
import java.io.FileReader;
import java.io.BufferedReader;


class StringMatch{
	static boolean isVerbose = false;

	static int Ncmp = 0;

	static char[] text;

	static char[] pat;


	public static String readFile(String path){
		StringBuffer sb = new StringBuffer();

		try{
			File file = new File(path);

			BufferedReader br = new BufferedReader(new FileReader(file));

			String str;

			while((str = br.readLine()) != null) sb.append(str);

			br.close();
		}catch(Exception e){
			System.out.println(e);
		}

		return new String(sb);
	}

	public static boolean cmp(char a, char b){
		if(isVerbose) System.out.printf("cmp(%c, %c)\n", a, b);

		Ncmp++;

		return a == b;
	}

	public static int naive(char[] text, char[] pat){
		int t = text.length;

		int p = pat.length;

		for(int i = 0; i < t - p + 1; i++){
			for(int j = 0; j < p; j++){
				if(!cmp(text[i + j], pat[j])) break;

				if(j == p - 1) return i;
			}
		}

		return -1;
	}

	public static int[] compnext(char[] pat){
		int len = pat.length;

		int[] table = new int[len];

		for(int i = 0; i < len; i++){
			int j = i - 1;

			while(j > 0){
				if(pat[0] == pat[j]) break;

				j--;
			}

			if(j > 0){
				int k = 1;

				int head = j;

				while(j < i){
					if(pat[k] != pat[head + k++]) break;

					j++;
				}

				table[i] = (j != i) ? j : i + 1;
			}else{
				table[i] =  pat[i] == pat[0] ? i + 1 : i;
			}
		}

		return table;
	}

	public static int kmp(char[] text, char[] pat){
		int t = text.length;

		int p = pat.length;

		int[] table = compnext(pat);

		if(isVerbose){
			System.out.println("stuff of table:");

			for(int j : table){
				System.out.println(j);
			}
		}

		int start = 0;

		for(int i = 0; i < p; i++){
			if(!cmp(text[start + i], pat[i])){
				start += table[i];

				if(start > t - p + 1) return -1;

				if(isVerbose) System.out.println("missed. shift searchPosition => " + start);

				i = -1;
			}

			if(i == p - 1) return start;
		}

		return -1;
	}


	public static void main(String[] args){
		if(args.length < 2){
			System.err.println("Usage: java StringMatch [-v] <text file> <pattern file> [-kmp]");

			System.exit(1);
		}

		int i = 0;

		if(args[i].equals("-v")){
			isVerbose = true;

			i++;
		}

		text = readFile(args[i++]).toCharArray();

		pat = readFile(args[i]).toCharArray();

		int comptext = -1;

		if(args.length > 3){
			if(args[3].equals("-kmp")){
				comptext = kmp(text, pat);
			}
		}else{
			comptext = naive(text, pat);
		}

		String stdout = "Pattern found at " + comptext + ".\n";

		if(isVerbose){
			stdout = "text size: " + text.length +

				"\npattern size: " + pat.length + "\n" +

				stdout + "# of comparisons: " + Ncmp + ".\n";
		}

		System.out.print(stdout);
	}
}
