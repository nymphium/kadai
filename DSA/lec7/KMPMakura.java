class KMPMakura {
	static int[] makuraGen(char[] pat) {
		int len = pat.length;
		int[] table = new int[len];

		for(int i = 0; i < len; i++) {
			int j = i - 1;

			while(j > 0) {
				if(pat[0] == pat[j]) break;

				j--;
			}

			if(j > 0) {
				int k = 1;
				int head = j;

				while(j < i) {
					if(pat[k] != pat[head + k++]) break;

					j++;
				}

				table[i] = (j != i) ? j : i + 1;
			}else{
				table[i] =  pat[i] == pat[0] ? i + 1 : i;
			}
		}

		return table;
	}

	public static void main(String[] args) {
		char pat[] = {'A', 'B', 'A', 'A', 'C'};
		int tbl[] = makuraGen(pat);

		for(int i = 0; i < tbl.length; i++) {
			System.out.println(tbl[i]);
		}
	}
}
