from functools import reduce

#  def gcd_(a, b):
    #  tmp = 0;

    #  while b > 0:
        #  tmp = b
        #  b = a % b
        #  a = tmp

    #  return a

#  def is_prime(n):
    #  if n % 2 == 0and n > 2:
        #  return False

    #  for i in range(3, int(math.sqrt(n)) + 1, 2):
        #  return all(n % i for i in range(3, int(math.sqrt(n)) + 1, 2))

#  def gcd(a, b):
    #  g = gcd_(a, b)

#  def perfect(n):
    #  acc = [1]

    #  for i in range(2, int(n / 2) + 1):
        #  g = gcd_(n, i)

        #  if g > 1 and not g in acc:
            #  acc.append(g)

    #  return n == reduce(lambda a, b: a + b, acc)


def perfect(n):
    def gcd(a, b):
        tmp = 0
        while b > 0:
            tmp = b
            b = a % b
            a = tmp
        return a

    acc = [1]

    for i in range(2, int(n / 2) + 1):
        (lambda g: acc.append(g) if not g in acc else None)(gcd(n, i))

    return n == reduce(lambda a, b: a + b, acc)

