import sys

def distance(graph, edg, dst):
    if edg > dst: return distance(graph, dst, edg)
    if graph[edg][dst] == 1: return 1

    t = [sys.maxsize if graph[edg][i] == 0 else distance(graph, i, dst) for i in range(edg + 1, len(graph) - 1)]

    if len(t) == 1 and edg + 1 == dst or len(t) == 0: return 0

    min_path = min(t)

    if min_path > 0: return min_path + 1
    else: return 0

graph = [[1, 1, 0, 0, 0], [1, 1, 1, 0, 1], [0, 1, 1, 1, 1], [0,0,1,1,1],[0,1,1,1,1]]
#  graph = [[1, 1, 0], [1, 1, 1], [0, 1, 1]]

for i in range(len(graph)):
    for j in range(len(graph)):
        print(i, "->", j, ":", 0 if i == j else distance(graph, i, j))

