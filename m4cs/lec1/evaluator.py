import sys

def foldl(fn, xroot, xs):
    return xroot if (len(xs) == 0) else foldl(fn, fn(xroot, xs[0]), xs[1:])

def upperprop(str):
    return str.replace("P", "p").replace("Q", "q").replace("AND", "and").replace("OR", "or")

def prop_to_fn(propstr):
    return eval("lambda p, q: {prop}".format(prop = upperprop(propstr)))

while True:
    sys.stdout.write(">> ")
    s1 = input()
    if len(s1) == 0:
        break

    sys.stdout.write(">> ")
    s2 = input()
    prop1 = prop_to_fn(s1)
    prop2 = prop_to_fn(s2)
    result = []

    for p in [True, False]:
        for q in [True, False]:
            result.append((prop1(p, q)) == (prop2(p, q)))

    print("==> ","Same" if foldl((lambda a, b: a and b), True, result) else "Not same")

