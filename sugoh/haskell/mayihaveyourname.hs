main :: IO ()
main = do
  putStrLn "May I have your name?"
  name <- getLine
  putStrLn $ "Nice to meet you, " ++ name
