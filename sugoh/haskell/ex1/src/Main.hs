module Main where

putHello = interact $ \x -> "Hello World"

myCat = do
  n <- getLine
  putStrLn n

main = do
  putHello
  myCat
