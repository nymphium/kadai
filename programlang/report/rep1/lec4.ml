type expr =
    | CstI of int
    | Prim of string * expr * expr
    | Plus1 of expr
    | Plus2 of int
    | Minus1 of expr
    | Minus2 of int
    | BinOp of (int -> int -> int) * expr * expr

(* CK abstract machine with stack *)
let evals e =
    let rec evals e k =
        match e with
        | CstI(i) -> apply k i
        | Prim("+", e1, e2) -> evals e2 ((Plus1 e1) :: k)
        | Prim("-", e1, e2) -> evals e2 ((Minus1 e1) :: k)
        | _ -> failwith "error at evals"
    and apply k n =
        match k with
        | Plus1(e) :: k' -> evals e ((Plus2 n) :: k')
        | Plus2(n') :: k' -> apply k' (n + n')
        | Minus1(e) :: k' -> evals e ((Minus2 n) :: k')
        | Minus2(n') :: k' -> apply k' (n - n')
        | [] -> n
        | _ -> failwith "error at apply"

    in evals e []

let id x = x

(* CK abstract machine with continuation *)
let evalc e =
    let rec evalc e k =
        match e with
        | CstI i -> k i
        | Prim("+", e1, e2) -> evalc e2 (fun x -> k ((evalc e1 id) + x))
        | Prim("-", e1, e2) -> evalc e2 (fun x -> k ((evalc e1 id) - x))
        | BinOp(f, e1, e2) -> evalc e2 (fun x -> k (f (evalc e1 id) x))
        | _ -> failwith "error"
    in evalc e id

