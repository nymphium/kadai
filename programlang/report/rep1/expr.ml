type expr e =
    | ConsI of int
    | Prim of (int -> int -> int) * expr * expr;;

let rec eval e =
    match e of
    | ConsI e -> e
    | Prim(f, e1, e2) -> f (eval  e1) (eval  e2)
    (* | _ -> failwith "Error";; *)

