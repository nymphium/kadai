let three_n_plus_one n =
    let rec f n acc =
        if n = 1 || acc > 99 then acc
        else
            if n mod 2 = 0 then f (n / 2) (acc + 1)
            else f (n * 3 + 1) (acc + 1)

    in f n 0;;

