type binarytree =
    | Leaf of int
    | Node of binarytree * binarytree;;

let rec minimum t =
    match t with
    | Node(Leaf tl, Leaf tr) -> if tl <= tr then tl else tr
    | Node(el, er)  ->
            let ml = minimum el
            and mr = minimum er in
            if ml <= mr then ml else mr
    | Leaf i -> i;;

let rec balanced t =
    match t with
    | Node(Leaf tl, Leaf tr) -> tl <= tr
    | Node(el, er) -> (minimum el) <= (minimum er)
    | _ -> true;;

let rec insert t v =
    match t with
    | Node(tl, tr) ->
            if v <= minimum(tl) then
                Node(insert tl v, tr)
            else
                Node(tl, insert tr v)
    | Leaf i -> if i <= v then Node(t, Leaf v) else Node(Leaf v, t)

