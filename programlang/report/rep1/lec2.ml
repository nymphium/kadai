let divisor n =
    let rec divisor n m acc =
        if m = 1 then acc
        else
            if n mod m = 0 then divisor n (m - 1) (acc + 1)
            else divisor n (m - 1) acc

    in divisor n n 1;;

