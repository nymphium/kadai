// Test2.java

// We assume that Point is a parent class, and ColoredPoint is its child class.

class Test2 {
	public static void main(String args[]) {

		Point         p = new Point(1.0, 2.0); 
		ColoredPoint cp = new ColoredPoint(3.0, 4.0, 7);

		Point         q = p;
		ColoredPoint cq = cp;

		// print each object
		System.out.println(p.toString()); 
		System.out.println(q.toString()); 
		System.out.println(cp.toString());
		System.out.println(cq.toString());
		System.out.println("");

		// Try the following test, and see what is printed
		System.out.println("test 1 for overload/override");
		q = p;
		foo(q);     
		foo(p);     
		foo(cp);    
		foo(p,cp);  
		foo(cp,cp); 
		foo(q,cp);  
		System.out.println("");

		System.out.println("test 2 for overload/override");
		q = cp;
		foo(q);     
		foo(p);     
		foo(cp);    
		foo(p,cp);  
		foo(cp,cp); 
		foo(q,cp);  
	}

	public static void foo(Point p) {
		System.out.println("foo-1:" + p.toString());
	}
	public static void foo(ColoredPoint cp) {
		System.out.println("foo-2:" + cp.toString());
	}
	public static void foo(Point p, ColoredPoint cp) {
		System.out.println("foo-3:" + p.toString() + ":" + cp.toString());
	}
}
