// Point class   
//   a 2-dimensional point with (x,y)-coordinates
//   the "distance" method  gives the distance between two points

class Point {
    private double x; 
    private double y;

    public double getX() { 
	return x;
    }
    public double getY() { 
	return y;
    }
    protected void move (double dx, double dy) { 
	x = x + dx; 
	y = y + dy;
    }
    public double distance (Point p) { 
	double dx = x - p.getX();
	double dy = y - p.getY();
	return  Math.sqrt (Math.pow(dx,2) + Math.pow(dy,2));
    }
    public String toString () { 
	return "Point(" + x + "," + y + ")";
    }
    Point (double xval, double yval) { 
	x = xval; 
	y = yval;
    }
}

