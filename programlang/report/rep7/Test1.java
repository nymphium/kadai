class Test1 {
	public static void main(String args[]) {
		// ClassA  x;  ClassB  y;

		// x = new ClassA();                 // test1
		// System.out.println(x.toString()); // test2
		// System.out.println(x.method1());  // test3
		//System.out.println(x.method2());  // test4
		//y = x;                            // test5
		//System.out.println(y.toString()); // test6
		//System.out.println(y.method1());  // test7
		//System.out.println(y.method2());  // test8
		// y = new ClassB();                 // test9
		// System.out.println(y.toString()); // test10
		// System.out.println(y.method1());  // test11
		// System.out.println(y.method2());  // test12
		//x = y;                            // test13
		//System.out.println(x.toString()); // test14
		//System.out.println(x.method1());  // test15
		//System.out.println(x.method2());  // test16

		// Point x;
		// ColoredPoint y;
		// x = new ColoredPoint(1, 1, 1);
		// System.out.println(x);

		// y = new Point(1., 1.);


		Point x; ColoredCircle c;

		x = new ColoredCircle(1, 1, 1, 1);
		System.out.println(x);

		// c = new Point(1, 1);
	}
}
