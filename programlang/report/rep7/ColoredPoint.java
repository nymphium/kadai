// ColoredPoint.java

class ColoredPoint extends Point {
    private int c; 

    public int getColor () { 
	return c;
    }
    protected void changeColor  (int cval) { 
	c = cval;
    }
    public String toString () { 
	return "ColoredPoint(" + getX() + "," 
	    + getY() + "," + getColor() + ")";
    }
    ColoredPoint (double xval, double yval, int cval) { 
	super (xval,yval); 
	changeColor (cval);
    }
}

