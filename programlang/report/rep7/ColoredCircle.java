class ColoredCircle extends ColoredPoint {
	private double radius;

	public double getRadius() {
		return radius;
	}

	public void setRadius(double r) {
		radius = r;
	}

	public String toString() {
		return "ColoredCircle(" + getX() + "," + getY() + "," + getRadius() + "," + getColor() +")";
	}

	ColoredCircle(double xval, double yval, double radius, int cval) {
		super(xval, yval, cval);
		setRadius(radius);
	}
}
