type binarytree' =
    | Leaf of float
    | Node of binarytree' * binarytree'

let rec minimum t =
    match t with
    | Node(Leaf tl, Leaf tr) ->
            if tl <= tr then tl else tr
    | Node(el, er)  ->
            let ml = minimum el in
            let mr = minimum er in
            if ml <= mr then ml else mr
    | Leaf i -> i

let rec balanced t =
    match t with
    | Node(Leaf tl, Leaf tr) -> tl <= tr
    | Node(el, er) -> minimum el <= minimum er
    | _ -> true

(* module BinaryTree' : sig *)
        (* type balanced_binarytree' = private *)
            (* [> | Leaf_ of float <] *)
            (* Node_ of binarytree' * binarytree' *)
        (* [> val tl : binarytree' <] *)
        (* [> val tr : binarytree' <] *)
        (* val dt: binarytree' -> binarytree' *)
        (* end *)
    (* = struct *)
        (* type balanced_binarytree' = *)
            (* Node_ of binarytree' * binarytree' *)
            (* let dt (Node(tl, tr)) = assert((balanced tl) && (balanced tr) &&(minimum tl) <= (minimum tr)); Node_(tl, tr) *)
        (* end;; *)

