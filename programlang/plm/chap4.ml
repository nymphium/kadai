(* 
 * DO NOT DISTRIBUTE THIS CODE.
 * 
 * Code originally written in F# quoted from the book:
 *        Sestoft, Programming Language Concepts, Springer, 2012
 * Comments and exercises by Yukiyoshi Kameyama
 *)

(* ---- Chapter4 ----  *)

(* ---------------------------------------------------------------------- *)

(* 4.2節 一階の関数型プログラム言語 の構文 *)

type expr = 
  | CstI of int
  | CstB of bool
  | Var of string
  | Let of string * expr * expr
  | Prim of string * expr * expr
  | If of expr * expr * expr
  | Letfun of string * string * expr * expr    (* (f, x, fBody, letBody) *)
  | Call of expr * expr

(* 
   ここで Letfun(f,x,fBody,letBody) は OCamlでいうところの
     let rec f x = fBody in letBody                   
   という再帰関数定義に相当する構文である。つまり letBodyの中で
   f を再帰的に呼び出して使うことができる(使わなくてもよい)。
   また、Call(f,e) は 関数呼び出し、あるいは関数適用であり、
   OCaml では、単に f e や f(e) と書かれるものである。
*)

(* ---------------------------------------------------------------------- *)

(* 4.3節 実行時の「値」 および 4.4節 環境 *)

(* 実行時の値(計算結果)は、整数もしくは、関数クロージャである *)

type value = 
  | Int of int
  | Closure of string * string * expr * value env       
 and 'v env = (string * 'v) list

(* 
   Closure(f, x, fBody, fDeclEnv) は、関数クロージャに対応し、
   環境 fDeclEnv のもとで
      let rec f x = fBody in ...  
   という再帰関数定義をおこなったときに生成される。
   fDeclEnv は「f が定義されたときの環境」という意味である。
 *)

(* 
   環境envを前と同じように定義する。ただし、今度は、変数に対応付けられ
   る「値」が整数値とは限らず、関数になるかもしれないので、環境の型も 
      (string * int) list でなく 
      (string * 'v) list としている 
   ここで、'v は、「型のパラメータ (型変数)」をあらわす表現であり、
   詳しくは「型システム」の講義で、後日説明する。いまの段階では、
   環境の型は、上記の 'v に valueをいれた、
      value env   
      (これは (string * value) list と同じ)
   になった、という点だけ知っておいてほしい。
*)

(* 
   lookup関数の定義は前と同じである。(前とは、型が違っているのに、
   まったく同じ定義ができるのは「すごい」ことであり、多相型と
   よばれる機能にも通じるのであるが、これも後日説明する。
 *)

let rec lookup env x =
    match env with 
    | []        -> failwith (x ^ " not found")
    | (y, v)::r -> if x=y then v else lookup r x;;

(* ---------------------------------------------------------------------- *)

(* 4.5節 環境に基づく評価器 *)

(* 
   この章の対象言語に対する評価器(インタープリタ)である evalは
   以下のように定義される。Letfun と Call 以外に対する処理は、本質的に 
   は前と同じであるが、たとえば、真理値定数をあらわす CstB b や
   if-then-else などが増えているので、すこし長くなっている。
 *)

let rec eval (e : expr) (env : value env) : int =
    match e with 
    | CstI i -> i
    | CstB b -> if b then 1 else 0
    | Var x  ->
            begin
                match lookup env x with
                 | Int i -> i 
                 | _     -> failwith "eval Var"
            end
     | Prim(ope, e1, e2) -> 
             let i1 = eval e1 env in
             let i2 = eval e2 env in
             begin
                 match ope with
                | "*" -> i1 * i2
                | "+" -> i1 + i2
                | "-" -> i1 - i2
                | "=" -> if i1 = i2 then 1 else 0
                | "<" -> if i1 < i2 then 1 else 0
                | _   -> failwith ("unknown primitive " ^ ope)
            end
    | Let(x, eRhs, letBody) -> 
            let xVal = Int(eval eRhs env) in
            let bodyEnv = (x, xVal) :: env in
            eval letBody bodyEnv
    | If(e1, e2, e3) -> 
            let b = eval e1 env in
            if b<>0 then eval e2 env
            else eval e3 env
    | Letfun(f, x, fBody, letBody) -> 
            let bodyEnv = (f, Closure(f, x, fBody, env)) :: env in
            eval letBody bodyEnv
    | Call(Var f, eArg) -> 
            let fClosure = lookup env f in
            begin
                match fClosure with
                | Closure (f, x, fBody, fDeclEnv) ->
                        let xVal = Int(eval eArg env) in
                        let fBodyEnv = (x, xVal) :: (f, fClosure) :: fDeclEnv in
                        eval fBody fBodyEnv
                | _ -> failwith "eval Call: not a function"
             end
    | Call _ -> failwith "eval Call: not first-order function"

(*
  この評価器の説明は授業でおこなったので、スライド等を参考にしてほしい。
  授業で話さなかった真理値型の処理は、
    | CstB b -> if b then 1 else 0
  とあるように、対象言語の CstB trueを計算すると整数 1 となり、
  対象言語の CstB false を計算すると整数0となるようにしている。
*)

(* 
   評価器evalを外から呼ぶときの関数 runは前と同じである。
*)

let run e = eval e [];;

(* 例 *)

let ex1 = Letfun("f1", "x", Prim("+", Var "x", CstI 1), 
                 Call(Var "f1", CstI 12));;

let ex1_run = run ex1 ;;

(* Example: factorial *)
(* 階乗の関数 *)

let ex2 = Letfun("fac", "x",
                 If(Prim("=", Var "x", CstI 0),
                    CstI 1,
                    Prim("*", Var "x", 
                              Call(Var "fac", 
                                   Prim("-", Var "x", CstI 1)))),
                 Call(Var "fac", Var "n"));;

(* let fac10 = eval ex2 [("n", Int 10)];; *)

(* Example: deep recursion to check for constant-space tail recursion *)
(* 再帰呼び出しが深くても、スタックを消費せずに実行できるかどうかのチェッ *)
(* ク -> 末尾再帰 に関係する。*)

let ex3 = Letfun("deep", "x", 
                 If(Prim("=", Var "x", CstI 0),
                    CstI 1,
                    Call(Var "deep", Prim("-", Var "x", CstI 1))),
                 Call(Var "deep", Var "count"));;
    
let rundeep n = eval ex3 [("count", Int n)];;

(* Example: static scope (result 14) or dynamic scope (result 25) *)
(* 静的スコープなら答えが14になり、動的スコープなら答えが 25 になる *)

let ex4 =
    Let("y", CstI 11,
        Letfun("f", "x", Prim("+", Var "x", Var "y"),
               Let("y", CstI 22, Call(Var "f", CstI 3))));;

(* Example: two function definitions: a comparison and Fibonacci *)
(* 2つの関数定義; 比較関数とフィボナッチ関数 *)

let ex5 = 
    Letfun("ge2", "x", Prim("<", CstI 1, Var "x"),
           Letfun("fib", "n",
                  If(Call(Var "ge2", Var "n"),
                     Prim("+",
                          Call(Var "fib", Prim("-", Var "n", CstI 1)),
                          Call(Var "fib", Prim("-", Var "n", CstI 2))),
                     CstI 1), Call(Var "fib", CstI 25)));;

(* 演習問題4.5-a: 
   上記の ex1 - ex5 を実行して、結果が予想されたもの通りか確認しなさい。
*)

(* 演習問題4.5-b: 
   静的束縛ができているか確認するため、以下のOCaml式に相当するexpr型の式
   を定義し、eval で評価しなさい。 
*)

(*   
   let x = 10 in 
   let rec f y = x + y in 
   let x = 20 in 
      f 30
*)

(* 演習問題4.5-c: 
   クロージャを使わないと、静的束縛でなく、動的束縛になることを
   確認するため、以下のことをやってみなさい。
   まず、eval の定義の うち、以下の部分を書きかえなさい。

    ...
    | Call(Var f, eArg) -> 
      let fClosure = lookup env f in
      begin
	match fClosure with
	| Closure (f, x, fBody, fDeclEnv) ->
           let xVal = Int(eval eArg env) in
           let fBodyEnv = (x, xVal) :: (f, fClosure) :: fDeclEnv in
                                                        ^^^^^^^^
                                                         envにする
           eval fBody fBodyEnv
	| _ -> failwith "eval Call: not a function"
      end
    ...

   このevalで、演習問題4.5-b の例題を実行して、たしかに動的束縛に
   なることを確認しなさい。

   注: 上記のように、fDeclEnv を使わないなら、最初からクロージャを作る
   必要はなく、eval の実装はもっと簡単になる。実際、John McCarthy が書
   いた初期の Lispインタプリタは動的束縛であった。
 *)

(* 演習問題4.5-d: 
   静的束縛と動的束縛において、実行時の環境の大きさ(リストの長さ)を
   比べてみよう。

   例として、上記の factorial関数の例をもちいて、実行時の環境が
   最大でどの程度の長さになるかを計算せよ。

   このためには、
        print_int (List.length env);
        print_newline ();
   という行を、関数 eval の最初の方にいれるとよい。これをいれると
   環境env の「長さ」を印刷して、改行をいれる、ということをしてくれる。

   factorialの例を通じて、実行時環境のについて、静的束縛と動的束縛の
   それぞれがどのようになるか、言葉で簡単に説明せよ。
 *)

(* 演習問題4.5-e: 
   前の章の演習問題 2.4-a では、「変数のインデックス」を計算して、
   高速化をおこなった。この手法は、静的束縛の場合であったが、
   同様のことが動的束縛でも可能か考えなさい。

   (これは期末試験ではないので、自分で考えた理由を述べればよい。
    後日の授業で、「正しい」解答を述べる予定である。)
 *)

(* ---------------------------------------------------------------------- *)

