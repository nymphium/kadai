(* Code from Sestoft, Programming Language Concepts, Springer, 2012 *)

(* ---- Chapter 7 ---- *)

(* microC 言語の構文と意味の定義  *)

(* 
   ここでは、F# や OCamlにある Map というモジュールを使ってストアを実 
   現している。Map や、モジュールの使い方そのものを学習する必要は(この
   授業では)なく、ストアに対する以下の3つの操作さえ理解すればよい。

      emptystore      空のストア 
      getSTo s x      ストアs において、x に対応する値を返す
      setSTo s (x,v)  ストアs において x に対応する値を v に変更する
 *)

module Naivestore = Map.Make(String) ;;
type naivestore = int Naivestore.t ;;   (* ストアの型 *)
let emptystore = Naivestore.empty ;;
let getSto (store : naivestore) x = Naivestore.find x store
let setSto (store : naivestore) (k, v) = Naivestore.add k v store

(* micro-C の式の構文 *)

type expr = 
  | CstI of int
  | Var of string
  | Prim of string * expr * expr
    
(* micro-C の式の評価; 環境でなくストアをもらっている点が以前のものと *)
(* は違うが、それ以外は、前と同じ。 *)

let rec eval e (store : naivestore) : int =
    match e with
    | CstI i -> i
    | Var x  -> getSto store x
    | Prim(ope, e1, e2) ->
      let i1 = eval e1 store in
      let i2 = eval e2 store in
      begin
      match ope with
      | "*"  -> i1 * i2
      | "+"  -> i1 + i2
      | "-"  -> i1 - i2
      | "==" -> if i1 = i2 then 1 else 0
      | "<"  -> if i1 < i2 then 1 else 0
      | _    -> failwith "unknown primitive"
      end

(* micro-C の文の構文 *)

type stmt = 
  | Asgn of string * expr
  | If of expr * stmt * stmt
  | Block of stmt list
  | For of string * expr * expr * stmt
  | While of expr * stmt
  | Print of expr

(* micro-C の文の評価; 文とストアをもらって、新しいストアを返す。
   つまり、ストアをどう変更するか、を表す。
   値を返すのではない点が、式の評価とは異なる。
   これも、「評価器」の一種ではあるが、「evaluate」というよりは、
   「execute」という感じなので、関数名が exec になっている。
 *)

let rec exec stmt (store : naivestore) : naivestore =
    match stmt with
    | Asgn(x, e) -> 
      setSto store (x, eval e store)
    | If(e1, stmt1, stmt2) -> 
      if eval e1 store <> 0 then exec stmt1 store
                            else exec stmt2 store
    | Block stmts -> 
      let rec loop ss sto =     (* このループは block内の文を順番に実 *)
				(* 行するためのもの *)
        match ss with 
        | []     -> sto
        | s1::sr -> loop sr (exec s1 sto)
      in loop stmts store
    | For(x, estart, estop, stmt) -> 
      let start = eval estart store in
      let stop  = eval estop  store in
      let rec loop i sto =      (* for文に対応するループ*)
                                (* ループ変数は1つずつ増える*)
              if i > stop then sto 
                          else loop (i+1) (exec stmt (setSto sto (x, i)))
      in loop start store 
    | While(e, stmt) -> 
      let rec loop sto =        (* e が 0 になるまで実行する *)
              if eval e sto = 0 then sto
                                else loop (exec stmt sto)
      in loop store
    | Print e ->               
      (Printf.printf "%d\n" (eval e store); store)

let run stmt = 
    let _ = exec stmt emptystore in
    ()

(* 演習問題 *)

(* 7-1. 以下の micro-C プログラムは、C言語のどのようなプログラムに(おおよそ)
   対応するか、具体的に書きなさい。
   また、その対応がただしいか、micro-C と C の双方で実際に、プログラム
   を実行して確認しなさい。
   なお、micro-C の for文は、C言語のfor文と微妙に異なることに注意せよ。
 *)

let ex1 =
    Block[Asgn("sum", CstI 0);
          For("i", CstI 0, CstI 100, 
              Asgn("sum", Prim("+", Var "sum", Var "i")));
          Print (Var "sum")];;
    
let ex2 =
    Block[Asgn("i", CstI 1);
          Asgn("sum", CstI 0);
          While (Prim("<", Var "sum", CstI 10000),
                 Block[Print (Var "sum");
                       Asgn("sum", Prim("+", Var "sum", Var "i"));
                       Asgn("i", Prim("+", CstI 1, Var "i"))]);
          Print (Var "i");
          Print (Var "sum")];;
    
(* 7-2. N の階乗をもとめる micro-C プログラムを書きなさい。
   ただし、micro-C には関数定義がないので、
       そのプログラムの最初に、Asgn("x", CstI N) といった形で
       N の値が 変数x に割り当てられる
   と仮定しなさい。 
   このプログラムを実際に、run して、意図通りのものが求まることを
   確認しなさい。
 *)

(* 7-3. 上記の ex1 のプログラムに対して、exce関数がどのように
   処理をするか、For の処理に焦点をあてて説明しなさい。
   つまり、For の処理の部分で定義されている loop 関数がどう
   いうものであって、その loop 関数がどのような引数で呼ばれて
   いくかを i=0,1,2 について書きなさい。(i=100 までやる必要はない。)
 *)

