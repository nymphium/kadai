(* Code from Sestoft, Programming Language Concepts, Springer, 2012 *)
(* ---- Chapter5 ---- *)

(* 高階の関数型言語 *)

(* Sestoft の教科書では、ここの構文は chap4.ml と同じものであったが、 *)
(* 亀山の説明の都合上 Lam を追加した。 *)
(* Lam("x",expr) は、ラムダ抽象、つまり無名関数を表す。*)
(* OCaml では fun x -> expr に相当する式である *)

type expr = 
  | CstI of int
  | CstB of bool
  | Var of string
  | Let of string * expr * expr
  | Prim of string * expr * expr
  | If of expr * expr * expr
  | Letfun of string * string * expr * expr    (* (f, x, fBody, letBody) *)
  | Call of expr * expr
  | Lam of string * expr

(* 環境は、chap4.ml と本質的には同じ *)

type 'v env = (string * 'v) list

let rec lookup env x =
    match env with 
    | []        -> failwith (x ^ " not found")
    | (y, v)::r -> if x=y then v else lookup r x;;

(* 値 (value) も chap4.ml と同じ *)

type value = 
  | Int of int
  | Closure of string * string * expr * value env       (* (f, x, fBody, fDeclEnv) *)

(* "name" for anonymous function (lambda function) *)
let dummy_name = "____"  (* this function name shouldn't be used by user programs *)

(* 環境に基づく評価器 *)
(* chap4.mlの eval との主な違いは以下の通り *)
(* 1. eval関数が返すものが、int でなく value となっている。これは、高 *)
(*    階関数を許すようになったため、int以外に「関数(クロージャ)」を返 *)
(*    す関数というものが出てくるようになったからである。*)
(* 2. そのため、整数13を返す場合も Int 13 のように Intというタグをつけ *)
(*    て返さなければいけない。このため、「e1 + e2」の処理では、e1とe2 *)
(*    のそれぞれの計算結果からIntというタグをとって、加算をして、その結果 *)
(*    に Intというタグをつける、という処理になっている。*)
(* 3. chap4.ml では、変数を評価したとき、その結果がint型でない(クロー *)
(*    ジャである)ときには、エラーであったが、こちらではクロージャをそ *)
(*    のまま返す *)
(* 4. chap4.ml では、Call(e1,e2) という項は、e1 が Var f の形でないと *)
(*    いけなかったが(一階言語という制限のため)、ここでは高階言語なの *)
(*    で、e1 はどんな式でもよい。 *)
(* 5. chap4.ml になかった、Lam の処理が加わっている。*)

let rec eval (e : expr) (env : value env) : value =
    match e with
    | CstI i -> Int i
    | CstB b -> Int (if b then 1 else 0)
    | Var x  -> lookup env x
    | Prim(ope, e1, e2) -> 
      let v1 = eval e1 env in
      let v2 = eval e2 env in
      begin
	match (ope, v1, v2) with
	| ("*", Int i1, Int i2) -> Int (i1 * i2)
	| ("+", Int i1, Int i2) -> Int (i1 + i2)
	| ("-", Int i1, Int i2) -> Int (i1 - i2)
	| ("=", Int i1, Int i2) -> Int (if i1 = i2 then 1 else 0)
	| ("<", Int i1, Int i2) -> Int (if i1 < i2 then 1 else 0)
	|  _ -> failwith "unknown primitive or wrong type"
      end
    | Let(x, eRhs, letBody) -> 
      let xVal = eval eRhs env in
      let letEnv = (x, xVal) :: env in
      eval letBody letEnv
    | If(e1, e2, e3) -> 
       begin
	 match eval e1 env with
	 | Int 0 -> eval e3 env
	 | Int _ -> eval e2 env
	 | _     -> failwith "eval If"
       end
    | Letfun(f, x, fBody, letBody) -> 
      let bodyEnv = (f, Closure(f, x, fBody, env)) :: env in
      eval letBody bodyEnv
    | Call(eFun, eArg) -> 
      let fClosure = eval eFun env in
      begin
	match fClosure with
	| Closure (f, x, fBody, fDeclEnv) ->
           let xVal = eval eArg env in
           let fBodyEnv = (x, xVal) :: (f, fClosure) :: fDeclEnv
           in eval fBody fBodyEnv
      end
    | Lam (x, body) -> Closure(dummy_name, x, body, env)

(* Evaluate in empty environment: program must have no free variables: *)

let run e = eval e [];;

(* Examples in abstract syntax *)

let ex1 = Letfun("f1", "x", Prim("+", Var "x", CstI 1), 
                 Call(Var "f1", CstI 12));;

let _ = run ex1;;

(* Factorial *)

let ex2 = Letfun("fac", "x", 
                 If(Prim("=", Var "x", CstI 0),
                    CstI 1,
                    Prim("*", Var "x", 
                              Call(Var "fac", 
                                   Prim("-", Var "x", CstI 1)))),
                 Call(Var "fac", Var "n"));;

let fac10 = eval ex2 [("n", Int 10)];;

(* 以下は、高階関数らしい例である. これは、OCaml ではどういう計算に対 *)
(* 応するだろうか、どういう高階関数だろうか？*)

let ex3 = 
    Letfun("tw", "g", 
           Letfun("app", "x", Call(Var "g", Call(Var "g", Var "x")), 
                  Var "app"),
           Letfun("mul3", "y", Prim("*", CstI 3, Var "y"), 
                  Call(Call(Var "tw", Var "mul3"), CstI 11)));;

let _ = run ex3;;

(* 以下のものも高階関数である。tw は何をあらわすだろうか？*)

let ex4 = 
    Letfun("tw", "g",
           Letfun("app", "x", Call(Var "g", Call(Var "g", Var "x")), 
                  Var "app"),
           Letfun("mul3", "y", Prim("*", CstI 3, Var "y"), 
                  Call(Var "tw", Var "mul3")));;

let _ = run ex4;;

(* 以下のものは、Lam の利用例である。*)

let ex5 = 
    Call(Lam("x",Var "x"),CstI 10);;

let _ = run ex5;;

let ex6 = 
    Let("f",Lam("x",Var "x"),Call(Var "f",Call(Var "f",CstI 20)));;

let _ = run ex6;;

let ex7 = 
    Let("f",Lam("x",Var "x"),Call(Call(Var "f",Var "f"),CstI 30));;

let _ = run ex7;;

let ex8 = 
    Let("double",Lam("f",Lam("x",Call(Var "f",Call(Var "f",Var "x")))),
	Call(Call(Var "double",Lam("x",Prim("+",Var "x",CstI 1))), CstI 50));;

let _ = run ex8;;

(* 実は、Lam("x",expr) は、この言語では、Letfun("f","x",expr,Var "f") *)
(* と、本質的に同じ関数クロージャを返す。*)

let ex9a = Lam("x",CstI 20);;
let _ = run ex9a;;

let ex9b = Letfun("f","x",CstI 20,Var "f");;
let _ = run ex9b;;

(* ただし、Lam("x",expr) の方は、再帰関数にはできない。なぜなら、再帰 *)
(* 呼び出しするためには、この関数に名前がないといけないから。*)

(* 演習問題 *)
(* 5-1. OCaml における関数合成、つまり、
       fun f -> fun g -> fun x -> g (f x)
   を表す高階関数を、この章の構文で定義せよ。*)
(* 5-2. OCaml において、整数nと関数f をもらい、f を n回適用する高階関数、つまり、
       fun n -> fun f -> fun x -> f (f ( ... f (x) ...))   (f はn回)
   を表す高階関数を、この章の構文で定義せよ。*)

(* 
   5-3. 上記の5-1を表す式 を e1とするとき、以下をこの章のeval/run で計算しなさい。
           e1 (fun x -> x + 1) (fun x -> x * 2) 10
           e1 (fun x -> x * 2) (fun x -> x + 1) 10
 *)
(* 
   5-4. 上記の5-2を表す式 を e2とするとき、以下をこの章のeval/run で計算しなさい。
           e2 10 (fun x -> x + 1) 2
           e2 10 (fun x -> x * 2) 2
 *)
(* 
   5-5. テキスト p.85 にある Y_v (recursion operator for call-by-value)
      と fact' を、この章の構文で表し、Y_v fact' 10 の計算を行いなさい。

[2015/6/3追加 はじめ]

もともとのchap5.ml には以下の式を書いていましたが、これは、閉じ括弧が
1 つ不足していました。
Y_v =  \h. (\x. (\a. h (x x) a) (\x. (\a. h (x x) a))

正確には、以下のものです。
Y_v =  \h. (\x. (\a. h (x x) a)) (\x. (\a. h (x x) a))

[2015/6/3追加 終わり]


fact' = \f. \n. if n = 0 then 1
                else n * f (n - 1)
		


 *)
(* 
   5-6. (発展課題; やれる人のみ)

      Y_v を用いて他の(通常なら再帰呼び出しが必要な)関数を定義してみなさい。
      たとえば、x の y 乗を計算する関数 power をY_vを用いて(let recを使
      わずに)この章の構文で定義しなさい。。

      このY_v は OCaml や F# で表現できるだろうか？ できるなら、それを
      書きなさい。できないなら、なぜできないか理由を書きなさい。
 *)


