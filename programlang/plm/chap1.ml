(* Code from Sestoft, Programming Language Concepts, Springer, 2012 *)
(* ---- Chapter1 ---- *)

(* ---------------------------------------------------------------------- *)

(* 1.3.1節 変数のない式 *)

(* exprという型の定義 *)
type expr = 
  | CstI of int                         (* 整数定数 *)
  | Prim of string * expr * expr;;      (* プリミティブ関数の利用 *)

(* 以下は expr型をもつ項の例 *)

let e1 = CstI 17;;    (* 対象言語の 17 という整数定数をあらわす *) 
let e2 = Prim("-", CstI 3, CstI 4);;   (* 対象言語の 3-4 という式をあらわす *)
let e3 = Prim("+", Prim("*", CstI 7, CstI 9), CstI 10);;
(* 対象言語の (7*9)+10 という式をあらわす *)

(* expr型の式の意味を決める評価関数 *)
(* eval : expr -> int という型をもち、expr型の式をもらい、整数を返す *)
(* ただし、途中で、"unknown primitive" という例外(エラーのようなもの) *)
(* を起こして止まってしまうことがある。*)

let rec eval (e : expr) : int =
    match e with
    | CstI i -> i
    | Prim("+", e1, e2) -> eval e1 + eval e2
    | Prim("*", e1, e2) -> eval e1 * eval e2
    | Prim("-", e1, e2) -> eval e1 - eval e2
    | Prim _            -> failwith "unknown primitive";;

(* 以下は eval の使用例 *)
let e1v = eval e1;;
let e2v = eval e2;;
let e3v = eval e3;;

(* 演習問題1.3.1-a:  1*(2*(3*(4+5))) という式を expr型の式として定め、それを *)
(*            evalで評価せよ。*)

    
(* 演習問題1.3.1-b:  上記の例では「例外」は発生しなかった。そのような *)
(*                  例を1つ定め、eval で評価して、たしかに例外が発生す *)
(*                  ることを確認せよ。 *)

(* 次の評価関数 evalm は、eval と似ているが、「引き算式」の評価のしか *)
(* たが異なる。*)

let rec evalm (e : expr) : int =
    match e with
    | CstI i -> i
    | Prim("+", e1, e2) -> evalm e1 + evalm e2
    | Prim("*", e1, e2) -> evalm e1 * evalm e2
    | Prim("-", e1, e2) -> 
      let res = evalm e1 - evalm e2 in
      if res < 0 then 0 else res 
    | Prim _            -> failwith "unknown primitive";;

(* 以下は evalm の使用例 *)

let e4v = evalm (Prim("-", CstI 10, CstI 27));;

(* 演習問題1.3.1-c:  evalm が eval とどう異なるか、言葉で書きなさい *)

(* ---------------------------------------------------------------------- *)

(* 1.3.2節 変数のある式*)

(* 環境は、たとえば、以下の形のものである *)

let env = [("a", 3); ("c", 78); ("baf", 666); ("b", 111)];;

(* 上記の行を、OCaml で実行すると、env の型がわかるが、 *)
(*   (string * int) list というちょっと複雑なものになる *)
(* これは、「「文字列型 と 整数型の直積 型」のリスト型」という意味である *)
(* 直積型の要素は組であり、OCaml では ( xx, yy, zz) のようにあらわす *)
(* リスト型の要素はリストであり、OCaml では [xx; yy; zz]のようにあらわす *)



(* 空の環境 (変数が1つも値をもっていない状態)は、以下のように、OCaml *)
(* の空リストで表現する *)

let emptyenv = [];; (* 空の環境 *)

(* lookup は 環境envで、変数x (に格納されている文字列) に対応する値を取りだす *)
(* もし、x が env にはいっていないときは、例外を起こす *)

let rec lookup env x =
    match env with 
    | []        -> failwith (x ^ " not found")   (* a^b は文字列の連結 *)
    | (y, v)::r -> if x=y then v else lookup r x;;

(* 以下は、lookup の使用例 *)

let cvalue = lookup env "c";;

(* exprという型の定義を拡張する。ここでは、前とまったく同じexpr という *)
(* 名前を使ってしまっているので、前のexpr型はこれ以降は使えない。 *)
(* (1つのファイルの中で、同じ名前を2回使うのは、普通はあまりよくないの *)
(* で、真似しないように。*)
(* なお、exprという名前の型を再定義しているので、CstI や Prim という名 *)
(* 前をまた使うことができる。*)

type expr = 
  | CstI of int
  | Var of string     (* 変数 *)
  | Prim of string * expr * expr;;

(* 以下は、拡張されたexpr型の式 *)

let e1 = CstI 17;;

let e2 = Prim("+", CstI 3, Var "a");;

let e3 = Prim("+", Prim("*", Var "b", CstI 9), Var "a");;

(* 拡張されたexpr型の式の意味を決める評価関数、これもまた前のeval関数 *)
(* と同じ名前をつかっている。 *)

let rec eval e (env : (string * int) list) : int =
    match e with
    | CstI i            -> i
    | Var x             -> lookup env x 
    | Prim("+", e1, e2) -> eval e1 env + eval e2 env
    | Prim("*", e1, e2) -> eval e1 env * eval e2 env
    | Prim("-", e1, e2) -> eval e1 env - eval e2 env
    | Prim _            -> failwith "unknown primitive";;

(* 以下は実行例である *)

let e1v  = eval e1 env;;
let e2v1 = eval e2 env;;
let e2v2 = eval e2 [("a", 314)];;
let e3v  = eval e3 env;;

(* 演習問題1.3.2-a:  以下のように、環境の中に同じ変数の値が2回ふくまれ *)
(* ていたら、どちらが使われれか、実際に例題を書いて確かめなさい。 *)
(*    
    env1 = [("a", 3); ("c", 78); ("a", 666); ("a", 111)];;
 *)

(* 演習問題1.3.2-b:  環境にふくまれない変数の値を取りにいくとどうなる *)
(* か確かめなさい。*)

(* ---------------------------------------------------------------------- *)
