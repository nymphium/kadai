(* Code from Sestoft, Programming Language Concepts, Springer, 2012 *)
(* ---- Chapter5 ---- *)

(* 高階の関数型言語 *)
(* chap5.ml とは異なる評価戦略のもの *)

type expr = 
  | CstI of int
  | CstB of bool
  | Var of string
  | Let of string * expr * expr
  | Prim of string * expr * expr
  | If of expr * expr * expr
  | Letfun of string * string * expr * expr    (* (f, x, fBody, letBody) *)
  | Call of expr * expr
  | Lam of string * expr

(* 環境は、chap4.ml と本質的には同じ *)

type 'v env = (string * 'v) list

let rec lookup env x =
    match env with 
    | []        -> failwith (x ^ " not found")
    | (y, v)::r -> if x=y then v else lookup r x;;

(* 値 (value) はa chap5.ml の value と異なる thunk が加わっている *)

type value = 
  | Int of int
  | Closure of string * string * expr * value env       (* (f, x, fBody, fDeclEnv) *)
  | Thunk of expr * value env    

(* "name" for anonymous function (lambda function) *)
let dummy_name = "____"  (* this function name shouldn't be used by user programs *)

(* chap5.ml と似ているが少しだけ違う eval *)

let rec eval (e : expr) (env : value env) : value =
    match e with
    | CstI i -> Int i
    | CstB b -> Int (if b then 1 else 0)
    | Var x  -> 
       begin
	 let e1 = lookup env x in
	 match e1 with
	 | Thunk(e2,env2) -> eval e2 env2
	 | _ -> e1
       end
    | Prim(ope, e1, e2) -> 
      let v1 = eval e1 env in
      let v2 = eval e2 env in
      begin
	match (ope, v1, v2) with
	| ("*", Int i1, Int i2) -> Int (i1 * i2)
	| ("+", Int i1, Int i2) -> Int (i1 + i2)
	| ("-", Int i1, Int i2) -> Int (i1 - i2)
	| ("=", Int i1, Int i2) -> Int (if i1 = i2 then 1 else 0)
	| ("<", Int i1, Int i2) -> Int (if i1 < i2 then 1 else 0)
	|  _ -> failwith "unknown primitive or wrong type"
      end
    | Let(x, eRhs, letBody) -> 
      let letEnv = (x, Thunk(eRhs,env)) :: env in
      eval letBody letEnv
    | If(e1, e2, e3) -> 
       begin
	 match eval e1 env with
	 | Int 0 -> eval e3 env
	 | Int _ -> eval e2 env
	 | _     -> failwith "eval If"
       end
    | Letfun(f, x, fBody, letBody) -> 
      let bodyEnv = (f, Closure(f, x, fBody, env)) :: env in
      eval letBody bodyEnv
    | Call(eFun, eArg) -> 
      let fClosure = eval eFun env in
      begin
	match fClosure with
	| Closure (f, x, fBody, fDeclEnv) ->
           let fBodyEnv = (x, Thunk(eArg,env)) :: (f, fClosure) :: fDeclEnv
           in eval fBody fBodyEnv
	| _ -> failwith "eval Call"
      end
    | Lam (x, body) -> Closure(dummy_name, x, body, env)

(* Evaluate in empty environment: program must have no free variables: *)

let run e = eval e [];;

(* Examples in abstract syntax *)

(* in OCaml,  let f x = 10 in let g x = g x in f (g 5) *)
let ex100 = Letfun("f", "x", CstI 10,
            Letfun("g", "x", Call(Var "g", Var "x"),
		   Call(Var "f", (Call (Var "g", CstI 5)))));;

let ex100r = run ex100;;

(* in OCaml,  let fac x = ... in fac 5000 *)
(* this computation takes time *)
let ex200 = Letfun("fac", "x", 
                 If(Prim("=", Var "x", CstI 0),
                    CstI 1,
                    Prim("*", Var "x", 
                              Call(Var "fac", 
                                   Prim("-", Var "x", CstI 1)))),
                 Call(Var "fac", CstI 5000));;

let ex200r = run ex200;;

(* in OCaml,  let f x = 10 in let fac x = ... in f (fac 5000) *)
(* どのくらいの時間がかかるだろうか ?*)
let ex300 = Letfun("f", "x", CstI 10,
                Letfun("fac", "x", 
                 If(Prim("=", Var "x", CstI 0),
                    CstI 1,
                    Prim("*", Var "x", 
                              Call(Var "fac", 
                                   Prim("-", Var "x", CstI 1)))),
                 Call(Var "f", Call(Var "fac", CstI 5000))));;

let ex300r = run ex300;;

(* in OCaml,  let f x = x + x + x in let fac x = ... in f (fac 5000) *)
(* どのくらいの時間がかかるだろうか ?*)
let ex400 = Letfun("f", "x", Prim("+", Prim("+", Var "x", Var "x"),
				  Var "x"),
                Letfun("fac", "x", 
                 If(Prim("=", Var "x", CstI 0),
                    CstI 1,
                    Prim("*", Var "x", 
                              Call(Var "fac", 
                                   Prim("-", Var "x", CstI 1)))),

                 Call(Var "f", Call(Var "fac", CstI 5000))));;

let ex400r = run ex400;;

(* ところで、計算結果はおかしくなる。これは、OCamlの整数が、有限長のた *)
(* め、あまりにも巨大な整数を計算しようとして、結果が狂ったためである。 *)
(* ここでは、計算時間だけを知りたいので、計算結果については気にしない *)
(* ことにする。*)

(* 演習問題 *)

(* 5a-1 ここまでの例題とおなじものを chap5.ml のeval でも実行しなさい。 *)

(* 5a-2 chap5.ml と chap5a.ml の eval で計算結果が異なるものはあっただ *)
(* ろうか、また、計算時間が(極端に)違うものがあっただろうか？ *)
(* これらをよく観察して(また、可能ならば eval のプログラムの中身を解読 *)
(* して、call by value (chap5.ml) と call by name (chap5a.ml) の違いを述べな *)
(* さい。*)

(* 5a-3 (発展課題) *)
(* ここでは call by value と call by name しかやらなかったが、もう1つ *)
(* call by need というのも、有力な計算戦略であり、Haskellというプログ *)
(* ラム言語で採用されている。上記の call by name の eval を改変して、 *)
(* call by need にすることができるだろうか？ なお、call by needは call *)
(* by name に似ているが、「引数を1度計算したら、その場所に計算結果を書 *)
(* きこみ、2回以上その引数を利用する場合は、前に計算した結果を利用する」 *)
(* ものである。*)
