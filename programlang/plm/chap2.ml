(* 
 * DO NOT DISTRIBUTE THIS CODE.
 * 
 * Code originally written in F# quoted from the book:
 *        Sestoft, Programming Language Concepts, Springer, 2012
 * Comments and exercises by Yukiyoshi Kameyama
 *)

(* ---- Chapter2 ---- *)

(* ---------------------------------------------------------------------- *)

(* 2.3.1節 変数束縛と静的スコープがある式 *)

(* 
   対象言語をさらに拡張する。
*)

type expr = 
  | CstI of int
  | Var of string
  | Let of string * expr * expr        (* let式を導入 *)
  | Prim of string * expr * expr;;

(* 以下は、拡張された expr型をもつ式の例 *)

let e1 = Let("z", CstI 17, Prim("+", Var "z", Var "z"));;

let e2 = Let("z", CstI 17, 
             Prim("+", Let("z", CstI 22, Prim("*", CstI 100, Var "z")),
                       Var "z"));;

let e3 = Let("z", Prim("-", CstI 5, CstI 4), 
             Prim("*", CstI 100, Var "z"));;

let e4 = Prim("+", Prim("+", CstI 20, Let("z", CstI 17, 
                                          Prim("+", Var "z", CstI 2))),
                   CstI 30);;

let e5 = Prim("*", CstI 2, Let("x", CstI 3, Prim("+", Var "x", CstI 4)));;

(* 演習問題2.3.1-a: 
   上記の式 e2 と e3を、OCaml の対応する式に直しなさい。
*)

(* 演習問題2.3.1-b: 
   逆に OCaml の以下の式は、expr型の要素として表すとどうなるか。
      let x = 10 in 
      let y = x + 20 in 
         (x + let x = 30 in x * y) 
*)

(* 上記env に対応した lookup関数は前と同じ *)

let rec lookup env x =
    match env with 
    | []        -> failwith (x ^ " not found")
    | (y, v)::r -> if x=y then v else lookup r x;;

(* 
   上記env に対応した eval関数では、Letの処理が増えている 
*)

let rec eval e (env : (string * int) list) : int =
    match e with
    | CstI i            -> i
    | Var x             -> lookup env x 
    | Let(x, erhs, ebody) -> 
      let xval = eval erhs env in
      let env1 = (x, xval) :: env in
      eval ebody env1
    | Prim("+", e1, e2) -> eval e1 env + eval e2 env
    | Prim("*", e1, e2) -> eval e1 env * eval e2 env
    | Prim("-", e1, e2) -> eval e1 env - eval e2 env
    | Prim _            -> failwith "unknown primitive";;

(* 
   式の評価をする際に、毎回、空の環境を与えるのは面倒なので、
   それをやってくれる関数 run を準備する 
*)

let run e = eval e [];;

(* 演習問題2.3.1-c: 
   e1,e2,e3,e4,e5 を run で評価してどのようになるかチェックしなさい。
*)

(* 演習問題2.3.1-d: 
   上記の演習問題 2.3.1-b の式を run して、スコープが入れ子になっ
   た場合の処理が、どのようにおこなわれるか、説明しなさい。 
*)

(* ---------------------------------------------------------------------- *)

(* 2.3.2節 閉じた式 (束縛されていない変数出現を1つも持たない式) *)

(* 準備として、x が リストvsに含まれるかどうか (true/false を返す)関数 *)
(* memを定義する。これは member を意味する。*)

let rec mem x vs = 
    match vs with
    | []      -> false
    | v :: vr -> x=v || mem x vr;;

(* 実行例 *)

let _ = mem "x"  ["y"; "z"; "w"]     ;;


let _ = mem "x"  ["y"; "x"; "w"]     ;;

(* 
   式e が、閉じているかどうか、true/false を返す関数 closedin
   ただし、それをする本物の関数は、後でのべるclosedin1 であり、ここで 
   は、その準備として、「リストvs にはいっている変数以外で、束縛されてい 
   ない変数出現をもたないかどうか」を調べる関数を closedin としている 
 *)

let rec closedin (e : expr) (vs : string list) : bool =
    match e with
    | CstI i -> true
    | Var x  -> List.exists (fun y -> x=y) vs
    | Let(x, erhs, ebody) -> 
      let vs1 = x :: vs in
      closedin erhs vs && closedin ebody vs1
    | Prim(ope, e1, e2) -> closedin e1 vs && closedin e2 vs;;

(* 実行例 *)

let _ = closedin (Prim("+", Var "y", Var "z")) ["x"; "z"] ;;

let _ = closedin (Prim("+", Var "y", Var "z")) ["y"; "z"] ;;

(* 上記のclosedin ができると、それを利用して、作りたかった関数を定義す *)
(* ることができる *)

let closed1 e = closedin e [];;

let _ = closed1 (Let("x", CstI 17, Var "y")) ;;

let _ = closed1 (Let("x", CstI 17, Var "x")) ;;

(* 演習問題2.3.2-a: 上記の e1,e2,e3,e4,e5 が閉じた式かどうか、確かめな *)
(* さい。*)

(* 演習問題2.3.2-b: 下記の式は閉じた式か *)
(* Let("x", CstI 17,  *)
(*     (Let "y", Var "x", Prim("+", Var "x", Var "y"))) *)

(* 演習問題2.3.2-c: 下記の式は閉じた式か *)
(* Let("x", CstI 17,  *)
(*     (Let "y", Var "y", Prim("+", Var "x", Var "y"))) *)

(* ---------------------------------------------------------------------- *)

(* 2.3.3節 式の自由変数 (式の中に、自由な出現をもつ変数) *)
(* 束縛されていない変数出現を、自由出現という。式eの自由変数とは、*)
(* e の中で、1つでも自由出現をもつ変数のことであり、一般にはたくさんあ *)
(* り得るので、集合であらわす。たとえば、OCaml の  *)
(*    let x = 1 in let y = x+y in z+y   *)
(* という式は、 y と z が自由出現をもつので(最後のy は束縛出現だが) *)
(* 自由変数の集合は {y,z} である *)


(* 2つのリストをそれぞれ集合とおもったときの、和集合(に対応するリスト) *)
(* を求める関数 *)

let rec union (xs, ys) = 
    match xs with 
    | []    -> ys
    | x::xr -> if mem x ys then union(xr, ys)
               else x :: union(xr, ys);;

(* 2つのリストをそれぞれ集合とおもったときの、差集合(に対応するリスト) *)
(* を求める関数 *)

let rec minus (xs, ys) = 
    match xs with 
    | []    -> []
    | x::xr -> if mem x ys then minus(xr, ys)
               else x :: minus (xr, ys);;

(* 式e の自由変数の集合をリストとして求める関数 *)

let rec freevars e : string list =
    match e with
    | CstI i -> []
    | Var x  -> [x]
    | Let(x, erhs, ebody) -> 
          union (freevars erhs, minus (freevars ebody, [x]))
    | Prim(ope, e1, e2) -> union (freevars e1, freevars e2);;

(* 閉じた式というのは、自由変数の集合が空集合であるもの、としても定義 *)
(* することができる。*)

let closed2 e = (freevars e = []);;

(* 演習問題2.3.3-a: 
   closed1 と closed2 が同じ結果を出すことを，いくつかの例題をいれて確
   認せよ。なお、「いつでも同じ」であることを示せる腕自慢の人は、その
   証明を書いてもよい。
*)

(* ---------------------------------------------------------------------- *)

(* 2.3.4節 変数に対する代入 *)
(* 注意; 代入(substituion)と置換え(repalcement) は違う!!! *)
(*     let x = 3 in x + y という OCaml式の y を x+3 に置換えたら *)
(* 単に let x = 3 in x + (x + 3) だが、    y に x+3 を代入したら *)
(*     let xx = 3 in xx + (x + 3) というように、束縛されたx が変更される。 *)


(* この節では、まず、置換えをおこなう関数 nsubstを定義し、そのあと、*)
(*                 代入をおこなう関数  subst を定義する *)


(* 最初の準備として、環境env の中に変数x がふくまれかどうかを探し、あれ *)
(* ばその値を返し、なければ、(例外ではなく) Var x を返す関数を定義する *)

let rec lookOrSelf env x =
    match env with 
    | []        -> Var x
    | (y, e)::r -> if x=y then e else lookOrSelf r x;;

let _ = lookOrSelf [("x", CstI 10); ("y", CstI 20)] "x" ;;

let _ = lookOrSelf [("x", CstI 10); ("y", CstI 20)] "z" ;;

(* 次の準備は、環境env から、変数x に対応する組を除去する関数である *)
(* ただし、x に対応する組がなければ、そのままenvを返す *)

let rec remove env x =
    match env with 
    | []        -> []
    | (y, e)::r -> if x=y then r else (y, e) :: remove r x;;

let _ = remove [("x",10); ("y",20)] "x" ;;

let _ = remove [("x",10); ("y",20)] "z" ;;

(* 置き換えをおこなう関数 nsubstを定義する。ここでは 「素朴な代入」を *)
(* 意味する naive substitution にちなんだ関数名になっているが、実際に *)
(* は代入ではなく置き換えである *)
(* 引数の e は、置き換え対象の式であり、env は、今までの環境と同じ名前 *)
(* をつかっているが、実は違っていて、「どの変数をどの式に置き換えるか」 *)
(* の対応付けをあらわす。つまりenv の型は (string*int) list ではなく、 *)
(* (string*expr) list である。その例はすぐ下にでてくる。*)

let rec nsubst (e : expr) (env : (string * expr) list) : expr =
    match e with
    | CstI i -> e
    | Var x  -> lookOrSelf env x
    | Let(x, erhs, ebody) ->
      let newenv = remove env x in
      Let(x, nsubst erhs env, nsubst ebody newenv)
    | Prim(ope, e1, e2) -> Prim(ope, nsubst e1 env, nsubst e2 env)

(* ここで、Let式の処理で remove を呼んで不思議なことをやっている理由は *)
(* 以下の式e7 に対する処理 e7s1 でわかるはずである *)

let e6 = Prim("+", Var "y", Var "z");;

let e6s1 = nsubst e6 [("z", CstI 17)];;

let e6s2 = nsubst e6 [("z", Prim("-", CstI 5, CstI 4))];;

let e6s3 = nsubst e6 [("z", Prim("+", Var "z", Var "z"))];;

let e7 = Prim("+", Let("z", CstI 22, Prim("*", CstI 5, Var "z")),
                   Var "z");;

(* 式e7 の中の z を CstI 100という定数式に置き換えるとどうなるか *)

let e7s1 = nsubst e7 [("z", CstI 100)];;

(* 式e8 も興味深い *)

let e8 = Let("z", Prim("*", CstI 22, Var "z"), Prim("*", CstI 5, Var "z"));;

let e8s1 = nsubst e8 [("z", CstI 100)];;

(* しかしながら、nsbust は、代入ではなく置き換えである。以下の例 e9s1で、そ *)
(* の違いがでてくる。 *)

let e9 = Let("z", CstI 22, Prim("*", Var "y", Var "z"));;

let e9s1 = nsubst e9 [("y", Var "z")];;

let e9s2 = nsubst e9 [("z", Prim("-", CstI 5, CstI 4))];;

(* e9s1 では、本来、Let("zz", CstI 22, Prim("*", Var "z", Var "zz")) *)
(* のように、変数z の衝突を避けてほしかったところであるが nsubst はそ *)
(* れをやってくれない。*)

(* そこで、本物の代入操作を定義しよう。準備として、「新しい変数名(とな *)
(* る文字列)を作る」関数 newVarを定義する *)

let newVar : string -> string = 
    let n = ref 0 in
    let varMaker x = (n := 1 + !n; x ^ string_of_int (!n)) in
    varMaker

(* 長い旅路を終えて、ようやく、本物の代入操作 substが定義できる *)

let rec subst (e : expr) (env : (string * expr) list) : expr =
    match e with
    | CstI i -> e
    | Var x  -> lookOrSelf env x
    | Let(x, erhs, ebody) ->
      let newx = newVar x in
      let newenv = (x, Var newx) :: remove env x in
      Let(newx, subst erhs env, subst ebody newenv)
    | Prim(ope, e1, e2) -> Prim(ope, subst e1 env, subst e2 env)

let e6s1a = subst e6 [("z", CstI 17)];;

let e6s2a = subst e6 [("z", Prim("-", CstI 5, CstI 4))];;

let e6s3a = subst e6 [("z", Prim("+", Var "z", Var "z"))];;

(* 以下の例が、nsubst と subst の違いを物語る *)

(* Shows renaming of bound variable z (to z1) *)
let e7s1a = subst e7 [("z", CstI 100)];;

(* Shows renaming of bound variable z (to z2) *)
let e8s1a = subst e8 [("z", CstI 100)];;

(* Shows renaming of bound variable z (to z3), avoiding capture of free z *)
let e9s1a = subst e9 [("y", Var "z")];;

(* 演習問題2.3.4-a: 上記の例を実行してみて、置き換えと代入の違いを自分 *)
(* の言葉で説明しなさい。*)

(* ---------------------------------------------------------------------- *)

(* 2.4節 変数名のかわりに、整数で変数を表す *)

(* 
   前節までのevalでは、環境env の中に変数xに対応する値がはいっている
   かどうかを知るために、頭から順番にリストの要素を全部調べていたため、
   「変数x の値が何かを知る」だけで、env の長さに比例する時間がかかっ
   ていた。これは遅い!

   そこで、コンパイラでは、変数x が環境の中の何番目に来るかをあらかじ 
   め計算しておいて、その数(インデックスとかオフセットという)で、置き
   かえておくことが普通である。これに加えて、もし、環境をあらわすデー
   タ構造をちょっと変更して、「そのN番目の要素」が「あっ」という間に
   取得できるなら、変数xの値を知るのも高速にできるようになるはずである。
   ここでは、後者はやっていないので、高速化の恩恵は実際に見ることは
   できないが、それでも、「コンパイラがやる高速化技術」の一例を感じら
   れるはずである。

   まず、expr の定義を変更して、変数名として文字列でなく整数を使ったも
   のにする。なお、先ほどと違う型にしたいので、texpr とか TCstI のよう
   に、頭にtやTをつけている。
*)

type texpr =                            
  | TCstI of int
  | TVar of int                         (* 整数を使った変数 *)
  | TLet of texpr * texpr               (* 変数名は使わないので、*)
		                        (* Let は2引数になる*)
  | TPrim of string * texpr * texpr;;


(* 変数のリストvsにおいて x が何番目にあるかを計算する *)

let rec getindex vs x = 
    match vs with 
    | []    -> failwith "Variable not found"
    | y::yr -> if x=y then 0 else 1 + getindex yr x;;


(* 変数名として文字列をつかった表現である  expr型から、*)
(* 変数名として整数  をつかった表現である texpr型への変換は、実は、*)
(* 簡単である。以下のものがそれである。*)

let rec tcomp (e : expr) (cenv : string list) : texpr =
    match e with
    | CstI i -> TCstI i
    | Var x  -> TVar (getindex cenv x)
    | Let(x, erhs, ebody) -> 
      let cenv1 = x :: cenv in
      TLet(tcomp erhs cenv, tcomp ebody cenv1)
    | Prim(ope, e1, e2) -> TPrim(ope, tcomp e1 cenv, tcomp e2 cenv);;

(* ここで cenv というのは、["x"; "y"; "z"] のような、変数名のリストで *)
(* あり、その式の処理時点で束縛されている変数たちを、「内側」から順番 *)
(* に並べたものである。*)

(* 実行例; なお tcompの第2引数cenvは、最初は空リスト [ ] とする *)

let te1 = tcomp e1 [] ;;

let te2 = tcomp e2 [] ;;

let te3 = tcomp e3 [] ;;

let te4 = tcomp e4 [] ;;

let te5 = tcomp e5 [] ;;

(* 演習問題2.4-a:  以下の OCaml式を expr型の式としてあらわした上で、 *)
(* tcomp でコンパイルしなさい。*)
(*    let x = 10 in (let y = x + 20 in x * y)   *)
(* この式における x+20 のx と x*y のx は、同じ x=10 で束縛されているx *)
(* であるが、tcomp で処理したあとは、異なるインデックスに変換されるこ *)
(* とを確認しなさい。これは何故か？ *)

(* 演習問題2.4-b:  ここで計算している「インデックス」は、ラムダ計算で *)
(* は、de Bruijn index と呼ばれているものである。これについて、どうし *)
(* てそのように呼ばれているか、どのように使われているか、文献にあたっ *)
(* て調べなさい。*)

(* 最後に、(eval型ではなく) teval型の式を評価するeval を定義する。*)

let rec teval (e : texpr) (renv : int list) : int =
    match e with
    | TCstI i -> i
    | TVar n  -> List.nth renv n    (* List.nth は n番目を取る関数 *)
    | TLet(erhs, ebody) -> 
      let xval = teval erhs renv in
      let renv1 = xval :: renv in
      teval ebody renv1 
    | TPrim("+", e1, e2) -> teval e1 renv + teval e2 renv
    | TPrim("*", e1, e2) -> teval e1 renv * teval e2 renv
    | TPrim("-", e1, e2) -> teval e1 renv - teval e2 renv
    | TPrim _            -> failwith "unknown primitive";;

(* 実行例 *)

let _ = teval te1 [] ;;

(* 演習問題2.4-c:  上記で作成した te2, ..., te5 の式を teval で評価し *)
(* て結果が正しく出るか確認しなさい。*)

(* 演習問題2.4-d:  「式eをevalで直接評価したもの」と「式e を tcomp でコンパ *)
(* イルしてからtevalで評価したもの」はいつでも一致するはずである。*)
(* つまり、   eval e []   と  teval (tcomp e []) []  はどんなe に対し *)
(* ても一致するはずである。これを、いろいろな例で確かめなさい。*)


(* ---------------------------------------------------------------------- *)

(* 2.5節 スタック機械 *)
(* 非常にシンプルなスタック機械をもちいて、コンパイルということがどう *)
(* いうことかもう少し見てみよう *)

(* ここでのスタック機械のプログラムは、命令の列である。1つ1つの命令は *)
(* 以下の型の要素である *)

type rinstr =
  | RCstI of int     (* 定数をスタックに積む命令 *)
  | RAdd             (* 足し算をやる命令 *)
  | RSub             (* 引き算をやる命令 *)
  | RMul             (* かけ算をやる命令 *)
  | RDup             (* スタックのトップを複製する命令 *)
  | RSwap            (* スタックのトップから2個をいれかえる命令 *)
  ;;

(* このスタック機械を実行する評価器は、reval のように書ける。結構複雑 *)
(* に見えるかもしれないが、授業で説明した通り、動作は非常に単純である。*)

let rec reval (inss : rinstr list) (stack : int list) : int =
    match (inss, stack) with 
    | ([], v :: _) -> v
    | ([], [])     -> failwith "reval: no result on stack!"
    | (RCstI i :: insr,             stk)  -> reval insr (i::stk)
    | (RAdd    :: insr, i2 :: i1 :: stkr) -> reval insr ((i1+i2)::stkr)
    | (RSub    :: insr, i2 :: i1 :: stkr) -> reval insr ((i1-i2)::stkr)
    | (RMul    :: insr, i2 :: i1 :: stkr) -> reval insr ((i1*i2)::stkr)
    | (RDup    :: insr,       i1 :: stkr) -> reval insr (i1 :: i1 :: stkr)
    | (RSwap   :: insr, i2 :: i1 :: stkr) -> reval insr (i1 :: i2 :: stkr)
    | _ -> failwith "reval: too few operands on stack";;

(* ここで第1引数が、命令列、つまり rinstr 型の命令のリストであり、第2 *)
(* 引数はスタックをOCamlリストであらわしたものである。ただし、スタック *)
(* の要素は整数だけである。最初はスタックは空なので、[] となる *)

(* スタック機械の命令列は、逆ポーランド記法、あるいは後置記法のプログ *)
(* ラムでもある。逆ポーランド記法は、ポーランドの航空会社の専売特許で *)
(* はなく、広く使われているものである。*)

(* 実行例 *)

let rpn1 = reval [RCstI 10; RCstI 17; RDup; RMul; RAdd] [] ;;

(* 演習問題2.5-a:  1+(2+(3+(4+5))) および (((1+2)+3)+4)+5 に相当する計 *)
(* 算をスタック機械の命令列として表現し、reval で実行してみなさい。*)

(* expr型の式を、スタック機械の命令列にコンパイルするコンパイラ(変換器)*)
(* ただし、この範囲では、変数や変数束縛には対応できないので、その場合 *)
(* は例外を発生させている *)

let rec rcomp (e : expr) : rinstr list =
    match e with
    | CstI i            -> [RCstI i]
    | Var _             -> failwith "rcomp cannot compile Var"
    | Let _             -> failwith "rcomp cannot compile Let"
    | Prim("+", e1, e2) -> rcomp e1 @ rcomp e2 @ [RAdd]
    | Prim("*", e1, e2) -> rcomp e1 @ rcomp e2 @ [RMul]
    | Prim("-", e1, e2) -> rcomp e1 @ rcomp e2 @ [RSub]
    | Prim _            -> failwith "unknown primitive";;
            
(* @ は OCamlリストを連結する関数である。*)
(* 例: [1;2] @ [3;4;5] = [1;2;3;4;5] *)

(* 演習問題2.5-b: 以下のexpr型の式をrcomp で変換し、さらにrevalで実行 *)
(* せよ *)
(*    Prim("+", Prim("*", CstI 10, CstI 20), CstI 30)   *)
(*    Prim("*", CstI 10, Prim("+", CstI 20, CstI 30))   *)

(* さて、変数や変数束縛をもつ式を、スタック機械で解釈するために、シン *)
(* プルなスタック機械をちょっと拡張する。拡張命令を sinstrという型であ *)
(* らわすと以下のようになる *)

type sinstr =
  | SCstI of int                        (* 整数を積む             *)
  | SVar of int                         (* 変数の値をとってきて積む *)
  | SAdd                                (* 足し算     *)
  | SSub                                (* 引き算     *)
  | SMul                                (* かけ算     *)
  | SPop                                (* スタックトップを捨てる   *)
  | SSwap;;                             (* トップ2個をいれかえる   *)
 
(* 拡張スタック機械の評価器 *)

let rec seval (inss : sinstr list) (stack : int list) =
    match (inss, stack) with
    | ([], v :: _) -> v
    | ([], [])     -> failwith "seval: no result on stack"
    | (SCstI i :: insr,          stk) -> seval insr (i :: stk) 
    | (SVar i  :: insr,          stk) -> seval insr (List.nth stk i :: stk) 
    | (SAdd    :: insr, i2::i1::stkr) -> seval insr (i1+i2 :: stkr)
    | (SSub    :: insr, i2::i1::stkr) -> seval insr (i1-i2 :: stkr)
    | (SMul    :: insr, i2::i1::stkr) -> seval insr (i1*i2 :: stkr)
    | (SPop    :: insr,    _ :: stkr) -> seval insr stkr
    | (SSwap   :: insr, i2::i1::stkr) -> seval insr (i1::i2::stkr)
    | _ -> failwith "seval: too few operands on stack";;


(* 拡張スタック機械へのコンパイルの際に、「その時点でスタックにつまれ *)
(* ているもの」が計算途中の値か、それとも、束縛変数の値(環境)であるか *)
(* を見分ける必要があり、それを区別するため、2種類のものからなる型 *)
(* stackvalue を定義する。*)

type stackvalue =
  | Value                               (* A computed value *)
  | Bound of string;;                   (* A bound variable *)

(* expr型の式全部を、拡張スタック機械の命令列に変換することができる。 *)
(* そのコンパイラを scomp とする *)

let rec scomp (e : expr) (cenv : stackvalue list) : sinstr list =
    match e with
    | CstI i -> [SCstI i]
    | Var x  -> [SVar (getindex cenv (Bound x))]
    | Let(x, erhs, ebody) -> 
          scomp erhs cenv @ scomp ebody (Bound x :: cenv) @ [SSwap; SPop]
    | Prim("+", e1, e2) -> 
          scomp e1 cenv @ scomp e2 (Value :: cenv) @ [SAdd] 
    | Prim("-", e1, e2) -> 
          scomp e1 cenv @ scomp e2 (Value :: cenv) @ [SSub] 
    | Prim("*", e1, e2) -> 
          scomp e1 cenv @ scomp e2 (Value :: cenv) @ [SMul] 
    | Prim _ -> failwith "scomp: unknown operator";;

(* 実行例 *)

let s1 = scomp e1 [];;
let s2 = scomp e2 [];;
let s3 = scomp e3 [];;
let s5 = scomp e5 [];;

(* 演習問題2.5-c: 上記の評価を見て、scomp における stackvalue型の値が *)
(* どう使われているか説明せよ。*)

let intsToFile (inss : int list) (fname : string) = 
    let text = String.concat " " (List.map string_of_int inss) in
    let outch = open_out fname in
    output_string outch text;
    close_out outch;;

(* -----------------------------------------------------------------  *)


