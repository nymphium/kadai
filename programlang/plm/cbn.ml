(* 
 * Written by Yukiyoshi Kameyama
 *)

(* ファイル chap4.ml では，
   一階の関数型プログラム言語 を使って，
       static (lexical) binding と dynamic binding
   の2つのインタープリタについて学習した．ここでは，
   もう1つの分類である
       call by value (値呼び)と call by name (名前呼び)
   の処理方式について学習する．
   正確にいうと，
   これら2つの分類の組合せは合計 4通りあるはずであるが，
     chap4.ml では static/dynamic binding で call by value 
   の評価器を扱い，
     ここでは static binding で call by name
   の評価器を扱う．(つまり，dynamic binding で call by name
   の評価器は扱わない．)
 *)


(* 値呼びと名前呼びの違いについて

   値呼びは，関数呼び出し f e において，e を計算して
   値にしてから関数fに に引き渡す．

   名前呼びは，関数呼び出し f e において，e を計算せずに
   e のまま，関数fに に引き渡す．

   例:
     let f x = x + x in
       f (2 * 3)
     という式の評価で，値呼びであれば，
       (1) 2*3 を計算して 6 を得る．
       (2) x=6 という束縛をつくって関数fの本体 x+xを計算する．
       (3) 6+6 となって 12 という答を得る．
     という手順であるが，名前呼びであれば，
       (1) x=2*3 という束縛をつくって関数fの本体 x+xを計算する．
       (1) (2*3)+(2*3) という計算をして，12 という答を得る．
     という手順となる．

   結局同じ答えが返ってきたのであるが，2*3 の計算が，値呼びで
   あれば 1回だけおこなわれ，名前呼びであれば 2回行われた点が
   違う．これは，プリント式をつかうと，もっとはっきりして，
     let f x = x + x in
       f (print_string "abc"; 2 * 3)
   という式であれば，値呼びなら abc を1度だけプリントし，
   名前呼びなら abc を2度プリントする．(OCaml は値呼びなので
   1度だけプリントする．)
 *)

(* 対象言語の構文は chap4.ml とおなじである．*)
type expr = 
  | CstI of int
  | CstB of bool
  | Var of string
  | Let of string * expr * expr
  | Prim of string * expr * expr
  | If of expr * expr * expr
  | Letfun of string * string * expr * expr    (* (f, x, fBody, letBody) *)
  | Call of expr * expr

(* 処理方式: ここでは，静的束縛で考えるので，
    値呼び方式の評価器は，chap4.ml に記載した通りである．

    名前呼び方式の評価器をつくるためには，
      上記の 2*3 のように「評価しないまま，変数x に束縛
      される式」を作り，それを束縛する．

      2*3 の値が必要になったら，「評価しないまま，変数x
      に束縛される式」を計算しにいく．
    という仕組みがあればよい．
    要するに計算を「中断(suspend)」する仕組みと，中断し
    ている計算を再開する仕組みがあればよい．
   
    このためには，「値」の世界をすこし拡張して，Susp
    をいれよう．
 *)
  
type value = 
  | Int of int
  | Closure of string * string * expr * value env       
  | Susp of expr * value env
 and 'v env = (string * 'v) list

(* 
   Susp(expr1, env1) は，環境env1 における式expr1 の計算
   を「中断」した値である．ここで大事なことは，exprt1 だけ
   でなく 環境 expr1 も保存しておくことである．これをやら
   ないと，計算を再開するときの環境がおかしくなる可能性が
   ある．
 *)

(* 
   lookup関数の定義は前と同じである。
 *)

let rec lookup env x =
    match env with 
    | []        -> failwith (x ^ " not found")
    | (y, v)::r -> if x=y then v else lookup r x;;

(* ---------------------------------------------------------------------- *)

(* 評価器は，Susp  だけ変更すればよい． 
   変更点には印をつけてある．
 *)

let rec eval (e : expr) (env : value env) : int =
    match e with 
    | CstI i -> i
    | CstB b -> if b then 1 else 0
    | Var x  ->
       begin
	 match lookup env x with
	 | Int i -> i 
	 | Susp(e1,env1) -> eval e1 env1  (* 変更点1 *)
	 | _     -> failwith "eval Var"
       end
    | Prim(ope, e1, e2) -> 
      let i1 = eval e1 env in
      let i2 = eval e2 env in
      begin
	match ope with
	| "*" -> i1 * i2
	| "+" -> i1 + i2
	| "-" -> i1 - i2
	| "=" -> if i1 = i2 then 1 else 0
	| "<" -> if i1 < i2 then 1 else 0
	| _   -> failwith ("unknown primitive " ^ ope)
      end
    | Let(x, eRhs, letBody) -> 
      let xVal = Int(eval eRhs env) in
      let bodyEnv = (x, xVal) :: env in
      eval letBody bodyEnv
    | If(e1, e2, e3) -> 
      let b = eval e1 env in
      if b<>0 then eval e2 env
      else eval e3 env
    | Letfun(f, x, fBody, letBody) -> 
      let bodyEnv = (f, Closure(f, x, fBody, env)) :: env in
      eval letBody bodyEnv
    | Call(Var f, eArg) -> 
      let fClosure = lookup env f in
      begin
	match fClosure with
	| Closure (f, x, fBody, fDeclEnv) ->
           (* let xVal = Int(eval eArg env) in *)
           let xVal = Susp(eArg,env) in  (* 変更点2 *)
           let fBodyEnv = (x, xVal) :: (f, fClosure) :: fDeclEnv in
           eval fBody fBodyEnv
	| _ -> failwith "eval Call: not a function"
      end
    | Call _ -> failwith "eval Call: not first-order function"

(*
  変更点は，以下の2つだけである．変更点2から説明する．

  変更点2: 関数呼び出しで，引数の値を計算せず，
           Susp(eArg,env) の形で，環境に格納している．

  変更点1: 変数の値をlookup したあと，Susp(e1,env1) の
           形のときは，それをそのまま返さず，e1 を
           env1 のもとで計算した結果を返す．
*)

(* 
   評価器evalを外から呼ぶときの関数 runは前と同じである。
*)

let run e = eval e [];;

(* 以下のものは，値呼びと名前呼びで，ふるまいが同じである．*)

let ex1 = Letfun("f1", "x", Prim("+", Var "x", CstI 1), 
                 Call(Var "f1", CstI 12));;

let ex1_run = run ex1 ;;

(* 以下のものは，値呼びと名前呼びで，ふるまいが異なる．*)

let ex2 = Letfun("loop", "x", Call(Var "loop", Var "x"),
                 Letfun("f", "y", CstI 10, 
                   Call(Var "f", Call(Var "loop", CstI 0))))

let ex2_run = run ex2 ;;
        

(* 演習問題4.extra-1: 
   上記の ex1, ex2 を実行して，値呼びと名前呼びの違いを
   説明せよ．
*)

(* 演習問題4.extra-2 (optional): 
   値呼びと名前呼びのどちらが優れているとお
   を定義し、eval で評価しなさい。 
*)

(* 演習問題4.extra-2 (optional): 
   call by need (必要呼び) について調べなさい．
   また，腕自慢の人は，上記の評価器を変更して必要呼びに
   変更しなさい．(ただし，OCaml の参照 (ref) を使う必要が
   ある．OCamlの ref は，C言語やJava言語の変数と同様に，
   値を書き換えることのできる「箱(記憶領域)」であり，
   たとえば，
       let r = ref 10 in
         r := (!r) + 20;
         r := (!r) * 3;
         !r
   のように使えるものである．詳細は各自で調べられたい．)
*)

(* type 'v refenv = (string * 'v ref) list;; *)
type value' = 
  | Int of int
  | Closure of string * string * expr * value' env'
  | Susp of expr * value' env'
 and 'v env' = (string * 'v ref) list

let rec eval' (e : expr) (env : value' env') : int =
    match e with 
    | CstI i -> i
    | CstB b -> if b then 1 else 0
    | Var x  ->
        begin
            match lookup env x with
            | {contents = Int i}          -> i 
            | {contents = Susp(e1, env1)} -> update x env e1 env1
            | _                           -> failwith "eval' Var"
        end
     | Prim(ope, e1, e2) -> 
        let i1 = eval' e1 env in
        let i2 = eval' e2 env in
        begin
            match ope with
            | "*" -> i1 * i2
            | "+" -> i1 + i2
            | "-" -> i1 - i2
            | "=" -> if i1 = i2 then 1 else 0
            | "<" -> if i1 < i2 then 1 else 0
            | _   -> failwith ("unknown primitive " ^ ope)
        end
    | Let(x, eRhs, letBody) ->
        let xVal = Int(eval' eRhs env) in
        let bodyEnv = (x, ref xVal) :: env in
        eval' letBody bodyEnv
    | If(e1, e2, e3) -> 
        let b = eval' e1 env in
        if b<>0 then eval' e2 env
        else eval' e3 env
    | Letfun(f, x, fBody, letBody) -> 
        let bodyEnv = (f, ref (Closure(f, x, fBody, env))) :: env in
        eval' letBody bodyEnv
    | Call(Var f, eArg) -> 
        let fClosure = lookup env f in
        begin
            match fClosure with
            | {contents = Closure (f, x, fBody, fDeclEnv)} ->
                    let xVal = ref (Susp(eArg,env)) in
                    let fBodyEnv  = (x, xVal) :: (f, fClosure) :: fDeclEnv in
                    eval' fBody fBodyEnv
            | _ -> failwith "eval' Call: not a function"
        end
    | Call _ -> failwith "eval' Call: not first-order function"
and update ks xs e1 env1 =
    match xs with
    | h::xsl ->
       let (k, v) = h in
       if k = ks then
           let e = eval' e1 env1 in
           let () = v := (Int e) in
           e
       else update ks xsl e1 env1
    | _ -> failwith "env Var"

