import java.io.*;

public class SelectionSort{
	public static int selectMax(int i, int[] a){
		int max = 0;
		for(int j = 0; j < i; j++){
			if(a[j] > a[max]){
				max = j;
			}
		}
	return max;
	}

	public static void swap(int[] a, int pos1, int pos2){
		int tmp = a[pos1];
		a[pos1] = a[pos2];
		a[pos2] = tmp;
	}

	public static void main(String[] args)throws IOException{
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		int ar[] = new int[5];
		System.out.println("Input " + ar.length + " data");
		for(int n = 0; n < ar.length; n++){
			String line = reader.readLine();
			ar[n] = Integer.parseInt(line);
		}

		for(int m = ar.length; m > 0; m--){
			swap(ar, selectMax(m, ar), m - 1);
		}

		System.out.println("Result:");
		for(int o = 0; o < ar.length; o++){
			System.out.println(ar[o]);
		}
	}
}
