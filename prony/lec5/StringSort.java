import java.io.*;

public class StringSort{
	static int data = 0;

	public static int findPosAndShift(String[] a, int p, String key){
		int j = p - 1;
		while(p > 0 && a[p - 1].compareTo(key.toLowerCase()) > 0){
			p--;
		}
		while(j >= p){
			a[j + 1] = a[j];
			j--;
		}
		return p;
	}

	public static void insertSort(String[] a){
			for(int i = 1; i < data; i++){
			String d = a[i];
			int p = findPosAndShift(a, i, d);
			a[p] = d;
		}
	}

	public static void main(String[] args)throws IOException{
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		String ar[] = new String[1000];
		while((ar[data] = reader.readLine()) != null){
			data++;
		}

		insertSort(ar);

		for(int o = 0; o < data; o++){
			System.out.println(ar[o]);
		}
	}
}
