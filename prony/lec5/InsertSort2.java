import java.util.Random;

public class InsertSort2{
	public static int findPosAndShift(int[] a, int p, int key){
		int j = p - 1;
		while(p > 0 && a[p - 1] > key){
				p--;
		}
		while(j >= p){
			a[j + 1] = a[j];
			j--;
		}
		return p;
	}

	public static void insertSort(int[] a){
		for(int i = 1; i < a.length; i++){
			int d = a[i];
			int p = findPosAndShift(a, i, d);
			a[p] = d;
		}
	}

	public static void main(String[] args){
		int ar[] = new int[100];

		for(int i = 0; i < 100; i++){
			Random rnd = new Random();

			ar[i] = rnd.nextInt(500);
		}

		insertSort(ar);

		System.out.println("Result:");
		for(int o: ar){
			System.out.println(o + "");
		}
	}
}
