import java.io.*;

public class saf2 {
	public static void main(String[] args)throws IOException {
		if(args.length != 1) {
			System.out.println("byoa : java ShowAllFiles hoge ");
			System.out.println("nuo : java ShowAllFiles doc");
			System.exit(0);
		}
		String dirname = args[0];
		File dir = new File(dirname);
		String[] dirlist = dir.list();
		for(int i = 0; i < dirlist.length; i++) {
			File files = new File(dirname, dirlist[i]);
			System.out.println("File: " + dirname + "/" + dirlist[i]);
			BufferedReader reader = new BufferedReader(new FileReader(files));
			String line;
			while((line = reader.readLine()) != null) {
				System.out.println(line);
			}
			System.out.println();
			reader.close();
		}
	}
}
