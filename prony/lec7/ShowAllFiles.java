import java.io.*;

public class ShowAllFiles{
	public static String fileToString(File file)throws IOException{
		BufferedReader br = null;
		try{
			br = new BufferedReader(new InputStreamReader(new FileInputStream(file)));
			StringBuffer sb = new StringBuffer();
			int c;
			while((c = br.read()) != -1){
				sb.append((char)c);
			}
			return sb.toString();
		}finally{
			br.close();
		}
	}

	public static void main(String[] args)throws IOException{
		if(args.length == 0){
			System.exit(1);
		}
		File dir = new File(args[0]);
		
		File[] files1 = dir.listFiles();
		for(int i = 0; i < files1.length; i++){
			File file = files1[i];
			if(files1[i].isFile()){
				System.out.println("File:" + file);
				String filein = fileToString(file);
				System.out.println(filein);
			}else if( files1[i].isDirectory()){
				System.out.println("File:" + file);
			}
		}
	}
}
