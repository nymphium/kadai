public class Parabola{
	public static void main(String[] args){
		for(int i = -8; i < 8; i++){
			int l = square(i);
			printGraph(l);
		}
	}
	public static int square(int sq){
		return (sq) * (sq);
	}
	public static void printGraph(int x){
		for(int k = 0; k < x; k++){
			System.out.print("*");
		}
		System.out.println("");
	}
}
