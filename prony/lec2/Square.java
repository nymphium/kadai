public class Square{
	public static void main(String[] args){
		for(int n=1;n<12;n=n+2){
			System.out.println("Squared "+n+" is "+sq(n)+".");
		}
	}
	public static int sq(int l){
		return l*l;
	}
}
