import java.io.*;
public class FibLoop{
	public static void main(String args[])throws IOException{
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		String line = reader.readLine();
		int n=new Integer(line);
		long a=0;
		long b=1;
		for(int i=0;i<n;i++){
			long tmp=b;
			b+=a;
			a = tmp;
		}
		System.out.println(a);
	}
}
//aho
