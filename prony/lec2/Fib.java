public class Fib{
	public static int f(int x){
		return x==0?0:x==1?1:f(x-1)+f(x-2);
		}
	public static void main(String args[]){
		int n=Integer.parseInt(args[0]);
			System.out.println(n<0?"Input positive integer.":args[0]+"th Fibonacci number is "+f(n));
	}
}
