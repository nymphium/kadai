public class Binary{
	public static void main(String[] args){
		int n=Integer.parseInt(args[0]);
		String bi="";
		for(int x=0;x<32;x++){
			int l=(int)Math.pow(2,(31-x));
			if(n>=l){
				bi=bi+"1";
				n-=l;
			}else{
				bi+="0";
			}
		}
		System.out.println("0b"+bi);
	}
}
