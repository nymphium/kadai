public class Ack{
	public static int a(int x,int y){
		return x==0?y+1:y==0?a(x-1,1):a(x-1,a(x,y-1));
	}
	public static void main(String[] args){
		int a1,a2=Integer.parseInt(args[0]),Integer.parseInt(args[1]);
		System.out.println(a1==0&&a2==0?"Input x,y(active integer)":(a(a1,a2)));
	}
}
