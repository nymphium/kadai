puts "Input String1"

line1 = gets.chomp

puts "Input String2"

line2 = gets.chomp

text = line1.length < line2.length ? line2 : line1

pat = line2.length < line1.length ? line2 : line1

0.upto(text.length){|i|
	if text[i..i + pat.length - 1] == pat
		puts "Found #{pat}"

		exit
	end
}

puts "Not Found"
