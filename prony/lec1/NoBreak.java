import java.io.*;

public class NoBreak{
	public static void main(String[] args)throws IOException{
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Input String");
		String line = reader.readLine();
		boolean found = false;
		for(int n = 0; n < line.length(); n++){
				found = line.charAt(n) =='X' ? true : false;
		}
	String str = "Found \"X\"";
	System.out.println(found ? str : "NOT " + str);
	}
}

