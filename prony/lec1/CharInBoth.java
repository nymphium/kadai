import java.io.*;

public class CharInBoth{
	public static void main(String[] args)throws IOException{
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Input String1");
		String line1 = reader.readLine();
		System.out.println("Input String2");
		String line2 = reader.readLine();
		String lng;
		String shrt;
		if(line1.length() <= line2.length()){
			lng = line2;
			shrt = line1;
		}else{
			lng = line1;
			shrt = line2;
		}
		for(int n = 0; n < lng.length(); n++){
			for(int m = 0; m < shrt.length(); m++){
				if(lng.charAt(n) == shrt.charAt(m)){
					System.out.println("Found " + shrt.charAt(m));
					System.exit(0);
					// break;
				}
			}
		}
		System.out.println("Not Found");
	}
}
