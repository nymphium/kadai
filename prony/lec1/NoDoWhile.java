import java.io.*;

public class NoDoWhile{
	public	static void main(String[] args){
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		try{
			String line;
			for(;;){
				line = reader.readLine();
				if(!line.equals("bye")){
					System.out.println("You said:" + line);
				}else{
					break;
				}
			}
		}catch(IOException e){
			System.out.println(e);
		}
	}
}
