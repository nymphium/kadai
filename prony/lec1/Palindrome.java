public class Palindrome{
	public static void main(String[] args){
		boolean plndrm = true;
		int L = args[0].length();
		for(int n = 0; n < L / 2; n++){
			if(args[0].charAt(n) != args[0].charAt(L - 1 - n)){
				plndrm = false;
			}
		}
		System.out.println(
			plndrm ? "it\'s palindrome"
			: "it isn\'t palindrome"
		);
	}
}
