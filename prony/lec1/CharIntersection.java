import java.io.*;

public class CharIntersection{
	public static void main(String[] args){
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		try{
			System.out.println("Input String1");
			
			String line1 = reader.readLine();
			System.out.println("Input String2");
			String line2 = reader.readLine();
			String lng;
			String shrt;
			String str = "";
			boolean match = false;
			if(line1.length() <= line2.length()){
				lng = line2;
				shrt = line1;
			}else{
				lng = line1;
				shrt = line2;
			}
			for(int n = 0; n < lng.length(); n++){
				for(int m = 0; m < shrt.length(); m++){
					if(lng.charAt(n) == shrt.charAt(m)){
						for(int l = 0; l < str.length(); l++){
							if(str.charAt(l) == shrt.charAt(m)){
								str = str + lng.charAt(m);
								match = true;
							}
						}
					}
				}
			}
			System.out.println(match ? "Found" + str : "Not Found");
		}catch(IOException e){
			System.out.println(e);
		}
	}
}
