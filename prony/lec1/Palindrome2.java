public class Palindrome2{
	public static void main(String[] args){
		StringBuffer m = new StringBuffer(args[0]);
		String m2 = m.toString();
		StringBuffer n = m.reverse();
		String n2 = n.toString();
		System.out.println(
			n2.equals(m2) ? "it\'s palindrome"
			: "it isn\'t palindrome"
		);
	}
}
