import java.io.*;

public class Continue{
	public static void main(String[] args){
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		try{
			String line = "";
			String command;
			while((command = reader.readLine()) != null){
				if (!command.startsWith("#")){
					String operand = reader.readLine();
					if(command.equals("ADD")){
						line = line + operand;
					}else if(command.equals("DEL")){
						int n = Integer.parseInt(operand);
						line = line.substring(n);
					}
				}
				System.out.println(line);
				}
			}catch(IOException e){
		}
	}
}
