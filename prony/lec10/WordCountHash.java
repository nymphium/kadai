import java.io.*;

class WordCountHash{

	final static int SzBkt = 100000;

	static class ListString{
		String name;
		ListString next;

		ListString(String name, ListString tail){
			this.name = new String(name);
			this.next = tail;
		}

		ListString(){
			this.name = null;
			this.next = null;
		}

		static int Length(ListString list){
			int countlength = 1;
			while(list != null){
				list = list.next;
				countlength++;
			}
			return countlength;
		}
	}

	static int HashFunction(String s){
		int result = (int)s.charAt(0) - (int)'a';
		return result % SzBkt;
	}

	static void SearchMethod(ListString list, String str){
		while(list != null){
			if(list.name.equals(str)){
				System.out.println("Found :" + str);
				break;
			}
			list = list.next;
		}
	}

	static boolean HaveString(String value){
		return (value == null || value.length() == 0) ? false : true;
	}

	public static void main(String[] args)throws IOException{
		long start = System.currentTimeMillis();

		ListString[] words = new ListString[SzBkt];

		for(int i = 0; i < SzBkt; i++){
			words[i] = null;
		}

		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		String line;
		int count = 0;
		while((line = reader.readLine()) != null && count < SzBkt){
			String line_splited[] = line.split(" ");
			for(int n = 0; n < line_splited.length; n++){
				if(line_splited[n].length() > 0){

					int insert_hash = HashFunction(line_splited[n]);
					int circle = 0;
					do{
						if(words[insert_hash].havestr > 0){
							if(words[insert_hash].name.equals(line_splited[n])){
								words[insert_hash] = new ListString(line_splited[n], words[insert_hash], 1);
							}
						}else{
							words[insert_hash] = new ListString(line_splited[n], words[insert_hash], 1);
						}
						insert_hash = (insert_hash + 1) % SzBkt;
						circle++;

					}while(circle < SzBkt);
				}
			}
		}

		for(int l = 0; l < SzBkt; l++){
			if(words[l].name == null){
				continue;
			}
			System.out.println(ListString.Length(words[l]) + " [====] " + words[l].name);
		}

		long end = System.currentTimeMillis();
		long tm = end - start;
		System.out.println("Exec Time: " + tm + "ms");
	}
}
