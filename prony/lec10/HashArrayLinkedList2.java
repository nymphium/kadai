import java.io.*;

class HashArrayLinkedList {
    static final int SzBkt = 5;
    static String [] persons = {"abe",        // 0 
			     "amagasa",    // 0
			     "sannomiya",  // 18
			     "sakuma",     // 18
			     "maeda",      // 12
			     "sato",       // 18
			     "tadano",     // 19
			     "sano",       // 18
			     "sugiki",     // 18
			     "oka"};       // 14

    static ListString [] HashTable = new ListString[SzBkt];

    static int String2Integer(String s) {
	int result = 0;
	
	result = (int)s.charAt(0);

	return result;
    }
    
    static int HashFunction(int l) {
	return l % SzBkt;
    }

    static void Display() {
	for (int i = 0; i < SzBkt; i++) {
	    ListString.Display(HashTable[i]);
	}
    }

    static int Count(String s, ListString l) {
	int count = 0;

	while (l != null) {
	    if (l.name.equals(s) == true)
		count++;
	    l = l.next;
	}

	return count;
    }

    static void Insert(String s) {
	int s2int = String2Integer(s);
	int pos = HashFunction(s2int);
	HashTable[pos] = ListString.Insert(s, HashTable[pos]);
    }

    static void Delete(String s) {
	int s2int = String2Integer(s);
	int pos = HashFunction(s2int);
	HashTable[pos] = ListString.Delete(s, HashTable[pos]);
    }

    static void Search(String s) {
	boolean found;
	int s2int = String2Integer(s);
	int pos = HashFunction(s2int);
	found = ListString.Search(s, HashTable[pos]);
	if (found == true)
	    System.out.println("Found: " + s);
	else
	    System.out.println("Not found: " + s);
    }

    static void Init() {
	for (int i = 0; i < SzBkt; i++) {
	    HashTable[i] = null;
	}

	// Constructing a hash structure
	for (int i = 0; i < persons.length; i++) {
	    Insert(persons[i]);
	}
    }

    public static void main (String[] args) {
	Init();

	BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
	try {
	    String line;
	    do {
		Display();
		System.out.print("> ");
		line = reader.readLine();
		if (line.equals("insert")) {
		    System.out.print("[insert] data ? ");
		    String data = reader.readLine();
		    System.out.println(data);
		    Insert(data);
		}
		else if (line.equals("delete")) {
		    System.out.print("[delete] data ? ");
		    String data = reader.readLine();
		    System.out.println(data);
		    Delete(data);
		}
		else if (line.equals("search")) {
		    System.out.print("[search] data ? ");
		    String data = reader.readLine();
		    System.out.println(data);
		    Search(data);
		}

	    } while (line.equals("exit") == false);
	} catch (IOException e) {
	}
    }
}
