import java.io.*;
import java.util.ArrayList;

class WordCountList{

	static class ListString{
		String name;
		ListString next;

		ListString(String name, ListString tail){
			this.name = new String(name);
			this.next = tail;
		}

		static int Length(ListString list){
			int countlength = 1;
			while(list != null){
				list = list.next;
				countlength++;
			}
			return countlength;
		}
	}

	public static void main(String[] args)throws IOException{
		long start = System.currentTimeMillis();
		int listnum = 0;

		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		String line = "";
		ListString words = new ListString(line, null);
		while((line = reader.readLine()) != null){
			String line_splited[] = line.split(" ");
			for(int n = 0; n < line_splited.length; n++){
				if(line_splited[n].length() > 0) words = new ListString(line_splited[n], words);
			}
		}

		while(words != null){
			int strcount = 0;
			int listcount = 0;

			String str = words.name;
			String[] storename = new String[ListString.Length(words)];

			while(words != null){
				if(!str.equals(words.name)){
					storename[listcount] = words.name;
					listcount++;
				}else{strcount++;}

				words = words.next;
			}

			for(int n = listcount - 1; n > -1; n--){
				words = new ListString(storename[n], words);
			}
			System.out.println(strcount + " [====] " + str);
		}

		long end = System.currentTimeMillis();
		long tm = end - start;
		System.out.println("Exec Time: " + tm + "ms");
	}
}
