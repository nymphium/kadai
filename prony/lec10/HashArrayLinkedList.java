import java.io.*;

class HashArrayLinkedList{

	static final int SzBkt = 5;

	static class ListString{
		String name;
		ListString next;

		ListString(String name, ListString tail){
			this.name= new String(name);
			this.next= tail;
		}

		static void Display(ListString list){
			while(list!=null){
				System.out.print(list.name+"-->");
				list=list.next;
			}
			System.out.println("null");
		}

		static int Length(ListString list){
			int countlength = 1;
			while(list != null){
				list = list.next;
				countlength++;
			}
			return countlength;
		}
	}


	static int HashFunction(String s){
		int result = 0;

		result = (int)s.charAt(0);

		return result % SzBkt;
	}

	static void Show(String str){
		System.out.print("[" + str + "] data ? ");
	}

	static void SearchMethod(ListString list, String str){
		while(list != null){
			if(list.name.equals(str)){
				System.out.println("Found :" + str);
				break;
			}
			list = list.next;
		}
	}

	public static void main(String[] args)throws IOException{
		String [] persons = {
			"abe",
			"amagasa",
			"sannomiya",
			"sakuma",
			"maeda",
			"sato",
			"tadano",
			"sano",
			"sugiki",
			"oka"
		};

		ListString[] HashTable = new ListString[SzBkt];

		for(int i = 0; i < SzBkt; i++){
			HashTable[i] = null;
		}

		for(int i = 0; i < persons.length; i++){
			int pos = HashFunction(persons[i]);

			HashTable[pos] = new ListString(persons[i], HashTable[pos]);
		}

		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

		String line;
		do{
			for(int i = 0; i < SzBkt; i++){
				ListString.Display(HashTable[i]);
			}

			System.out.print("> ");
			line = reader.readLine();

			// command search
			if(line.equals("search")){
				Show(line);
				String data = reader.readLine();
				if(data.length() < 1) continue;

				SearchMethod(HashTable[HashFunction(data)], data);
			}

			// command insert
			else if(line.equals("insert")){
				Show(line);
				String data = reader.readLine();
				if(data.length() < 1) continue;

				int insert_hash = HashFunction(data);
				HashTable[insert_hash] = new ListString(data, HashTable[insert_hash]);
			}

			// command delete
			else if(line.equals("delete")){
				Show(line);
				String data = reader.readLine();
				if(data.length() < 1) continue;

				int delete_hash = HashFunction(data);
				int count = 0;
				String[] storename = new String[ListString.Length(HashTable[delete_hash])];

				while(HashTable[delete_hash] != null){
					if(!data.equals(HashTable[delete_hash].name)){

						storename[count] = HashTable[delete_hash].name;
						count++;
					}
					HashTable[delete_hash] = HashTable[delete_hash].next;
				}

				for(int n = count - 1; n > -1; n--){
					HashTable[delete_hash] = new ListString(storename[n], HashTable[delete_hash]);
				}
			}
		}while(!line.equals("exit"));
	}
}
