import java.io.*;

class HashArrayOpenAddress{

	static final int SzBkt = 13;
	static final int Shift = 1;

	static int HashFunction(String s){
		int result = (int)s.charAt(0) - (int)'a';

		return result % SzBkt;
	}

	static void Show(String str){
		System.out.print("[\033[1m" + str + "\033[0m] data ? ");
	}

	// escape null string
	static boolean HaveString(String value){
		return (value == null || value.length() == 0) ? false : true;
	}

	public static void main (String[] args)throws IOException{
		String[] persons = {
			"abe",
			"amagasa",
			"sannomiya",
			"sakuma",
			"maeda",
			"sato",
			"tadano",
			"sano",
			"sugiki",
			"oka"};

		String[] HashTable = new String[SzBkt];

		for(int i = 0; i < persons.length; i++){
			int pos = HashFunction(persons[i]);

			while(HashTable[pos] != null){
				pos = (pos + Shift) % SzBkt;
			}

			HashTable[pos] = new String(persons[i]);
		}

		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

		String line;
		do{

			// display hashtable
			for (int i = 0; i < SzBkt; i++){
				System.out.println("Position " + i + "\t" + HashTable[i]);
			}

			System.out.print("[\033[31;1m" + System.getProperty("user.name") + "\033[0;39m ] > ");

			line = reader.readLine();

			// command search
			if(line.equals("search")){
				Show(line);
				String data = reader.readLine();
				if(data.length() < 1) continue;

				int search_hash = HashFunction(data);

				while(HaveString(HashTable[search_hash])){
					if(HashTable[search_hash].equals(data)){
						System.out.println("Found :" + data);
						break;
					}

					search_hash = (search_hash + Shift) % SzBkt;
				}
			}

			// command insert
			else if(line.equals("insert")){
				Show(line);
				String data = reader.readLine();
				if(data.length() < 1) continue;

				int insert_hash = HashFunction(data);
				int circle = insert_hash;

				do{
					if(!HaveString(HashTable[insert_hash])){
						HashTable[insert_hash] = data;
						break;
					}
				}while((insert_hash = (insert_hash + Shift) % SzBkt) != circle);

				if(insert_hash == circle) System.out.println("No space left");
			}

			// command delete
			else if(line.equals("delete")){
				Show(line);
				String data = reader.readLine();
				if(data.length() < 1) continue;

				int delete_hash = HashFunction(data);
				int circle = delete_hash;

				do{
					if(HaveString(HashTable[delete_hash]) && HashTable[delete_hash].equals(data)){
						HashTable[delete_hash] = null;
					}
				}while((delete_hash = (delete_hash + Shift) % SzBkt) != circle);

			}
		}while(!line.equals("exit"));
	}
}
