class ListString{
	String name;
	ListString next;

	ListString(String name, ListString tail){
		this.name = new String(name);
		this.next = tail;
	}

	// static ListString Insert(String s, ListString list){
		// return new ListString(s, list);
	// }

	static void Display(ListString list){
		while(list != null){
			System.out.print(list.name + "-->");
			list = list.next;
		}
		System.out.println("null");
	}

	static int length(ListString list){
		int countlength = 1;
		while(list != null){
			list = list.next;
			countlength++;
		}
		return countlength;
	}

	static void DisplayReverse(ListString list){
		int countlength_rev = 0;
		String[] cach = new String[ListString.length(list)];
		while(list != null){
			cach[countlength_rev] = list.name;
			list = list.next;
			countlength_rev++;
		}
		System.out.print("null");
		for(int m = countlength_rev - 1; m > -1; m--){
			System.out.print("<--" + cach[m]);
		}
		System.out.println();
	}

	static ListString Delete(String str, ListString list){
		int countlength_del = ListString.length(list);
		int register = 0;
		ListString[] listarray = new ListString[countlength_del];
		while(list.next != null){
			if(list.next.next == null){
				list.next = null;
				break;
			}
			listarray[register] = list;
			register++;
			list = list.next;
		}
		for(int m = 0; m < register; m++){
			list = listarray[register - 1 - m];
		}
		return list;
	}
}

class ListJava{
	public static void main(String[] args){
		ListString l1 = new ListString("Tokyo", null);
		ListString l2 = new ListString("Kyoto", l1);
		ListString l3 = new ListString("Tsukuba", l2);
		ListString l4 = new ListString("Nara", l3);
		ListString l5 = new ListString("Ueno", l4);
		ListString l6 = new ListString("Ehime", l5);

		ListString.Display(l6);
		System.out.println("Number of Elements:" + ListString.length(l5));
		ListString.DisplayReverse(l6);
		ListString l6_deleted = ListString.Delete("Tokyo", l6);
		ListString.Display(l6_deleted);
		ListString l7 = ListString.Delete("Ueno", l6_deleted);
		ListString.Display(l7);
		ListString l8 = ListString.Delete("Ehime", l7);
		ListString.Display(l8);
	}
}
