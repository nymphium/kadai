class HashArrayLinkedList{

	static class ListString{
		String name;
		ListString next;

		ListString(String name, ListString tail){
			this.name=new String(name);
			this.next=tail;
		}

		static ListString Insert(String s, ListString list){
			return new ListString(s, list);
		}

		static void Display(ListString list){
			while(list!=null){
				System.out.print(list.name+"-->");
				list=list.next;
			}
			System.out.println("null");
		}
	}


	static final int SzBkt = 5;

	static int String2Integer(String s){
		int result = 0;

		result = (int)s.charAt(0);

		return result;
	}

	static int HashFunction(int l){
		return l % SzBkt;
	}

	public static void main (String[] args){
		String [] persons = {
			"abe",        // 0
			"amagasa",    // 0
			"sannomiya",  // 3 
			"sakuma",     // 3
			"maeda",      // 2
			"sato",       // 3
			"tadano",     // 4
			"sano",       // 3
			"sugiki",     // 3
			"oka"};       // 4

		ListString[] HashTable = new ListString[SzBkt];

		for (int i = 0; i < SzBkt; i++){
			HashTable[i] = null;
		}

		// Constructing a hash structure
		for (int i = 0; i < persons.length; i++){
			int s2int = String2Integer(persons[i]);
			int pos = HashFunction(s2int);

			// Write your code here (data insertion)
			HashTable[pos] = new ListString(persons[i], HashTable[pos]);
		}

		for (int i = 0; i < SzBkt; i++){
			ListString.Display(HashTable[i]);
		}
	}
}
