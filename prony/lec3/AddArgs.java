public class AddArgs{
	public static void main(String[] args){
		int sum = 0;
		int hoge = 0;
		if(args.length == 0){
			System.out.println("Sum:0");
			System.exit(1);
		}
		for(int a = 0; a < args.length; a++){
			hoge = Integer.parseInt(args[a]);
			sum += hoge;
		}
		System.out.println("Sum:" + sum);
	}
}
