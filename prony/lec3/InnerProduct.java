public class InnerProduct{
	public static void main(String[] args){
		double d1[] = {1.0d,1.5d,2.0d,2.5d};
		double d2[] = {3.1d,2.1d,1.1d,0.1d};
		double sum = 0.0d;
		for(int a = 0; a < 4; a++){
			sum += (d1[a] * d2[a]); 
		}
		System.out.println("InnerProduct is \"" + sum + "\".");
	}
}
