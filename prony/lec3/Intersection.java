import java.io.*;

public class Intersection{
	public static void main(String[] args)throws IOException{
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Input ArrayA numbers");
		int arra[] = new int[10];
		int arrb[] = new int[10];
		for(int a = 0; a < 10; a++){
			String m = reader.readLine();
			int m2 = Integer.parseInt(m);
			arra[a] = m2;
		}
		System.out.println("Input ArrayB numbers");
		for(int b = 0; b < 10; b++){
			String n = reader.readLine();
			int n2 = Integer.parseInt(n);
			arrb[b] = n2;
		}
		System.out.println("Result:");
		for(int c = 0; c < 10; c++){
			for(int d = 0; d < 10; d++){
				if(arra[c] == arrb[d]){
					System.out.println(arra[c]);
				}
			}
		}
	}
}
