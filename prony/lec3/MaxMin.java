import java.io.*;

public class MaxMin{
	public static void main(String[] args)throws IOException{
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		String a0 = reader.readLine();
		int a01 = Integer.parseInt(a0);
		int max = a01;
		int min = a01;
		int root[] = new int[10];
		root[0] = a01;
		for(int b = 0; b < 9; b++){
			String a02 = reader.readLine();
			int a03 = Integer.parseInt(a02);
			root[b + 1] = a03;
			max = (max >= root[b + 1] ? max : root[b + 1]);
			min = (min <= root[b + 1] ? min : root[b + 1]);
		}
		System.out.println("Max:" + max);
		System.out.println("Min:" + min);
	}
}
