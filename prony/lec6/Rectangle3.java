public class Rectangle3{
	int width;
	int height;

	Rectangle3(){
		setSize(0, 0);
	}

	Rectangle3(int width, int height){
		setSize(width, height);
	}

	void setSize(int w, int h){
		this.width = w;
		this.height = h;
	}

	@Override
	public String toString(){
		return "[ width = " + width + " , height = " + height + " ]";
	}


	static class PlacedRectangle extends Rectangle3{
		int x;
		int y;

		PlacedRectangle(){
			setLocation(0, 0);
		}

		PlacedRectangle(int x, int y, int w, int h){
			setLocation(x, y);
			setSize(w, h);
		}

		void setLocation(int new_x, int new_y){
			this.x = new_x;
			this.y = new_y;
		}

		public String toString(){
			return "[(" + x + ", " + y + "),(" + width + ", " + height + ")]";
		}
	}
	public static void main(String[] args){
		PlacedRectangle p = new PlacedRectangle(1, 2, 3, 4);
		System.out.println(p);
	}
}
