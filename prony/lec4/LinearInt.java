import java.io.*;

public class LinearInt{
	public static void main(String[] args)throws IOException{
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Input 10 numbers data:");
		int a[] = new int[10];
		for(int n = 0; n < 10; n++){
			String s = reader.readLine();
			int m = Integer.parseInt(s);
			a[n] = m;
		}
		System.out.print("Input key: ");
		String l = reader.readLine();
		int l2 = Integer.parseInt(l);
		for(int o = 0; o < 10; o++){
			System.out.print(l2 == a[o] ? "Found[" + o + "]: " + l2 + "\n" : "");
		}
	}
}
