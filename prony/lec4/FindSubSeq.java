import java.io.*;

public class FindSubSeq{
	public static void main(String[] args)throws IOException{
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Input 10 data:");
		String a[] = new String[10];
		for(int m = 0; m < 10; m++){
			String l = reader.readLine();
			a[m] = l;
		}
		for(int n = 0; n < 8; n++){
			System.out.print(a[n].equals(args[0]) && a[n + 1].equals(args[1]) && a[n + 2].equals(args[2]) ? "Found at " + (n) + " Sequence: " + args[0] + " " + args[1] + " " + args[2] + "\n" : "");
		}
	}
}
