public class SubPalindrome{
	public static void main(String[] args){
		int ar1 = Integer.parseInt(args[0]);
		int L = args[1].length();
		for(int a = 0; a <= L - ar1; a++){
			for(int b = ar1; b <= L - a; b++){
				String str = args[1].substring(a,(a + b));
				StringBuffer rts = new StringBuffer(str).reverse();
				System.out.print(str.equals(rts.toString()) ? "[" + str + "]" + " at " + a + "\n" : "");
			}
		}
	}
}
