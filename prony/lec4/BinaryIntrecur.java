import java.io.*;

public class BinaryIntrecur{
	public static void main(String[] args)throws IOException{
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Input 10 numbers data:");
		int a[] = new int[10];
		int lng = a.length;
		for(int n = 0; n < lng; n++){
			String s = reader.readLine();
			int m = Integer.parseInt(s);
			a[n] = m;
		}

		System.out.print("Input key: ");
		String l = reader.readLine();
		int l2 = Integer.parseInt(l);

		if(a[lng / 2 - 1] < l2){
			for(int o = 0; o < lng / 2; o++){
				System.out.print(l2 == a[o + lng / 2] ? "Found[" + (o + lng / 2) + "]: " + l2 + "\n" : "");
			}
		}else{
			for(int p = 0; p < lng / 2; p++){
				System.out.print(l2 == a[p] ? "Found[" + p + "]: " + l2 + "\n" : "");
			}
		}
	}
}
