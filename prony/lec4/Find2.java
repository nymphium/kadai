import java.io.*;

public class Find2{
	public static void main(String[] args)throws IOException{
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		if(args.length != 1){
			System.out.println("使用法： java Find2 検索文字列 < 検索対象ファイル");
			System.out.println("例： java Find2 piyo < hoge.txt");
			System.exit(0);
		}

		String fs = args[0];
		System.out.println("検索文字列は「" + fs + "」です。");
		String l;
		int ln = 1;
		while((l = reader.readLine()) != null){
			for(int m = 0; m < l.length(); m++){
				System.out.print(l.startsWith(fs,m) ? (ln + ":" + l + "\n") : "");
			}
			ln++;
		}
	}
}
