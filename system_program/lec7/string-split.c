
/*
  string_split.c -- split a string with a delimiter character
  ~yas/syspro/string/string-split.c
  Created on: 2008/05/22 15:59:17
*/

#include <stdio.h>
#include <stdlib.h>	/* exit() */
#include <string.h>	/* strlen() */

extern void string_split_test( char *str, char c );
extern int  string_split( char *str, char del, int *countp, char ***vecp  );
extern void free_string_vector( int qc, char **vec );
extern int  countchr( char *s, char c );
    
void
usage( char *cmd )
{
	fprintf(stderr,"Usage: %% %s \"string\" \'c\'\n",cmd );
	exit( 1 );
}

int
main( int argc, char *argv[], char *envp[] )
{
	char *str;
	char c;

	if( argc != 3 )
		usage( argv[0] );
	if( strlen(argv[2])!=1 )
		usage( argv[0] );
	str = argv[1];
	c = argv[2][0];
	string_split_test( str, c );
}

void
string_split_test( char *str, char c )
{
	int count;
	char **vec;
	int i ;

	char s2[100];
	strcpy(s2, "GET /index.html HTTP/1.1");

	if( string_split(s2, c, &count, &vec ) < 0 )
	{
		perror("string_split-malloc");
		exit( 1 );
	}
	printf("splitting \"%s\" with '%c' returned %d strings.\n", s2, c, count );
	for( i=0 ; i< count; i++ )
		printf("%d: %s\n", i, vec[i] );
	free_string_vector( count, vec );
}

int
string_split( char *str, char del, int *countp, char ***vecp  )
{
	char **vec ;
	int  count_max, i, len ;
	char *s, *p ;

	if( str == 0 )
		return( -1 );
	count_max = countchr(str,del)+1 ;
	vec = malloc( sizeof(char *)*(count_max+1) );
	if( vec == 0 )
		return( -1 );

	for( i=0 ; i<count_max ; i++ )
	{
		while( *str == del )
			str ++ ;
		if( *str == 0 )
			break;
		for( p = str ; *p!=del && *p!=0 ; p++ )
			continue;
		/* *p == del || *p=='\0' */
		len =  p - str ;
		s = malloc( len+1 );
		if( s == 0 )
		{
			int j ;
			for( j=0 ; j<i; j++ )
			{
				free( vec[j] );
				vec[j] = 0 ;
			}
			return( -1 );
		}
		memcpy( s, str, len );
		s[len] = 0 ;
		vec[i] = s ;
		str = p ;
	}
	vec[i] = 0 ;
	*countp = i ;
	*vecp = vec ;
	return( i );
}

void
free_string_vector( int qc, char **vec )
{
	int i ;
    	for( i=0 ; i<qc ; i++ )
	{
		if( vec[i] == NULL )
			break;
		free( vec[i] );
	}
	free( vec );
}

int
countchr( char *s, char c )
{
	int count ;
	for( count=0 ; *s ; s++ )
		if( *s == c )
			count ++ ;
	return( count );
}
