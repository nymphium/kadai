#include <stdio.h>
#include <bsd/string.h>
#include <stdlib.h>

size_t my_strlcpy(char* s1, const char* s2, size_t size) {
	size_t len;

	int cnt = 0;

	for(len = 1; len < size; len++) {
		if((s1[cnt] = s2[cnt]) == '\0') break;

		cnt++;
	}

	if(size) s1[cnt] = '\0';

	while(s2[++cnt]) len++;

	return --len;
}

char* my_strdup(const char* str) {
	int len = strlen(str) + 1;
	char* p;
	
	if(! (p = malloc(len))) return NULL;

	memcpy(p, str, len);

	return p;
}

int test_strlcpy(char* str) {
	size_t len = strlen(str) + 1;
	char p1[len];
	char p2[len];

	size_t retcpy = strlcpy(p1, str, len);
	size_t retmycpy = my_strlcpy(p2, str, len);

	puts("cpy src>>>");
	puts(p1);
	puts("<<<\ndst>>>");
	puts(p2);
	puts("<<<");

	printf("%ld %ld\n", retcpy, retmycpy);

	return retmycpy == retcpy && strcmp(p1, p2) == 0;
}

int test_strdup(char* str) {
	char* p1 = my_strdup(str);
	char* p2 = strdup(str);
	int ret = strcmp(p1, p2) == 0;

	puts("dup src>>>");
	puts(p1);
	puts("<<<\ndst>>>");
	puts(p2);
	puts("<<<");

	free(p1);
	free(p2);

	return ret;
}

int tests(){
	 char str[10][265] = {
		"The quick, brown fox jumps over a lazy dog.",
		"DJs flock by when MTV ax quiz prog.",
		"Junk MTV quiz graced",
		"The European languages are members of the same family.",
		"Lorem ipsum dolor sit amet, consectetuer adipiscing elit.",
		"One morning, when Gregor Samsa woke from troubled dreams, he found himself transformed in his bed into a horrible vermin.",
		"He lay on his armour-like back, and if he lifted his head a little he could see his brown belly, slightly domed and divided by arches into stiff sections.",
		"The bedding was hardly able to cover it and seemed ready to slide off any moment.",
		"His many legs, pitifully thin compared with the size of the rest of him, waved about helplessly as he looked.",
		"\"What's happened to me? \" he thought. It wasn't a dream. His room, a proper human."
	 };

	int sum = 0;

	for(int i = 0; i < 10; i++) {
		int result_dup = test_strdup(str[i]);
		int result_cpy = test_strlcpy(str[i]);

		sum += result_dup + result_cpy;
		printf("strdup[%d]: %d\n", i, result_dup);
		printf("strlcpy[%d]: %d\n",i,  result_cpy);
	}

	return (sum / 10) - 2;
}

int main() {
	return tests();
}

