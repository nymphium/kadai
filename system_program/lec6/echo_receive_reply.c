int echo_receive_reply( FILE *in, char buf[], int size )
{
        char *res;
        res = fgets( buf, size, in ); /* receive a reply message */
        if( res )
                return( strlen(buf) );
        else
                return( -1 );
}
