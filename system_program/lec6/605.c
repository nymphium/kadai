#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>  /* socket() */
#include <netdb.h>      /* getaddrinfo() */
#include <unistd.h>     /* close() */

#define PORTNO_BUFSIZE 30

int tcp_connect(char *server, int portno);
int fdopen_sock(int sock, FILE **inp, FILE **outp);
int wcat(char* server, int portno, char* file);

int main(int argc, char** argv) {
	char *file ;

	if(argc != 4) {
		fprintf(stderr,"Usage: %s server port 'file'\n",argv[0]);
		exit(-1);
	}

	char* server  = argv[1] ;
	int portno  = strtol(argv[2],0,10);
	file = argv[3];
	return wcat(server, portno, file);
}

int wcat(char* server, int portno, char* file){
	int sock = tcp_connect(server, portno);

	if(sock < 0) {
		return -1;
	}

	FILE *in, *out;

	if(fdopen_sock(sock, &in, &out) < 0) {
		fprintf(stderr, "fdopen()\n");
		close(sock);
		return -1;
	}

	int res = fprintf(out, "GET %s HTTP/1.0\nHost: %s\n\r\n", file, server);

	do{
		putchar(fgetc(in));
	}while(!feof(in));

	puts("");

	fclose(in);
	fclose(out);

	return res;
}

int tcp_connect( char *server, int portno ) {
	struct addrinfo hints, *ai;
	char portno_str[PORTNO_BUFSIZE];
	int s, err;
	snprintf( portno_str,sizeof(portno_str),"%d",portno );
	memset( &hints, 0, sizeof(hints) );
	hints.ai_socktype = SOCK_STREAM;
	if( (err = getaddrinfo( server, portno_str, &hints, &ai )) )
	{
		fprintf(stderr,"unknown server %s (%s)\n",server,
				gai_strerror(err) );
		goto error0;
	}
	if( (s = socket(ai->ai_family, ai->ai_socktype, ai->ai_protocol)) < 0 )
	{
		perror("socket");
		goto error1;
	}
	if( connect(s, ai->ai_addr, ai->ai_addrlen) < 0 )
	{
		perror( server );
		goto error2;
	}
	freeaddrinfo( ai );
	return( s );
error2:
	close( s );
error1:
	freeaddrinfo( ai );
error0:
	return( -1 );
}

int fdopen_sock( int sock, FILE **inp, FILE **outp ) {
	int sock2 ;
	if( (sock2=dup(sock)) < 0 )
	{
		return( -1 );
	}
	if( (*inp = fdopen( sock2, "r" )) == NULL )
	{
		close( sock2 );
		return( -1 );
	}
	if( (*outp = fdopen( sock, "w" )) == NULL )
	{
		fclose( *inp );
		*inp = 0 ;
		return( -1 );
	}
	setvbuf(*outp, (char *)NULL, _IONBF, 0);
	return( 0 );
}
