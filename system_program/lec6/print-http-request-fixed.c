#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void send_http_request_fixec(FILE *out, char* file) {
	char str[1024];
	strcpy(str, "GET ");
	strcat(str, file);
	strcat(str, "HTTP/1.0\nHost: www.coins.tsukuba.ac.jp\n\n");
	fprintf(out, str);

	/* fprintf(out, "GET /~syspro/index.html HTTP/1.0\nHost: www.coins.tsukuba.ac.jp\n"); */
}

int main(int argc, char **argv) {
	send_http_request_fixec(stdout, argv[1]);
}

