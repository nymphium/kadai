#define BUFFERSIZE      1024

int echo_client_one( char *server, int portno, char *message ) {
        int sock ;
        FILE *in, *out ;
        char rbuf[BUFFERSIZE];
        int res;

        sock = tcp_connect( server, portno );
        if( sock<0 )
                return( 1 );
        if( fdopen_sock(sock,&in,&out) < 0 )
        {
                fprintf(stderr,"fdooen()\n");
                close( sock );
                return( 1 );
        }
        res = echo_send_request( out, message );
        if( res < 0 )
        {
                fprintf(stderr,"fprintf()\n");
                fclose( in );
                fclose( out );
                return( 1 );
        }
        printf("sent: %d bytes [%s\n]\n",res,message );
        res = echo_receive_reply( in, rbuf, BUFFERSIZE );
        if( res < 0 )
        {
                fprintf(stderr,"fprintf()\n");
                fclose( in );
                fclose( out );
                return( 1 );
        }
        printf("received: %d bytes [%s]\n", res, rbuf );
        fclose( in );
        fclose( out );
        return( 0 );
}
