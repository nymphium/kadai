#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[])
{
	int     c;
	FILE    *src, *dst;

	if (argc != 3) {
		printf("Usage: %s from_file to_file\n", argv[0]);
		exit(1);
	}

	src = fopen(argv[1], "r");
	if (src == NULL) {
		perror(argv[1]);
		exit(1);
	}

	dst = fopen(argv[2], "w");
	if (dst == NULL) {
		perror(argv[2]);
		fclose(src);
		exit(1);
	}

	while ((c = fgetc(src)) != EOF)
		fputc(c, dst);

	fclose(src);
	fclose(dst);
}
