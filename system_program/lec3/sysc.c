#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>

int main(int argc, char *argv[]) {
	int     src, dst;
	int     count;

	if (argc != 3) {
		printf("Usage: %s from_file to_file\n", argv[0]);
		exit(1);
	}


	src = open(argv[1], O_RDONLY);
	if (src < 0) {
		perror(argv[1]);
		exit(1);
	}

	dst = open(argv[2], O_WRONLY | O_CREAT | O_TRUNC, 0666);
	if (dst < 0) {
		perror(argv[2]);
		close(src);
		exit(1);
	}

	char c[BUFSIZ];
	/* char c; */

	while ((count = read(src, &c, BUFSIZ)) > 0) {
		if (write(dst, &c, count) < 0) {
			perror("write");
			exit(1);
		}
	}

	if (count < 0) {
		perror("read");
		exit(1);
	}

	close(src);
	close(dst);
}
