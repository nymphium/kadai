#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[])
{
	FILE    *src, *dst;

	if (argc != 3) {
		printf("Usage: %s from_file to_file\n", argv[0]);
		exit(1);
	}

	src = fopen(argv[1], "r");
	if (src == NULL) {
		perror(argv[1]);
		exit(1);
	}

	dst = fopen(argv[2], "w");
	if (dst == NULL) {
		perror(argv[2]);
		fclose(src);
		exit(1);
	}

	char buf[BUFSIZ + 1];

	while ((fgets(buf, sizeof buf, src)) != NULL) fputs(buf, dst);

	fclose(src);
	fclose(dst);
}
