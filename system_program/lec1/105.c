#include <stdio.h>
#include <string.h>

#ifdef VERBOSE
#define PRINT_DATA(a, len) for (int i = 0; i < len; i++) printf("%2d ", a[i]); puts("");
#define PRINT_COMP(i, j) printf("compare[%d] %d and %d\n", compare_count, i, j);
#define PRINT_SWAP(a, i, j) printf("[%d]=%d > [%d]=%d\n", i, a[i], j, a[j]);
#else
#define PRINT_DATA(_, __);
#define PRINT_COMP(_, __);
#define PRINT_SWAP(_, __, ___);
#endif

#define SAMPLE_COUNT 6
int sample[] = {8, 12, 3, 15, 7, 4};
int swp_count = 0;
int compare_count = 0;

void swap_array(int a[], int i, int j, int len) {
	int tmp = a[i];
	a[i] = a[j];
	a[j] = tmp;

	PRINT_SWAP(a, i, j);
	PRINT_DATA(a, len);
}

int compare(int i, int j) {
	compare_count++;
	PRINT_COMP(i, j);

	// i <  j --> -1
	// i == j --> 0
	// i >  j --> 1
	return i < j ? -1 : i == j ? 0 : 1;
}

// quick sort
void quick_sort(int array[], int left, int right, int len) {
	int l = left;
	int r = right;
	int p = array[right];

	while(1) {
		while(compare(array[l], p) == -1) l++;

		while(compare(array[r], p) == 1) r--;

		if(l >= r) break;

		swap_array(array, l++, r--, len);
	}

	if(left < l - 1) quick_sort(array, left, l - 1, len);

	if(r + 1 < right) quick_sort(array, r + 1, right, len);
}


void sort(int data[], int len) {
	quick_sort(data, 0, len - 1, len);
}

void result(int array[], int len) {
	sort(array, len);

	PRINT_DATA(array, len);

	printf("compare: %d\n", compare_count);
}

int main(int argc, char* argv[]) {
	if(argc > 1) {
		int len = 10000;
		int array[len];
		int x;
		int i = 0;
		FILE *f = fopen("rands", "r");

		while(fscanf(f, "%d", &x) != EOF) {
			array[i++] = x;
		}

		fclose(f);

		PRINT_DATA(array, len);

		result(array, len);

		return 0;
	}

	result(sample, SAMPLE_COUNT);
}

