#!/usr/bin/env ruby
require 'webrick'
require 'webrick/httpservlet'
require 'erb'
require 'cgi'

module WEBrick::HTTPServlet
	FileHandler.add_handler('rb', CGIHandler)
	FileHandler.add_handler('html', ERBHandler)
	FileHandler.add_handler('rhtml', ERBHandler)
	FileHandler.add_handler('erb', ERBHandler)
end

s = WEBrick::HTTPServer.new(
	:DocumentRoot => "./",
	:Port => 8000,
	:CGIInterpreter => "/usr/bin/ruby",
	:ERBInterpreter => "/usr/bin/erb",
	:MimeTypes => WEBrick::HTTPUtils::DefaultMimeTypes.merge({"rhtml"=>"text/html"}),
)

s.config[:MimeTypes]["rhtml"] = "text/html"
# s.mount("/usr/bin/erb", WEBrick::HTTPServlet::FileHandler, "./")

trap(:INT){s.shutdown}

s.start
