#encoding: utf-8
require'cgi'

cgi = CGI.new
data = nil
accfile = "acc.txt"

begin
	data = open(accfile, "r:UTF-8")
rescue
	data = open(accfile, "a+:UTF-8")
end

curr = data.gets

unless curr
	curr = 0
else
	curr = curr.to_i
end

data.close

print <<EOF
#{cgi.header("text/html; charset=utf-8")}
<html>
	<body>
EOF

begin
	begin
		data = open(accfile, "w:UTF-8")
	rescue
		data = open(accfile, "a+:UTF-8")
	end

	num = if n = cgi['num'] then
			if n.match(/\D/)
				puts "<p style='color:red;'>invalid parameter: #{CGI.escapeHTML(n)}</p>"
				data.to_i
			else
				n.to_i
			end
		else curr end

	print <<-EOF
		<p style="font-size: #{num < 13 ? 13 : num};">#{num}</p>
		<a href="acc.rb?num=#{num+curr}">update</a>
	EOF
	data.write(num + curr)
	data.close
rescue
	print <<-EOF
		<p>internal server error</p>
	EOF
end

print <<EOF
	</body>
</html>
EOF

data.close

