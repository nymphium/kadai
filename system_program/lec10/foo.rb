require'cgi'

cgi = CGI.new

x = if x = cgi['x'] then x.length > 0 ? x.to_i : 0 else 0 end

print <<EOF
#{cgi.header("text/html; charset=utf-8")}
<html><body>
<form method="post" action="foo.rb?x=#{x+1}">
<input value="hoge" type="submit"/>
</form>
</body></html>
EOF
