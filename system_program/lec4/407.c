#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <fcntl.h>

#define READ 0
#define WRITE 1
static int pn = 0;

void wrapwrite(int p, char* str, int len) {
	if(write(p, str, len) < 0) {
		perror("write");
		exit(1);
	}
}

void wrapread(int p, char* str, int len) {
	if(read(p, str, len) < 0) {
		perror("read");
		exit(1);
	}
}

int childproc(int p_ptoc[2], int p_ctop[2], int num, int cnt, int dst) {
	char buffer[10];

	wrapread(p_ptoc[READ], buffer, 10);
	if((cnt = atoi(buffer)) >= num) return 1;

	if(cnt % 2 < 1) {
		char c = 48 + pn++ % 10;
		wrapwrite(dst, &c, 1);
		sprintf(buffer, "%d", cnt + 1);
		wrapwrite(p_ctop[WRITE], buffer, strlen(buffer) + 1);
	}

	return 0;
}

int parentproc(int p_ptoc[2], int p_ctop[2], int num, int cnt, int dst) {
	char buffer[10];
	wrapread(p_ctop[READ], buffer, 10);
	if((cnt = atoi(buffer)) >= num) return 1;

	if(cnt % 2 > 0) {
		char c = 65 + pn++ % 26;
		wrapwrite(dst, &c, 1);
		sprintf(buffer, "%d", cnt + 1);
		wrapwrite(p_ptoc[WRITE], buffer, strlen(buffer) + 1);
	}

	return 0;
}



int main(int _, char* argv[]) {
	int pipe_ctop[2], pipe_ptoc[2];

	pipe(pipe_ctop);
	pipe(pipe_ptoc);

	pid_t pid ;

	if((pid = fork()) == 0) {
		close(pipe_ctop[READ]);
		close(pipe_ptoc[WRITE]);
	}else {
		close(pipe_ctop[WRITE]);
		close(pipe_ptoc[READ]);
		wrapwrite(pipe_ptoc[WRITE], "0", 1);
	}

	int dst;

	if((dst= open("/dev/stdout", O_WRONLY | O_CREAT | O_TRUNC, 0666)) < 0) {
		perror("open");
		exit(1);
	}

	int num = atoi(argv[1]);
	int cnt = 0;

	while(num > cnt) {
		if(pid == 0) {
			if(childproc(pipe_ptoc, pipe_ctop, num, cnt, dst)) break;
		}else {
			if(parentproc(pipe_ptoc, pipe_ctop, num, cnt, dst)) break;
		}
	}

	close(pipe_ctop[WRITE]);
	close(pipe_ptoc[READ]);
	close(dst);
}

