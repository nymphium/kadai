#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define READ 1
#define WRITE 0

int main(int _, char* argv[]) {
	pid_t pid ;
	int pp[2], qq[2];
	char tmp[10];

	int num = atoi(argv[1]);

	pipe(pp); // 親へデータを送るためのパイプ
	pipe(qq); // 計算結果を子に送るためのパイプ

	pid = fork();  // 親子のプロセスの生成

	if(pid == 0) {
		close(pp[READ]);
		close(qq[WRITE]);
		/* char* tmp; */
		read(qq[READ], tmp, 10);
		puts("piyo");
		puts(tmp);
	}else {
		close(pp[WRITE]);
		close(qq[READ]);
		write(qq[WRITE], argv[1], strlen(argv[1]) + 1);
		puts("hoge");
	}

/* PUTS: */
	/* if(num > 0) { */
		/* if ( pid == 0 ) { */
			/* char* tmp; */
			/* int num_; */

			/* read(qq[READ], tmp, sizeof(char*)); */
			/* num_ = atoi(tmp); */

			/* static int n = 0; */
			/* putchar(48 + n++ % 10); */

			/* sprintf(tmp, "%d", --num_); */
			/* write(pp[WRITE], tmp, sizeof(char*)); */
		/* } else { */
			/* char* tmp; */
			/* int num_; */

			/* read(pp[READ], tmp, sizeof(char*)); */
			/* num_ = atoi(tmp); */

			/* static int n = 0; */
			/* putchar(65 + n++ % 26); */

			/* sprintf(tmp, "%d", --num_); */
			/* write(qq[WRITE], tmp, sizeof(char*)); */
		/* } */

		/* goto PUTS; */
	/* } */

	return 0;
}
