#include <stdio.h>
#include <stdlib.h>

#ifndef close
#include <unistd.h>
#endif

#ifndef wait
#include <sys/wait.h>
#endif

int pipe_fd[2];

	void
do_child()
{
	char    *p = "Hello, dad!!\n";

	printf("this is child\n");

	close(pipe_fd[0]);

	close(1);
	if (dup2(pipe_fd[1], 1) < 0) {
		perror("dup2 (child)");
		exit(1);
	}
	close(pipe_fd[1]);

	while (*p) {
		putchar(*p++);
		putchar(50);
	}
}

	void
do_parent()
{
	char    c;
	int status;

	printf("this is parent\n");

	close(pipe_fd[1]);

	close(0);
	if (dup2(pipe_fd[0], 0) < 0) {
		perror("dup2 (parent)");
		exit(1);
	}
	close(pipe_fd[0]);

	while ((c = getchar()) != EOF) {
		putchar(c);
		putchar(55);
	}

	if (wait(&status) < 0) {
		perror("wait");
		exit(1);
	}
}

int main()
{
	int child;

	if (pipe(pipe_fd) < 0) {
		perror("pipe");
		exit(1);
	}

	if ((child = fork()) < 0) {
		perror("fork");
		exit(1);
	}

	if (child)
		do_parent();
	else
		do_child();
}
