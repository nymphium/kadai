#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <errno.h>
#include <string.h>
#include <sys/time.h>
#include <unistd.h>
#include <time.h>

void printmem(unsigned int sn, siginfo_t si) {
	printf("%d\n", sn);
}

int main() {
	struct sigaction sa_alarm;

	memset(&sa_alarm, 0, sizeof(sa_alarm));

	sa_alarm.sa_sigaction = (void *)printmem;
	sa_alarm.sa_flags = SA_SIGINFO;

	if (sigaction(SIGSEGV, &sa_alarm, NULL) < 0) {
		perror("sigaction");
		exit(1);
	}

	int inv[1];

	inv[2] = 0;

	/* printf("%s\n"); */
}
