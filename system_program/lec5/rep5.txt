----------------------------------------------------------------------
学籍番号: 201311350
名前: 河原 悟
課題番号：5
練習問題番号：505
題名：シグナル

＜内容＞
----------------------------------------------------------------------

```505.c
 1	#include <stdio.h>
 2	#include <stdlib.h>
 3	#include <signal.h>
 4	#include <string.h>
 5	#include <sys/time.h>
 6	#include <unistd.h>
 7	#include <time.h>
 8	
 9	volatile sig_atomic_t ut = 5;
10	
11	void printimer(){
12		time_t t;
13		time(&t);
14		printf("%s", ctime(&t));
15	}
16	
17	void tzero() {
18		ut = 0;
19	}
20	
21	int mygetchar(int t) {
22		struct sigaction sa_distrb, sa_oa;
23		struct itimerval itimer, ite;
24	
25		memset(&sa_distrb, 0, sizeof(sa_distrb));
26		sa_distrb.sa_handler = tzero;
27	
28		if (sigaction(SIGALRM, &sa_distrb, &sa_oa) < 0) {
29			perror("sigaction");
30			printimer();
31			return -3;
32		}
33	
34		itimer.it_value.tv_sec  = itimer.it_interval.tv_sec  = t;
35		itimer.it_value.tv_usec = itimer.it_interval.tv_usec = 0;
36	
37		if (setitimer(ITIMER_REAL, &itimer, &ite) < 0) {
38			perror("setitimer");
39			printimer();
40			return -3;
41		}
42	
43		char c = getchar();
44	
45		itimer.it_value.tv_sec  = itimer.it_interval.tv_sec  = 0;
46		itimer.it_value.tv_usec = itimer.it_interval.tv_usec = 0;
47	
48		if (sigaction(SIGALRM, &sa_oa, NULL) < 0) {
49			perror("sigaction");
50			printimer();
51			return -3;
52		}
53	
54		if (setitimer(ITIMER_REAL, &ite, &itimer) < 0) {
55			perror("setitimer");
56			printimer();
57			return -3;
58		}
59	
60		if(ut > 0){
61			if(feof(stdin)) {
62				printimer();
63				return -1;
64			}else if(c < 0) {
65				printimer();
66				return -3;
67			}else {
68				printimer();
69				return c;
70			}
71		}
72	
73		printimer();
74		return -2;
75	}
76	
77	int main() {
78		printimer();
79	
80		printf("waiting time: %d\n", ut);
81	
82		int c = mygetchar(ut);
83	
84		switch(c) {
85			case -3: puts("error"); break;
86			case -2: puts("timeout"); break;
87			case -1: puts("EOF"); break;
88			default: printf("this is %c\n", c);
89		}
90	}
91	
```

以下の実行結果が得られる｡

```
$ gcc 505.c`
$ ./a.out
Tue May 24 23:48:40 2016
waiting time: 5
a
Tue May 24 23:48:42 2016
this is a
$ ./a.out
Tue May 24 23:48:58 2016
waiting time: 5
Tue May 24 23:49:03 2016
timeout
$ ./a.out
Tue May 24 23:49:23 2016
waiting time: 5
Tue May 24 23:49:24 2016
EOF
```

