#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <string.h>
#include <sys/time.h>
#include <unistd.h>
#include <time.h>

volatile sig_atomic_t ut = 5;

void printimer(){
	time_t t;
	time(&t);
	printf("%s", ctime(&t));
}

void tzero() {
	ut = 0;
}

int mygetchar(int t) {
	struct sigaction sa_distrb, sa_oa;
	struct itimerval itimer, ite;

	memset(&sa_distrb, 0, sizeof(sa_distrb));
	sa_distrb.sa_handler = tzero;

	if (sigaction(SIGALRM, &sa_distrb, &sa_oa) < 0) {
		perror("sigaction");
		printimer();
		return -3;
	}

	itimer.it_value.tv_sec  = itimer.it_interval.tv_sec  = t;
	itimer.it_value.tv_usec = itimer.it_interval.tv_usec = 0;

	if (setitimer(ITIMER_REAL, &itimer, &ite) < 0) {
		perror("setitimer");
		printimer();
		return -3;
	}

	char c = getchar();

	itimer.it_value.tv_sec  = itimer.it_interval.tv_sec  = 0;
	itimer.it_value.tv_usec = itimer.it_interval.tv_usec = 0;

	if (sigaction(SIGALRM, &sa_oa, NULL) < 0) {
		perror("sigaction");
		printimer();
		return -3;
	}

	if (setitimer(ITIMER_REAL, &ite, &itimer) < 0) {
		perror("setitimer");
		printimer();
		return -3;
	}

	if(ut > 0){
		if(feof(stdin)) {
			printimer();
			return -1;
		}else if(c < 0) {
			printimer();
			return -3;
		}else {
			printimer();
			return c;
		}
	}

	printimer();
	return -2;
}

int main() {
	printimer();

	printf("waiting time: %d\n", ut);

	int c = mygetchar(ut);

	switch(c) {
		case -3: puts("error"); break;
		case -2: puts("timeout"); break;
		case -1: puts("EOF"); break;
		default: printf("this is %c\n", c);
	}
}

